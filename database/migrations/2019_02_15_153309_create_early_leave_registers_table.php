<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEarlyLeaveRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('early_leave_registers')) {
            Schema::create('early_leave_registers', function (Blueprint $table) {
                $table->increments('early_leave_register_id');

                $table->integer('admin_id')->unsigned()->nullable();
                $table->foreign('admin_id')->references('admin_id')->on('admins');

                $table->date('leave_date')->nullable();
                $table->dateTime('start_time')->nullable();
                $table->dateTime('end_time')->nullable();
                $table->tinyInteger('leave_status')->default('2')->comment('Approved-1, Pending-2 or Cancel-3');
                $table->tinyInteger('status')->default('1');
                $table->text('leave_reason')->nullable();
                $table->text('leave_comment')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('early_leave_registers');
    }
}
