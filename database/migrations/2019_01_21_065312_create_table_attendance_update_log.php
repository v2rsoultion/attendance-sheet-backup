<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAttendanceUpdateLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendancedata_logs', function (Blueprint $table) {
            $table->increments('attendancedata_log_id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');                
            $table->integer('employee_id')->unsigned()->nullable();
            $table->foreign('employee_id')->references('admin_id')->on('admins')->onDelete('cascade');                
            $table->integer('attendancedata_id')->unsigned()->nullable();
            $table->foreign('attendancedata_id')->references('attendancedata_id')->on('attendancedatas')->onDelete('cascade');                
            $table->string('month_year')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendancedata_logs');
    }
}
