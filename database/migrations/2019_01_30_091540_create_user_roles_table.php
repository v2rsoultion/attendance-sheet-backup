<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_roles')) {
            Schema::create('user_roles', function (Blueprint $table) {
                $table->increments('user_role_id');
                $table->string('user_role_name')->nullable();
                $table->enum('user_role_status', ['0', '1'])->default('1');
                $table->tinyInteger('isWorkTimeRequired')->default('0');
                $table->enum('isfixed', ['0', '1'])->default('0');
                $table->timestamps();
            });

            $user_role_list  =  \Config::get('custom.user_role_list');
            // Insert some stuff
            foreach ($user_role_list as $key => $value) {
                DB::table('user_roles')->insert(
                    array(
                        'user_role_name'         =>  $value,
                        'user_role_status'       =>  '1',
                        'isfixed'                =>  '1',
                        'created_at'             =>  date('Y-m-d H:i:s'),
                        'updated_at'             =>  date('Y-m-d H:i:s'),
                    )
                );
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_roles');
    }
}
