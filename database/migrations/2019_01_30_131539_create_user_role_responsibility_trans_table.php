<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRoleResponsibilityTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_role_responsibility_transs'))
        {
            Schema::create('user_role_responsibility_transs', function (Blueprint $table) {
                $table->increments('user_Role_responsibility_trans_id');
                $table->integer('user_role_id')->unsigned();
                $table->foreign('user_role_id')->references('user_role_id')->on('user_roles');
                $table->integer('work_type_id')->unsigned();
                $table->foreign('work_type_id')->references('work_type_id')->on('work_types');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_role_responsibility_transs');
    }
}
