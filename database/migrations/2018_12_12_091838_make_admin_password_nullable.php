<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeAdminPasswordNullable extends Migration
{
    // Adding Constructer for modify table column
    public function __construct()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (Schema::hasColumn('admins', 'password')) {
            Schema::table('admins', function (Blueprint $table) {
                $table->text('password')->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        if (Schema::hasColumn('admins', 'password')) {
            Schema::table('admins', function (Blueprint $table) {
                $table->string('password')->change();
            });
        }
    }
}
