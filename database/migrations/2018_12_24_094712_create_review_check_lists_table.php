<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewCheckListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('review_check_lists')) {
            Schema::create('review_check_lists', function (Blueprint $table) {
                $table->increments('review_check_id');
                $table->string('review_check');
                $table->integer('weekly_time')->default(0);
                $table->integer('fortnightly_time')->default(0);
                $table->integer('monthly_time')->default(0);
                $table->tinyInteger('status')->default(1)->comment('1 - Active, 0 - Inactive');
                $table->timestamps();
            });
            $code_review_checks  =  \Config::get('custom.code_review_checks');
            // Insert some stuff
            foreach ($code_review_checks as $key => $value) {
                DB::table('review_check_lists')->insert(
                    array(
                        'review_check' =>  $value,
                        'status'       =>  '1',
                        'created_at'   =>  date('Y-m-d H:i:s'),
                        'updated_at'   =>  date('Y-m-d H:i:s'),
                    )
                );
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_check_lists');
    }
}
