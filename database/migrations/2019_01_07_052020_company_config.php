<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompanyConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('company_configs'))
        {
            Schema::create('company_configs', function (Blueprint $table)
            {
                $table->increments('company_config_id');
                $table->text('office_hours')->nullable();
                $table->text('lunch_hours')->nullable();
                $table->text('direct_half_time')->nullable();
                $table->string('late_half_days')->nullable();
                $table->timestamps();
            });
        }
        DB::table('company_configs')->insert(
            array(
                'company_config_id' => 1,
                'office_hours'      => '9',
                'lunch_hours'       => '0.5',
                'direct_half_time'  => '12:00 PM',
                'late_half_days'    => '3',
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_configs');
    }
}
