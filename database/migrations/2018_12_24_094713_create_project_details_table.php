<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('project_details'))
        {
            Schema::create('project_details', function (Blueprint $table) {
                $table->increments('project_detail_id');
                $table->integer('project_id')->unsigned()->nullable();
                $table->foreign('project_id')->references('project_id')->on('projects')->onDelete('cascade');                
                $table->integer('platform_id')->unsigned()->nullable();
                $table->foreign('platform_id')->references('platform_id')->on('platforms')->onDelete('restrict');                
                $table->integer('coder_id')->unsigned()->nullable();
                $table->foreign('coder_id')->references('admin_id')->on('admins')->onDelete('restrict');                
                $table->integer('code_reviewer_id')->unsigned()->nullable();
                $table->foreign('code_reviewer_id')->references('admin_id')->on('admins')->onDelete('restrict');                
                $table->string('frequently_code_review')->nullable();
                $table->string('code_review_day')->nullable(); 
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_details');
    }
}
