<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendancedatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendancedatas', function (Blueprint $table) {
            $table->increments('attendancedata_id');
            $table->integer('attendance_id')->unsigned()->nullable();
            $table->foreign('attendance_id')->references('attendance_id')->on('attendances')->onDelete('cascade');                
            $table->integer('admin_id')->unsigned()->nullable();
            $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');                
            $table->string('month_year')->nullable();
            $table->text('json_data')->nullable();
            $table->text('wfh_json_data')->nullable();
            $table->double('full_leaves', 8, 2)->nullable();
            $table->double('half_leaves', 8, 2)->nullable();
            $table->double('dhalf_leaves',8,2)->nullable();
            $table->double('extra_working_hrs', 8, 2)->nullable();
            $table->double('late_days', 8, 2)->nullable();
            $table->double('leaves_after_less', 8, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendancedatas');
    }
}
