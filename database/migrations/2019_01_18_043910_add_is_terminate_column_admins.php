<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsTerminateColumnAdmins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admins', function (Blueprint $table) {
            $table->enum('is_terminate', ['0', '1'])->default('0')->after('status');
            $table->enum('is_resign', ['0', '1'])->default('0')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('admins', ['is_terminate','is_resign'])){
            Schema::table('admins', function (Blueprint $table) {
                $table->dropColumn('is_terminate');
                $table->dropColumn('is_resign');
            });
        }
    }
}
