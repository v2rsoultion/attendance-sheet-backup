<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('work_types'))
        {
            Schema::create('work_types', function (Blueprint $table) {
                $table->increments('work_type_id');
                $table->string('work_type_name')->nullable();
                $table->enum('work_type_status',['0','1'])->default('1');
                $table->text('work_type_description')->nullable();
                $table->timestamps();
            });

            $work_type_list  =  \Config::get('custom.work_type_list');
            // Insert some stuff
            foreach ($work_type_list as $key => $value) {
                DB::table('work_types')->insert(
                    array(
                        'work_type_name'         =>  $value,
                        'work_type_description'  =>  $value,
                        'work_type_status'       => '1',
                        'created_at'             => date('Y-m-d H:i:s'),
                        'updated_at'             => date('Y-m-d H:i:s'),
                    )
                );
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_types');
    }
}
