<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReleasePlanColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('release_plans', function (Blueprint $table) {
            $table->decimal('total', 8,2)->nullable()->change();
            $table->decimal('jan', 8,2)->nullable()->change();
            $table->decimal('feb',8,2)->nullable()->change();
            $table->decimal('mar',8,2)->nullable()->change();
            $table->decimal('apr',8,2)->nullable()->change();
            $table->decimal('may',8,2)->nullable()->change();
            $table->decimal('jun',8,2)->nullable()->change();
            $table->decimal('july',8,2)->nullable()->change();
            $table->decimal('aug',8,2)->nullable()->change();
            $table->decimal('sep',8,2)->nullable()->change();
            $table->decimal('oct',8,2)->nullable()->change();
            $table->decimal('nov',8,2)->nullable()->change();
            $table->decimal('dec',8,2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('release_plans', function (Blueprint $table) {
            $table->text('total')->nullable()->change();
            $table->integer('jan')->nullable()->change();
            $table->integer('feb')->nullable()->change();
            $table->integer('mar')->nullable()->change();
            $table->integer('apr')->nullable()->change();
            $table->integer('may')->nullable()->change();
            $table->integer('jun')->nullable()->change();
            $table->integer('july')->nullable()->change();
            $table->integer('aug')->nullable()->change();
            $table->integer('sep')->nullable()->change();
            $table->integer('oct')->nullable()->change();
            $table->integer('nov')->nullable()->change();
            $table->integer('dec')->nullable()->change();
        });
    }
}
