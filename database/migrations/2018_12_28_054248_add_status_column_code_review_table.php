<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusColumnCodeReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('code_reviews', function (Blueprint $table) {
            $table->enum('status',['0','1'])->default('0'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('code_reviews', 'status')){
            Schema::table('code_reviews', function (Blueprint $table) {
                $table->dropColumn('status');
            });
        }
    }
}
