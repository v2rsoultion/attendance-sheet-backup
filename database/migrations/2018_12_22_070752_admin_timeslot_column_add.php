<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdminTimeslotColumnAdd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admins', function (Blueprint $table) {
            $table->integer('time_slot_id')->unsigned()->nullable();
        });
        Schema::table('admins', function(Blueprint $table){
            $table->foreign('time_slot_id')->references('time_slot_id')->on('time_slot')->onDelete('RESTRICT');                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('admins', 'time_slot_id')){
            Schema::table('admins', function (Blueprint $table) {
                $table->dropColumn('time_slot_id');
            });
        }
    }
}
