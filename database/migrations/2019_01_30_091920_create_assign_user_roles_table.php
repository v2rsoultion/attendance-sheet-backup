<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignUserRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('assign_user_roles'))
        {
            Schema::create('assign_user_roles', function (Blueprint $table) {
                $table->increments('assign_user_role_id');
                $table->integer('admin_id')->unsigned();
                $table->foreign('admin_id')->references('admin_id')->on('admins');
                $table->integer('user_role_id')->unsigned();
                $table->foreign('user_role_id')->references('user_role_id')->on('user_roles');
                $table->enum('assign_user_role_status', ['0', '1'])->default('1');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_user_roles');
    }
}
