<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodeReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('code_reviews')) {
            Schema::create('code_reviews', function (Blueprint $table) {
                $table->increments('code_review_id');

                $table->integer('project_id')->unsigned()->nullable();
                $table->foreign('project_id')->references('project_id')->on('projects')->onDelete('cascade');

                $table->integer('coder_id')->unsigned()->nullable();
                $table->foreign('coder_id')->references('admin_id')->on('admins')->onDelete('restrict');

                $table->integer('code_reviewer_id')->unsigned()->nullable();
                $table->foreign('code_reviewer_id')->references('admin_id')->on('admins')->onDelete('restrict');

                $table->string('frequently_code_review')->nullable();
                $table->string('review_start_date');
                $table->string('comments_for_reviewer')->nullable();
                $table->text('check_points')->comment('Ids of check points');
                $table->tinyInteger('status')->default(1)->comment('Active - 1, Inactive - 0');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('code_reviews');
    }
}
