<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodeReviewDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('code_review_details', function (Blueprint $table) {
            $table->increments('code_review_detail_id');

            $table->integer('code_review_info_id')->unsigned()->nullable();
            $table->foreign('code_review_info_id')->references('code_review_info_id')->on('code_review_infos')->onDelete('cascade');

            $table->integer('code_review_id')->unsigned()->nullable();
            $table->foreign('code_review_id')->references('code_review_id')->on('code_reviews')->onDelete('cascade');

            $table->integer('review_check_id')->unsigned()->nullable();
            $table->foreign('review_check_id')->references('review_check_id')->on('review_check_lists')->onDelete('cascade');

            $table->text('comments')->nullable();
            $table->string('review_status');
            $table->enum('status', ['0', '1'])->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('code_review_details');
    }
}
