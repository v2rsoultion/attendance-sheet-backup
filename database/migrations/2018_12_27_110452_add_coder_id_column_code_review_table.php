<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoderIdColumnCodeReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('code_reviews', function (Blueprint $table) {
            $table->integer('coder_id')->unsigned()->nullable();
        });
        Schema::table('code_reviews', function(Blueprint $table){
            $table->foreign('coder_id')->references('admin_id')->on('admins')->onDelete('cascade');                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('code_reviews', 'coder_id')){
            Schema::table('code_reviews', function (Blueprint $table) {
                $table->dropColumn('coder_id');
            });
        }
    }
}
