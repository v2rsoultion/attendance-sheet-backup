<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('projects'))
        {
            Schema::create('projects', function (Blueprint $table) {
                $table->increments('project_id');
                $table->string('project_name')->nullable();
                $table->text('platform')->nullable();
                $table->enum('status',['0','1']);
                $table->integer('project_owner')->unsigned()->nullable();
                $table->foreign('project_owner')->references('admin_id')->on('admins')->onDelete('cascade');   
                $table->integer('project_manager')->unsigned()->nullable();
                $table->foreign('project_manager')->references('admin_id')->on('admins')->onDelete('cascade');   
                $table->date('project_start_date')->nullable();
                $table->date('project_end_date')->nullable();
                $table->text('project_description')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}