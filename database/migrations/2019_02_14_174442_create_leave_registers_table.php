<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('leave_registers')) {
            Schema::create('leave_registers', function (Blueprint $table) {
                $table->increments('leave_register_id');

                $table->integer('admin_id')->unsigned()->nullable();
                $table->foreign('admin_id')->references('admin_id')->on('admins');

                $table->tinyInteger('leave_category');
                $table->date('leave_start_date')->nullable();
                $table->date('leave_end_date')->nullable();
                $table->tinyInteger('leave_status')->default('2')->comment('Approved-1, Pending-2 or Cancel-3');
                $table->tinyInteger('status')->default('1');
                $table->text('leave_reason')->nullable();
                $table->text('leave_comment')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_registers');
    }
}
