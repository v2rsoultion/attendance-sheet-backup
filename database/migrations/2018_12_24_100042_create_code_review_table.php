<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodeReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('code_reviews', function (Blueprint $table) {
            $table->increments('code_review_id');
            $table->integer('project_id')->unsigned()->nullable();
            $table->foreign('project_id')->references('project_id')->on('projects')->onDelete('cascade');                
            $table->integer('project_detail_id')->unsigned()->nullable();
            $table->foreign('project_detail_id')->references('project_detail_id')->on('project_details')->onDelete('cascade');                
            $table->integer('code_reviewer_id')->unsigned()->nullable();
            $table->foreign('code_reviewer_id')->references('admin_id')->on('admins')->onDelete('cascade');                
            $table->integer('coder_id')->unsigned()->nullable();
            $table->foreign('coder_id')->references('admin_id')->on('admins');                
            $table->text('code_review_feedback')->nullable();
            $table->text('code_review_point')->nullable();
            $table->enum('status',['0','1'])->default('0'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('code_reviews');
    }
}
