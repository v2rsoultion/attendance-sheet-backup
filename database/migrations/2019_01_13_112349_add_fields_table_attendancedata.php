<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsTableAttendancedata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendancedatas', function (Blueprint $table) {
            $table->double('full_leaves', 8, 2)->nullable()->after('wfh_json_data');
            $table->double('half_leaves', 8, 2)->nullable()->after('wfh_json_data');
            $table->double('extra_working_hrs', 8, 2)->nullable()->after('wfh_json_data');
            $table->double('late_days', 8, 2)->nullable()->after('wfh_json_data');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('attendancedatas', 'late_half_days')){
            Schema::table('attendancedatas', function (Blueprint $table) {
                $table->dropColumn('full_leaves');
                $table->dropColumn('half_leaves');
                $table->dropColumn('extra_working_hrs');
                $table->dropColumn('late_days');
            });
        }
    }
}
