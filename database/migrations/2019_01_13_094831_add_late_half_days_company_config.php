<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLateHalfDaysCompanyConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_configs', function (Blueprint $table) {
            $table->string('late_half_days')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('company_configs', 'late_half_days')){
            Schema::table('company_configs', function (Blueprint $table) {
                $table->dropColumn('late_half_days');
            });
        }
    }
}
