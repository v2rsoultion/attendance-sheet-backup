<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('attendance_registers')) {
            Schema::create('attendance_registers', function (Blueprint $table) {
                $table->increments('attendance_register_id');

                $table->integer('admin_id')->unsigned()->nullable();
                $table->foreign('admin_id')->references('admin_id')->on('admins');

                $table->date('attendance_date')->nullable();
                $table->dateTime('attendance_intime')->nullable();
                $table->dateTime('attendance_outtime')->nullable();
                $table->integer('attendance_total_time')->comment('Total time in minutes')->nullable();
                $table->tinyInteger('attendance_status')->default(1);
                $table->string('attendance_key')->nullable();
                $table->tinyInteger('attendance_isRequest')->default(0);
                $table->tinyInteger('isRequest_status')->comment('Request approved or not by admin. 0 - approved, 1 - not approved')->default(0);
                $table->text('attendance_desc')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_registers');
    }
}
