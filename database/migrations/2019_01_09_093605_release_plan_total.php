<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReleasePlanTotal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('release_plans', function (Blueprint $table) {
            $table->text('total')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('release_plans', 'total')){
            Schema::table('release_plans', function (Blueprint $table) {
                $table->dropColumn('total');
            });
        }
    }
}
