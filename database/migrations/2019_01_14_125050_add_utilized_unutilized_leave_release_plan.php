<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUtilizedUnutilizedLeaveReleasePlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('release_plans', function (Blueprint $table) {
            $table->double('utilized', 8, 2)->default(0);
            $table->double('unutilized', 8, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('release_plans', ['utilized','unutilized'])){
            Schema::table('release_plans', function (Blueprint $table) {
                $table->dropColumn('utilized');
                $table->dropColumn('unutilized');
            });
        }
    }
}
