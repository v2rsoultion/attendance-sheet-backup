<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodeReviewInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('code_review_infos', function (Blueprint $table) {
            $table->increments('code_review_info_id');

            $table->integer('project_id')->unsigned()->nullable();
            $table->foreign('project_id')->references('project_id')->on('projects')->onDelete('cascade');

            $table->integer('code_reviewer_id')->unsigned()->nullable();
            $table->foreign('code_reviewer_id')->references('admin_id')->on('admins')->onDelete('restrict');

            $table->string('review_date')->nullable();

            $table->string('code_review_status')->nullable()->comment('1 - Ontime, 2 - Pending, 3 - Expire');

            $table->string('review_upcoming_date');

            $table->tinyInteger('isSubmit')->default(0)->comment('0 - Save, 1 - Save & Submit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('code_review_infos');
    }
}
