<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMonthsFieldsReleasePlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('release_plans', function (Blueprint $table) {
            $table->integer('jan')->nullable();
            $table->integer('feb')->nullable();
            $table->integer('mar')->nullable();
            $table->integer('apr')->nullable();
            $table->integer('may')->nullable();
            $table->integer('jun')->nullable();
            $table->integer('july')->nullable();
            $table->integer('aug')->nullable();
            $table->integer('sep')->nullable();
            $table->integer('oct')->nullable();
            $table->integer('nov')->nullable();
            $table->integer('dec')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('release_plans', ['jan','feb','mar','apr','may','jun','july','aug','sep','oct','nov','dec'])){
            Schema::table('release_plans', function (Blueprint $table) {
                $table->dropColumn('jan');
                $table->dropColumn('feb');
                $table->dropColumn('mar');
                $table->dropColumn('apr');
                $table->dropColumn('may');
                $table->dropColumn('jun');
                $table->dropColumn('july');
                $table->dropColumn('aug');
                $table->dropColumn('sep');
                $table->dropColumn('oct');
                $table->dropColumn('nov');
                $table->dropColumn('dec');
            });
        }
    }
}
