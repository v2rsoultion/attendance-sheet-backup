<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('work_times')) {
            Schema::create('work_times', function (Blueprint $table) {
                $table->increments('work_time_id');

                $table->integer('admin_id')->unsigned()->nullable();
                $table->foreign('admin_id')->references('admin_id')->on('admins');

                $table->date('work_time_date')->nullable();
                $table->text('total_work_time_hr')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_times');
    }
}
