<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeSlotHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //if (!Schema::hasTable('admins'))
        {
            Schema::create('time_slot_history', function (Blueprint $table)
            {
                $table->increments('time_slot_history_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->integer('time_slot_id')->unsigned()->nullable();
                $table->timestamps();
            });
            Schema::table('time_slot_history', function(Blueprint $table){
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('CASCADE');                
            });
            Schema::table('time_slot_history', function(Blueprint $table){
                $table->foreign('time_slot_id')->references('time_slot_id')->on('time_slot')->onDelete('CASCADE');                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_slot_history');
    }
}
