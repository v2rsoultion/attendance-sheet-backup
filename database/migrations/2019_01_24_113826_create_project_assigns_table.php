<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectAssignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('project_assigns'))
        {
            Schema::create('project_assigns', function (Blueprint $table) {
                $table->increments('project_assign_id');
                $table->integer('project_id')->unsigned()->nullable();
                $table->foreign('project_id')->references('project_id')->on('projects');                
                $table->integer('admin_id')->unsigned()->nullable();
                $table->foreign('admin_id')->references('admin_id')->on('admins');                
                $table->enum('project_assign_status',['0','1'])->default('1');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_assigns');
    }
}
