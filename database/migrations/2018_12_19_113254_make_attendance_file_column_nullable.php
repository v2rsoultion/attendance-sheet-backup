<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeAttendanceFileColumnNullable extends Migration
{
    // Adding Constructer for modify table column
    public function __construct()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (Schema::hasColumn('attendances', 'attendance_file')) {
            Schema::table('attendances', function (Blueprint $table) {
                $table->text('attendance_file')->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        if (Schema::hasColumn('attendances', 'attendance_file')) {
            Schema::table('attendances', function (Blueprint $table) {
                $table->text('attendance_file')->change();
            });
        }
    }
}
