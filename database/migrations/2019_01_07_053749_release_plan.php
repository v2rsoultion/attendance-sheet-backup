<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReleasePlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('release_plans'))
        {
            Schema::create('release_plans', function (Blueprint $table)
            {
                $table->increments('release_plan_id');
                $table->integer('admin_id')->unsigned()->nullable();
                $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');                
                $table->text('year')->nullable();
                $table->text('json')->nullable();
                $table->decimal('total', 8,2)->nullable();
                $table->decimal('jan', 8,2)->nullable();
                $table->decimal('feb',8,2)->nullable();
                $table->decimal('mar',8,2)->nullable();
                $table->decimal('apr',8,2)->nullable();
                $table->decimal('may',8,2)->nullable();
                $table->decimal('jun',8,2)->nullable();
                $table->decimal('july',8,2)->nullable();
                $table->decimal('aug',8,2)->nullable();
                $table->decimal('sep',8,2)->nullable();
                $table->decimal('oct',8,2)->nullable();
                $table->decimal('nov',8,2)->nullable();
                $table->decimal('dec',8,2)->nullable();
                $table->double('utilized', 8, 2)->default(0);
                $table->double('unutilized', 8, 2)->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('release_plans');
    }
}
