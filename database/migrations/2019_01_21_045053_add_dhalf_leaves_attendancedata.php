<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDhalfLeavesAttendancedata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendancedatas', function (Blueprint $table) {
            $table->double('dhalf_leaves',8,2)->nullable()->after('half_leaves');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('attendancedatas','dhalf_leaves')){
            Schema::table('attendancedatas', function (Blueprint $table) {
                $table->dropColumn('dhalf_leaves');
            });
        }
    }
}
