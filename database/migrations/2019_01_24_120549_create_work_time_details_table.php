<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkTimeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('work_time_details')) {
            Schema::create('work_time_details', function (Blueprint $table) {
                $table->increments('work_time_detail_id');
                $table->integer('project_id')->unsigned();
                $table->foreign('project_id')->references('project_id')->on('projects');
                $table->integer('work_time_id')->unsigned();
                $table->foreign('work_time_id')->references('work_time_id')->on('work_times');
                $table->integer('work_type_id')->unsigned();
                $table->foreign('work_type_id')->references('work_type_id')->on('work_types');
                $table->text('work_time_detail_hrs')->nullable();
                $table->text('work_time_detail_description')->nullable();
                $table->timestamps();

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_time_details');
    }
}
