<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlatformTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('platforms'))
        {
            Schema::create('platforms', function (Blueprint $table) {
                $table->increments('platform_id');
                $table->string('platform_name'); 
                $table->enum('status',['0','1'])->default('1');
                $table->timestamps();
            });
            // Insert some stuff
            DB::table('platforms')->insert(
                array([
                    'platform_id'      => 1,
                    'platform_name'    => 'Android',
                    'status'           => '1',
                    'created_at'       => date('Y-m-d H:i:s'),
                    'updated_at'       => date('Y-m-d H:i:s'),
                ],
                [
                    'platform_id'      => 2,
                    'platform_name'    => 'IOS',
                    'status'           => '1',
                    'created_at'       => date('Y-m-d H:i:s'),
                    'updated_at'       => date('Y-m-d H:i:s'),
                ],
                [
                    'platform_id'      => 3,
                    'platform_name'    => 'Web',
                    'status'           => '1',
                    'created_at'       => date('Y-m-d H:i:s'),
                    'updated_at'       => date('Y-m-d H:i:s'),
                ],
                [
                    'platform_id'      => 4,
                    'platform_name'    => 'API',
                    'status'           => '1',
                    'created_at'       => date('Y-m-d H:i:s'),
                    'updated_at'       => date('Y-m-d H:i:s'),
                ],
                [
                    'platform_id'      => 5,
                    'platform_name'    => 'MEAN Stack',
                    'status'           => '1',
                    'created_at'       => date('Y-m-d H:i:s'),
                    'updated_at'       => date('Y-m-d H:i:s'),
                ],)
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('platforms');
    }
}
