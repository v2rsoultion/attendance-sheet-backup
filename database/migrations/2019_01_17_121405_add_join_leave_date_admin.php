<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJoinLeaveDateAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admins', function (Blueprint $table) {
            $table->date('joining_date')->nullable();
            $table->date('leaving_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('admins', ['joining_date', 'leaving_date'])){
            Schema::table('admins', function (Blueprint $table) {
                $table->dropColumn('joining_date');
                $table->dropColumn('leaving_date');
            });
        }
    }
}
