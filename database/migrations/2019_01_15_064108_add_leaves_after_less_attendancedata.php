<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLeavesAfterLessAttendancedata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendancedatas', function (Blueprint $table) {
            $table->string('month_year')->nullable()->after('admin_id');
            $table->double('leaves_after_less', 8, 2)->nullable()->after('full_leaves');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('attendancedatas', 'month_year')){
            Schema::table('attendancedatas', function (Blueprint $table) {
                $table->dropColumn('month_year');
                $table->dropColumn('leaves_after_less');
            });
        }
    }
}
