<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('admins'))
        {
            Schema::create('admins', function (Blueprint $table)
            {
                $table->increments('admin_id');
                $table->string('emp_id')->unique()->nullable();
                $table->string('admin_name')->nullable();
                $table->string('email')->unique();
                $table->string('phone')->nullable();
                $table->integer('department_id')->unsigned()->nullable();
                $table->foreign('department_id')->references('department_id')->on('departments');                
                $table->enum('type',['1','2'])->default('2');
                $table->date('joining_date')->nullable();
                $table->date('leaving_date')->nullable();
                $table->enum('status',['0','1'])->default('1');
                $table->enum('is_terminate', ['0', '1'])->default('0');
                $table->enum('is_resign', ['0', '1'])->default('0');
                $table->enum('verification_status',['0','1'])->default('0');
                $table->integer('time_slot_id')->unsigned()->nullable(); 
                $table->foreign('time_slot_id')->references('time_slot_id')->on('time_slot');
                $table->string('saturday_off')->nullable();
                $table->text('platforms')->nullable();
                $table->string('password')->nullable();
                $table->text('admin_profile_img')->nullable();
                $table->rememberToken();
                $table->timestamps();
            });
            // Schema::table('admins', function(Blueprint $table){
            // });
        }
        // Insert some stuff
        DB::table('admins')->insert(
            array(
                'admin_id'          =>  1,
                'admin_name'        => 'Admin',
                'email'             => 'admin@gmail.com',
                'type'              => '1',
                'status'            => '1',
                'password'          => '$2y$10$CRFA.bp9jE6bG9iYd0BcUugBhFSur3FEtfy7t2iHZbq2Uyp4dDgdW',
                'remember_token'    => 'oJJmPSevfhpZdKdRf2iSfnKLM8boFxrkWkYP1GjANsSkmcU6CMBNHkdMIfcx',
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}