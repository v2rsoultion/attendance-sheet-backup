<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReleasePlanLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('release_plan_logs', function (Blueprint $table) {
            $table->increments('release_plan_log_id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->foreign('admin_id')->references('admin_id')->on('admins')->onDelete('cascade');                
            $table->string('month_year');
            $table->double('utilized', 8, 2);	
            $table->double('unutilized', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('release_plan_logs');
    }
}
