<?php

return [
    'project_status' => array(
        '' => 'Project Status',
        '1' => 'Active',
        '0' => 'Inactive',
    ),
    'user_role_status' => array(
        '' => 'User Role Status',
        '1' => 'Active',
        '0' => 'Inactive',
    ),
    // 'project_status'            => array(
    //     ''   =>  'Project Status',
    //     '1'  => 'Yet to Start',
    //     '2'  => 'In Progress',
    //     '3'  => 'Hold',
    //     '4'  => 'Complete',
    // ),
    'work_type_list' => array(
        '1' => 'Client Meeting & Demo',
        '2' => 'Change Request & Smoke Testing',
        '3' => 'Design',
        '4' => 'Development & Smoke Testing',
        '5' => 'Support & Bug Fixing',
        '6' => 'Testing',
        '7' => 'Team Meeting',
        '8' => 'Work on Tester Report',
        '9' => 'Code Review',
        '10' => 'Review & Guideline',
        '11' => 'Reporting',
        '12' => 'Documentation / Mockups',
    ),
    // This list for User Role Migration
    'user_role_list' => array(
        '1' => 'Owner',
        '2' => 'Manager',
        '3' => 'Code Reviewer'
    ),
    'frequently_code_review' => array(
        '1' => 'Once a week',
        '2' => 'Fortnightly',
        '3' => 'Once a month',
    ),
    'code_review_day' => array(
        '1' => 'Monday',
        '2' => 'Tuesday',
        '3' => 'Wednesday',
        '4' => 'Thursday',
        '5' => 'Friday',
        '6' => 'Saturday',
    ),
    'saturday_off' => array(
        '0' => '0',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => 'All',
    ),
    'emp_id_prefix' => 'V2R',
    'max_extra_working_hrs' => 60,
    // For Late Mark
    'time_relaxation' => 10,
    'attendance_key' => array(
        'present' => 'Present',
        'late' => 'Late',
        'dhalf' => 'Direct',
        'full_day' => 'Full Day',
        'half_day' => 'Half Day',
        'na' => 'NA',
        'sunday' => 'Sunday',
        'saturday' => 'Saturday',
        'ch' => 'Company Holiday',
    ),
    // For Leave Module
    'leave_category' => array(
        '1' => 'Casual Leave',
        '2' => 'Emergency Leave',
        '3' => 'Out of Town',
        '4' => 'Planned Leave',
        '5' => 'Sick Leave',
        '6' => 'Unplanned Leave',
    ),
    // For Leave Status 
    'leave_status' => array(
        '1' => 'Approved',
        '2' => 'Pending',
        '3' => 'Cancelled',
    ),
    // Total Chance for Short Leave
    'early_leave_chance' => 2,
    'notification_message' => array(
        'leave' => 'Apply For Leave',
        'early_leave' => 'Apply For Early Leave',
        'attendance_request' => 'Request For Attendance',
    ),
    'code_review_checks' => array(
        '1' => 'Code Comments',
        '2' => 'Coding Standards',
        '3' => 'Database',
        '4' => 'Logic',
        '5' => 'Syntax',
    ),
    'code_review_status' => array(
        '1' => 'All Okey',
        '2' => 'Not Okey Not Closed',
        '3' => 'Need Improvement But Closed',
    ),
    'review_status' => array(
        '1' => 'Ontime',
        '2' => 'Pending',
        '3' => 'Delay',
    ),
];
