@if(isset($department['department_id']) && !empty($department['department_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('department_id',old('department_id',isset($department['department_id']) ? $department['department_id'] : ''),['class' => 'gui-input', 'id' => 'department_id', 'readonly' => 'true']) !!}
@if($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{$errors->first()}}
    </div>
@endif
<!-- <h3 class="box-title m-b-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">Category</h3>
<p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13"> Enter new {!! trans('language.department') !!} </p> -->
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.department_name') !!}">{!! trans('language.department_name') !!}</label>
        {!! Form::text('department_name', old('department_name',isset($department['department_name']) ? $department['department_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.department_name'), 'id' => 'department_name','for'=>trans('language.department_name')]) !!}
    </div>
</div>
<div class=" col-sm-9">
    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10 no-radius">{!! $submit_button !!}</button>
</div>
<div class="clearfix"></div>
<script type="text/javascript">    
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Department name should be alphabetic.");

        $("#department-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                department_name: {
                    required: true,
                },
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        }); 
    });
</script>