@extends('admin-panel.layout.header')

@section('content')
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">{!! trans('language.profile') !!}</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                        <li class="active">{!! trans('language.profile') !!}</li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session()->get('success') }}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{$errors->first()}}
                </div>
            @endif
            <!-- .row -->
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <div class="white-box">
                        <div class="user-bg">
                            <div class="overlay-box">
                                <div class="user-content">
                                    {{-- @if(isset($login_info['img'])) --}}
                                        <a href="javascript:void(0)"><img src="{!! URL::to('public/admin/plugins/images/default.jpg') !!}" class="thumb-lg img-circle" alt="img"></a>
                                    {{-- @endif --}}
                                    <h4 class="text-white">{!! $team_data['admin_name'] !!}</h4>
                                    <h5 class="text-white">{!! $team_data['email'] !!}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-xs-12">
                    <div class="white-box">
                        <ul class="nav nav-tabs tabs customtab">
                            <li class="tab active">
                                <a href="#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Profile</span> </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12"> <strong>{!! trans('language.team_name') !!}</strong>
                                        <br>
                                        <p class="text-muted">{!! $team_data['admin_name'] !!}</p>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12"> <strong>{!! trans('language.team_phone') !!}</strong>
                                        <br>
                                        <p class="text-muted">@if(isset($team_data['phone'])) {!! $team_data['phone'] !!} @else ---- @endif</p>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12"> <strong>{!! trans('language.team_email') !!}</strong>
                                        <br>
                                        <p class="text-muted">@if(isset($team_data['email'])) {!! $team_data['email'] !!} @else ---- @endif</p>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12"> <strong>{!! trans('language.team_department') !!}</strong>
                                        <br>
                                        <p class="text-muted">@if(isset($team_data['department_id'])) {!! $team_data['get_department']['department_name'] !!} @else ---- @endif</p>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12"> <strong>{!! trans('language.team_time_slot') !!}</strong>
                                        <br>
                                        <p class="text-muted">@if(isset($team_data['time_slot_id'])) {!! get_all_time_slot($team_data['time_slot_id']) !!} @else ---- @endif</p>
                                    </div>
                                </div>                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
@endsection

