
@if($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{$errors->first()}}
    </div>
@endif
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.current_password') !!}">{!! trans('language.current_password') !!}</label>
        {!! Form::password('current_password', ['class' => 'form-control', 'id' => 'current_password','for'=>trans('language.current_password')]) !!}
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.new_password') !!}">{!! trans('language.new_password') !!}</label>
        {!! Form::password('new_password', ['class' => 'form-control', 'id' => 'new_password','for'=>trans('language.new_password')]) !!}
    </div>
    <span>(Note: Enter a combination of at least eight numbers, letters, and special characters.)</span>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.confirm_password') !!}">{!! trans('language.confirm_password') !!}</label>
        {!! Form::password('confirm_password', ['class' => 'form-control', 'id' => 'confirm_password','for'=>trans('language.confirm_password')]) !!}
    </div>
</div>
<div class=" col-sm-9">
    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10 no-radius">{!! $submit_button !!}</button>
</div>
<div class="clearfix"></div>
<script type="text/javascript">    
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("validPassword", function(value, element) {
            return this.optional(element) || /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/i.test(value);
        }, "Please enter strong password.");

        $("#change-password").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                current_password: {
                    required: true,
                },
                new_password: {
                    required: true,
                    validPassword: true,
                },
                confirm_password: {
                    required: true,
                    equalTo: "#new_password"
                },
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        }); 
    });
</script>