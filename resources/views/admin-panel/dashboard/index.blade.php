@extends('admin-panel.layout.header')

@section('content')
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{trans('language.dashboard')}}</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                       
                        <ol class="breadcrumb">
                            <li class="active">{{trans('language.dashboard')}}</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                @if(isset($login_info) && $login_info['type'] == 1)
                    <div class="row">
                        <div class="col-lg-3 col-sm-6 col-xs-12">
                            <div class="white-box analytics-info">
                                <h3 class="box-title">Total Users</h3>
                                <ul class="list-inline two-part">
                                    <li>
                                        <div id="sparklinedash"></div>
                                    </li>
                                    <li class="text-right"><i class="ti-arrow-up text-success"></i> <span class="counter text-success">{{$teamCounter}}</span></li>
                                </ul>   
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-xs-12">
                            <div class="white-box analytics-info">
                                <h3 class="box-title">Total Departments</h3>
                                <ul class="list-inline two-part">
                                    <li>
                                        <div id="sparklinedash2"></div>
                                    </li>
                                    <li class="text-right"><i class="ti-arrow-up text-purple"></i> <span class="counter text-purple">{{$departmentCounter}}</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-xs-12">
                            <div class="white-box analytics-info">
                                <h3 class="box-title">Total Projects</h3>
                                <ul class="list-inline two-part">
                                    <li>
                                        <div id="sparklinedash3"></div>
                                    </li>
                                    <li class="text-right"><i class="ti-arrow-up text-purple"></i> <span class="counter text-purple">{{$projectCounter}}</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-xs-12">
                            <a href="{!! URL::to('admin-panel/attendanceregister/view-in-out/pending') !!}">
                                <div class="white-box analytics-info">
                                    <h3 class="box-title">Pending Attendance Request</h3>
                                    <ul class="list-inline two-part">
                                        <li>
                                            <div id="sparklinedash3"></div>
                                        </li>
                                        <li class="text-right"><i class="ti-arrow-up text-danger"></i> <span class="counter text-danger">{{$attendanceRequestCounter}}</span></li>
                                    </ul>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-xs-12">
                            <a href="{!! URL::to('admin-panel/leave/view-leave/pending') !!}">
                                <div class="white-box analytics-info">
                                    <h3 class="box-title">Pending Leave Request</h3>
                                    <ul class="list-inline two-part">
                                        <li>
                                            <div id="sparklinedash3"></div>
                                        </li>
                                        <li class="text-right"><i class="ti-arrow-up text-info"></i> <span class="counter text-info">{{$leaveRequestCounter}}</span></li>
                                    </ul>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-xs-12">
                            <a href="{!! URL::to('admin-panel/early-leave/view-early-leave/pending') !!}">
                                <div class="white-box analytics-info">
                                    <h3 class="box-title">Pending Early Leave Request</h3>
                                    <ul class="list-inline two-part">
                                        <li>
                                            <div id="sparklinedash3"></div>
                                        </li>
                                        <li class="text-right"><i class="ti-arrow-up text-info"></i> <span class="counter text-info">{{$earlyLeaveRequestCounter}}</span></li>
                                    </ul>
                                </div>
                            </a>
                        </div>
                    </div>
                @endif
                @if(isset($login_info) && $login_info['type'] == 2)
                    <div class="row">
                        <div class="col-md-12 col-lg-4">
                            <div class="white-box">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2 class="m-b-0 font-medium">{{ isset($in_out) ? $in_out['in'] : '--'}}</h2>
                                        <h5 class="text-muted m-t-0">Today In</h5></div>
                                    <div class="col-sm-6">
                                        <button type ="button" class="btn btn-block btn-info register-time-in no-radius" status="in">In</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="white-box">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2 class="m-b-0 font-medium">{{ isset($in_out) ? $in_out['out'] : '--'}}</h2>
                                        <h5 class="text-muted m-t-0">Today Out</h5></div>
                                    <div class="col-sm-6">
                                        <button type ="button" class="btn btn-block btn-inverse register-time-out no-radius" status="out">Out</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="white-box">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2 class="m-b-0 font-medium">Pending Task</h2>
                                        <h5 class="text-muted m-t-0">Action Here</h5></div>
                                    <div class="col-sm-6">
                                        <a href="{!! url('admin-panel\pending-task') !!}" class="btn btn-block btn-success no-radius">Action</a>
                                        {{-- <button class="btn btn-block btn-success no-radius"  data-target="#applyLeaveModal" data-toggle="modal">Action</button> --}}
                                        {{-- <button class="btn btn-block btn-success no-radius"  data-target="#applyLeaveModal" data-toggle="modal">Action</button> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.row -->
                    <!-- .row -->
                    <div class="row">
                        <div class="col-md-7 col-sm-12 col-lg-8">
                            <div class="white-box p-b-0">
                                <div class="row">
                                    <div class="col-xs-8">
                                        <h4 class="font-medium m-t-0">Today's @if($in_out['out'] == '-:- -') Working @else Worked @endif Time :- {{ $total_work_time }}</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-8">
                                        <h2 class="font-medium m-t-0">{{ date('F')}}</h2>
                                    </div>
                                </div>
                                <div class="row m-t-10">
                                    <div class="col-sm-12 col-sm-6 b-t b-r">
                                        <a href="{{ url('admin-panel/in-out/view-in-out') }}">
                                            <ul class="expense-box">
                                                <li><i class="ti-time text-info"></i>
                                                    <div>
                                                        <h2>{{$attendance_detail['total_late']}}</h2>
                                                        <h4>Late</h4>
                                                    </div>
                                                </li>
                                            </ul>
                                        </a>
                                    </div>
                                    <div class="col-sm-12 col-sm-6  b-t">
                                        <a href="{{ url('admin-panel/in-out/view-in-out') }}">
                                            <ul class="expense-box">
                                                <li><i class="ti-alarm-clock text-info"></i>
                                                    <div>
                                                        <h2>{{$attendance_detail['total_dhalf']}}</h2>
                                                        <h4>Direct Half</h4>
                                                    </div>
                                                </li>
                                            </ul>
                                        </a>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-sm-12 col-sm-6  b-t b-r">
                                        <ul class="expense-box">
                                            <li><i class="ti-wallet text-info"></i>
                                                <div>
                                                    <h2>{{$attendance_detail['release_unutilized']}}</h2>
                                                    <h4>Release Unutilized Leave</h4>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-12 col-sm-6  b-t">
                                        <ul class="expense-box">
                                            <li><i class="icon-logout text-info"></i>
                                                <div>
                                                    <h2>{!! $attendance_detail['early_leave_chance'] !!}</h2>
                                                    <h4>Early Leaves ({!! $attendance_detail['early_leave_count'] !!})</h4>
                                                </div>
                                            </li>
                                            <li>
                                                <button class="btn btn-block btn-success no-radius" data-target="#applyEarlyLeaveModal" data-toggle="modal">Apply </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-lg-4">
                            <div class="white-box">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2 class="m-b-0 font-medium">Leave</h2>
                                        <h5 class="text-muted m-t-0">Apply Here</h5></div>
                                    <div class="col-sm-6">
                                        <button class="btn btn-block btn-success no-radius"  data-target="#applyLeaveModal" data-toggle="modal">Apply</button>
                                    </div>
                                </div>
                            </div>
                            <div class="white-box">
                                <h3 class="box-title">Applied Leaves</h3>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>DATE</th>
                                                <th>STATUS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $applyLeaveCounter = 1; @endphp
                                            @foreach ($applied_leave as $key => $value)                             
                                            <tr>
                                                <td>{!! $applyLeaveCounter++ !!}</td>
                                                <td class="txt-oflo">
                                                    {!! $value[0] !!} 
                                                    <br/><span class="text-muted">{!! $value[1] !!}
                                                </td>
                                                <td>
                                                    <span class="label @if($value[2] == 'Approved')label-success @elseif($value[2] == 'Cancelled') label-danger @elseif($value[2] == 'Pending') label-info @endif label-rouded">{!! $value[2] !!}</span>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <a href="{{ url('admin-panel/my-leave/view-my-leave') }}"><span class="pull-right text-info">View More</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                @endif
            </div>
            <div id="in-out-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Request or Mark Attendance</h4> 
                        </div>
                        <div class="modal-body">
                            <form name="get-out-details" id="get-out-details">
                                <div class="form-group">
                                    <label for="atten-option" class="control-label">Request For:</label>
                                    <select class="selectpicker form-control" name="atten-option" id="outOption" data-style="form-control">
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="atten-desc" class="control-label">Description:</label>
                                    <textarea class="form-control" name="atten-desc" id="atten-desc"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect no-radius" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info waves-effect waves-light register-time-out-submit no-radius attendance-request-btn"><div class="attendance-request">Save changes</div></button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="applyLeaveModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="applyLeaveModalLabel" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Apply For Leave</h4> 
                        </div>
                        {!! Form::open(['files'=>TRUE,'id' => 'apply-leave-details' ,'autocomplete' => "off",  'class'=>'']) !!}
                            <input type="hidden" name="leave_register_id" value="" id="leave_register_id" />
                        <div class="modal-body">
                            <div class="alert alert-success alert-dismissible" id="leaveSuccess" style="display:none;">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span class="leaveSuccessMessage"></span>
                            </div>
                            <div class="alert alert-danger alert-dismissible" id="leaveError" style="display:none;">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span class="leaveErrorMessage"></span>
                            </div>
                            <div class="form-group">
                                <label for="leave_category" class="control-label">Leave Category:</label>
                                {!!Form::select('leave_category', $leave_category, '', ['class' => 'selectpicker form-control', 'data-style' => 'form-control', 'id'=>'leave_category'])!!}
                            </div>
                            <div class="form-group">
                                <label for="leave_date" class="control-label">Leave Date:</label>
                                <input class="form-control input-daterange-datepicker" type="text" name="leave_date" id="leave_date" value="{!! date('d/m/Y') !!}" />
                            </div>
                            <div class="form-group">
                                <label for="leave_reason" class="control-label">Leave Reason:</label>
                                <textarea class="form-control" name="leave_reason" id="leave_reason"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect no-radius" data-dismiss="modal">Close</button>
                            <button type="submit" name="submit" class="btn btn-info waves-effect waves-light leave-apply submitFormButton no-radius"><div class="leave-apply-text">Apply</div></button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            {{-- <div id="applyEarlyLeaveModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Request for Early Leave</h4> 
                        </div>
                        <form name="early_leave_form" id="early_leave_form">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="early_leave_reason" class="control-label">Reason:</label>
                                    <textarea class="form-control" name="early_leave_reason" id="early_leave_reason"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="leave_start_time" class="control-label">Start Time:</label>
                                    <div class='input-group bootstrap-timepicker timepicker'>
                                        <input type='text' class="form-control input-small" name="leave_start_time" id="leave_start_time" readonly/>
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="leave_end_time" class="control-label">End Time:</label>
                                    <div class='input-group bootstrap-timepicker timepicker'>
                                        <input type='text' class="form-control input-small" name="leave_end_time" id="leave_end_time" readonly/>
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect no-radius" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-info waves-effect waves-light no-radius early-leave-btn"><div class="early-leave">Submit</div></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> --}}
            @include('admin-panel.dashboard.early-leave-model');
<script>
    $( document ).ready(function() {
        $('.input-daterange-datepicker').daterangepicker({
            locale: {
               format: 'YYYY/MM/DD'
            },
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-danger',
            cancelClass: 'btn-inverse',
        });
        $('.register-time-in').click(function(){
            $('.preloader').css('display','inline');
            var attendance_type = $(this).attr('status');
            $.ajax(
            {
                url: "{{ url('admin-panel/attendance-time-in') }}",
                type: 'GET',
                data: { 'attendance_type': attendance_type },
                success: function (res)
                {
                    $('.preloader').css('display','none');
                    if(res.status == 'not_login') {
                        window.location.href = '{!! url("admin-panel") !!}';
                    } else {
                        if (res.status == "success") {
                            swal({
                                title: res.message,
                                text: '',
                                type: "success", 
                            }).then(function(){
                                location.reload();
                            });
                        }
                        else if(res.status == "error") {
                            swal({
                                title: res.message,
                                text: '',
                                type: "success", 
                            }).then(function(){
                                location.reload();
                            });
                        }
                    }
                }
            });
        });
        $('.register-time-out').click(function(){
            swal({
                title: "Are you sure you want to mark out ?",
                text: "You will not be able to recover this action!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
            }).then(result => {
                if (result.value) {
                    $('.preloader').css('display','inline');
                    var attendance_type = $(this).attr('status');
                    $.ajax(
                    {
                        url: "{{ url('admin-panel/attendance-time-out') }}",
                        type: 'GET',
                        data: { 'attendance_type': attendance_type },
                        success: function (res)
                        {
                            $('.preloader').css('display','none');
                            if(res.status == 'not_login') {
                                window.location.href = '{!! url("admin-panel") !!}';
                            } else if(res.status == 'error_work_time') {
                                window.location.href = res.url;
                            } else {                         
                                if (res.status == "success") {
                                    if(res.message == 'request_full_day'){
                                        $('#in-out-modal').modal({
                                            show: 'false'
                                        });
                                        $('#outOption').empty();
                                        $('#outOption').append('<option value="" selected>Select Option</option>');
                                        $.each( res.optArray, function( key, value ) {
                                            opt = '<option value="'+key+'">' + value + '</option>';
                                            $('#outOption').append(opt);
                                        });
                                        $("#outOption").selectpicker("refresh");
                                    }
                                    else if(res.message == 'request_half_day'){
                                        $('#in-out-modal').modal({
                                            show: 'false'
                                        });
                                        $('#outOption').empty();
                                        $('#outOption').append('<option value="" selected>Select Option</option>');
                                        $.each( res.optArray, function( key, value ) {
                                            opt = '<option value="'+key+'">' + value + '</option>';
                                            $('#outOption').append(opt);
                                        });
                                        $("#outOption").selectpicker("refresh");
                                    }
                                    else{
                                        swal({
                                            title: res.message,
                                            text: '',
                                            type: "success", 
                                        }).then(function(){
                                            location.reload();
                                        });
                                    }
                                }
                                else if(res.status == "error") {
                                    swal({
                                        title: res.message,
                                        text: '',
                                        type: "success", 
                                    }).then(function(){
                                        location.reload();
                                    });
                                }
                            }
                        }
                    });
                } else {
                    // handle dismissals
                    // result.dismiss can be 'cancel', 'overlay', 'esc' or 'timer'
                    swal("Cancelled", "You cancelled your action", "error");
                }
            })             
        });
        $('.register-time-out-submit').click(function(e){
            $.ajax(
            {
                url: "{{ url('admin-panel/attendance-time-out-submit') }}",
                type: 'GET',
                data: $('#get-out-details').serialize(),
                beforeSend: function(){
                    $('.attendance-request-btn').attr("disabled", true);
                    $('.attendance-request').html('<i class="fa fa-spinner fa-spin"></i> Loading...');
                },
                success: function (res)
                {
                    $('.attendance-request-btn').attr("disabled", false);
                    $('.attendance-request').html('Save changes');
                    if(res.status == 'not_login') {
                        window.location.href = '{!! url("admin-panel") !!}';
                    } else {
                        if (res.status == "success") {
                            $('#in-out-modal').modal({
                                show: 'true'
                            });
                            swal({
                                title: res.message,
                                text: '',
                                type: "success", 
                                closeOnClickOutside: false,
                            }).then(function(){
                                location.reload();
                            });
                        }
                        else if(res.status == "error") {
                            $('#in-out-modal').modal({
                                show: 'true'
                            });
                            swal({
                                title: res.message,
                                text: '',
                                type: "success", 
                                closeOnClickOutside: false,
                            }).then(function(){
                            location.reload();
                            });
                        }
                    }
                }
            });
            e.preventDefault();
        });
    });
    jQuery(document).ready(function () {
        $('#applyLeaveModal').on('hidden.bs.modal', function () {
            location.reload();
        });
        $("#apply-leave-details").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                leave_category: {
                    required: true,
                },
                leave_date: {
                    required: true,
                }
            },
            message: {
                leave_date: {
                    required: "Please select date",
                },
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Apply');
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Apply');
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                $(".submitFormButton").html('Apply');
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            },
            submitHandler: function(form) {
                $('.leave-apply-text').html('<i class="fa fa-spinner fa-spin"></i> Loading...');
                $('.leaveSuccessMessage').html(null);
                $('#leaveSuccess').css('display', 'none');
                $('.leaveErrorMessage').html(null);
                $('#leaveError').css('display', 'none');
                $.ajax({
                    url: "{!! $apply_leave_url !!}",
                    type: 'get',
                    data: $(form).serialize(),
                    beforeSend: function(){
                        $('.leave-apply').attr("disabled", true);
                        $('.leave-apply-text').html('<i class="fa fa-spinner fa-spin"></i> Loading...');
                    },
                    success: function(response) {
                        $('.leave-apply').attr("disabled", false);
                        $('.leave-apply-text').html('Apply');
                        if(response.status == 'not_login') {
                            window.location.href = '{!! url("admin-panel") !!}';
                        } else {
                            if(response.status == 'success'){
                                // Clear the form
                                $(':input','#apply-leave-details')
                                .not(':button, :submit, :reset, :hidden')
                                .val('')
                                .removeAttr('checked')
                                .removeAttr('selected');
                                
                                $('.leaveSuccessMessage').html(response.message);
                                $('#leaveSuccess').fadeIn();
                            }
                            else if(response.status == 'error'){
                                $('.leaveErrorMessage').html(response.message);
                                $('#leaveError').fadeIn(function() {
                                    setTimeout(function() {
                                        $('#leaveError').fadeOut();
                                    }, 2500);
                                });
                            }
                            else if(response.status == 'error-validate'){
                                $('.leaveErrorMessage').html(response.message);
                                $('#leaveError').fadeIn(function() {
                                    setTimeout(function() {
                                        $('#leaveError').fadeOut();
                                    }, 2500);
                                });
                            }
                        }
                    }            
                });
            }
        });

        $('#applyEarlyLeaveModal').on('hidden.bs.modal', function () {
            location.reload();
        });
        /*
        $("#early_leave_form").validate({
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            rules: {
                early_leave_reason: {
                    required: true,
                },
                leave_start_time: {
                    required: true,
                },
                leave_end_time: {
                    required: true,
                }
            },
            highlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Apply');
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Apply');
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                $(".submitFormButton").html('Apply');
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "{!! $apply_short_leave_url !!}",
                    type: 'get',
                    data: $(form).serialize(),
                    beforeSend: function(){
                        $('.early-leave-btn').attr("disabled", true);
                        $('.early-leave').html('<i class="fa fa-spinner fa-spin"></i> Loading...');
                    },
                    success: function(response) {
                        $('.early-leave-btn').attr("disabled", false);
                        $('.early-leave').html('Submit');
                        if(response.status == 'not_login') {
                            window.location.href = '{!! url("admin-panel") !!}';
                        } else {
                            if(response.status == 'success'){
                                swal({
                                    title: response.message,
                                    text: '',
                                    type: "success", 
                                    closeOnClickOutside: false,
                                }).then(function(){
                                    location.reload();
                                });
                            }
                            else{
                                swal ( "Oops" ,  "Something went wrong!" ,  "error" );
                            }
                        }
                    }            
                });
            }
        });*/ 
    });
</script>
@endsection

