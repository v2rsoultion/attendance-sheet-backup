<div id="applyEarlyLeaveModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Request for Early Leave</h4> 
            </div>
            <form name="early_leave_form" id="early_leave_form">
                <div class="modal-body">
                    @isset($employee_list)
                    <div class="form-group">
                        <label for="early_leave_reason" class="control-label">Employee:</label>
                        {!! Form::select('early_leave_employee', isset($employee_list) ? $employee_list : '', null, ['class' => 'form-control', 'name' => 'early_leave_employee'])!!}
                    </div>
                    @endisset
                    <div class="form-group">
                        <label for="early_leave_reason" class="control-label">Reason:</label>
                        <textarea class="form-control" name="early_leave_reason" id="early_leave_reason"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="leave_start_time" class="control-label">Start Time:</label>
                        <div class='input-group bootstrap-timepicker timepicker'>
                            <input type='text' class="form-control input-small" name="leave_start_time" id="leave_start_time" readonly/>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="leave_end_time" class="control-label">End Time:</label>
                        <div class='input-group bootstrap-timepicker timepicker'>
                            <input type='text' class="form-control input-small" name="leave_end_time" id="leave_end_time" readonly/>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect no-radius" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info waves-effect waves-light no-radius early-leave-btn"><div class="early-leave">Submit</div></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('#leave_start_time').timepicker({
    });
    $('#leave_end_time').timepicker({
    });
    $(document).ready(function(){
        $("#early_leave_form").validate({
            /* @validation states + elements 
            ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
            ------------------------------------------ */
            rules: {
                @isset($employee_list)
                early_leave_employee: {
                    required: true,
                },
                @endisset
                early_leave_reason: {
                    required: true,
                },
                leave_start_time: {
                    required: true,
                },
                leave_end_time: {
                    required: true,
                }
            },
            /* @validation highlighting + error placement  
            ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Apply');
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Apply');
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                $(".submitFormButton").html('Apply');
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "{!! $apply_short_leave_url !!}",
                    type: 'get',
                    data: $(form).serialize(),
                    beforeSend: function(){
                        $('.early-leave-btn').attr("disabled", true);
                        $('.early-leave').html('<i class="fa fa-spinner fa-spin"></i> Loading...');
                    },
                    success: function(response) {
                        $('.early-leave-btn').attr("disabled", false);
                        $('.early-leave').html('Submit');
                        if(response.status == 'not_login') {
                            window.location.href = '{!! url("admin-panel") !!}';
                        } else {
                            if(response.status == 'success'){
                                swal({
                                    title: response.message,
                                    text: '',
                                    type: "success", 
                                    closeOnClickOutside: false,
                                }).then(function(){
                                    location.reload();
                                });
                            }
                            else{
                                swal ( "Oops" ,  "Something went wrong!" ,  "error" );
                            }
                        }
                    }            
                });
            }
        });  
    })
</script>