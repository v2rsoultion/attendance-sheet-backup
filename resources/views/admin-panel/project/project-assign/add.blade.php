@extends('admin-panel.layout.header')

@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="{!! $redirect_url !!}">{!! trans('language.view_project') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box white-box-modify">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="input-group">
                            <label class="p-r-20" for="{!! trans('language.project_name') !!}">{!! trans('language.project_name') .':  '!!}</label>{!! isset($project['project_name']) ? $project['project_name'] : '----' !!}
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="input-group">
                            <label class="p-r-20" for="{!! trans('language.project_owner') !!}">{!! trans('language.project_owner') .':  '!!} </label>{!! isset($project['project_owner']) ? $developer_list[$project['project_owner']] : '----' !!}
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="input-group">
                            <label class="p-r-20" for="{!! trans('language.project_manager') !!}">{!! trans('language.project_manager') .':  '!!} </label> {!! isset($project['project_manager']) ? $developer_list[$project['project_manager']] : '----' !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="input-group">
                            <label class="p-r-20" for="{!! trans('language.project_status') !!}">{!! trans('language.project_status') .':  '!!}</label>{!! isset($project['status']) ? $project_status[$project['status']] : '----' !!}
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="input-group">
                            <label class="p-r-20" for="{!! trans('language.project_start_date') !!}">{!! trans('language.project_start_date') .':  '!!} </label>{!! isset($project['project_start_date']) ? $project['project_start_date'] : '----' !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'project-assign-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                        @include('admin-panel.project.project-assign._form',['submit_button' => $submit_button])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

