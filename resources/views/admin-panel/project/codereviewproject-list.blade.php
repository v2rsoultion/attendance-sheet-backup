@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="#">{!! trans('language.codereview'); !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table dataTable " id="department-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.project_name') !!}</th>
                                    <th>{!! trans('language.project_platform') !!}</th>
                                    <th>{!! trans('language.project_status') !!}</th>
                                    <th>Coder</th>
                                    <th>Code Review Day</th>
                                    <th>{!! trans('language.project_code_review') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->        
    </div>
    <!-- /.container-fluid -->
</div>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();   
        var table = $('#department-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/my-project/codereview-projectdata')}}',
                data: function (d) { 
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'project_name', name: 'project_name'},
                {data: 'platform_name', name: 'platform_name'},
                {data: 'project_status', name: 'project_status'},
                {data: 'coder', name: 'coder'},
                {data: 'code_review_day', name: 'code_review_day'},
                {data: 'add_view_code_review', name: 'add_view_code_review'},
            ],
            columnDefs: [
                {
                    "targets": 0,
                    "width": "7%"
                },
                {
                    "targets": 1,
                    "width": "30%",
                },
                {
                    "targets": 6, // your case first column
                    "width": "15%"
                },
                {
                    targets: [6],
                    className: 'textAlig'
                }
            ]
        });
    });
</script>
@endsection






