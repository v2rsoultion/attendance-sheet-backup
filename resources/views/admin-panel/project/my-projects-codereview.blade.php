@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="{!! URL::to('admin-panel/my-project/project-list') !!}">{!! trans('language.view_my_project'); !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->    
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title">{!! trans('language.code_review_project_name') !!}:&nbsp;{!! isset($page_sub_title)? $page_sub_title : '-------' !!}</h3>
                    <div class="table-responsive">
                        <table class="table dataTable " id="project-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.code_review_feedback') !!}</th>
                                    <th>{!! trans('language.code_review_point') !!}</th>
                                    <th>{!! trans('language.code_review_date') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->        
    </div>
    <!-- /.container-fluid -->
</div>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();   
        var table = $('#project-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/my-project/myproject-review-data')}}',
                data: function (d) { 
                    d.project_id = "{!! isset($project_id) ? $project_id : 0 !!}";
                    d.platform_id = "{!! isset($platform_id) ? $platform_id : 0 !!}";
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'code_review_feedback', name: 'code_review_feedback'},
                {data: 'code_review_point', name: 'code_review_point'},
                {data: 'created_at', name: 'created_at'},
            ], columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "7%"
                },
                {
                    "targets": 2,
                    "width": "15%",
                },
                {
                    "targets": 3,
                    "width": "15%"
                }
            ]
        });
    });
</script>
@endsection






