@extends('admin-panel.layout.header')
@section('content')
<style>
    td.details-control {
        background: url(" {{ URL::asset('public/assets/images/details_open.png') }} ") no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url(" {{ URL::asset('public/assets/images/details_close.png') }} ") no-repeat center center;
    }
    .dt-buttons{
        float:right !important;
    }
    .excelButton{
        margin-right:10px;
    }
    .statusNotOkey{
        color:#fd0f0f !important;
    }
    .statusOkeyButClosed{
        color:#41b3f9 !important;
    }
    .statusOkey{
        color:#7ace4c !important;
    }
    .alignCenterMiddel{
        text-align: center !important; 
        vertical-align: middle !important; 
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="#">{!! trans('language.project'); !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table dataTable " id="project-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.project_name') !!}</th>
                                    <th>{!! trans('language.project_status') !!}</th>
                                    <th>{!! trans('language.project_code_review') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->        
    </div>
    <!-- /.container-fluid -->
</div>
<script>
    $(document).ready(function () {
        var table = $('#project-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/project/projectdata')}}',
                data: function (d) { 
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'project_name', name: 'project_name'},
                {data: 'status', name: 'status'},
                {
                    "class":          "details-control",
                    "orderable":      false,
                    "searchable":     false,
                    "data":           null,
                    "defaultContent": "",
                },
            ], columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "7%"
                },
                {
                    "targets": 3, // your case first column
                    "width": "15%"
                },
                {
                    targets: [3],
                    className: 'textAlig'
                }
            ],
        });

                // Array to track the ids of the details displayed rows
        var detailRows = [];
        $('#project-table').on('click','tr td.details-control', function () {
            var tr = $(this).closest('tr');
            var table = $('#project-table').DataTable();
            var row = table.row(tr);
            var idx = $.inArray(tr.attr('id'),detailRows);
            if (row.child.isShown()) {
                tr.removeClass('details');
                row.child.hide();
            }
            else {
                tr.addClass('details');
                row.child(format(row.data())).show();
            }
        });
        // On each draw, loop over the `detailRows` array and show any child rows
        $(document).on('click','.mylabel',function(e){
            var isUpdate = $(this).attr('updateDetail');
            if(isUpdate == 'update'){
                var button_id = $(this).attr('id');
                var id = button_id.split('_');

                $('body').addClass("loading");
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('admin-panel/attendance/update-attendance-data')}}",
                    type: 'GET',
                    data: {
                        'emp_id': id[0],
                        'attendance_data_id': $('#' + id[0] + '_ADI').val(),
                        'full_leaves': $('#' + id[0] + '_FL').val(),
                        'half_leaves': $('#' + id[0] + '_HL').val(),
                        'dhalf_leaves': $('#' + id[0] + '_DHL').val(),
                        'LD': $('#' + id[0] + '_LD').val(),
                        'EWD': $('#' + id[0] + '_EWD').val(),
                    },
                    success: function (data) {
                        $('body').removeClass("loading");
                        if(data['status'] == 0){
                            swal(data.message,'', "success");
                            $('#attendance-data-table').DataTable().ajax.reload( null, false );
                        }
                        else{
                            swal('Error',data.message);
                        }
                        $('#' + button_id).html('Edit');
                        $('#' + button_id).attr('updateDetail', 'edit');
                        $('#' + id[0] + '_FL').attr("readonly");
                        $('#' + id[0] + '_HL').attr("readonly");
                        $('#' + id[0] + '_DHL').attr("readonly");
                        $('#' + id[0] + '_LD').attr("readonly");
                        $('#' + id[0] + '_EWD').attr("readonly");
                        
                    }
                });
            }
            else{
                var button_id = $(this).attr('id');
                var id = button_id.split('_');
                $('#' + button_id).html('Update');
                $('#' + button_id).attr('updateDetail', 'update');
                $('#' + id[0] + '_FL').removeAttr("readonly");
                $('#' + id[0] + '_HL').removeAttr("readonly");
                $('#' + id[0] + '_DHL').removeAttr("readonly");
                $('#' + id[0] + '_LD').removeAttr("readonly");
                $('#' + id[0] + '_EWD').removeAttr("readonly");
            }
        });
        function format (d) {
            var reviews_list = d.review_log;
            console.log(reviews_list);
            $my_var =      
            '<table class="table table-bordered" style="background:#f7f7f7;">' +
                '<tbody>' +
                    '<tr>' +
                        '<th>Review Date</th>' +
                        '<th>Code Reviewer</th>' +
                        '<th>Review Check</th>' +
                        '<th>Description</th>' +
                        '<th>Status</th>' +
                    '</tr>';
            var i, j;
            $.each(reviews_list, function(index, value) {
                var counter = 0;
                for (i = 0; i < value.length; i++) {
                    var className = '';
                    if(value[i].review_status_id == 1){
                        className = 'statusOkey';
                    }else if(value[i].review_status_id == 2){
                        className = 'statusNotOkey';
                    }else if(value[i].review_status_id == 3){
                        className = 'statusOkeyButClosed';
                    }           
                    $my_var += 
                        '<tr>' ;
                        if(counter == 0){
                            $my_var += 
                            '<td class="alignCenterMiddel" rowspan='+value.length+'>' + value[i].review_date + '</td>' +
                            '<td class="alignCenterMiddel" rowspan='+value.length+'>' + value[i].code_reviewer + '</td>';
                        }
                        $my_var += 
                            '<td>' + value[i].review_check + '</td>' +
                            '<td>' + value[i].comments + '</td>' +
                            '<td class="'+ className +'">' + value[i].review_status + '</td>' +
                        '</tr>';
                    counter++;
                }
            });
            $my_var += '</tbody>' + 
            '</table>';
            return $my_var;
        }
    });
</script>
@endsection






