@if($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{$errors->first()}}
    </div>
@endif
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="input-group">
            <label for="{!! trans('language.code_review_project_name') !!}">{!! trans('language.code_review_project_name') .':  '!!} {!! isset($project[0]['project_name']) ? $project[0]['project_name'] : '----' !!}</label>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="input-group">
            <label for="{!! trans('language.frequently_code_review') !!}">{!! trans('language.frequently_code_review') .':  '!!} {!! isset($project[0]['frequently_code_review']) ?  $frequently_code_review_arr[$project[0]['frequently_code_review']] : '----' !!}</label>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="input-group">
            <label for="{!! trans('language.code_review_project_platform') !!}">Date: {!! date('Y-m-d') !!}</label>
        </div>
    </div>
    {{-- <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="input-group">
            <label for="{!! trans('language.code_review_status') !!}">{!! trans('language.code_review_status') .':  '!!} @if(isset($code_review_status) && $code_review_status == 0) <span class="text-success"> On time </span> @elseif(isset($code_review_status) && $code_review_status == 1) <span class="text-danger"> Delay</span> @elseif(isset($code_review_status) && $code_review_status == 2) <span class="text-success"> Completed </span> @else <span class="text-default"> ----- </span> @endif</label>
        </div>
    </div> --}}
    @foreach ($code_review as $key => $value)
        @if($value['status'] == 1 && !empty($code_review_details) && $code_review_details[$value['coder_id']]['get_code_review_details'] != '')        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h5 style="font-weight:500">{!! $employee_list[$value['coder_id']] !!}</h5>
            {!! Form::hidden('map['.$value['code_review_id'].'][coder_id]', $value['coder_id']) !!}
            {!! Form::hidden('map['.$value['code_review_id'].'][code_review_id]', $value['code_review_id']) !!}
            <hr class="m-t-0 m-b-20">
            @php $check_points = explode(',', $value['check_points']); @endphp
            @foreach ($check_points as $check_point)
                <!--/row-->
                <div class="row">
                    <div class="col-md-3">
                        {!! Form::hidden('map['.$value['code_review_id'].'][details]['.$check_point.'][code_review_detail_id]', isset($code_review_details[$value['coder_id']]['get_code_review_details'][$check_point]['code_review_detail_id']) ? $code_review_details[$value['coder_id']]['get_code_review_details'][$check_point]['code_review_detail_id'] : '') !!}
                        <h5 class="pull-right">{!! $review_check_list[$check_point] !!}</h5>
                        {!! Form::hidden('map['.$value['code_review_id'].'][details]['.$check_point.'][review_check_id]', $check_point) !!}
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">{!! trans('language.review_comments') !!}</label>
                            <div class="col-md-9">
                                {!! Form::textarea('map['.$value['code_review_id'].'][details]['.$check_point.'][comment]', isset($code_review_details[$value['coder_id']]['get_code_review_details'][$check_point]['code_review_detail_id']) ? $code_review_details[$value['coder_id']]['get_code_review_details'][$check_point]['comments'] : '', ['class' => 'form-control', 'rows' => '3', 'readonly' => $form_submitted]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">{!! trans('language.review_check_status') !!}</label>
                            <div class="col-md-9">
                                {!!Form::select('map['.$value['code_review_id'].'][details]['.$check_point.'][review_status]', $code_review_status_arr, isset($code_review_details[$value['coder_id']]['get_code_review_details'][$check_point]['code_review_detail_id']) ? $code_review_details[$value['coder_id']]['get_code_review_details'][$check_point]['review_status'] : '',['required', 'class' => 'form-control', 'disabled' => $form_submitted]) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->
            @endforeach
        </div>
        @endif
    @endforeach
    @if(!$form_submitted)
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class=" col-sm-9">
                <button type="submit" name="submit" value="save" class="btn btn-info waves-effect waves-light m-t-10 no-radius">{!! $submit_button !!}</button>
                <button type="submit" name="submit" value="submit" class="btn btn-info waves-effect waves-light m-t-10 no-radius">Save &amp; Submit</button>
            </div>
        </div>
    @endif
{{-- @endif --}}
<div class="clearfix"></div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#code-review-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                code_review_feedback: {
                    required: true,
                },
                code_review_point: {
                    required: true,
                }
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        }); 
    });
</script>