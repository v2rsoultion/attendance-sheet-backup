@extends('admin-panel.layout.header')

@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="{!! URL::to('admin-panel/codereview-projects/view-codereview') !!}">{!! trans('language.codereview') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12" style="@if(isset($form_flag) && $form_flag == true) display:block; @else display:none; @endif">
                <div class="white-box white-box-modify">
                    {!! Form::open(['files'=>TRUE,'id' => 'code-review-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                        @include('admin-panel.project.codereview._form',['submit_button' => $submit_button])
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table dataTable " id="code-review-log-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.code_review_coder') !!}</th>
                                    <th>{!! trans('language.review_date') !!}</th>
                                    <th>{!! trans('language.review_duration') !!}</th>
                                    <th>{!! trans('language.code_review_status') !!}</th>
                                    <th>{!! trans('language.action') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();   
        var table = $('#code-review-log-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/codereview/data')}}',
                data: function (d) { 
                    d.project_id         = "{!! isset($project_id) ? $project_id : ''; !!}";
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'code_review_coder', name: 'code_review_coder'},
                {data: 'code_review_date', name: 'code_review_date'},
                {data: 'review_duration', name: 'review_duration'},
                {data: 'code_review_status', name: 'code_review_status'},
                {data: 'edit_code_review', name: 'edit_code_review'},
            ], 
            columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "7%"
                },
                {
                    "targets": 1,
                    "width": "30%"
                },
                {
                    "targets": 3,
                    "width": "15%"
                },
                {
                    targets: [4 ],
                    className: 'textAlig'
                }
            ]
        });
    });
</script>
@endsection

