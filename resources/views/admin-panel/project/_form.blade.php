{!! Form::hidden('project_id',old('project_id',isset($project['project_id']) ? $project['project_id'] : ''),['class' => 'gui-input', 'id' => 'project_id', 'readonly' => 'true']) !!}
@if($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{$errors->first()}}
    </div>
@endif
<div class="clearfix"></div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.project_name') !!}">{!! trans('language.project_name') !!} <span class="red-text">*</span></label>
        {!! Form::text('project_name', old('project_name',isset($project['project_name']) ? $project['project_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.project_name'), 'id' => 'project_name','for'=>trans('language.project_name')]) !!}
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.project_owner') !!}">{!! trans('language.project_owner') !!} <span class="red-text">*</span></label>
        <div class="clearfix"></div>
        {!!Form::select('project_owner', $owner_list, isset($project['project_owner']) ? $project['project_owner'] : '', ['class' => 'widthFull selectpicker', 'data-style' => 'form-control', 'id'=>'project_owner'])!!}
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.project_manager') !!}">{!! trans('language.project_manager') !!} <span class="red-text">*</span></label>
        <div class="clearfix"></div>
        {!!Form::select('project_manager', $manager_list, isset($project['project_manager']) ? $project['project_manager'] : '', ['class' => 'widthFull selectpicker', 'data-style' => 'form-control', 'id'=>'project_manager'])!!}
    </div>
</div>
<div class="clearfix"></div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.project_status') !!}">{!! trans('language.project_status') !!} <span class="red-text">*</span></label>
        <div class="clearfix"></div>
        {!!Form::select('project_status', Config::get('custom.project_status'), isset($project['status']) ? $project['status'] : '', ['class' => 'widthFull selectpicker', 'data-style' => 'form-control', 'id'=>'project_status'])!!}
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group" id="datepickerbox">
        <label for="{!! trans('language.project_start_date') !!}">{!! trans('language.project_start_date') !!} <span class="red-text">*</span></label>
        {!!Form::text('project_start_date', old('project_start_date', isset($project['project_start_date']) ? $project['project_start_date'] : '') ,['class' => 'form-control','id' => 'project_start_date', 'placeholder' => 'yyyy-mm-dd']) !!}            
    </div>    
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group" id="datepickerbox2">
        <label for="{!! trans('language.project_end_date') !!}">{!! trans('language.project_end_date') !!}</label>
        {!!Form::text('project_end_date', old('project_end_date', isset($project['project_end_date']) ? $project['project_end_date'] : '') ,['class' => 'form-control','id' => 'project_end_date', 'placeholder' => 'yyyy-mm-dd' ]) !!}            
    </div>    
</div>
<div class="clearfix"></div>
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.project_description') !!}">{!! trans('language.project_description') !!}</label>
        <br>
        {!! Form::textarea('project_description', old('project_description', isset($project['project_description']) ? $project['project_description']: ''), ['id' => 'project_description', 'rows' => 5, 'class' => 'form-control', 'style' => 'resize:none']) !!}
    </div> 
</div>
<div class="clearfix"></div>
<div class="col-sm-12 col-md-4">
    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10 no-radius">{!! $submit_button !!}</button>
</div>
<div class="clearfix"></div>
<script>    
    jQuery(document).ready(function () {
        $("#project-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                project_name: {
                    required: true,
                },
                project_owner: {
                    required: true,
                },
                project_status: {
                    required: true,
                },
                project_start_date: {
                    required: true,
                },
            },
            messages: { 
                project_name: "Please enter project name",
                project_owner: "Please enter project owner name",
                project_status: "Please select project status",
                project_start_date: "Please enter project start date",
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
             highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".input-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".input-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        }); 
    });
    jQuery('#project_start_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "yyyy-mm-dd",
        container:'#datepickerbox'
    });
    jQuery('#project_end_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "yyyy-mm-dd",
        container:'#datepickerbox2'
    });
</script>