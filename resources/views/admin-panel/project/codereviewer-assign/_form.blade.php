{!! Form::hidden('project_id',old('project_id',isset($project['project_id']) ? $project['project_id'] : ''),['class' => 'gui-input', 'id' => 'project_id', 'readonly' => 'true']) !!}
@if($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{$errors->first()}}
    </div>
@endif
<div class="clearfix"></div>
<div class="alert alert-danger alert-dismissible" id="checkBoxError" style="display:none;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    Please select at least one check point for reviewer.
</div>
<div class="clearfix"></div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.frequently_code_review') !!}">{!! trans('language.frequently_code_review') !!} <span class="red-text">*</span></label>
        {!! Form::select('frequently_code_review', $frequently_code_review, isset($code_review['frequently_code_review']) ? $code_review['frequently_code_review'] : '', ['class' => 'widthFull selectpicker', 'data-style' => 'form-control', 'id'=>'frequently_code_review']) !!}
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <div class="input-group" id="datepickerbox">
        <label for="{!! trans('language.review_start_date') !!}">{!! trans('language.review_start_date') !!} <span class="red-text">*</span></label>
        {!!Form::text('review_start_date', old('review_start_date', isset($code_review['review_start_date']) ? $code_review['review_start_date'] : '') ,['class' => 'form-control','id' => 'review_start_date', 'placeholder' => 'yyyy-mm-dd']) !!}            
    </div>    
</div>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.reviewer_comments') !!}">{!! trans('language.reviewer_comments') !!}</label>
        <br>
        {!! Form::textarea('reviewer_comments', old('reviewer_comments', isset($code_review['comments_for_reviewer']) ? $code_review['comments_for_reviewer']: ''), ['id' => 'reviewer_comments', 'rows' => 2, 'class' => 'form-control', 'style' => 'resize:none']) !!}
    </div> 
</div>
<div class="clearfix"></div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <table class="table table-hover">
        <thead>
            <tr>
                <th width="70" class="text-center">#</th>
                <th>Developer</th>
                <th>Code Reviewer</th>
            </tr>
        </thead>
        @if(!empty($project['getProjectAssign']))
        <tbody>
            @php 
                $project_counter = 1;
                $checkBoxCounter = 1;
            @endphp
            @foreach ($project['getProjectAssignActive'] as $project_assign)
                    <tr>
                        <td class="text-center">{!! $project_counter !!}</td>
                        <td>
                            {!! $employee_list[$project_assign['admin_id']] !!}
                            {!! Form::hidden('map['.$project_counter.'][coder]', $project_assign['admin_id']) !!}
                            {!! Form::hidden('map['.$project_counter.'][code_review_id]', isset($code_review['details'][$project_assign['admin_id']]) ?  $code_review['details'][$project_assign['admin_id']]['code_review_id'] : '' ) !!}
                        </td>
                        <td>
                            {!!Form::select('map['.$project_counter.'][code_reviewer]', $code_reviewer_list, isset($code_review['details'][$project_assign['admin_id']]) ? $code_review['details'][$project_assign['admin_id']]['code_reviewer_id'] : '', ['codeReviewerSelectBox' => 'collapse-'.$project_assign['project_assign_id'],'class' => 'selectpicker selectCodeReviewer', 'data-style' => 'form-control', 'id'=>'code_reviewer'])!!}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="3">
                            <div id="collapse-{!! $project_assign['project_assign_id'] !!}" rel="{!! $project_counter !!}" class="collapse-container"  style=" @if(isset($code_review['details'][$project_assign['admin_id']])) display:block; @else display:none; @endif">
                                @foreach ($review_check_list as $key => $value)
                                    <div class="col-md-3">
                                        <div class="checkbox checkbox-success">
                                            {!! Form::checkbox('map['.$project_counter.'][check_list]['.$value['review_check_id'].']', null, isset($code_review['details'][$project_assign['admin_id']]['check_points']) ?  in_array($value['review_check_id'], $code_review['details'][$project_assign['admin_id']]['check_points']) ? true : false : null, ['id' => 'isCheckPoint_'.$value['review_check_id'].'_'.$checkBoxCounter, 'class' => "checkBox", 'weekly' => $value['weekly_time'], 'fortnightly' => $value['fortnightly_time'], 'monthly' => $value['monthly_time']]) !!}
                                            <label for="isCheckPoint_{!! $value['review_check_id'].'_'.$checkBoxCounter !!}"> {!! $value['review_check'] !!} </label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="help-block pull-right">
                                    <small>Note: - Approximate time for this code review is <span id="review-take-time-{!! $project_counter !!}">0</span> mint.</small>
                                </span>
                            </div>
                        </td>
                    </tr>
                @php $checkBoxCounter++; @endphp
                @php $project_counter++; @endphp
            @endforeach
        </tbody>
        @endif
    </table>
</div>
<div class="clearfix"></div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-10">
    <div class="input-group">
        <button type="submit" name="submit" class="btn btn-info waves-effect submitFormButton waves-light m-t-10 no-radius p-l-30 p-r-30">{!! $submit_button !!}</button>
    </div>
</div>
<div class="clearfix"></div>
<script type="text/javascript">
    jQuery('#review_start_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "yyyy-mm-dd",
        container:'#datepickerbox'
    });
    jQuery(document).ready(function () {
        $(".select2").select2();
        $('.selectpicker').selectpicker();
        calculateReviewTime();
        $(document).on('change', '.selectCodeReviewer', function(){
            var id = $(this).attr('codeReviewerSelectBox');
            if($(this).val() != '') {
                $('#'+id).show();
            } else {
                $('#'+id).hide();
            }
        });
        $("#reviewer-assign-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                'frequently_code_review': 'required',
                'review_start_date': 'required',
                'reviewer_comments': 'required'
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Save');
                $( element ).parents( ".input-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Save');
                $( element ).parents( ".input-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                $(".submitFormButton").html('Save');
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        }); 
        $('#reviewer-assign-form').submit(function() {
            $('#checkBoxError').hide();
            var checkbox_containers = document.getElementsByClassName("collapse-container");
            var okay=false;
            for(var i=0,l=checkbox_containers.length;i<l;i++)
            {
                const ckbChecked = document.querySelectorAll("#"+checkbox_containers[i].id +" input[type=checkbox]:checked");
                if(ckbChecked.length == 0 && !$("#"+checkbox_containers[i].id).is(':hidden')){
                    okay=false;
                    break;
                }else{
                    okay=true;
                }                
            }
            if(!okay){
                $('#checkBoxError').show();
                return false;
            }else{
                $('#checkBoxError').hide();
                return true;
            }
        });
        $(document).on('click', '.checkBox', function(){
            calculateReviewTime();
        });

        $(document).on('change', '#frequently_code_review', function(){
            calculateReviewTime();
        });

        function calculateReviewTime()
        {
            var checkbox_containers = document.getElementsByClassName("collapse-container");
            var okay=false;
            var frequently_code_review = $('#frequently_code_review').val();
            for(var i=0,l=checkbox_containers.length;i<l;i++)
            {
                var ckbChecked = document.querySelectorAll("#"+checkbox_containers[i].id +" input[type=checkbox]:checked");
                var review_take_time = 0;
                if(ckbChecked.length > 0){
                    for(var j = 0; j < ckbChecked.length; j++) {
                        if(frequently_code_review == 1){
                            review_take_time += +$('#'+ckbChecked[j].id).attr('weekly');
                        } else if(frequently_code_review == 2){
                            review_take_time += +$('#'+ckbChecked[j].id).attr('fortnightly');
                        } else if(frequently_code_review == 3){
                            review_take_time += +$('#'+ckbChecked[j].id).attr('monthly');
                        }
                    }
                    var rel = $('#'+checkbox_containers[i].id).attr('rel');
                    $('#review-take-time-'+rel).html(review_take_time);
                } else{
                    var rel = $('#'+checkbox_containers[i].id).attr('rel');
                    $('#review-take-time-'+rel).html(0);
                }
            }
        }
    });
</script>