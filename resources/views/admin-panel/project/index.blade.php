@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="#">{!! trans('language.project'); !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    <div class="alert alert-success alert-dismissible" id="ChangeStatusMessage" style="display:none;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        Status Changed Successfully.
                    </div>
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!! Form::text('project_name', old('project_name',isset($project['project_name']) ? $project['project_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.project_name'), 'id' => 'project_name','for'=>trans('language.project_name')]) !!}
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                {!! Form::submit('Search', ['class' => 'btn btn-info no-radius m-r-10','name'=>'Search']) !!}
                                {!! Form::button('Clear', ['class' => 'btn btn-inverse no-radius','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="col-md-2 row pull-right">
                        <select id="sortingField" class="form-control input-sm m-b-10">
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                            <option value="2">Show All</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table dataTable " id="project-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.project_name') !!}</th>
                                    <th>{!! trans('language.project_assign') !!}</th>
                                    <th>{!! trans('language.codereviewer_assign') !!}</th>
                                    <th>{!! trans('language.action') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->        
    </div>
    <!-- /.container-fluid -->
</div>
<script>
    $(document).ready(function () {
        $('#sortingField').change(function(){
            var table = $('#project-table').DataTable();
            table.ajax.reload( null, false );
        });
        $('[data-toggle="tooltip"]').tooltip();   
        var table = $('#project-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/project/data')}}',
                data: function (d) { 
                    d.project_name = $('input[name=project_name]').val();
                    d.project_status = $('#sortingField').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'project_name', name: 'project_name'},
                {data: 'assign_project', name: 'assign_project'},
                {data: 'assign_codereviewer', name: 'assign_codereviewer'},
                {data: 'action', name: 'action'},
            ],
            columnDefs: [
                { "targets": [2,3,4], "orderable": false }
            ]
        });
        $('#search-form').on('submit', function(e) {
           table.draw();
           e.preventDefault();
        });
        $('#clearBtn').click(function(){
           location.reload();
        });
    });
    function changeStatus(id,status){
        var token = '{!!csrf_token()!!}';
        $('.preloader').css('display','inline');
        $.ajax(
        {
            url: "{{ url('admin-panel/project/project-status') }}",
            type: 'GET',
            data: {
                'project_id': id,
                'project_status': status,
            },
            success: function (res)
            {
                $('.preloader').css('display','none');
                if (res == "Success")
                {
                    var table = $('#project-table').DataTable();
                    table.ajax.reload( null, false );
                    $('#ChangeStatusMessage').fadeIn(function() {
                        setTimeout(function() {
                            $('#ChangeStatusMessage').fadeOut();
                        }, 2500);
                    });
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }
        });
    }
</script>
@endsection






