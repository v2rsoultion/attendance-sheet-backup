@extends('admin-panel.layout.header')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!! Form::select('release_plan_year', $yearOption , isset($year) ? $year: date('Y'), ['class' => 'form-control selectpicker pull-right', 'autocomplete' => 'off', 'id'=>'release_plan_year', 'data-live-search' => 'true'])!!}
                                </div>  
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!! Form::text('team_name', old('team_name',isset($team['team_name']) ? $team['team_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.team_name'), 'id' => 'team_name','for'=>trans('language.team_name')]) !!}
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                {!! Form::submit('Search', ['class' => 'btn btn-info no-radius m-r-10','name'=>'Search']) !!}
                                {!! Form::button('Clear', ['class' => 'btn btn-inverse no-radius','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive" >
                        <table class="table dataTable " id="releaseplan-data-table">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Employee ID</th>
                                    <th>Name</th>
                                    <th>Annual Leaves</th>
                                    <th>Release Plan</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- .model -->
        {!! Form::open(['files'=>TRUE,'id' => 'release-plan-form' , 'class'=>'form-horizontal']) !!}
        <div class="modal fade bs-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel1">Month Wise Release Plan</h4> 
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissible" id="success-block" style="display:none;">
                            </div>
                            <div class="alert alert-danger alert-dismissible" id="error-block" style="display:none;">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6 col-sm-12 paddingLeftNope pull-left">
                                <h4>Employee ID: <span id="emp_id"></span></h4>
                                <input type="hidden" value="" name="admin_id" id="admin_id" />
                                <input type="hidden" value="" name="admin_year" id="admin_year" />
                            </div>
                            <div class="col-md-6 col-sm-12 paddingRightNope pull-right">
                                <h4>Annual Leaves: <span id="annual_leaves"></span></h4>
                            </div>
                        </div>
                        <div class="clearfix" style="padding-bottom:10px;"></div>
                        <div id="ad_month">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-5 pull-right">
                            <button type="button" class="btn btn-default no-radius" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info no-radius" id="isUpdate" style="display:none;">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        {!! Form::close() !!}
        <!-- / .model -->
    </div>
    <!-- /.container-fluid -->
</div>
<script> 
    $(document).ready(function () {
        var table = $('#releaseplan-data-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/release-plan/show-release-plan-data')}}',
                data: function (d) { 
                    d.year      = $('#release_plan_year').val();
                    d.team_name      = $('input[name=team_name]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'emp_id', name: 'emp_id'},
                {data: 'admin_name', name: 'admin_name'},
                {data: 'total', name: 'total'},
                {data: 'show_month_wise', name: 'show_month_wise'},
            ],
            columnDefs: [
                { "targets": [3,4], "orderable": false }
            ]
        });
        $('#search-form').on('submit', function(e) {
           table.draw();
           e.preventDefault();
        });
        $('#clearBtn').click(function(){
           location.reload();
        })

        $('#release-plan-form').on('submit', function(e) { 
            var formData   = $(this).serialize();
            $('.preloader').css('display','inline');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type     : "POST",
                url      : "{{ $save_url }}",
                data     : formData,
                cache    : false,
                success  : function(data) {
                    $('.preloader').css('display','none');
                    if(data['status'] == 0){
                        $("#success-block").html(data['message'] + '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
                        $("#success-block").show();
                        $("#error-block").html("");
                        $("#error-block").hide();
                        $('#success-block').fadeIn(function() {
                            setTimeout(function() {
                                $('#success-block').fadeOut();
                            }, 2500);
                        });
                    } else {
                        $("#success-block").html("");
                        $("#success-block").hide();
                        $("#error-block").html(data['message'] + '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
                        $("#error-block").show();
                        $('#error-block').fadeIn(function() {
                            setTimeout(function() {
                                $('#error-block').fadeOut();
                            }, 2500);
                        });
                    }
                }
            })
            e.preventDefault();
            table.draw();

        });
        $(document).on('click','.mylabel',function(e){
            var year_id = $(this).attr('year-id');
            $('body').addClass("loading");
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{url('admin-panel/release-plan/get-monthwise-data')}}",
                type: 'GET',
                data: {
                    'year_id': year_id
                },
                success: function (data) {
                    $('body').removeClass("loading");
                    if(data != ''){
                        $('#emp_id').html(data['emp_id']);
                        $('#admin_id').val(data['admin_id']);
                        $('#admin_year').val(data['year']);
                        $('#ad_month').html(data['json']);
                        $('#annual_leaves').html(data['total']);
                        if(data['isUpdate'] == true){
                            $('#isUpdate').css('display','inline-block');
                        }
                        else{
                            $('#isUpdate').css('display','none');
                        }
                    }
                }
            });
            $('#exampleModal').modal('show');
        });
        $( "#release_plan_year" ).change(function(e) {
            table.draw();
        });

        $(document).on('keyup','.month',function(e){
            var team_id = $(this).attr('team-id');
            var total = 0;
            for(var i = 1; i <= 12; i++){
                if($('#team_'+team_id+'_'+i).val() == ''){
                    total = parseFloat(total) + parseFloat(0);
                }
                else{
                    total = parseFloat(total) + parseFloat($('#team_'+team_id+'_'+i).val());
                }
                console.log($('#team_'+team_id+'_'+i).val());
            }
            console.log(total);
            $('#annual_leaves').html(total);
        });
    });
</script>

@endsection






