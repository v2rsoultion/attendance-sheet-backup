@if($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{$errors->first()}}
    </div>
@endif
<div class="alert alert-danger alert-dismissible" id="ChangeStatusMessage" style="display:none;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    Release plan for this year is already created.
</div>
<!-- <h3 class="box-title m-b-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">Category</h3>
<p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13"> Enter new {!! trans('language.release_plan') !!} </p> -->
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="input-group">
        {!! Form::select('release_plan_year', $yearOption , isset($year) ? $year: '', ['class' => 'form-control selectpicker', 'autocomplete' => 'off', 'id'=>'release_plan_year', ($is_update == TRUE) ? 'readonly' : '', 'data-live-search' => 'true'])!!}
    </div>
</div>
<div class="clearfix"></div>
<div class="col-lg-12 col-md-12 col-sm-12" id="tableBlock" @if(isset($enc_year) && $enc_year != '') style="display: display;" @else style="display: none;" @endif>
    <table class="table table-striped table-bordered table-responsive" id="editable-datatable">
        <thead>
            <tr>
                <th>{!! trans('language.serial_no') !!}</th>
                <th>{!! trans('language.release_plan_emp_name') !!}</th>
                <th>Total</th>
                <th>Jan</th>
                <th>Feb</th>
                <th>Mar</th>
                <th>Apr</th>
                <th>May</th>
                <th>Jun</th>
                <th>July</th>
                <th>Aug</th>
                <th>Sep</th>
                <th>Oct</th>
                <th>Nov</th>
                <th>Dec</th>
            </tr>
        </thead>
    </table>
</div>
<div class="col-sm-9">
    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10 no-radius">{!! $submit_button !!}</button>
</div>
<div class="clearfix"></div>
<script>
    var table;
    jQuery(document).ready(function () {
        table = $('#editable-datatable').DataTable({
            // pageLength: 20,
            processing: true,
            serverSide: true,
            bPaginate: false,
            bLengthChange: false,
            paging:   false,
            ordering: false,
            info:     false,
            ajax: {
                url: '{{url('admin-panel/release-plan/release-plan-team-list')}}',
                data: function (d) { 
                    d.year = "{{$enc_year}}";
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'admin_name', name: 'admin_name'},
                {data: 'total', name: 'total'},
                {data: 'jan', name: 'jan'},
                {data: 'feb', name: 'feb'},
                {data: 'mar', name: 'mar'},
                {data: 'apr', name: 'apr'},
                {data: 'may', name: 'may'},
                {data: 'jun', name: 'jun'},
                {data: 'july', name: 'july'},
                {data: 'aug', name: 'aug'},
                {data: 'sep', name: 'sep'},
                {data: 'oct', name: 'oct'},
                {data: 'nov', name: 'nov'},
                {data: 'dec', name: 'dec'},
            ], columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "5%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "25%"
                },
                {
                    "targets": 2, // your case first column
                    "width": "30%"
                },
                {
                    targets: [ 0, 1, 2, 3],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
    });     
    $(document).ready(function() {
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
            event.preventDefault();
            return false;
            }
        });
    });
    $( "#release_plan_year" ).change(function(e) {
        var release_plan_year = $('#release_plan_year').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{url('admin-panel/release-plan/year-validate')}}",
            type: 'GET',
            data: {
                'year': release_plan_year
            },
            success: function (data) {
                if(data == 'valid'){
                    $('#tableBlock').css('display','block');
                    table.draw();
                }
                else{
                    $('#tableBlock').css('display','none');
                    $('#ChangeStatusMessage').fadeIn(function() {
                        setTimeout(function() {
                            $('#ChangeStatusMessage').fadeOut();
                        }, 2500);
                    });
                }
            }
        });
    });    
    jQuery(document).ready(function () {
        $("#release-plan-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            /* @validation rules 
             ------------------------------------------ */
            rules: {
                release_plan_year: {
                    required: true,
                },
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        }); 
    });
    $(document).on('keyup','.month',function(e){
        var team_id = $(this).attr('team-id');
        var total = 0;
        for(var i = 1; i <= 12; i++){
            if($('#map_'+team_id+'_'+i).val() == ''){
                total = parseInt(total) + parseInt(0);
            }
            else{
                total = parseInt(total) + parseInt($('#map_'+team_id+'_'+i).val());
            }
        }
        $('#map_'+team_id+'_total').val(total);
    });
</script>