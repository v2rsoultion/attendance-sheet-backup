<!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <!-- Logo -->
                <div class="top-left-part logo1">
                    <a class="logo" href="#">
                        <span class="hidden-xs">
                            <span style="color:black;"><b>V2R Solution </b></span>
                        </span>
                    </a>
                </div>
                <!-- /Logo -->
                <!-- Search input and Toggle icon -->
                <ul class="nav navbar-top-links navbar-left">
                    <li><a href="javascript:void(0)" class="open-close waves-effect waves-light"><i class="ti-menu"></i></a></li>
                    @php 
                        $admin_notif = get_loggedin_user_data();
                        $admin = App\Model\Team\Team::find($admin_notif['admin_id']);     
                    @endphp
                    <li class="dropdown markAsRead">
                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"> <i class="fas fa-bell"></i>
                            @if($admin->unreadNotifications->count())                           
                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            @endif
                        </a>
                        <ul class="dropdown-menu mailbox animated bounceInDown" style="max-height:300px;">
                            @if($admin->notifications->count())
                                @if($admin->unreadNotifications->count())
                                    <li>
                                        <div class="drop-title">You have {!! $admin->unreadNotifications->count() !!} new notifications</div>
                                    </li>
                                @endif
                                <li>
                                    <div class="message-center">
                                        @foreach( $admin->unreadNotifications  as $notification)
                                            <a href="#">
                                                <div class="user-img" style="float:right;"><span class="profile-status online pull-right"></span> </div>
                                                <div class="mail-contnet">
                                                    <h5>{!! $notification->data['data']['emp_name'] !!}</h5> <span class="mail-desc">{!! $notification->data['data']['message'] !!}</span> <span class="time"> {!! $notification->created_at->diffForHumans(); !!} </span> 
                                                </div>
                                            </a>
                                        @endforeach
                                        @foreach( $admin->readNotifications  as $notification)
                                            <a href="#">
                                                {{-- <div class="user-img"> <img src="../plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div> --}}
                                                <div class="mail-contnet">
                                                    <h5>{!! $notification->data['data']['emp_name'] !!}</h5> <span class="mail-desc">{!! $notification->data['data']['message'] !!}</span> <span class="time"> {!! $notification->created_at->diffForHumans(); !!} </span> </div>
                                            </a>
                                        @endforeach
                                        {{-- <a href="#">
                                            <div class="user-img"> <img src="../plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                            <div class="mail-contnet">
                                                <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                        </a> --}}
                                    </div>
                                </li>
                            @else
                            <li>
                                <a class="text-center" href="javascript:void(0);"> <strong>empty</strong></a>
                            </li>
                            @endif
                        </ul>
                        <!-- /.dropdown-messages -->
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="{!! URL::to('public/admin/plugins/images/default.jpg') !!}" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">{!! $login_info['admin_name'] !!}</b><span class="caret"></span> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="{!! URL::to('public/admin/plugins/images/default.jpg') !!}" alt="user" /></div>
                                    <div class="u-text">
                                        <h4>{!! $login_info['admin_name'] !!}</h4>
                                        <p class="text-muted">{!! $login_info['email'] !!}</p>
                                    </div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{!! URL::to('admin-panel/profile') !!}"><i class="ti-user"></i> My Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{!! URL::to('admin-panel/changepassword') !!}"><i class="ti-settings"></i> Change Password</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ url('/admin-panel/logout') }}" title="Logout"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> 
                </div>
                <ul class="nav" id="side-menu">
                        {{-- Dashboard For Employee & Admin --}}
                        <li style="padding-top:60px;"> <a href="{{ url('/admin-panel/dashboard') }}" class="waves-effect"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i> <span class="hide-menu"> Dashboard </a></li>
                    @if(isset($login_info['type']) && ($login_info['type'] == 1))
                        {{-- Menus For Admin --}}
                        <li><a href="{{ url('/admin-panel/configuartion/view-configuartion') }}" class="waves-effect @if(Request::segment(2) == 'configuartion')  active  @endif"><i  class="mdi mdi-settings fa-fw"></i> <span class="hide-menu"> Company Configuration</span></a> </li>
                        <li><a href="javascript:void(0)" class="waves-effect @if(Request::segment(2) == 'team' || Request::segment(2) == 'release-plan' || Request::segment(2) == 'attendance' || Request::segment(2) == 'attendance-register' || Request::segment(2) == 'leave' || Request::segment(2) == 'early-leave' || Request::segment(2) == 'attendanceregister') active  @endif"><i class="mdi mdi-account-multiple fa-fw"></i> <span class="hide-menu"> Employee<span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level  @if(Request::segment(2) == 'team' || Request::segment(2) == 'release-plan' || Request::segment(2) == 'attendance' || Request::segment(2) == 'attendance-register' || Request::segment(2) == 'attendanceregister')  in  @endif">
                                <li><a href="javascript:void(0)" class="waves-effect @if(Request::segment('2') == 'team') active @endif"><i class="mdi mdi-account-circle fa-fw"></i><span class="hide-menu"> Manage Employee</span><span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level @if(Request::segment(3) == 'view-resign-team')  in  @endif"">
                                        <li> <a href="{{ url('/admin-panel/team/add-team') }}"><i class="mdi mdi-account-plus fa-fw"></i><span class="hide-menu">Add Employee</span></a></li>
                                        <li> <a href="{{ url('/admin-panel/team/view-team') }}" class="waves-effect @if(Request::segment('3') == 'view-resign-team') active @endif"><i class="mdi mdi-account-edit fa-fw"></i><span class="hide-menu">View Employee</span></a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ url('/admin-panel/release-plan/view-release-plan') }}" class="waves-effect @if(Request::segment(2) == 'release-plan')  active  @endif"><i class="fa-fw">R</i><span class="hide-menu"> Release Plan</span></a></li>
                                <li><a href="javascript:void(0)" class="waves-effect @if(Request::segment('2') == 'attendance' || Request::segment(2) == 'attendance-register' || Request::segment(2) == 'attendanceregister') active @endif"><i class="mdi mdi-calendar-range fa-fw"></i><span class="hide-menu"> Attendance</span><span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level @if(Request::segment(2) == 'attendance' || Request::segment(2) == 'attendance-register')  in  @endif">
                                        <li> <a href="{{ url('/admin-panel/attendance/add-attendance') }}"><i class="mdi mdi-calendar-plus fa-fw"></i><span class="hide-menu"> Upload Attendance</span></a></li>
                                        <li> <a href="{{ url('/admin-panel/attendance/view-attendance') }}"  class="waves-effect @if(Request::segment('3') == 'show-attendance') active @endif"><i class="mdi mdi-calendar-text fa-fw"></i><span class="hide-menu"> View Attendance</span></a></li>
                                        <li> <a href="{{ url('/admin-panel/attendanceregister/view-in-out') }}"  class="waves-effect @if(Request::segment('3') == 'view-in-out') active @endif"><i class="mdi mdi-calendar-check fa-fw"></i><span class="hide-menu"> View In Out</span></a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ url('/admin-panel/leave/view-leave') }}" class="waves-effect @if(Request::segment(2) == 'leave')  active  @endif"><i class="mdi mdi-clipboard-check fa-fw"></i><span class="hide-menu"> Manage Leave</span></a></li>
                                <li><a href="{{ url('/admin-panel/early-leave/view-early-leave') }}" class="waves-effect @if(Request::segment(2) == 'early-leave')  active  @endif"><i class="mdi mdi-clipboard-arrow-left fa-fw"></i><span class="hide-menu"> Manage Early Leave</span></a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0)" class="waves-effect @if(Request::segment(2) == 'project') active  @endif"><i class="mdi mdi-cube fa-fw"></i> <span class="hide-menu"> Project<span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level  @if(Request::segment(2) == 'project' || Request::segment(2) == 'work-type')  in  @endif">
                                <li><a href="javascript:void(0)" class="waves-effect @if(Request::segment('2') == 'project') active @endif"><i class="mdi mdi-cube-outline fa-fw"></i><span class="hide-menu"> Manage Project</span><span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level @if(Request::segment(3) == 'assign-project') in @endif">
                                        <li> <a href="{{ url('/admin-panel/project/add-project') }}"><i class="ti-layout-media-left-alt fa-fw"></i><span class="hide-menu">Add Project</span></a></li>
                                        <li> <a href="{{ url('/admin-panel/project/view-project') }}" class="waves-effect @if(Request::segment('3') == 'assign-project') active @endif"><i class="ti-layout-media-center-alt fa-fw"></i><span class="hide-menu">View Project</span></a></li>
                                    </ul>
                                </li>
                                <li> <a href="{{ url('/admin-panel/report/code-review-report') }}"  class="waves-effect"><i class="mdi mdi-file-document fa-fw"></i><span class="hide-menu"> Code Review</span></a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0)" class="waves-effect @if(Request::segment(2) == 'work-type' || Request::segment(2) == 'userrole') active @endif"><i class="mdi mdi-crown fa-fw"></i> <span class="hide-menu"> Roles & Responsibility<span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level @if(Request::segment(2) == 'work-type' || Request::segment(2) == 'userrole')  in  @endif">
                                <li> <a href="{{ url('/admin-panel/userrole/view-userrole') }}" class="waves-effect @if(Request::segment('3') == 'assign-responsibility') active @endif"><i class="icon-people fa-fw"></i><span class="hide-menu"> Manage Roles</span></a></li>
                                <li> <a href="{{ url('/admin-panel/work-type/view-work-type') }}"  class="waves-effect"><i class="icon-user-follow fa-fw"></i><span class="hide-menu"> Manage Responsibility</span></a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0)" class="waves-effect @if(Request::segment(2) == 'report') active @endif"><i class="mdi mdi-chart-areaspline fa-fw"></i> <span class="hide-menu"> Reports<span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">
                                <li> <a href="{{ url('/admin-panel/report/view-employee') }}" class="waves-effect"><i class="mdi mdi-file-document fa-fw"></i><span class="hide-menu"> Employee Wise</span></a></li>
                                <li> <a href="{{ url('/admin-panel/report/view-project') }}"  class="waves-effect"><i class="mdi mdi-file-document fa-fw"></i><span class="hide-menu"> Project Wise</span></a></li>
                                <li> <a href="{{ url('/admin-panel/report/view-summery') }}"  class="waves-effect"><i class="mdi mdi-file-document fa-fw"></i><span class="hide-menu"> Projects Summery</span></a></li>
                            </ul>
                        </li>
                    @elseif(isset($login_info['type']) && ($login_info['type'] == 2))
                        {{-- Menus For Employee --}}
                        {{-- View Attendance --}}
                        <li><a href="{{ url('/admin-panel/my-attendance/view-attendance') }}" class="waves-effect @if(Request::segment(2) == 'my-attendance')  active  @endif"><i  class="mdi mdi-calendar-check fa-fw"></i> <span class="hide-menu"> Attendance</span></a></li>
                        {{-- Project Related Menu --}}                
                        <li><a href="javascript:void(0)" class="waves-effect @if(Request::segment(2) == 'project-work-time' || Request::segment(2) == 'codereview-projects' || Request::segment(2) == 'project') active  @endif"><i class="mdi mdi-cube fa-fw"></i> <span class="hide-menu"> Project<span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level  @if(Request::segment(2) == 'project-work-time')  in  @endif">
                                {{-- Project Work Time --}}
                                <li><a href="javascript:void(0)" class="waves-effect @if(Request::segment(2) == 'project-work-time') active  @endif"><i class="mdi mdi-book fa-fw"></i> <span class="hide-menu"> Work Time<span class="fa arrow"></span></span></a>
                                    <ul class="nav nav-third-level  @if(Request::segment(2) == 'project-work-time')  in  @endif">
                                        <li> <a href="{{ url('/admin-panel/project-work-time/add-project-work-time') }}"><i class="mdi mdi-book-plus fa-fw"></i><span class="hide-menu">Add Work Time</span></a></li>
                                        <li> <a href="{{ url('/admin-panel/project-work-time/view-project-work-time') }}" class="waves-effect"><i class="mdi mdi-book-open-page-variant fa-fw"></i><span class="hide-menu">View Work Time</span></a></li>
                                    </ul>
                                </li>
                                @if(isset($login_info['code_reviewer']) && ($login_info['code_reviewer'] == 1))
                                {{-- Code review Projects --}}
                                <li>
                                    <a href="{{ url('/admin-panel/codereview-projects/view-codereview') }}" class="waves-effect @if(Request::segment(3) == 'codereview-project-list')  active  @endif"><i class="mdi mdi-eye fa-fw" data-icon="v"></i> <span class="hide-menu">Manage Code Review</span></a>
                                </li>
                                @endif
                                {{-- Projects List --}}
                                <li>
                                    <a href="{{ url('/admin-panel/project/my-project') }}" class="waves-effect @if(Request::segment(3) == 'my-project')  active  @endif"><i class="mdi mdi-cube-outline fa-fw" data-icon="v"></i> <span class="hide-menu">Code Review</span></a>
                                </li>
                            </ul>
                        </li>
                        {{-- View In Out --}}
                        <li><a href="{{ url('/admin-panel/in-out/view-in-out') }}" class="waves-effect @if(Request::segment(2) == 'view-in-out')  active  @endif"><i  class="mdi mdi-calendar-multiple-check fa-fw"></i> <span class="hide-menu"> In Out View</span></a></li>
                        {{-- View My Leaves --}}
                        <li><a href="{{ url('/admin-panel/my-leave/view-my-leave') }}" class="waves-effect @if(Request::segment(2) == 'my-leave')  active  @endif"><i  class="mdi mdi-animation fa-fw"></i> <span class="hide-menu"> Manage Leave</span></a></li>
                    @endif
                </ul>
            </div>
        </div>