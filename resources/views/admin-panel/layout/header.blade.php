<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{!! URL::to('public/assets/images/favicon--32x32.png') !!}">
    <title>{!! $page_title !!} | {!!trans('language.project_title') !!}</title>
    <!-- Bootstrap Core CSS -->
    {!! Html::style('public/admin/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- Menu CSS -->
    {!! Html::style('public/admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') !!}
    <!-- toast CSS -->
    {!! Html::style('public/admin/plugins/bower_components/toast-master/css/jquery.toast.css') !!}
    <!-- morris CSS -->
    {!! Html::style('public/admin/plugins/bower_components/morrisjs/morris.css') !!}
    <!-- chartist CSS -->
    {!! Html::style('public/admin/plugins/bower_components/chartist-js/dist/chartist.min.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') !!}
    <!-- Calendar CSS -->
    {!! Html::style('public/admin/plugins/bower_components/calendar/dist/fullcalendar.css') !!}
    <!-- animation CSS -->
    {!! Html::style('public/admin/css/animate.css') !!}
    <!-- Custom CSS -->
    {!! Html::style('public/admin/css/style.css') !!}
    {!! Html::style('public/admin/css/custom.css') !!}
    <!-- color CSS -->
    {!! Html::style('public/admin/css/colors/default.css') !!}

    {!! Html::style('public/admin/plugins/bower_components/custom-select/dist/css/select2.min.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/switchery/dist/switchery.min.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/bootstrap-select/bootstrap-select.min.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/multiselect/css/multi-select.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/sweetalert/sweetalert.css') !!}

    {!! Html::style('public/admin/plugins/bower_components/datatables/media/css/dataTables.bootstrap.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/dropify/dist/css/dropify.min.css') !!}
    {!! Html::style('public/assets/css/bootstrap-timepicker.min.css') !!}
    {!! Html::style('public/admin/css/jquery-ui.css') !!}
    {!! Html::style('public/admin/css/bootstrap-datepicker.css') !!}
    {!! Html::script('public/admin/js/jquery_1.js') !!}
    {!! Html::script('public/admin/js/bootstrap-datepicker.js') !!}
    {!! Html::script('public/assets/js/bootstrap-timepicker.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/sweetalert/sweetalert.min.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js') !!}

    {{-- Including Particular Page Style Here --}}
    @stack('styles')
    <style>
        input[type=number]::-webkit-inner-spin-button, 
        input[type=number]::-webkit-outer-spin-button { 
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            margin: 0; 
        }

        /* When the body has the loading class, we turn
        the scrollbar off with overflow:hidden */
        body.loading .loaderModel {
            overflow: hidden;   
        }
        .monthReleaseTest tr th{
            color: white;
        }
        .monthReleaseTest tr td{
            color: white;
            padding-left:5px;
        }

        /* Anytime the body has the loading class, our
        loaderModel element will be visible */
        body.loading .loaderModel {
            display: block;
        }
       
        .paddingLeftNope{
            padding-left: 0px !important;
        }
        .paddingRightNope{
            padding-right: 0px !important;
            text-align:right;
        }
        .colorTD{
            background-color: #41b3f9;
            color: #ffffff;
            width: 130px; 
        }
        .td_box{
            width: 150px; 
            padding-top: 0px !important;
        }
        .td_input_box{
            width: 100%;
        }
        .loader img {
            height: 25px;
            width: 25px;
            margin: 0px;
            display: block;
            position:inherit;
            z-index: 9999999;
        }
     
        .disable-url{
            cursor: not-allowed;
            border-radius:10px !important;
            background:#41b3f9 !important;
            height:22px;
        }
        .disable-url:hover{
            color:#ffffff;
            opacity: 0.8 !important;
        }
        
        /* This classes for Add Project Work Time textarea and form footer  */
        .textareaFocus{
            height: 85px !important;
            resize:none;
        }
        .textareaFocus:focus { 
            outline: none !important;
            border-color: #719ECE;
            box-shadow: 0 0 10px #719ECE;
            height: 130px !important;
            position:relative;
        }
        .form-footer{
            position:fixed;
            bottom:7.5%; 
            right:0%;
            background-color:#fff; 
            width:100%;
            padding: 5px 30px !important;
        }
        body{
            height:auto !important;
        }
        .footer{
            position: fixed !important;
            z-index:999;
        }
        #reportViewBlock div{
            min-height: 20px;
        }
        .widthFull{
            width: 100% !important;
        }
        .jq-toast-wrap{
            right: 15px !important;
            top: 15px !important;
            left: auto !important;
            position: absolute;
        }
        textarea {
           resize: none;
        }
    </style>
</head>
<body class="fix-header">
@include('admin-panel.layout.sidebar')
@yield('content')
@extends('admin-panel.layout.footer')