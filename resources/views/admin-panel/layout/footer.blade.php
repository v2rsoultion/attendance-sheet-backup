    <!-- /.container-fluid -->
    <footer class="footer text-center"> {{ now()->year }} &copy; {!!trans('language.project_tagline') !!} </footer>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    </div>  
</div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    {!! Html::script('public/admin/plugins/bower_components/jquery/dist/jquery.min.js')!!}
    {!! Html::script('public/admin/plugins/bower_components/calendar/jquery-ui.min.js')!!}
    <!-- ============================================================== -->
    <!-- Bootstrap Core JavaScript -->
    {!! Html::script('public/admin/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- Menu Plugin JavaScript -->
    {!! Html::script('public/admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') !!}
    <!--slimscroll JavaScript -->
    {!! Html::script('public/admin/js/jquery.slimscroll.js') !!}
    <!--Wave Effects -->
    {!! Html::script('public/admin/js/waves.js') !!}
    <!-- chartist chart -->
    {!! Html::script('public/admin/plugins/bower_components/chartist-js/dist/chartist.min.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') !!}
    <!-- Sparkline chart JavaScript -->
    {{-- {!! Html::script('public/admin/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js') !!} --}}
    <!-- Custom Theme JavaScript -->
    {!! Html::script('public/admin/js/custom.min.js') !!}
    {!! Html::script('public/admin/js/jasny-bootstrap.js') !!}
    <!-- Toaster -->
    {!! Html::script('public/admin/plugins/bower_components/toast-master/js/jquery.toast.js') !!}
    {!! Html::script('public/admin/js/toastr.js') !!}

    <!--Style Switcher -->
    {!! Html::script('public/admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/custom-select/dist/js/select2.full.min.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/bootstrap-select/bootstrap-select.min.js') !!}
    {{-- {!! Html::script('public/admin/plugins/bower_components/inputmask/dist/min/jquery.inputmask.bundle.min.js') !!} --}}
    {{-- {!! Html::script('public/admin/js/mask.js') !!} --}}
    {!! Html::script('public/admin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') !!}
    {{-- {!! Html::script('public/admin/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') !!} --}}
    {!! Html::script('public/admin/plugins/bower_components/multiselect/js/jquery.multi-select.js') !!}

    {!! Html::script('public/admin/plugins/bower_components/datatables/datatables.min.js') !!}
    {{-- <script src="//cdn.rawgit.com/ashl1/datatables-rowsgroup/v1.0.0/dataTables.rowsGroup.js"></script> --}}
    {{-- {!! Html::script('public/admin/plugins/bower_components/datatables/datatables.min.js') !!} --}}
    {!! Html::script('public/admin/download/dataTables.buttons.min.js') !!}
    {!! Html::script('public/admin/download/buttons.flash.min.js') !!}
    {!! Html::script('public/admin/download/jszip.min.js') !!}
    {!! Html::script('public/admin/download/pdfmake.min.js') !!}
    {!! Html::script('public/admin/download/vfs_fonts.js') !!}
    {!! Html::script('public/admin/download/buttons.html5.min.js') !!}
    {!! Html::script('public/admin/download/buttons.print.min.js') !!}

    {!! Html::script('public/admin/plugins/bower_components/moment/moment.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/calendar/calendar-3.10/fullcalendar.min.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/calendar/dist/cal-init.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/calendar/dist/cal-init1.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') !!}
    {!! Html::script('public/admin/download/jquery.validate.min.js') !!}
    {!! Html::script('public/admin/download/additional-methods.min.js') !!}
    {{-- Including Particular Page Script Here --}}
    @stack('scripts')
    
    <script>
        $(document).ready(function(){
            @php
                $details = get_attendance_update();
            @endphp
            @if($details['isPending'] == true)
                $.toast({
                    heading: "Welcome to team portal",
                    text: "Please mark your attendance before logout.",
                    loaderBg: "#ff6849",
                    icon: "warning",
                    hideAfter: 5000,
                    stack: 6
                });
            @endif
            $('.markAsRead').click(function(){
                $.ajax(
                {
                    url: "{{ url('admin-panel/markAsRead') }}",
                    type: 'GET',
                    success: function (res)
                    {
                        if (res == true)
                        {
                            $('.notify').hide();
                        }
                    }
                });
            });
        });
    </script>
    
</body>
</html>