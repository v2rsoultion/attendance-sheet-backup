  <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> 
                </div>
                <!-- Sidebar Start -->
                <ul class="nav" id="side-menu">
                    <!--  Dashboard Menu -->
                    <li style="margin-top: 55px;">
                        <a href="{{ url('/admin-panel/dashboard') }}" class="waves-effect"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i> <span class="hide-menu">Dashboard</span></a>
                    </li>
                    @if(isset($login_info['type']) && ($login_info['type'] == 1))
                         <!-- Department Menu -->
                         {{-- <li>
                            <a href="#" class="waves-effect @if(Request::segment(2) == 'department')  active  @endif"><i class=" ti-harddrives fa-fw" data-icon="v"></i> <span class="hide-menu"> Department <span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="{{ url('/admin-panel/department/add-department') }}"><span class="hide-menu">Add Department</span></a> </li>
                                <li> <a href="{{ url('/admin-panel/department/view-department') }}"><span class="hide-menu">View Department</span></a> </li>
                            </ul>
                        </li> --}}
                        <!-- Team Member Menu -->
                        <!-- Configuration Menu -->
                        <li>
                            <a href="{{ url('/admin-panel/configuartion/view-configuartion') }}" class="waves-effect @if(Request::segment(2) == 'configuartion')  active  @endif"><i class="mdi mdi-settings fa-fw" data-icon="v"></i> <span class="hide-menu">Configuartion</span></a>
                        </li>
                        <li>
                            <a href="#" class="waves-effect @if(Request::segment('2') == 'team') active @endif"><i class="mdi mdi-apps fa-fw" data-icon="v"></i> <span class="hide-menu"> Employee <span class="fa arrow"></span> </span></a>
                            <ul class="nav nav-second-level collapse">
                                <li> <a href="{{ url('/admin-panel/team/add-team') }}"><span class="hide-menu">Add Employee</span></a> </li>
                                <li> <a href="{{ url('/admin-panel/team/view-team') }}"><span class="hide-menu">View Employees</span></a> </li>
                                
                            </ul>
                        </li>
                        <!-- Attendance Menu -->
                        <li>
                            <a href="#" class="waves-effect @if(Request::segment('2') == 'attendance') active @endif"><i class="fas fa-book " style="margin-right:15px;" data-icon="v"></i> <span class="hide-menu"> Attendance <span class="fa arrow"></span> </span></a>
                            <ul class="nav nav-second-level collapse">
                                <li> <a href="{{ url('/admin-panel/attendance/add-attendance') }}"><span class="hide-menu">Upload Attendance</span></a> </li>
                                <li> <a href="{{ url('/admin-panel/attendance/view-attendance') }}"><span class="hide-menu">View Attendance</span></a> </li>
                            </ul>
                        </li>
                        <!-- Project Menu -->
                        {{-- <li>
                            <a href="#" class="waves-effect @if(Request::segment(2) == 'project')  active  @endif"><i class="fas fa-table fa-fw" style="margin-right:15px;" data-icon="v"></i> <span class="hide-menu"> Project <span class="fa arrow"></span> </span></a>
                            <ul class="nav nav-second-level collapse">
                                <li> <a href="{{ url('/admin-panel/project/add-project') }}"><span class="hide-menu">Add Project</span></a> </li>
                                <li> <a href="{{ url('/admin-panel/project/view-project') }}"><span class="hide-menu">View Project</span></a> </li>
                            </ul>
                        </li> --}}
                        
                         <!-- Release Plan Menu -->
                         <li>
                            <a href="#" class="waves-effect @if(Request::segment(2) == 'release-plan')  active  @endif"><i class="fab fa-telegram-plane fa-fw" data-icon="v"></i> <span class="hide-menu"> Release Plan <span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li> <a href="{{ url('/admin-panel/release-plan/add-release-plan') }}"><span class="hide-menu">Add Release Plan</span></a> </li>
                                <li> <a href="{{ url('/admin-panel/release-plan/view-release-plan') }}" class="@if(Request::segment(3) == 'show-release-plan')  active  @endif"><span class="hide-menu">View Release Plan</span></a> </li>
                            </ul>
                        </li>
                    @endif
                    @if(isset($login_info['type']) && ($login_info['type'] == 2))
                         <!-- Project Listing Menu -->
                         <li>
                            <a href="{{ url('/admin-panel/my-project/project-list') }}" class="waves-effect @if(Request::segment(2) == 'my-project')  active  @endif"><i class=" ti-harddrives fa-fw" data-icon="v"></i> <span class="hide-menu">Project List</span></a>
                         </li>
                         @if(isset($login_info['code_reviewer']) && ($login_info['code_reviewer'] == 1))
                         <!-- Project Listing Menu -->
                         <li>
                            <a href="{{ url('/admin-panel/my-project/codereview-project-list') }}" class="waves-effect @if(Request::segment(3) == 'codereview-project-list')  active  @endif"><i class=" ti-harddrives fa-fw" data-icon="v"></i> <span class="hide-menu">Code Review Projects </span></a>
                         </li>
                         @endif
                         <li>
                            <a href="{{ url('/admin-panel/my-attendance/view-attendance') }}" class="waves-effect @if(Request::segment(2) == 'my-attendance')  active  @endif"><i class=" ti-harddrives fa-fw" data-icon="v"></i> <span class="hide-menu">Attendance Sheet</span></a>
                        </li>
                    @endif
                </ul>
                <!-- Sidebar end -->
            </div>
        </div>