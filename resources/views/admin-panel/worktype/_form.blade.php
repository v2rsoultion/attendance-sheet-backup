@if(isset($work_type['work_type_id']) && !empty($work_type['work_type_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('work_type_id',old('work_type_id',isset($work_type['work_type_id']) ? $work_type['work_type_id'] : ''),['class' => 'gui-input', 'id' => 'work_type_id', 'readonly' => 'true']) !!}
@if($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{$errors->first()}}
    </div>
@endif
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.responsibility_name') !!}">{!! trans('language.responsibility_name') !!} <span class="red-text">*</span></label>
        {!! Form::text('work_type_name', old('work_type_name',isset($work_type['work_type_name']) ? $work_type['work_type_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.responsibility_name'), 'id' => 'work_type_name','for'=>trans('language.work_type_name')]) !!}
    </div>
</div>
<div class="col-lg-4 col-md-8 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.responsibility_description') !!}">{!! trans('language.responsibility_description') !!} <span class="red-text">*</span></label>
        {!! Form::textarea('work_type_description', old('work_type_description',isset($work_type['work_type_description']) ? $work_type['work_type_description']: ''), ['class' => 'form-control','placeholder'=>trans('language.responsibility_description'), 'id' => 'work_type_description', 'rows' => '3' ,'for'=>trans('language.work_type_description')]) !!}
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-4 col-sm-12">
    <button type="submit" name="submit" class="btn btn-info btn-md waves-effect waves-light m-t-10 m-r-10 no-radius">{!! $submit_button !!}</button>
    <button type="reset" name="reset" id="reset" class="btn btn-inverse btn-md waves-effect waves-light m-t-10 no-radius">Clear</button>
</div>
<div class="clearfix"></div>
<script type="text/javascript">    
    jQuery(document).ready(function () {
        $("#work-type-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                work_type_name: {
                    required: true,
                },
                work_type_description: {
                    required: true,
                }
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Save');
                $( element ).parents( ".input-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Save');
                $( element ).parents( ".input-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                $(".submitFormButton").html('Save');
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        }); 
    });
    $(document).on('click', '#reset', function(e){
        window.location = "{!! URL::to('admin-panel/work-type/view-work-type') !!}";
    })
</script>