@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="#">{!! trans('language.project'); !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            {{-- <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!! Form::text('project_name', old('project_name',isset($project['project_name']) ? $project['project_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.project_name'), 'id' => 'project_name','for'=>trans('language.project_name')]) !!}
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-info no-radius','name'=>'Search']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-info no-radius','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div> --}}
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table dataTable " id="project-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.project_name') !!}</th>
                                    <th>{!! trans('language.project_platform') !!}</th>
                                    <th>{!! trans('language.project_status') !!}</th>
                                    <th>Reviews / Feedback</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->        
    </div>
    <!-- /.container-fluid -->
</div>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();   
        var table = $('#project-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/my-project/projectdata')}}',
                data: function (d) { 
                    // d.project_name = $('input[name=project_name]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'project_name', name: 'project_name'},
                {data: 'platform_name', name: 'platform_name'},
                {data: 'project_status', name: 'project_status'},
                {data: 'view_review', name: 'view_review'},
            ], columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "7%"
                },
                {
                    "targets": 4, // your case first column
                    "width": "15%"
                },
                {
                    targets: [4],
                    className: 'textAlig'
                }
            ]
        });
    });
</script>
@endsection






