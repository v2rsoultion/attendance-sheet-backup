@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="{!! URL::to('admin-panel/project/view-project') !!}">{!! trans('language.view_project') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title">Project: {!! isset($project) ? $project->project_name : '-----' !!}</h3>
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!!Form::select('platform_id',isset($platform) ? $platform : '','' , ['class' => 'form-control select2','id'=>'platform_id']) !!}
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!!Form::select('code_reviewer_id',isset($code_reviewer) ? $code_reviewer : '','' , ['class' => 'form-control select2','id'=>'code_reviewer_id']) !!}
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!!Form::select('coder_id',isset($coder) ? $coder : '','' , ['class' => 'form-control select2','id'=>'coder_id']) !!}
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-info no-radius','name'=>'Search']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-info no-radius','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div> 
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table dataTable " id="project-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.code_review_coder') !!}</th>
                                    <th>{!! trans('language.code_review_code_reviewer') !!}</th>
                                    <th>{!! trans('language.code_review_feedback') !!}</th>
                                    <th>{!! trans('language.code_review_point') !!}</th>
                                    <th>{!! trans('language.code_review_project_platform') !!}</th>
                                    <th>{!! trans('language.code_review_date') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class="modal fade bs-example-modal-lg" id="codeReviewMore" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myLargeModalLabel">Code Review Feedback / Remarks</h4> 
                    </div>
                    <div class="modal-body" id="CodeReviewFeedback">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info waves-effect text-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>      
    </div>
    <!-- /.container-fluid -->
</div>
<script>
    function readMore(code_review_id){
        $.ajax(
        {
            url: "{{ url('admin-panel/codereview/dataForCodeFeeback') }}",
            type: 'GET',
            data: {
                'code_review_id': code_review_id,
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    $("#CodeReviewFeedback").html(res);
                    $('#codeReviewMore').modal('toggle');
                } else
                {
                    $("#CodeReviewFeedback").html(res);
                    $('#codeReviewMore').modal('toggle');
                }
            }
        });
    }
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();   
        var table = $('#project-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/codereview/data')}}',
                data: function (d) { 
                    d.project_id        = "{{ isset($project_id) ? $project_id : ''}}";
                    d.platform_id       = $('select[name=platform_id]').val();
                    d.code_reviewer_id  = $('select[name=code_reviewer_id]').val();
                    d.coder_id          = $('select[name=coder_id]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'coder', name: 'coder'},
                {data: 'code_reviewer', name: 'code_reviewer'},
                {data: 'code_review_feedback', name: 'code_review_feedback'},
                {data: 'code_review_point', name: 'code_review_point'},
                {data: 'platform', name: 'platform'},                
                {data: 'review_date', name: 'review_date'},                
            ]
        });
        $('#search-form').on('submit', function(e) {
           table.draw();
           e.preventDefault();
        });
        $('#clearBtn').click(function(){
           location.reload();
        });
    });
</script>
@endsection






