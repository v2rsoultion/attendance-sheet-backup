{!! Form::hidden('project_id',old('project_id',isset($project['project_id']) ? $project['project_id'] : ''),['class' => 'gui-input', 'id' => 'project_id', 'readonly' => 'true']) !!}
@if($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{$errors->first()}}
    </div>
@endif

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="input-group">
            <label for="{!! trans('language.project_name') !!}">{!! trans('language.project_name') !!}</label>
            {!! Form::text('project_name', old('project_name',isset($project['project_name']) ? $project['project_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.project_name'), 'id' => 'project_name','for'=>trans('language.project_name')]) !!}
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="input-group">
            <label for="{!! trans('language.project_status') !!}">{!! trans('language.project_status') !!}</label>
            <div class="clearfix"></div>
            {!!Form::select('project_status', Config::get('custom.project_status'), isset($project['status']) ? $project['status'] : '', ['class' => 'form-control select2','id'=>'project_status'])!!}
        </div>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="input-group">
            <label for="">{!! trans('language.project_platform') !!}</label>
            <div class="checkbox checkbox-success pad-left-5">
                @foreach ($platforms as $platform)
                    <div class="col-lg-2 col-md-2">
                        <input id="project_platform_{{ $platform->platform_id }}" name="project_platform[]" value="{{ $platform->platform_id }}" @if(!empty($project->platform) && strpos($project->platform, (string)$platform->platform_id) !== false) checked @endif type="checkbox" onclick="add_module({{ $platform->platform_id }});" required />
                        <label for="project_platform_{{ $platform->platform_id }}" style="font-weight: 500;">{{ $platform->platform_name }}</label>
                    </div>
                @endforeach
            </div>
        </div>            
    </div>
</div>
@php $arr_project_details = isset($project_details) ? array_column($project_details,'platform_id'): []; @endphp 
@for ($i = 0; $i < count($platform_details); $i++)
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" @if(in_array($platform_details[$i]['id'], $arr_project_details)) style="display:block;" @else style="display:none;" @endif id={{'platform_box_'.$platform_details[$i]['id']}}>
        <h3 class="box-title form-box">{{ $platform_details[$i]['name']}}</h3>
        <hr class="form-box-hr">
        {!! Form::hidden('platform_'.$platform_details[$i]['id'].'[project_detail_id]',old('platform_'.$platform_details[$i]['id'].'[project_detail_id]', isset($project_details[$platform_details[$i]['id']]) ? $project_details[$platform_details[$i]['id']]['project_detail_id'] : ''),['class' => 'gui-input', 'id' => 'platform_'.$platform_details[$i]['id'].'[project_detail_id]', 'readonly' => 'true']) !!}
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.project_coder') !!}">{!! trans('language.project_coder') !!}</label>
                <div class="clearfix"></div>
                {!!Form::select('platform_'.$platform_details[$i]['id'].'[coder_id]', $platform_details[$i]['coder'], isset($project_details[$platform_details[$i]['id']])? $project_details[$platform_details[$i]['id']]['coder_id'] : '' ,['class' => 'form-control select2','id'=>'platform_'.$platform_details[$i]['id'].'[coder_id]' ] ) !!}
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.project_code_reviewer') !!}">{!! trans('language.project_code_reviewer') !!}</label>
                <div class="clearfix"></div>
                {!!Form::select('platform_'.$platform_details[$i]['id'].'[code_reviewer_id]',$platform_details[$i]['code_reviewer'], isset($project_details[$platform_details[$i]['id']])? $project_details[$platform_details[$i]['id']]['code_reviewer_id'] : '' ,['class' => 'form-control select2','id'=>'platform_'.$platform_details[$i]['id'].'[code_reviewer_id]' ] ) !!}
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.frequently_code_review') !!}">{!! trans('language.frequently_code_review') !!}</label>
                <div class="clearfix"></div>
                {!!Form::select('platform_'.$platform_details[$i]['id'].'[fcr_id]',$platform_details[$i]['frequently_code_review'], isset($project_details[$platform_details[$i]['id']])? $project_details[$platform_details[$i]['id']]['frequently_code_review'] : '' ,['class' => 'form-control select2','id'=>'platform_'.$platform_details[$i]['id'].'[coder_id]' ] ) !!}
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.code_review_day') !!}">{!! trans('language.code_review_day') !!}</label>
                <div class="clearfix"></div>
                {!!Form::select('platform_'.$platform_details[$i]['id'].'[code_review_day_id]',$platform_details[$i]['code_review_day'], isset($project_details[$platform_details[$i]['id']])? $project_details[$platform_details[$i]['id']]['code_review_day'] : '' ,['class' => 'form-control select2','id'=>'platform_'.$platform_details[$i]['id'].'[coder_id]' ] ) !!}
            </div>
        </div>
    </div>
@endfor
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="input-group">
            <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10 no-radius">{!! $submit_button !!}</button>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<script type="text/javascript">    
    function add_module(platform_id) {
            if(document.getElementById('project_platform_'+platform_id).checked == true)
            {
                document.getElementById('platform_box_'+platform_id).style.display = "block";
            }
            else if(document.getElementById('project_platform_'+platform_id).checked == false){
                document.getElementById('platform_box_'+platform_id).style.display = "none";
            }
    }
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Project name should be alphabetic.");
        
        $("#project-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                project_name: {
                    required: true,
                    lettersonly: true
                },
                project_status: {
                    required: true,
                },
                "project_platform[]":{
                    required:function(){
                        // alert('sfds');
                    }
                }
            },
            messages: { 
                "project_platform[]": "please Select One Checkbox",
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        }); 
    });
</script>