{!! Form::hidden('id', old('code_review_id',isset($codereview['code_review_id']) ? $codereview['code_review_id'] : ''),['class' => 'gui-input', 'id' => 'code_review_id', 'readonly' => 'true']) !!}
{!! Form::hidden('project_id', old('project_id',isset($project['project_id']) ? $project['project_id'] : ''),['class' => 'gui-input', 'id' => 'project_id', 'readonly' => 'true']) !!}
{!! Form::hidden('project_detail_id', old('project_detail_id',isset($project['project_detail_id']) ? $project['project_detail_id'] : ''),['class' => 'gui-input', 'id' => 'project_detail_id', 'readonly' => 'true']) !!}
{!! Form::hidden('coder_id', old('coder_id',isset($project['coder_id']) ? $project['coder_id'] : ''),['class' => 'gui-input', 'id' => 'coder_id', 'readonly' => 'true']) !!}
{!! Form::hidden('code_review_status', old('code_review_status',isset($project['code_review_status']) ? $project['code_review_status'] : ''),['class' => 'gui-input', 'id' => 'code_review_status', 'readonly' => 'true']) !!}
@if($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{$errors->first()}}
    </div>
@endif
@if($project['status'] == 1)
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-info alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            Project not stated yet.
        </div>
        {{-- <div class="alert alert-info"> Project not stated yet.</div> --}}
    </div>
@elseif($project['status'] == 3)
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-info alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            Project on hold.
        </div>
    </div>
@elseif($project['status'] == 4)
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-info alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            Project Completed.
        </div>
    </div>
@endif
<!-- <h3 class="box-title m-b-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">Category</h3>
<p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13"> Enter new {!! trans('language.department') !!} </p> -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="input-group">
            <label for="{!! trans('language.code_review_project_name') !!}">{!! trans('language.code_review_project_name') .':  '!!} {!! isset($project['project_name']) ? $project['project_name'] : '----' !!}</label>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="input-group">
            <label for="{!! trans('language.code_review_project_platform') !!}">{!! trans('language.code_review_project_platform') .':  '!!} {!! isset($project['project_name']) ? $project['platform_name'] : '----' !!}</label>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="input-group">
            <label for="{!! trans('language.code_review_project_platform') !!}">Date: {!! date('Y-m-d') !!}</label>
        </div>
    </div>
</div>
@if($project['status'] == 2)
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.code_review_status') !!}">{!! trans('language.code_review_status') .':  '!!} @if(isset($code_review_status) && $code_review_status == 0) <span class="text-success"> On time </span> @elseif(isset($code_review_status) && $code_review_status == 1) <span class="text-danger"> Delay</span> @elseif(isset($code_review_status) && $code_review_status == 2) <span class="text-success"> Completed </span> @else <span class="text-default"> ----- </span> @endif</label>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.code_review_coder') !!}">{!! trans('language.code_review_coder') .':  '!!} {!! isset($project['coder_name']) ? $project['coder_name'] : '----' !!}</label>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.code_review_feedback') !!}">{!! trans('language.code_review_feedback') !!}</label>
                {!! Form::textarea('code_review_feedback', old('code_review_feedback', isset($codereview['code_review_feedback']) ? $codereview['code_review_feedback']: ''), ['id' => 'code_review_feedback', 'rows' => 5, 'style' => 'resize:none']) !!}
            </div> 
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.code_review_point') !!}">{!! trans('language.code_review_point') !!}</label>
                <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                    <span class="input-group-btn input-group-prepend"></span>
                        <input class="form-control tch" placeholder="{!! trans('language.code_review_point') !!}" id="code_review_point" for="{!! trans('language.code_review_point') !!}" data-bts-button-down-class="btn btn-default btn-outline" data-bts-button-up-class="btn btn-default btn-outline" name="code_review_point" type="text" value="{!! isset($codereview['code_review_point']) ? $codereview['code_review_point']: '' !!}">
                    <span class="input-group-btn input-group-append"></span>
                </div>
            </div>
            <span>(Note: Points between 1 to 10.)</span>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class=" col-sm-9">
            <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10 no-radius">{!! $submit_button !!}</button>
        </div>
    </div>
@endif
{!! Html::script('public/admin/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') !!}
<div class="clearfix"></div>
<script>
    $("input[name='code_review_point']").TouchSpin({
        min: 0,
        max: 10,
    });    
</script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#code-review-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                code_review_feedback: {
                    required: true,
                },
                code_review_point: {
                    required: true,
                }
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        }); 
    });
</script>