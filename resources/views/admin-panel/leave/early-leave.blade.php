@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="#">{!! trans('language.team'); !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    <div class="alert alert-success alert-dismissible" id="ApproveLeaveMessage" style="display:none;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span id="ApproveLeaveMessageText"></span>
                    </div>
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="col-lg-3 col-md-3">
                            <div class="input-group">
                                {!!Form::select('employee_name', $employee_list, '', ['class' => 'form-control
                                select2','id'=>'employee_name'])!!}
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            {!! Form::submit('Search', ['class' => 'btn btn-info no-radius m-r-10','name'=>'Search']) !!}
                            {!! Form::button('Clear', ['class' => 'btn btn-inverse no-radius','name'=>'Clear', 'id' => "clearBtn"]) !!}
                        </div>
                    {!! Form::close() !!}
                        <div class="col-sm-12 col-md-2 pull-right">
                            {!! Form::button('Add Early Leave', ['class' => 'btn btn-info no-radius', 'data-target' => "#applyEarlyLeaveModal", 'data-toggle' => "modal", 'name'=>'add-early-leave']) !!}
                        </div>
                        <div class="col-sm-12 col-md-2 pull-right">
                            {!! Form::button('View Early Leve Status', ['class' => 'btn btn-inverse no-radius', 'data-toggle' => "modal", 'data-target' => ".bs-example-modal-lg", 'name'=>'view-early-leave']) !!}
                        </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="col-md-2 row pull-right">
                        {!!Form::select('sortingField', $leave_status, isset($condition) ? $condition : '' , ['class' => 'form-control input-sm m-b-10','id'=>'sortingField'])!!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table dataTable " id="early-leaves-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.team_name') !!}</th>
                                    <th>{!! trans('language.early_leave_time') !!}</th>
                                    <th>{!! trans('language.early_leave_details') !!}</th>
                                    <th>{!! trans('language.early_leave_status') !!}</th>
                                    <th>{!! trans('language.early_leave_apply_date') !!}</th>
                                    <th>{!! trans('language.action') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->        
    </div>
    <!-- /.container-fluid -->
@include('admin-panel.dashboard.early-leave-model');
    <!-- Early leave month count -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Ealry Leave Monthly ({!! date('F') !!}) Count</h4> </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table dataTable " id="early-leaves-count-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.team_name') !!}</th>
                                    <th>{!! trans('language.early_leave_count') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<script>
    $('#leave_start_time').timepicker({
    });
    $('#leave_end_time').timepicker({
    });
    $(document).ready(function () {
        $('#sortingField').change(function(){
            var table = $('#early-leaves-table').DataTable();
            table.ajax.reload( null, false );
        });
        var table = $('#early-leaves-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/early-leave/early-leave-data')}}',
                data: function (d) { 
                    d.employee = $('#employee_name').val();
                    d.leave_category = $('#leave_category').val();
                    d.condition = $('#sortingField').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'employee_name', name: 'employee_name'},
                {data: 'leave_time', name: 'leave_time'},
                {data: 'leave_details', name: 'leave_details'},
                {data: 'leave_status', name: 'leave_status'},
                {data: 'leave_apply_date', name: 'leave_apply_date'},
                {data: 'action', name: 'action'},
            ],
            columnDefs: [
                { "targets": [2,3,4,6], "orderable": false }
            ]
        });
        $('#search-form').on('submit', function(e) {
           table.draw();
           e.preventDefault();
        });
        $('#clearBtn').click(function(){
           location.reload();
        });
        var tableForCount = $('#early-leaves-count-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/early-leave/early-leave-count-data')}}',
                data: function (d) { 
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'admin_name', name: 'admin_name'},
                {data: 'leave_count', name: 'leave_count'},
            ]
        });
    });
    function changeStatus(id,status){
        var token = '{!!csrf_token()!!}';
        $.ajax(
        {
            url: "{{ url('admin-panel/early-leave/early-leave-change-status') }}",
            type: 'GET',
            data: {
                'early_leave_register_id': id,
                'leave_status': status,
            },
            success: function (res)
            {
                if (res.status == "success")
                {
                    var table = $('#early-leaves-table').DataTable();
                    table.ajax.reload( null, false );
                    $('#ApproveLeaveMessageText').html(res.message);
                    $('#ApproveLeaveMessage').fadeIn(function() {
                        setTimeout(function() {
                            $('#ApproveLeaveMessage').fadeOut();
                        }, 2500);
                    });
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
                else{

                }
            }
        });
    }
</script>
@endsection






