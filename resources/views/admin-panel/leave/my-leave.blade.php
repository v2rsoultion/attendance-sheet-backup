@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    <div class="alert alert-success alert-dismissible" id="ApproveLeaveMessage" style="display:none;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span id="ApproveLeaveMessageText"></span>
                    </div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="col-lg-3 col-md-3">
                            <div class="input-group">
                                {!!Form::select('leave_category', $leave_category, '', ['class' => 'form-control
                                select2','id'=>'leave_category'])!!}
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            {!! Form::submit('Search', ['class' => 'btn btn-info no-radius m-r-10','name'=>'Search']) !!}
                            {!! Form::button('Clear', ['class' => 'btn btn-inverse no-radius','name'=>'Clear', 'id' => "clearBtn"]) !!}
                        </div>
                    {!! Form::close() !!}
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="col-md-2 row pull-right">
                        {!!Form::select('sortingField', $leave_status, isset($condition) ? $condition : '' , ['class' => 'form-control input-sm m-b-10','id'=>'sortingField'])!!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table dataTable " id="leaves-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.leave_date') !!}</th>
                                    <th>{!! trans('language.leave_details') !!}</th>
                                    <th>{!! trans('language.leave_status') !!}</th>
                                    <th>{!! trans('language.leave_comment') !!}</th>
                                    <th>{!! trans('language.leave_apply_date') !!}</th>
                                    <th>{!! trans('language.action') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="applyLeaveModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="applyLeaveModalLabel" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Apply For Leave</h4> 
                        </div>
                        {!! Form::open(['files'=>TRUE,'id' => 'apply-leave-details' ,'autocomplete' => "off",  'class'=>'']) !!}
                            <input type="hidden" name="leave_register_id" value="" id="leave_register_id" />
                        <div class="modal-body">
                            <div class="alert alert-success alert-dismissible" id="leaveSuccess" style="display:none;">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span class="leaveSuccessMessage"></span>
                            </div>
                            <div class="alert alert-danger alert-dismissible" id="leaveError" style="display:none;">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span class="leaveErrorMessage"></span>
                            </div>
                            <div class="form-group">
                                <label for="leave_category" class="control-label">Leave Category:</label>
                                {!!Form::select('leave_category', $leave_category, '', ['class' => 'selectpicker form-control', 'data-style' => 'form-control', 'id'=>'leave_category'])!!}
                            </div>
                            <div class="form-group">
                                <label for="leave_date" class="control-label">Leave Date:</label>
                                <input class="form-control input-daterange-datepicker" type="text" name="leave_date" id="leave_date" value="{!! date('d/m/Y') !!}" />
                            </div>
                            <div class="form-group">
                                <label for="leave_reason" class="control-label">Leave Reason:</label>
                                <textarea class="form-control" name="leave_reason" id="leave_reason"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect no-radius" data-dismiss="modal">Close</button>
                            <button type="submit" name="submit" class="btn btn-info waves-effect waves-light leave-apply submitFormButton no-radius">Apply</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->        
    </div>
    <!-- /.container-fluid -->
</div>
<script>
    $(document).ready(function () {
        $('.input-daterange-datepicker').daterangepicker({
            locale: {
               format: 'YYYY/MM/DD'
            },
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-danger',
            cancelClass: 'btn-inverse',
        });
        $('#sortingField').change(function(){
            var table = $('#leaves-table').DataTable();
            table.ajax.reload( null, false );
        });
        var table = $('#leaves-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/my-leave/my-leave-data')}}',
                data: function (d) { 
                    d.leave_category = $('#leave_category').val();
                    d.condition = $('#sortingField').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'leave_date', name: 'leave_date'},
                {data: 'leave_details', name: 'leave_details'},
                {data: 'leave_status', name: 'leave_status'},
                {data: 'leave_comments', name: 'leave_comments'},
                {data: 'leave_apply_date', name: 'leave_apply_date'},
                {data: 'action', name: 'action'},
            ],
            columnDefs: [
                { "targets": [2,3,4,6], "orderable": false }
            ]
        });
        $('#search-form').on('submit', function(e) {
           table.draw();
           e.preventDefault();
        });
        $('#clearBtn').click(function(){
           location.reload();
        });
        $(document).on('click', '.deleteRequest', function(){
            var id = $(this).attr('leave-register-id');
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
                }).then(result => {
                    if (result.value) {
                        // handle Confirm button click
                        // result.value will contain `true` or the input value
                        $.ajax({
                            url: "{{ url('admin-panel/my-leave/delete')}}",
                            type: 'GET',
                            data: {
                                'id': id
                            },
                            success: function (data) {
                                if (data.status == 'success') { 
                                    swal({
                                        title: "Deleted",
                                        text: "Your record has been deleted.",
                                        type: "success"
                                    }).then(function () {
                                        window.location.reload();
                                    });
                                } else {
                                    swal("Error",
                                        "Unable to delete. Something went wrong!)",
                                        "error");
                                }
                            }
                        });
                    } else {
                        // handle dismissals
                        // result.dismiss can be 'cancel', 'overlay', 'esc' or 'timer'
                        swal("Cancelled", "You cancelled your action", "error");
                    }
                });
        });

        $(document).on('click', '.modifyRequest', function(){
            $('.preloader').css('display','inline');
            var id = $(this).attr('leave-register-id');
            $.ajax(
            {
                url: "{{ url('admin-panel/my-leave/get-leave-data') }}",
                type: 'GET',
                data: { 'id': id },
                success: function (res)
                {
                    $('.preloader').css('display','none');
                    if (res.status == "success") {
                        var data = res.data;
                        $('select[name=leave_category]').val(data.leave_category);
                        $('.selectpicker').selectpicker('refresh')
                        $('#leave_date').val(data.leave_date);
                        $('#leave_reason').val(data.leave_reason);
                        $('input[id=leave_register_id]').val(data.leave_register_id);
                        $('#applyLeaveModal').modal({
                            show: 'false'
                        });
                    }
                    else if(res.status == "error") {
                        swal("Error","Unable to find record. Something went wrong!)","error");
                    }
                }
            });
        });
    });
    jQuery(document).ready(function () {
        $('#applyLeaveModal').on('hidden.bs.modal', function () {
            location.reload();
        });
        $("#apply-leave-details").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                leave_category: {
                    required: true,
                },
                leave_date: {
                    required: true,
                }
            },
            message: {
                leave_date: {
                    required: "Please select date",
                },
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Apply');
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Apply');
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                $(".submitFormButton").html('Apply');
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            },
            submitHandler: function(form) {
                $.ajax({
                    url: "{!! $apply_leave_url !!}",
                    type: 'get',
                    data: $(form).serialize(),
                    success: function(response) {
                        if(response.status == 'success'){
                            $('.leaveSuccessMessage').html(response.message);
                            $('#leaveSuccess').fadeIn(function() {
                                setTimeout(function() {
                                    $('#leaveSuccess').fadeOut();
                                }, 2500);
                            });
                        }
                        else if(response.status == 'error'){
                            $('.leaveErrorMessage').html(response.message);
                            $('#leaveError').fadeIn(function() {
                                setTimeout(function() {
                                    $('#leaveError').fadeOut();
                                }, 2500);
                            });
                        }
                        else if(response.status == 'error-validate'){
                            $('.leaveErrorMessage').html(response.message);
                            $('#leaveError').fadeIn(function() {
                                setTimeout(function() {
                                    $('#leaveError').fadeOut();
                                }, 2500);
                            });
                        }
                    }            
                });
            }
        });
    });
</script>
@endsection






