@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="#">{!! trans('language.team'); !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    <div class="alert alert-success alert-dismissible" id="ApproveLeaveMessage" style="display:none;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span id="ApproveLeaveMessageText"></span>
                    </div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="col-lg-3 col-md-3">
                            <div class="input-group">
                                {!!Form::select('employee_name', $employee_list, '', ['class' => 'form-control
                                select2','id'=>'employee_name'])!!}
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="input-group">
                                {!!Form::select('leave_category', $leave_category, '', ['class' => 'form-control
                                select2','id'=>'leave_category'])!!}
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            {!! Form::submit('Search', ['class' => 'btn btn-info no-radius m-r-10','name'=>'Search']) !!}
                            {!! Form::button('Clear', ['class' => 'btn btn-inverse no-radius','name'=>'Clear', 'id' => "clearBtn"]) !!}
                        </div>
                    {!! Form::close() !!}
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="col-md-2 row pull-right">
                        {!!Form::select('sortingField', $leave_status, isset($condition) ? $condition : '' , ['class' => 'form-control input-sm m-b-10','id'=>'sortingField'])!!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table dataTable " id="leaves-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.team_name') !!}</th>
                                    <th>{!! trans('language.leave_date') !!}</th>
                                    <th>{!! trans('language.leave_details') !!}</th>
                                    <th>{!! trans('language.leave_status') !!}</th>
                                    <th>{!! trans('language.leave_comment') !!}</th>
                                    <th>{!! trans('language.leave_apply_date') !!}</th>
                                    <th>{!! trans('language.action') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="leave-cancel-model" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="applyLeaveModalLabel" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Leave Cancel</h4> 
                        </div>
                        <div class="alert alert-success alert-dismissible" id="cancelLeaveMessage" style="display:none;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span id="cancelLeaveMessageText"></span>
                        </div>
                        {!! Form::open(['files'=>TRUE,'id' => 'cancel-leave-details' ,'autocomplete' => "off"]) !!}
                        <input type="hidden" name="leave_register_id" id="leave_register_id" value=""/>
                        <input type="hidden" name="leave_status" id="leave_status" value="3"/>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="leave_comment" class="control-label">Comment:</label>
                                <textarea class="form-control" name="leave_comment" rows="4" id="leave_comment" style="resize:none;"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default waves-effect no-radius" data-dismiss="modal">Close</button>
                            <button type="submit" name="submit" class="btn btn-info waves-effect waves-light leave-apply submitFormButton no-radius">Submit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->        
    </div>
    <!-- /.container-fluid -->
</div>
<script>
    $(document).ready(function () {
        $('#sortingField').change(function(){
            var table = $('#leaves-table').DataTable();
            table.ajax.reload( null, false );
        });
        var table = $('#leaves-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/leave/leave-data')}}',
                data: function (d) { 
                    d.employee = $('#employee_name').val();
                    d.leave_category = $('#leave_category').val();
                    d.condition = $('#sortingField').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'employee_name', name: 'employee_name'},
                {data: 'leave_date', name: 'leave_date'},
                {data: 'leave_details', name: 'leave_details'},
                {data: 'leave_status', name: 'leave_status'},
                {data: 'leave_comment', name: 'leave_comment'},
                {data: 'leave_apply_date', name: 'leave_apply_date'},
                {data: 'action', name: 'action'},
            ],
            columnDefs: [
                { "targets": [3,4,5,7], "orderable": false }
            ]
        });
        $('#search-form').on('submit', function(e) {
           table.draw();
           e.preventDefault();
        });
        $('#clearBtn').click(function(){
           location.reload();
        });
    });
    function changeStatus(id,status, comment=''){
        $('.preloader').css('display','inline');
        var token = '{!!csrf_token()!!}';
        $.ajax(
        {
            url: "{{ url('admin-panel/leave/leave-change-status') }}",
            type: 'GET',
            data: {
                'leave_register_id': id,
                'leave_status': status,
                'leave_comment': '',
            },
            success: function (res)
            {
                $('.preloader').css('display','none');
                if (res.status == "success")
                {
                    var table = $('#leaves-table').DataTable();
                    table.ajax.reload( null, false );
                    $('#ApproveLeaveMessageText').html(res.message);
                    $('#ApproveLeaveMessage').fadeIn(function() {
                        setTimeout(function() {
                            $('#ApproveLeaveMessage').fadeOut();
                        }, 2500);
                    });
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }
        });
    }
    $(document).on('click','.cancelRequest', function(){
        $('#leave_register_id').val($(this).attr('leave-register-id'));
        $('#leave-cancel-model').modal({
            show: 'false'
        });   
    });
    jQuery(document).ready(function () {
        /*$('#leave-cancel-model').on('hidden.bs.modal', function () {
            location.reload();
        });*/
        $("#cancel-leave-details").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                leave_comment: {
                    required: true,
                },
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Submit');
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Submit');
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                $(".submitFormButton").html('Submit');
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            },
            submitHandler: function(form) {
                $('.preloader').css('display','inline');
                $.ajax(
                {
                    url: "{{ url('admin-panel/leave/leave-change-status') }}",
                    type: 'GET',
                    data: $(form).serialize(),
                    success: function (res)
                    {
                        $('.preloader').css('display','none');
                        if (res.status == "error")
                        {
                            $('#cancelLeaveMessageText').html(res.message);
                            $('#cancelLeaveMessage').fadeIn(function() {
                                setTimeout(function() {
                                    $('#cancelLeaveMessage').fadeOut();
                                }, 2500);
                            });
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        }
                        else{
                            $('#cancel-leave-details')[0].reset();
                            $('#leave-cancel-model').modal('toggle');
                            var table = $('#leaves-table').DataTable();
                            table.ajax.reload( null, false );
                            $('#ApproveLeaveMessageText').html(res.message);
                            $('#ApproveLeaveMessage').fadeIn(function() {
                                setTimeout(function() {
                                    $('#ApproveLeaveMessage').fadeOut();
                                }, 2500);
                            });
                            $("html, body").animate({ scrollTop: 0 }, "slow");

                        }
                    }
                });
            }
        }); 
    });
</script>
@endsection






