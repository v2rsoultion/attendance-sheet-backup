@if(isset($work_time['work_time_id']) && !empty($work_time['work_time_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('work_time_id',old('work_time_id',isset($work_time['work_time_id']) ? $work_time['work_time_id'] : ''),['class' => 'gui-input', 'id' => 'work_time_id', 'readonly' => 'true']) !!}
@if($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{$errors->first()}}
    </div>
@endif
<div class="alert alert-danger alert-dismissible" id="ChangeDateMessage" style="display:none;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    This date work already done. Please select new date.
</div>
<div class="clearfix"></div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label for="{!! trans('language.work_time_date') !!}">{!! trans('language.work_time_date') !!} <span class="red-text">*</span></label>
    <div class="input-group">
        <input type="text" class="form-control" id="datepicker-autoclose" autocomplete="off" value="{!! isset($work_time_date) ? $work_time_date : '' !!}" name="work_date" placeholder="yyyy/mm/dd">
        <span class="input-group-addon"><i class="icon-calender"></i></span> 
    </div>
</div>
<div class="clearfix"></div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <table class="table table-hover">
        <thead>
            <tr>
                <th width="70" class="text-center">#</th>
                <th>Assigned Project</th>
                <th>Add Work Time</th>
                <th>Project Status</th>
            </tr>
        </thead>
        @if(!empty($project_assigns))
        <tbody>
            @php 
                $project_counter = 1;
                $addrow_counter = 1;
            @endphp
            @foreach ($project_assigns as $project)
                @php $addrow_counter = 1 @endphp
                @if($project['get_project']['status'] == '1')
                    <tr>
                        <td class="text-center">{!! $project_counter++ !!}</td>
                        <td>{!! $project['get_project']['project_name'] !!}</td>
                        <td>
                            {!! Form::button('Add Time', ['data-toggle' => 'collapse', 'data-target' => '#collapse-'.$project['get_project']['project_id'] ,'class' => 'btn btn-sm btn-info no-radius'])  !!}
                        </td>
                        <td> @if($project['get_project']['status'] == '0') Inactive @else Active @endif
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="3">
                            <div id="collapse-{!! $project['get_project']['project_id'] !!}" class="collapse @if(!empty($work_time_details) && isset($project_existing_data[$project['get_project']['project_id']]) && $project_existing_data[$project['get_project']['project_id']] != '') in @endif">
                                <div class="row work-set" id="work-set-{!! $project['get_project']['project_id'] !!}">
                                    @if(!empty($work_time_details) && isset($project_existing_data[$project['get_project']['project_id']]) && $project_existing_data[$project['get_project']['project_id']] != '')
                                        @php $i = 1; @endphp
                                        @foreach ($work_time_details as $key =>$details)
                                            @if($project['get_project']['project_id'] == $details['project_id'])
                                                <div class="col-md-12 clearfix" @if($addrow_counter != 1) element-id="{!! $project['get_project']['project_id'] !!}_{!! $i !!}" @endif>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">{!! trans("language.work_type") !!}</label>
                                                            <div class="col-sm-8">
                                                                <select class="form-control input-sm select2" id="map_'{!! $project['get_project']['project_id'] !!}'_worktype_'{!! $i !!}" name="map[{!! $project['get_project']['project_id'] !!}][{!! $i !!}][work_type]" required>
                                                                    @foreach($work_type as $key => $value)
                                                                        <option value="{{ $key }}" @if($details['work_type_id'] == $key) selected @endif>{{$value}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <input type="hidden" class="gui-input" name="map[{!! $project['get_project']['project_id'] !!}][{!! $i !!}][work_time_detail_id]" value="{!! $details['work_time_detail_id'] !!}" readonly="true" />
                                                            </div> 
                                                        </div> 
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">{!! trans("language.work_time_hrs") !!}</label>
                                                            <div class="col-sm-8">
                                                                <input type="number" class="form-control input-sm work-hr" placeholder="Working Hrs" value="{!! $details['work_time_detail_hrs'] !!}" id="map_{!! $project['get_project']['project_id'] !!}_worktime_{!! $i !!}" min="0" max="24"  name="map[{!! $project['get_project']['project_id'] !!}][{!! $i !!}][work_time]" required/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <textarea class="form-control textareaFocus" placeholder="Description" id="map_{!! $project['get_project']['project_id'] !!}_workdesc_{!! $i !!}" name="map[{!! $project['get_project']['project_id'] !!}][{!! $i !!}][work_desc]">{!! $details['work_time_detail_description'] !!}</textarea>
                                                        </div>
                                                    </div>
                                                    @if($addrow_counter == 1)
                                                        @php $addrow_counter = $addrow_counter + 1 @endphp          
                                                        <div class="col-md-2">
                                                            <button type="button" class="btn btn-sm btn-info no-radius add-row" add-id="{{ $project_existing_data[$project['get_project']['project_id']] }}" project-id="{!! $project['get_project']['project_id'] !!}" >Add Row</button>
                                                        </div>
                                                    @else
                                                        <div class="col-md-2">
                                                            <button type="button" onclick="removeRow({!! $project['get_project']['project_id'] !!}, {!! $i !!}, {!! $details['work_time_detail_id'] !!})" class="btn btn-sm btn-danger no-radius remove-row" project-id="{!! $project['get_project']['project_id'] !!}" remove-id="{!! $i !!}">Remove</button>
                                                        </div>
                                                    @endif
                                                </div>
                                                @php $i++ ; @endphp
                                            @endif
                                        @endforeach
                                    @else
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">{!! trans('language.work_type') !!} </label>
                                                    <div class="col-sm-8">
                                                        {!!Form::select('map['.$project['get_project']['project_id'].'][1][work_type]', $work_type,'' ,['required', 'class' => 'form-control input-sm select2','id'=>'map_' .$project['get_project']['project_id'] . '_worktype_1']) !!}
                                                        {!! Form::hidden('map['.$project['get_project']['project_id'].'][1][work_time_detail_id]','',['class' => 'gui-input', 'id' => 'work_time_detail_id', 'readonly' => 'true']) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">{!! trans('language.work_time_hrs') !!}</label>
                                                    <div class="col-sm-8">
                                                        {!!Form::number('map['.$project['get_project']['project_id'].'][1][work_time]', '',['required', 'min' => '0', 'max' => '24', 'class' => 'form-control input-sm work-hr', 'id' => 'map_' .$project['get_project']['project_id'] . '_worktime_1', 'placeholder' => 'Working Hrs' ]) !!}  
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <textarea class="form-control textareaFocus" placeholder="Description" id="map_{!! $project['get_project']['project_id'] !!}_workdesc_1" name="map[{!! $project['get_project']['project_id'] !!}][1][work_desc]"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="button" class="btn btn-sm btn-info no-radius add-row" add-id="1" project-id="{!! $project['get_project']['project_id'] !!}" >Add Row</button>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </td>
                    </tr>
                @elseif($project['get_project']['status'] == '0')
                    @if(isset($work_time['work_time_id']) && $work_time['work_time_id'] != '')
                        @if(isset($project_existing_data[$project['get_project']['project_id']]))                
                            <tr>
                                <td class="text-center">{!! $project_counter++ !!}</td>
                                <td>{!! $project['get_project']['project_name'] !!}</td>
                                <td>
                                    {!! Form::button('Add Time', ['data-toggle' => 'collapse', 'data-target' => '#collapse-'.$project['get_project']['project_id'] ,'class' => 'btn btn-sm btn-info no-radius'])  !!}
                                </td>
                                <td> @if($project['get_project']['status'] == '0') Inactive @else Active @endif
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="3">
                                    <div id="collapse-{!! $project['get_project']['project_id'] !!}" class="collapse @if(!empty($work_time_details) && isset($project_existing_data[$project['get_project']['project_id']]) && $project_existing_data[$project['get_project']['project_id']] != '') in @endif">
                                        <div class="row work-set" id="work-set-{!! $project['get_project']['project_id'] !!}">
                                            @if(!empty($work_time_details) && isset($project_existing_data[$project['get_project']['project_id']]) && $project_existing_data[$project['get_project']['project_id']] != '')
                                                @php $i = $project['get_project']['project_id'];  $i = 1; @endphp
                                                @foreach ($work_time_details as $key =>$details)
                                                    @if($project['get_project']['project_id'] == $details['project_id'])
                                                        <div class="col-md-12 clearfix" @if($addrow_counter != 1) element-id="{!! $project['get_project']['project_id'] !!}_{!! $i !!}" @endif>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">{!! trans("language.work_type") !!}</label>
                                                                    <div class="col-sm-8">
                                                                        <select class="form-control input-sm select2" id="map_'{!! $project['get_project']['project_id'] !!}'_worktype_'{!! $i !!}" disabled name="map[{!! $project['get_project']['project_id'] !!}][{!! $i !!}][work_type]" required>
                                                                            @foreach($work_type as $key => $value)
                                                                                <option value="{{ $key }}" @if($details['work_type_id'] == $key) selected @endif>{{$value}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        <input type="hidden" class="gui-input" name="map[{!! $project['get_project']['project_id'] !!}][{!! $i !!}][work_time_detail_id]" value="{!! $details['work_time_detail_id'] !!}" readonly/>
                                                                    </div> 
                                                                </div> 
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label">{!! trans("language.work_time_hrs") !!}</label>
                                                                    <div class="col-sm-8">
                                                                        <input type="number" readonly class="form-control input-sm work-hr" placeholder="Working Hrs" value="{!! $details['work_time_detail_hrs'] !!}" id="map_{!! $project['get_project']['project_id'] !!}_worktime_{!! $i !!}" min="0" max="24"  name="map[{!! $project['get_project']['project_id'] !!}][{!! $i !!}][work_time]" required/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <textarea class="form-control textareaFocus" readonly placeholder="Description" id="map_{!! $project['get_project']['project_id'] !!}_workdesc_{!! $i !!}" name="map[{!! $project['get_project']['project_id'] !!}][{!! $i !!}][work_desc]">{!! $details['work_time_detail_description'] !!}</textarea>
                                                                </div>
                                                            </div>
                                                            {{-- @if($addrow_counter == 1)
                                                                @php $addrow_counter = $addrow_counter + 1 @endphp          
                                                                <div class="col-md-2">
                                                                    <button type="button" class="btn btn-sm btn-info no-radius add-row" add-id="{{ $project_existing_data[$project['get_project']['project_id']] }}" project-id="{!! $project['get_project']['project_id'] !!}" disabled>Add Row</button>
                                                                </div>
                                                            @else
                                                                <div class="col-md-2">
                                                                    <button type="button" onclick="removeRow({!! $project['get_project']['project_id'] !!}, {!! $i !!}, {!! $details['work_time_detail_id'] !!})" class="btn btn-sm btn-danger no-radius remove-row" project-id="{!! $project['get_project']['project_id'] !!}" remove-id="{!! $i !!}" disabled>Remove</button>
                                                                </div>
                                                            @endif --}}
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @else
                                                <div class="col-md-12">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">{!! trans('language.work_type') !!} </label>
                                                            <div class="col-sm-8">
                                                                {!!Form::select('map['.$project['get_project']['project_id'].'][1][work_type]', $work_type,'' ,['required', 'class' => 'form-control input-sm select2','id'=>'map_' .$project['get_project']['project_id'] . '_worktype_1']) !!}
                                                                {!! Form::hidden('map['.$project['get_project']['project_id'].'][1][work_time_detail_id]','',['class' => 'gui-input', 'id' => 'work_time_detail_id', 'readonly' => 'true']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label">{!! trans('language.work_time_hrs') !!}</label>
                                                            <div class="col-sm-8">
                                                                {!!Form::number('map['.$project['get_project']['project_id'].'][1][work_time]', '',['required', 'min' => '0', 'max' => '24', 'class' => 'form-control input-sm work-hr', 'id' => 'map_' .$project['get_project']['project_id'] . '_worktime_1', 'placeholder' => 'Working Hrs' ]) !!}  
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <textarea class="form-control textareaFocus" placeholder="Description" id="map_{!! $project['get_project']['project_id'] !!}_workdesc_1" name="map[{!! $project['get_project']['project_id'] !!}][1][work_desc]"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button type="button" class="btn btn-sm btn-info no-radius add-row" add-id="1" project-id="{!! $project['get_project']['project_id'] !!}" >Add Row</button>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endif
                @endif
            @endforeach
        </tbody>
        @endif
    </table>
</div>
<div class="clearfix"></div>
@if(!empty($project_assigns))
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label for="{!! trans('language.total_working_hr') !!}">{!! trans('language.total_working_hr') !!} </label>
    <div class="input-group">
        <input type="number" class="form-control" id="total_working_hrs" name="total_work_time_hrs" readonly="true" value="{!! isset($work_time['total_work_time_hr']) ? $work_time['total_work_time_hr'] : '' !!}" />
    </div>
</div>
<div class="clearfix"></div>
<div class="container-fluid form-footer text-right">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="">
                {{-- <button type="reset" class="btn btn-inverse waves-effect waves-light m-t-10 no-radius">Reset</button> --}}
                <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10 no-radius">{!! $submit_button !!}</button>
            </div>
        </div>
    </div>
</div>
@endif
<div class="clearfix"></div>
<script>
    $body = $("body");
    $(document).ready(function (){        
        var work_time_id = $('#work_time_id').val();
        if(work_time_id !== ''){
            $('.form-footer').hide();
        }
        else{
            $('.form-footer').slideDown(800, function(){ });
        }
        $("form :input").change(function() {
            $('.form-footer').slideDown(800, function(){ });
        });
    });

    function removeRow(project_id, remove_id, work_time_detail_id = null)
    {
        $body.addClass("loading");
        if(work_time_detail_id != null){
            $.ajax({
                url: "{{url('admin-panel/project-work-time/project-work-time-detail-delete')}}",
                type: "GET",
                data: {id : work_time_detail_id},
                success: function(data){
                    $body.removeClass("loading");
                    if(data == 'success'){
                        var element_id = project_id+'_'+remove_id;
                        $("[element-id="+element_id+"]").remove();
                        calculateTotalHrs();

                    }else if(data == 'error'){

                    }
                }
            });
        }else{
            var element_id = project_id+'_'+remove_id;
            $("[element-id="+element_id+"]").remove();
            calculateTotalHrs();
            $body.removeClass("loading");
        }
    }
    
    function calculateTotalHrs()
    {
        var total_sum = 0;
        $('.work-hr').each(function(e){
            var value = parseFloat($(this).val()); 
            console.log(value);
            if(!isNaN(value)){
                console.log('value' + value);
                total_sum = parseFloat(total_sum) + value; 
                console.log('total' + total_sum);
            }
        });
        $('#total_working_hrs').val(total_sum);
    }

    $(document).on('keyup', '.work-hr', function(){
        calculateTotalHrs();
    });
    
    $(document).on('change', '#datepicker-autoclo', function(){ 
        if($(this).val() != ''){
            $("body").addClass("loading");
            $.ajax({
                url: "{{url('admin-panel/project-work-time/project-work-time-date-check')}}",
                type: "GET",
                data: {date : $(this).val()},
                success: function(data){
                    $("body").removeClass("loading");
                    if(data == 'error'){
                        $('#ChangeDateMessage').fadeIn(function() {
                            setTimeout(function() {
                                $('#ChangeDateMessage').fadeOut();
                            }, 2500);
                        });
                    }
                }
            });
        }
    });
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy/mm/dd'
    }).on('change', function() {
        $('.form-footer').slideDown(800, function(){ });
        $(this).valid();  // triggers the validation test
        // '$(this)' refers to '$("#datepicker")'
    });
    $(document).ready(function(){
         var add_id = $(this).attr('add-id',1);
        $('.add-row').click( function(){
            $('.form-footer').slideDown(800, function(){ });
            var project_id = $(this).attr('project-id');
            var add_id = $(this).attr('add-id');
            var incrment_add_id  =  parseInt(add_id) + 1;
            $(this).attr('add-id', incrment_add_id);
            $('#work-set-'+project_id).append(
                '<div class="col-md-12 clearfix" element-id="' + project_id +'_'+ add_id +'">' +
                    '<div class="col-md-4">' +
                        '<div class="form-group">'+
                            '<label class="col-sm-4 control-label">{!! trans("language.work_type") !!}</label>' +
                            '<div class="col-sm-8">' +
                                '<select class="form-control input-sm select2" id="map_' + project_id + '_worktype_' + incrment_add_id + '" name="map[' + project_id + '][' + incrment_add_id +'][work_type]" required>' +
                                    '@foreach($work_type as $key => $value)' +
                                        '<option value="{{ $key }}">{{$value}}</option>' +
                                    '@endforeach'+
                                '</select>' +
                                '<input type="hidden" class="gui-input" name="map[' + project_id + '][' + incrment_add_id +'][work_time_detail_id]" readonly="true" />' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label class="col-sm-4 control-label">{!! trans("language.work_time_hrs") !!}</label>' +
                            '<div class="col-sm-8">' +
                                '<input type="number" class="form-control input-sm work-hr" placeholder="Working Hrs" id="map_' + project_id + '_worktime_' + incrment_add_id + '" min="0" max="24"  name="map[' + project_id + '][' + incrment_add_id +'][work_time]" required/>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-md-6">' +
                        '<div class="form-group">' +
                            '<textarea class="form-control textareaFocus" placeholder="Description" id="map_' + project_id + '_workdesc_' + incrment_add_id + '" name="map[' + project_id + '][' + incrment_add_id + '][work_desc]"></textarea>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-md-2">' +
                        '<button type="button" onclick="removeRow(' + project_id +', ' + add_id +')" class="btn btn-sm btn-danger no-radius remove-row" project-id="'+ project_id +'" remove-id="'+ add_id +'">Remove</button>' +
                    '</div>'+
                '</div>');
        });
    });    
</script>
<script type="text/javascript">    
    jQuery(document).ready(function () {
        $("#work-time-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            autoWidth: false,
                fixedHeader: {
                    header: false,
                    footer: false
            },
            /* @validation rules    
             ------------------------------------------ */
            ignore: ":hidden",
            rules: {
                work_date: {
                    required: true,
                    remote: {
                        url:"{{url('admin-panel/project-work-time/project-work-time-date-check')}}",
                        async:false,
                        type: "get",
                        data: { work_time_id: $( "#work_time_id" ).val() }
                    }
                },
                total_work_time_hrs: {
                    required: true,
                }
            },
            messages: {
                work_date: {
                    required: "Please select date",
                    remote: "Selected date already exist."
                },

            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Save');
                $( element ).parents( ".input-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Save');
                $( element ).parents( ".input-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                $(".submitFormButton").html('Save');
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
    });
</script>