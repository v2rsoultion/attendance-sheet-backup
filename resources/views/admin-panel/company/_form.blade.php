@if(isset($company_config['company_config_id']) && !empty($company_config['company_config_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('company_config_id',old('company_config_id',isset($company_config['company_config_id']) ? $company_config['company_config_id'] : ''),['class' => 'gui-input', 'id' => 'company_config_id', 'readonly' => 'true']) !!}
@if($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{$errors->first()}}
    </div>
@endif
<!-- <h3 class="box-title m-b-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">Category</h3>
<p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13"> Enter new {!! trans('language.department') !!} </p> -->
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.office_hours') !!}">{!! trans('language.office_hours') !!} <span class="red-text">*</span></label>
        <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
            {!! Form::number('office_hours', old('office_hours',isset($company_config['office_hours']) ? substr($company_config['office_hours'],0,5): ''), ['class' => 'form-control','placeholder'=>trans('language.office_hours'), 'min' => '0', 'max' => '12', 'id' => 'office_hours','for'=>trans('language.office_hours')]) !!}
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.lunch_hours') !!}">{!! trans('language.lunch_hours') !!} <span class="red-text">*</span></label>
        <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
            {!! Form::number('lunch_hours', old('lunch_hours',isset($company_config['lunch_hours']) ? substr($company_config['lunch_hours'],0,5): ''), ['class' => 'form-control','placeholder'=>trans('language.lunch_hours'), 'min' => '0', 'max' => '12', 'id' => 'lunch_hours','for'=>trans('language.lunch_hours')]) !!}
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <div class="input-group">    
        <label for="{!! trans('language.direct_half_time') !!}">{!! trans('language.direct_half_time') !!} <span class="red-text">*</span></label>    
        <div class="input-group bootstrap-timepicker timepicker">
            {!! Form::text('direct_half_time', old('direct_half_time',isset($company_config['direct_half_time']) ? $company_config['direct_half_time']: ''), ['class' => 'form-control input-small','placeholder'=>trans('language.direct_half_time'), 'id' => 'direct_half_time','for'=>trans('language.direct_half_time'), 'readonly']) !!}
            {{-- <input id="timepicker1" type="text" class="form-control input-small" readonly> --}}
            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
        </div>
    </div>
    {{-- <div class="input-group">
        <label for="{!! trans('language.direct_half_time') !!}">{!! trans('language.direct_half_time') !!} <span class="red-text">*</span></label>
        <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
            {!! Form::number('direct_half_time', old('direct_half_time',isset($company_config['direct_half_time']) ? substr($company_config['direct_half_time'],0,5): ''), ['class' => 'form-control','placeholder'=>trans('language.direct_half_time'), 'min' => '0', 'max' => '12', 'id' => 'direct_half_time','for'=>trans('language.direct_half_time')]) !!}
        </div>
    </div> --}}
</div> 
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.late_half_days') !!}">{!! trans('language.late_half_days') !!} <span class="red-text">*</span></label>
        <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
            {!! Form::number('late_half_days', old('late_half_days',isset($company_config['late_half_days']) ? $company_config['late_half_days']: ''), ['class' => 'form-control','placeholder'=>trans('language.late_half_days'), 'min' => '0', 'max' => '12', 'id' => 'late_half_days','for'=>trans('language.late_half_days')]) !!}
        </div>
    </div>
</div> 
<div class="clearfix"></div>
<div class=" col-sm-9">
    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10 no-radius">{!! $submit_button !!}</button>
</div>
<div class="clearfix"></div>
<script>
    $(document).on('click','.myButton',function(e){
        alert(document.getElementById('calendar').value);
    });
</script>
<script type="text/javascript">    
    $('#direct_half_time').timepicker();
    jQuery(document).ready(function () {
        $("#company-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                office_hours: {
                    required: true,
                },
                lunch_hours: {
                    required: true,
                },
                direct_half_time: {
                    required: true,
                },
                late_half_days: {
                    required: true,
                }
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".input-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".input-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        }); 
    });
</script>