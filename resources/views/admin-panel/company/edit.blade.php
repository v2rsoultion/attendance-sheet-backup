@extends('admin-panel.layout.header')
@section('content')
<style>
    .hover-end{
        padding:0;
        margin:0;
        font-size:75%;
        text-align:center;
        position:absolute;
        bottom:0;
        width:100%;
        opacity:.8
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box white-box-modify">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    {!! Form::open(['files'=>TRUE,'id' => 'company-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                        @include('admin-panel.company._form',['submit_button' => $submit_button])
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-md-3">
                <div class="white-box">
                    <h3 class="box-title">Drag and drop your event</h3>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div id="calendar-events" class="m-t-10">
                                <div class="m-b-5 m-l-10" data-class="bg-warning"><i class="fa fa-circle text-warning p-r-10"></i> Sunday</div>
                                <div class="calendar-events" data-class="bg-saturday"><i class="fa fa-circle text-inverse"></i> Saturday</div>
                                <div class="calendar-events" data-class="bg-companyHoliday"><i class="fa fa-circle text-success"></i> Company Holiday</div>
                                <div class="calendar-events" data-class="bg-danger"><i class="fa fa-circle text-danger"></i> Company Event</div>
                            </div>
                            <!-- checkbox -->
                            <div class="m-l-10 m-t-10">
                                <div class="checkbox">
                                    <input id="drop-remove" type="checkbox">
                                    <label for="drop-remove">
                                        Remove after drop
                                    </label>
                                </div>
                            </div>
                            <a href="#" data-toggle="modal" data-target="#add-new-event" class="btn btn-lg m-t-40 btn-danger btn-block waves-effect waves-light">
                                <i class="ti-plus"></i> Add New Event
                            </a>
                            <button id="submitEvents" class="btn btn-lg m-t-10 btn-info btn-block waves-effect waves-light">
                                <i class="fas fa-save m-r-10"></i> Save Event
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="white-box">
                    <div id="calendar" my-url="xyz"></div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- BEGIN MODAL -->
        <div class="modal fade none-border" id="my-event">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><strong>Add Event</strong></h4>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success save-event waves-effect waves-light">Create event</button>
                        <button type="button" class="btn btn-danger delete-event waves-effect waves-light" id="deleteModel">Delete</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Add Category -->
        <div class="modal fade none-border" id="add-new-event">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><strong>Add</strong> a category</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Category Name</label>
                                    <input class="form-control form-white" placeholder="Enter name" type="text" name="category-name" />
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Choose Category Color</label>
                                    <select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
                                        <option value="success">Success</option>
                                        <option value="danger">Danger</option>
                                        <option value="info">Info</option>
                                        <option value="primary">Primary</option>
                                        <option value="warning">Warning</option>
                                        <option value="inverse">Inverse</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect waves-light save-category" data-dismiss="modal">Save</button>
                        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MODAL -->
    </div>
</div>
<script>
    $('#submitEvents').click( function(){
        var events_arr = $('#calendar').fullCalendar('clientEvents');
        var events     = []; 
        events_arr.forEach(function(element) {
            events.push({'eventId':element.eventId,'title':element.title,'start':moment(element.start).format('YYYY-MM-DD hh:mm'),'end':moment(element.end).format('YYYY-MM-DD hh:mm'),'className':element.className});
        });
        $('.preloader').css('display','inline');
        $.ajax({
            contentType: "application/json",
            url: '{{ url("admin-panel/configuartion/save-calendar") }}',
            datatType: 'json',
            type: 'GET',
            data: { 'events' : events } ,
            success: function (response) {
                $('.preloader').css('display','none');
                if(response.status == 0){
                    swal(response.success,'', "success");
                    setInterval(function() {
                        location.reload(1);
                    }, 1500);
                }
                else{
                    alert(response.success);
                }
            },
            error: function () {
                alert("error");
            }
        });
    });
    $(function(){
        $('#calendar2').fullCalendar({
            defaultView: 'month',
            defaultDate: '2019-02-12',
            eventRender: function(eventObj, $el) {
                $el.popover({
                title: eventObj.title,
                content: eventObj.description,
                trigger: 'hover',
                placement: 'top',
                container: 'body'
                });
            },
            events: [
                {
                title: 'All Day Event',
                description: 'description for All Day Event',
                start: '2019-02-01'
                },
                {
                title: 'Long Event',
                description: 'description for Long Event',
                start: '2019-02-07',
                end: '2019-02-10'
                },
                {
                id: 999,
                title: 'Repeating Event',
                description: 'description for Repeating Event',
                start: '2019-02-09T16:00:00'
                },
                {
                id: 999,
                title: 'Repeating Event',
                description: 'description for Repeating Event',
                start: '2019-02-16T16:00:00'
                },
                {
                title: 'Conference',
                description: 'description for Conference',
                start: '2019-02-11',
                end: '2019-02-13'
                },
                {
                title: 'Meeting',
                description: 'description for Meeting',
                start: '2019-02-12T10:30:00',
                end: '2019-02-12T12:30:00'
                },
                {
                title: 'Lunch',
                description: 'description for Lunch',
                start: '2019-02-12T12:00:00'
                },
                {
                title: 'Meeting',
                description: 'description for Meeting',
                start: '2019-02-12T14:30:00'
                },
                {
                title: 'Birthday Party',
                description: 'description for Birthday Party',
                start: '2019-02-13T07:00:00'
                },
                {
                title: 'Click for Google',
                description: 'description for Click for Google',
                url: 'http://google.com/',
                start: '2019-02-28'
                }
            ]
        });

    });
    /*
    $(document).on('click','#deleteModel',function(e){
        var event_id = $('#eventId').attr('eventid');
        if(event_id != ''){
            $.ajax({
                url: "{{url('admin-panel/configuartion/delete-event')}}",
                type: 'GET',
                data: {
                    'event_id': event_id
                },
                success: function (data) {
                    $('#my-event').modal('toggle');
                    $('#calendar').fullCalendar('rerenderEvents');
                    $('#calendar').fullCalendar('refetchEvents');
                }
            });
        }
    });*/
</script>
@endsection

