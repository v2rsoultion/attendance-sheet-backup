<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="telephone=no" name="format-detection">
    <title></title>
    <!--[if (mso 16)]>
    <style type="text/css">
    a {text-decoration: none;}
    </style>
    <![endif]-->
    <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
    <!-- Bootstrap Core CSS -->
    {!! Html::style('public/admin/css/mail.css') !!}
</head>

<body>
    <div class="es-wrapper-color">
        <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td class="esd-email-paddings" valign="top">
                        <table class="es-content es-preheader esd-header-popover" width="100%" align="center" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="es-adaptive esd-stripe" style="background-color: rgb(247, 247, 247);" esd-custom-block-id="8428" align="center" bgcolor="#f7f7f7">
                                        <table class="es-content-body" style="background-color: transparent;" align="center" width="600" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure es-p10" align="left">
                                                        <!--[if mso]><table width="580"><tr><td width="280" valign="top"><![endif]-->
                                                        <table class="es-left" align="left" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" align="left" width="280">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="es-infoblock esd-block-text es-m-txt-c" align="left">
                                                                                        {{-- <p>Put your preheader text here</p> --}}
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso]></td><td width="20"></td><td width="280" valign="top"><![endif]-->
                                                        <table class="es-right" align="right" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" align="left" width="280">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="es-infoblock esd-block-text es-m-txt-c" align="right">
                                                                                        {{-- <p><a href="https://stripo.email/" target="_blank">View in browser</a></p> --}}
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso]></td></tr></table><![endif]-->
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="esd-footer-popover es-content" width="100%" align="center" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" align="center">
                                        <table class="es-content-body" style="background-color: transparent;" align="center" width="600" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure es-p5t es-p5b es-p20r es-p20l" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" align="center" width="560" valign="top">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-image es-infoblock" align="center">
                                                                                        <a target="_blank" href="https://www.v2rsolution.com/"> <img src="https://www.v2rsolution.com/wp-content/uploads/2017/03/v2logo.png" alt="" width="125" style="display: block;"> </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="es-content" width="100%" align="center" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" align="center">
                                        <table class="es-content-body" style="border-width: 1px; border-style: solid; border-color: transparent; background-color: rgb(0, 164, 222);" align="center" width="100%" cellspacing="0" cellpadding="0" bgcolor="#00A4DE">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure es-p20t es-p40b es-p40r es-p40l" esd-custom-block-id="8537" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" align="left" width="518">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-c es-p20t" align="center" bgcolor="transparent">
                                                                                        <h2 style="color: rgb(255, 255, 255);">Welcome to V2R Solutions</h2>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="es-content" width="100%" align="center" cellspacing="0" cellpadding="0" style="margin-bottom:20px;margin-top:20px;">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" align="center">
                                        <table class="es-content-body" style="border-left:1px solid transparent;border-right:1px solid transparent;border-top:1px solid transparent;border-bottom:1px solid transparent;" align="center" width="800px" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure es-p20t es-p10b es-p40r es-p40l es-p40t " esd-custom-block-id="8537" align="left">
                                                        <table width="800px" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" align="left" width="518">
                                                                        <table width="800px" cellspacing="0" cellpadding="0" style="margin-bottom:20px;">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-c es-p15t es-p20l es-p15b" align="left">
                                                                                            <span style="
                                                                                            font-family: arial, 'helvetica neue', helvetica, sans-serif;
                                                                                            color: #333333;">Please verify your account and set your password by clicking on the following button:</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-button es-p20t es-p15b es-p10r es-p10l" style="padding-bottom:20px;" align="center"> <span class="es-button-border"><a href="{{isset($path) ? $path : ''}}" class="es-button-border" style="border-style: solid solid solid solid;
                                                                                        border-color: #474745 #474745 #474745 #474745;
                                                                                        background: #474745;
                                                                                        border-width: 0px 0px 0px 0px;
                                                                                        display: inline-block;
                                                                                        border-radius: 5px;
                                                                                        width: auto; padding-left:5px; padding-right:5px;color:#ffffff;font-size:16px;line-height:44px;text-decoration:none;width:140px;">Verify &rarr;</a> </span> </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-c es-p15t es-p20l" align="left">
                                                                                            <span style="
                                                                                            font-family: arial, 'helvetica neue', helvetica, sans-serif;
                                                                                            color: #333333;">If you face any issues, use the below link:</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="es-p20l es-p5t"><strong style="float:left;color: #333333;">Verify Here:</strong><span style="color: #333333; display:block; float:left; padding-left:10px;">{{ isset($path) ? $path : ''}}</span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-c es-p20l es-p20t" align="left" bgcolor="transparent">
                                                                                        <span style="
                                                                                        font-family: arial, 'helvetica neue', helvetica, sans-serif;
                                                                                        line-height: 150%;font-size: 18px;color: #333333;">Your account information</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-c es-p20l" align="left" bgcolor="transparent">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td><strong style="float:left;color: #333333;">Name:</strong> <span style="color: #333333; display:block; float:left; padding-left:10px;">{{ isset($to_name) ? $to_name : ''}}</span></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="es-p5t"><strong style="float:left;color: #333333;">Email:</strong> <span style="color: #333333; display:block; float:left; padding-left:10px;">{{ isset($email) ? $email : ''}}</span></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="esd-structure es-p40b es-p40r es-p40l" esd-custom-block-id="8537" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" align="left" width="518">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-c es-p20l es-p20t" align="left" bgcolor="transparent">
                                                                                        <span style="
                                                                                        font-family: arial, 'helvetica neue', helvetica, sans-serif;
                                                                                        line-height: 150%;font-size: 18px;color: #333333;">Need Support ?</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-c es-p20l" align="left" bgcolor="transparent">
                                                                                        <span style="
                                                                                        font-family: arial, 'helvetica neue', helvetica, sans-serif;
                                                                                        color: #333333;">Feel free to email us if you have any questions, comments or suggestions. We'll be happy resolve all your issues.</span>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-c es-p20l" align="left" bgcolor="transparent">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td><strong style="float:left;color: #333333;">Contact Us:</strong><span style="color: #333333; display:block; float:left; padding-left:10px;">info@v2rsolution.com</span></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="es-content es-p40t" width="100%" align="center" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" esd-custom-block-id="8442" style="background-color: rgb(247, 247, 247);" align="center" bgcolor="#f7f7f7">
                                        <table class="es-footer-body" align="center" width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure es-p20t es-p20r es-p20l" esd-general-paddings-checked="false" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" align="center" width="100%" valign="top">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-p5b" align="center">
                                                                                        <h3 style="line-height: 150%;">Let's get social</h3>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-social es-p10t es-p10b" align="center">
                                                                                        <table class="es-table-not-adapt es-social" cellspacing="0" cellpadding="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td class="es-p20r" align="center" valign="top">
                                                                                                        <a href="https://www.facebook.com/V2RSolution-422457951527930/"><img title="Facebook" src="https://tlr.stripocdn.email/content/assets/img/social-icons/logo-black/facebook-logo-black.png" alt="Fb" width="32" height="32"></a>
                                                                                                    </td>
                                                                                                    <td align="center" valign="top">
                                                                                                        <a href="https://twitter.com/V2rSolution" target="_blank"><img title="Twitter" src="https://tlr.stripocdn.email/content/assets/img/social-icons/logo-black/twitter-logo-black.png" alt="Tw" width="32" height="32"></a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-p10t es-p10b" align="center">
                                                                                        <p style="line-height: 150%;">You are receiving this email because you register for V2R Solution team. You can&nbsp;<a target="_blank" style="line-height: 150%;">unsubscribe here</a>&nbsp;or&nbsp;<a target="_blank" style="line-height: 150%;">update your subscription preferences</a>.</p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-p10t es-p10b" align="center">
                                                                                        <p>© 2018</p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="esd-footer-popover es-content" width="100%" align="center" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" align="center">
                                        <table class="es-content-body" style="background-color: transparent;" align="center" width="600" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure es-p5t es-p5b es-p20r es-p20l" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" align="center" width="560" valign="top">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-image es-infoblock" align="center">
                                                                                        <a target="_blank" href="https://www.v2rsolution.com/"> <img src="https://www.v2rsolution.com/wp-content/uploads/2017/03/v2logo.png" alt="" width="125" style="display: block;"> </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="position: absolute; left: -9999px; top: -9999px; margin: 0px;"></div>
</body>
</html>