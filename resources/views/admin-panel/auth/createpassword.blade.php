

<!DOCTYPE html>  
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{!! URL::to('public/assets/images/favicon--32x32.png') !!}">
    <title>{!! trans('language.project_title') !!}</title>
    <!-- Bootstrap Core CSS -->
    {!! Html::style('public/admin/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- animation CSS -->
    {!! Html::style('public/admin/css/animate.css') !!}
    <!-- Custom CSS -->
    {!! Html::style('public/admin/css/style.css') !!}
    {!! Html::style('public/admin/css/custom.css') !!}
    <!-- color CSS -->
    {!! Html::style('public/admin/css/colors/default.css') !!}
    <!-- <link href="public/admin/css/colors/default.css" id="theme"  rel="stylesheet"> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .padding-for-error{
            padding-left: 15px;
        }
    </style>
    </head>
    <body>
        <!-- Preloader -->
        <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
        </div>
        <section id="wrapper" class="new-login-register">
            <div class="lg-info-panel">
                <div class="inner-panel">
                    <a href="javascript:void(0)" class="p-20 di"> <img src="{!! URL::to('public/assets/images/favicon--32x32.png') !!}" alt=""></a>
                    <div class="lg-content" style="margin-top:30px;">
                        <img src="{!! URL::to('public/assets/images/v2logo.png') !!}" alt="">
                        <h2>{!! trans('language.project_title') !!}</h2>
                        <p class="text-muted">{!! trans('language.project_tagline') !!}</p>
                    </div>
                </div>
            </div>
            <div class="new-login-box">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Set Password</h3>
                    <small>Enter your details below</small>
                    {!! Form::open(['files'=>TRUE, 'id' => 'team-password-set', 'class' => 'form-horizontal new-lg-form', 'url' => '/create-password']) !!}
                        @if (isset($team_id))
                            {!! Form::hidden('team_id', isset($team_id)? $team_id : '' )!!}
                        @endif

                        @if(session()->has('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session()->get('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                            {{$errors->first()}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        
                        @if($errors->any())
                        <p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
                        @endif
                        
                        <div class="form-group  m-t-20">
                            <div class="col-xs-12">
                                <label>Password</label>
                                <input type="password" name="team_password" id="team_password" class="form-control" />
                                <span>(Note: Enter a combination of at least eight numbers, letters, and special characters.)</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label>Confirm Password</label>
                                <input type="password" name="team_confirm_password" id="team_confirm_password" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light no-radius" type="submit">Set Password</button>
                        </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>            
        </section>
        <!-- jQuery -->
        {!! Html::script('public/admin/plugins/bower_components/jquery/dist/jquery.min.js') !!}
        <!-- Bootstrap Core JavaScript -->
        {!! Html::script('public/admin/bootstrap/dist/js/bootstrap.min.js') !!}
        <!-- Menu Plugin JavaScript -->
        {!! Html::script('public/admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') !!}
        <!--slimscroll JavaScript -->
        {!! Html::script('public/admin/js/jquery.slimscroll.js') !!}
        <!--Wave Effects -->
        {!! Html::script('public/admin/js/waves.js') !!}
        <!-- Custom Theme JavaScript -->
        {!! Html::script('public/admin/js/custom.min.js') !!}
        <!--Style Switcher -->
        {!! Html::script('public/admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') !!}
        {!! Html::script('public/admin/download/jquery.validate.min.js') !!}
    </body>
    <script>
        jQuery(document).ready(function () {
            jQuery.validator.addMethod("validPassword", function(value, element) {
                return this.optional(element) || /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/i.test(value);
            }, "Please enter strong password.");

            $("#team-password-set").validate({

                /* @validation states + elements 
                ------------------------------------------- */

                errorClass: "state-error padding-for-error",
                validClass: "state-success padding-for-error",
                errorElement: "em",

                /* @validation rules 
                ------------------------------------------ */

                rules: {
                    team_password: {
                        required: true,
                        validPassword: true,
                    },
                    team_confirm_password: {
                        required: true,
                        equalTo: "#team_password"
                    },
                },
                messages:{
                    team_password: {
                        required: 'Password is required',
                    },
                    team_confirm_password: {
                        required:'Password is required',
                        equalTo: 'Password and confirm password not same.',
                    },
                },
                /* @validation highlighting + error placement  
                ---------------------------------------------------- */
                highlight: function (element, errorClass, validClass) {
                    $(element).closest('.field').addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).closest('.field').removeClass(errorClass).addClass(validClass);
                },
                errorPlacement: function (error, element) {
                    if (element.is(":radio") || element.is(":checkbox")) {
                        element.closest('.option-group').after(error);
                    } else {
                        error.insertAfter(element.parent());
                    }
                }
            });
            
        });
    </script>
</html>

