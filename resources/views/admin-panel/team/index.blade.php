@extends('admin-panel.layout.header')
@section('content')
<style>
.caret-reversed{
  -ms-transform: rotate(180deg);
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
}
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="#">{!! trans('language.team') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    <div class="alert alert-success alert-dismissible" id="ChangeStatusMessage" style="display:none;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        Status Changed Successfully.
                    </div>
                    <div class="clearfix"></div>
                    <div class="row clearfix">
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="col-lg-3 col-md-3">
                            <div class="input-group">
                                {!! Form::text('team_name', old('team_name',isset($team['team_name']) ? $team['team_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.team_name'), 'id' => 'team_name','for'=>trans('language.team_name')]) !!}
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            {!! Form::submit('Search', ['class' => 'btn btn-info no-radius m-r-10','name'=>'Search']) !!}
                            {!! Form::button('Clear', ['class' => 'btn btn-inverse no-radius','name'=>'Clear', 'id' => "clearBtn"]) !!}
                        </div>
                        {!! Form::close() !!}
                        <div class="pull-right m-r-20">
                            <a href="{{ url('/admin-panel/team/view-resign-team') }}" class="btn btn-info no-radius" role="button"><span class="fas fa-user-times m-r-5"></span>Former Employees</a>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Loader for Page --}}
            {{-- End - Loader for Page --}}
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table dataTable " id="team-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.team_emp_id') !!}</th>
                                    <th>{!! trans('language.team_name') !!}</th>
                                    <th>{!! trans('language.team_email') !!}</th>
                                    <th>{!! trans('language.joining_date') !!}</th>
                                    <th>{!! trans('language.user_role') !!}</th>
                                    <th>{!! trans('language.team_verification_status') !!}</th>
                                    <th>{!! trans('language.action') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->   
        <div class="modal fade" id="isDeatils" tabindex="-1" role="dialog" aria-labelledby="isDeatils" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['files'=>FALSE,'id' => 'resign-form','url' =>$resign_url]) !!}
                    <input type="hidden" value="" name="team_id" id="team_id"/>
                    <input type="hidden" value="" name="condtionIs" id="condtionIs"/>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="leaving_date" class="col-form-label">Leaving Date:</label>
                            {!!Form::text('leaving_date', date('Y-m-d') ,['class' => 'form-control date-inputmask','id' => 'leaving_date', 'placeholder' => 'Enter Date' ]) !!}            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>     
    </div>
    <!-- /.container-fluid -->
</div>
<script>
    $body = $("body");
    $(document).on('click', '.caret-icon', function() {
        $(this).toggleClass('fa-caret-up fa-caret-down');
    });
    $(document).on('click','.isCondition',function(e){
        var condition = $(this).attr('condition');
        console.log(condition);
        var condition_for = $(this).attr('condition-for');
        $('#team_id').val(condition_for);
        $('#condtionIs').val(condition);

        if(condition == 'isResign')
        {    
            $('#exampleModalLabel').text('Employee Resign'); 
        }
        if(condition == 'isTerminate')
        {    
            $('#exampleModalLabel').text('Employee Termination');
        }
    });
    
    $(document).ready(function () {
        var table = $('#team-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/team/data')}}',
                data: function (d) { 
                    d.team_name = $('input[name=team_name]').val();
                 }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'emp_id', name: 'emp_id'},
                {data: 'admin_name', name: 'admin_name'},
                {data: 'email', name: 'email'},
                {data: 'joining_date', name: 'joining_date'},
                {data: 'user_roles', name: 'user_roles'},
                {data: 'VerificationProcess', name: 'VerificationProcess'},
                {data: 'action', name: 'action'},
            ],
            columnDefs: [
                { "targets": [7,6,5], "orderable": false }
            ]
        });
        $('#search-form').on('submit', function(e) {
           table.draw();
           e.preventDefault();
        });
        $('#clearBtn').click(function(){
           location.reload();
        })
    });
    
    function changeStatus(id,status){
        var token = '{!!csrf_token()!!}';
        $('.preloader').css('display','inline');
        $.ajax(
        {
            url: "{{ url('admin-panel/team/team-status') }}",
            type: 'GET',
            data: {
                'team_id': id,
                'team_status': status,
            },
            success: function (res)
            {
                $('.preloader').css('display','none');
                if (res == "Success")
                {
                    var table = $('#team-table').DataTable();
                    table.ajax.reload( null, false );
                    $('#ChangeStatusMessage').fadeIn(function() {
                        setTimeout(function() {
                            $('#ChangeStatusMessage').fadeOut();
                        }, 2500);
                    });
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }
        });
    }
    jQuery(document).ready(function () {
        $("#resign-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                leaving_date: {
                    required: true,
                },
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            }
        }); 
    });
</script>
@endsection






