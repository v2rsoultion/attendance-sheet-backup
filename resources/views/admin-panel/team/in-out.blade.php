@extends('admin-panel.layout.header')
@section('content')
<style>
.caret-reversed{
  -ms-transform: rotate(180deg);
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
}
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="row clearfix">
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="col-lg-3 col-md-3">
                            <div class="input-group">
                                <input class="form-control input-daterange-datepicker" type="text" name="in_out_date"
                                    id="in_out_date" value="{{date('m/01/Y') .' - '. date('m/t/Y')}}"/>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            {!! Form::submit('Search', ['class' => 'btn btn-info no-radius m-r-10','name'=>'Search']) !!}
                            {!! Form::button('Clear', ['class' => 'btn btn-inverse no-radius','name'=>'Clear', 'id' => "clearBtn"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table dataTable " id="in-out-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.in_out_date') !!}</th>
                                    <th>{!! trans('language.in_out_intime') !!}</th>
                                    <th>{!! trans('language.in_out_outtime') !!}</th>
                                    <th>{!! trans('language.in_out_total') !!}</th>
                                    <th>{!! trans('language.in_out_key') !!}</th>
                                    <th>{!! trans('language.in_out_desc') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->       
    </div>
    <!-- /.container-fluid -->
</div>
<script>   
    $(document).ready(function () {
        // Daterange picker
        $('.input-daterange-datepicker').daterangepicker({
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-danger',
            cancelClass: 'btn-inverse',
        });
        var table = $('#in-out-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/in-out/in-out-data')}}',
                data: function (d) { 
                    d.dates = $('#in_out_date').val();
                 }
            },
            columns: [
                {data: 'in_out_date', name: 'in_out_date'},
                {data: 'in_out_intime', name: 'in_out_intime'},
                {data: 'in_out_outtime', name: 'in_out_outtime'},
                {data: 'in_out_total', name: 'in_out_total'},
                {data: 'in_out_key', name: 'in_out_key'},
                {data: 'in_out_desc', name: 'in_out_desc'},
            ],
            columnDefs: [
                { "targets": [1,2,3,4,5], "orderable": false }
            ]
        });
        $('#search-form').on('submit', function(e) {
           table.draw();
           e.preventDefault();
        });
        $('#clearBtn').click(function(){
           location.reload();
        })
    });
</script>
@endsection






