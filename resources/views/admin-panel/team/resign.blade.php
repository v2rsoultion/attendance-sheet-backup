@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="{!! $redirect_url !!}">{!! trans('language.view_team') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    <div class="row clearfix">
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="col-lg-3 col-md-3">
                            <div class="input-group">
                                {!! Form::text('team_name', old('team_name',isset($team['team_name']) ? $team['team_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.team_name'), 'id' => 'team_name','for'=>trans('language.team_name')]) !!}
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-1">
                            {!! Form::submit('Search', ['class' => 'btn btn-info no-radius','name'=>'Search']) !!}
                        </div>
                        <div class="col-lg-1 col-md-1">
                            {!! Form::button('Clear', ['class' => 'btn btn-info no-radius','name'=>'Clear', 'id' => "clearBtn"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            {{-- Loader for Page --}}
            {{-- End - Loader for Page --}}
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table dataTable " id="team-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.team_emp_id') !!}</th>
                                    <th>{!! trans('language.team_name') !!}</th>
                                    <th>{!! trans('language.team_email') !!}</th>
                                    <th>{!! trans('language.joining_date') !!}</th>
                                    <th>{!! trans('language.leaving_date') !!}</th>
                                    <th>{!! trans('language.action') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->       
         <div class="modal fade" id="isDeatils" tabindex="-1" role="dialog" aria-labelledby="isDeatils" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['files'=>FALSE,'id' => 'resign-form','url' =>$rejoin_url]) !!}
                    <input type="hidden" value="" name="team_id" id="team_id"/>
                    <input type="hidden" value="" name="condtionIs" id="condtionIs"/>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="joining_date" class="col-form-label">Joining Date:</label>
                            {!!Form::text('joining_date', date('Y-m-d') ,['class' => 'form-control date-inputmask','id' => 'joining_date', 'placeholder' => 'Enter Date' ]) !!}            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Submit</button>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div> 
    </div>
    <!-- /.container-fluid -->
</div>
<script>
    $body = $("body");
    $(document).on('click', '.caret-icon', function() {
        $(this).toggleClass('fa-caret-up fa-caret-down');
    });
    $(document).on('click','.isCondition',function(e){
        var condition = $(this).attr('condition');
        console.log(condition);
        var condition_for = $(this).attr('condition-for');
        $('#team_id').val(condition_for);
        $('#condtionIs').val(condition);
        if(condition == 'isRejoin')
        {    
            $('#exampleModalLabel').text('Employee Joining'); 
        }
    });
    $(document).ready(function () {
        var table = $('#team-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/team/resign-data')}}',
                data: function (d) { 
                    d.team_name = $('input[name=team_name]').val();
                 }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'emp_id', name: 'emp_id'},
                {data: 'admin_name', name: 'admin_name'},
                {data: 'email', name: 'email'},
                {data: 'joining_date', name: 'joining_date'},
                {data: 'leaving_date', name: 'leaving_date'},
                {data: 'action', name: 'action'},
            ]
        });
        $('#search-form').on('submit', function(e) {
           table.draw();
           e.preventDefault();
        });
        $('#clearBtn').click(function(){
           location.reload();
        })
    });
    jQuery(document).ready(function () {
        $("#resign-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                leaving_date: {
                    required: true,
                },
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            }
        }); 
    });
</script>

@endsection






