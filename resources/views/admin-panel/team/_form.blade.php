@if(isset($team['admin_id']) && !empty($team['admin_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif
{!! Form::hidden('admin_id',old('admin_id',isset($team['admin_id']) ? $team['admin_id'] : ''),['class' => 'gui-input', 'id' => 'team_id', 'readonly' => 'true']) !!}
@if($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{$errors->first()}}
    </div>
@endif
<div class="clearfix"></div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.team_name') !!}">{!! trans('language.team_name') !!} <span class="red-text">*</span></label>
        {!! Form::text('team_name', old('team_name',isset($team['admin_name']) ? $team['admin_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.team_name'), 'id' => 'team_name','for'=>trans('language.team_name')]) !!}
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.team_email') !!}">{!! trans('language.team_email') !!} <span class="red-text">*</span></label>
        {!! Form::email('team_email', old('team_email',isset($team['email']) ? $team['email']: ''), ['class' => 'form-control','placeholder'=>trans('language.team_email'), 'id' => 'team_email','for'=>trans('language.team_email')]) !!}
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.team_phone') !!}">{!! trans('language.team_phone') !!} <span class="red-text">*</span></label>
        {!! Form::text('team_phone', old('team_phone',isset($team['phone']) ? $team['phone']: ''), ['class' => 'form-control','placeholder'=>trans('language.team_phone'), 'id' => 'team_phone','for'=>trans('language.team_phone')]) !!}
    </div>
</div>
<div class="clearfix"></div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.team_department') !!}">{!! trans('language.team_department') !!} <span class="red-text">*</span></label>
        {!!Form::select('team_department', $team['arr_department'], isset($team['department_id']) ? $team['department_id'] : '', ['class' => 'widthFull selectpicker', 'data-style' => 'form-control', 'id'=>'team_department'])!!}
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.team_time_slot') !!}">{!! trans('language.team_time_slot') !!} <span class="red-text">*</span></label>
        {!!Form::select('team_time_slot', $team['arr_time_slot'], isset($team['time_slot_id']) ? $team['time_slot_id'] : '', ['class' => 'widthFull selectpicker', 'data-style' => 'form-control', 'id'=>'team_time_slot'])!!}
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.saturday_off') !!}">{!! trans('language.saturday_off') !!} <span class="red-text">*</span></label>
        {!!Form::select('saturday_off', $team['arr_saturday_off'], isset($team['saturday_off'])? $team['saturday_off'] : '' ,['class' => 'widthFull selectpicker', 'data-style' => 'form-control', 'id'=>'saturday_off']) !!}
    </div>
</div>
<div class="clearfix"></div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group" id="datepickerbox">
        <label for="{!! trans('language.joining_date') !!}">{!! trans('language.joining_date') !!} <span class="red-text">*</span></label>
        {!!Form::text('joining_date', old('joining_date', isset($team['joining_date']) ? $team['joining_date'] : '') ,['class' => 'form-control','id' => 'joining_date', 'placeholder' => 'yyyy-mm-dd']) !!}            
    </div>    
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.user_role') !!}">{!! trans('language.user_role') !!} <span class="red-text">*</span></label>
        {!!Form::select('team_roles[]', $user_roles, isset($selected_roles) ? $selected_roles : '', ['class' => ' widthFull selectpicker', 'multiple', 'data-style' => 'form-control' ,'id'=>'team_roles'])!!}
    </div>            
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.team_platform') !!}">{!! trans('language.team_platform') !!} 
        {{-- <span class="mytooltip tooltip-effect-2"> <span class="fas fa-info-circle"></span> <span class="tooltip-content clearfix"> <span class="tooltip-text-1">You can select multiple platforms here. Click on platform to select them.</span> </span> </span> --}}
        </label>
        {!!Form::select('team_platform[]', $team['arr_platform'], isset($team['platforms']) ? explode(",",$team['platforms']) : '', ['class' => 'widthFull selectpicker', 'multiple', 'data-style' => 'form-control','id'=>'team_platform'])!!}
    </div>
</div>
<div class="clearfix"></div>
<div class=" col-sm-12 col-md-4">
    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10 no-radius submitFormButton">{!! $submit_button !!}</button>
</div>
<div class="clearfix"></div>
{!! Html::script('public/admin/plugins/bower_components/switchery/dist/switchery.min.js') !!}
<script type="text/javascript">
    $('.submitFormButton').click(function (){
        $(".submitFormButton").html('<div class="loader" style=""><img src="{{url("public/assets/images/loading.gif")}}" alt="img loading"></div>');
    });
    jQuery(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[A-Za-z0-9\s\-\_]+$/i.test(value);
        }, "Only alphabetical, Number, hyphen and underscore allow");

        jQuery.validator.addMethod("validIndianNumber", function(value, element) {
            return this.optional(element) || /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[56789]\d{9}$/i.test(value);
        }, "Please enter valid phone no.");

        $("#teamForm").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                team_name: {
                    required: true,
                    lettersonly: true,
                },
                team_email: {
                    required: true,
                    email: true,
                },
                team_phone: {
                    required: true,
                    maxlength:10,
                    minlength:10
                },
                team_department: {
                    required: true,
                },
                team_time_slot: {
                    required: true,
                },
                team_platform: {
                    required: true,
                },
                saturday_off: {
                    required: true,
                },
                joining_date: {
                    required: true,
                },
                'team_roles[]': {
                    required: true,
                },
            },
            messages: { 
                team_phone: {
                    minlength: "Please enter valid phone number",
                    maxlength: "Please enter valid phone number",
                },
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Save');
                $( element ).parents( ".input-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Save');
                $( element ).parents( ".input-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                $(".submitFormButton").html('Save');
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        }); 
    });
    jQuery('#joining_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "yyyy-mm-dd",
        container:'#datepickerbox'
    });
</script>