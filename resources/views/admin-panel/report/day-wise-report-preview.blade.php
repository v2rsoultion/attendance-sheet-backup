@if(!empty($final_array))
<table class="table dataTable table-bordered" id="table-preview">
    <thead>
        <tr>
            <th>Sr.</th>
            <th>Date</th>
            <th>Project Name</th>
            <th>Work Type</th>
            <th>Description</th>
            <th>Working Hrs</th>
        </tr>
    </thead>
    <tbody>
    @php 
        $i = 1; 
        $total_working_hrs = 0;
    @endphp
    @foreach ($final_array as $data)
        @php $work_time_date = $data['work_time_date']; 
            $counter = 1;
        @endphp
        @foreach ($data['details'] as $details)
            @php 
                $counter_project = 1;
                $project_name = $details['project_name']; 
            @endphp
            @foreach ($details['details'] as $details_project)
                <tr>
                    @if($counter == 1) <td rowspan="{{ $data['count'] }}">{!! $i!!}</td>@endif
                    @if($counter == 1) <td rowspan="{{ $data['count'] }}">{!! date_create($work_time_date)->format('d-m-Y') !!}</td>@endif
                    @if($counter_project == 1) <td rowspan="{{ count($details['details']) }}">{!! $project_name !!}</td>@endif
                    <td>{!! $details_project['work_type_name'] !!}</td>
                    <td>{!! $details_project['work_time_detail_description'] !!}</td>
                    <td>{!! $details_project['work_time_detail_hrs'] !!}</td>
                </tr>
                @php
                    $counter_project++; 
                    $counter++ ; 
                    $total_working_hrs += $details_project['work_time_detail_hrs'];
                @endphp
            @endforeach
        @endforeach
        @php $i++ ; @endphp
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th colspan="6" class="text-right">
                Total Working Hrs: {!! $total_working_hrs !!}
            </th>
        </tr>
    </tfoot>

    {{-- @php 
        $i = 1; 
        $total_working_hrs = 0;
    @endphp
    @foreach ($final_array as $data)
        @php $work_time_date = $data['work_time_date']; 
            $counter = 1;
        @endphp
        @foreach ($data['details'] as $details)
            <tr>
                @if($counter == 1) <td rowspan="{{ count($data['details']) }}">{!! $i!!}</td>@endif
                @if($counter == 1) <td rowspan="{{ count($data['details']) }}">{!! date_create($work_time_date)->format('d-m-Y') !!}</td>@endif
                <td>{!! $details['project_name'] !!}</td>
                <td>{!! $details['work_type_name'] !!}</td>
                <td>{!! $details['work_time_detail_description'] !!}</td>
                <td>{!! $details['work_time_detail_hrs'] !!}</td>
            </tr>
            @php 
                $counter++ ; 
                $total_working_hrs += $details['work_time_detail_hrs'];
            @endphp
        @endforeach
        @php $i++ ; @endphp
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th colspan="6" class="text-right">
                Total Working Hrs: {!! $total_working_hrs !!}
            </th>
        </tr>
    </tfoot> --}}
    {{-- @php $counter = 1;  @endphp
    @foreach ($summeryByWorkType as $key => $value)
        <tr>
            @if($counter == 1) <th rowspan="{{ count($summeryByWorkType) }}"></th> @endif
            @if($counter == 1) <th rowspan="{{ count($summeryByWorkType) }}">Summary By Work Type</th> @endif
            <th>{!! $value['work_type_name'] !!}</th>
            <th>{!! array_sum(array_column($value['time'],'total_hour')) !!}</th>
        </tr>
        @php $counter++;  @endphp
    @endforeach
    @php $counter = 1;  @endphp
    @foreach ($final_array as $key => $value)
        <tr>
            @if($counter == 1) <th rowspan="{{ count($final_array) }}"></th> @endif
            @if($counter == 1) <th rowspan="{{ count($final_array) }}">Summary By Project</th> @endif
            <th>{!! $value['project_name'] !!}</th>
            <th>{!! array_sum(array_column($value['details'],'total_hour')) !!}</th>
        </tr>
        @php $counter++;  @endphp
    @endforeach --}}
</table>
@endif
