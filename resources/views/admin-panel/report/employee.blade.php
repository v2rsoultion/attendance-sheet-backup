@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="#">{!! trans('language.report'); !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ session()->get('success') }}
                    </div>
                    @endif
                    @if($errors->any())
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{$errors->first()}}
                    </div>
                    @endif
                    <div class="clearfix"></div>
                    <div class="row clearfix">
                        {!! Form::open(['files'=>TRUE, 'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="col-lg-3 col-md-3">
                            <div class="input-group">
                                {!!Form::select('employee_name', $employee_list, '', ['class' => 'form-control
                                select2','id'=>'employee_name'])!!}
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="input-group">
                                <input class="form-control input-daterange-datepicker" type="text" name="report_date"
                                    id="report_date" value="{!! date('d/m/Y') !!}" />
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="input-group">
                                {!!Form::select('projects', $project_list, '', ['class' => 'form-control
                                select2','id'=>'projects'])!!}
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="input-group">
                                {!!Form::select('work_types', $work_type_list, '', ['class' => 'form-control
                                select2','id'=>'work_types'])!!}
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            {!! Form::submit('Search', ['class' => 'btn btn-info no-radius m-t-10 m-r-10', 'id' => 'search',
                            'name'=>'Search']) !!}
                            {!! Form::button('Clear', ['class' => 'btn btn-inverse no-radius m-t-10','name'=>'Clear', 'id' =>
                            "clearBtn"]) !!}
                        </div>
                        {{-- <div class="col-lg-1 col-md-1">
                        </div> --}}
                        <div class="col-lg-2 col-md-2 pull-right m-t-10 ">
                            <div class="input-group">
                                <select class="form-control select2" id="report_type" name="report_type">
                                    <option value="1" selected="selected">Complete</option>
                                    <option value="2">Day Wise</option>
                                </select>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-sm-12" id="reportViewBlock" style="display:none;">
                <div class="white-box">
                    <div class="col-md-12 text-center" id="recordNotFound" style="display:none;">Record Not Found.<br></div>
                    <button type="button" class="btn btn-info no-radius m-t-10 m-r-10 m-b-10" id="printView" style="display:none;"><span class="fas fa-print m-r-5"></span>Print</button>                
                    <button type="button" class="btn btn-info no-radius m-t-10 m-b-10" id="excelView" style="display:none;"><span class="fas fa-file-excel m-r-5"></span>Excel</button>                    
                    <div class="table-responsive" id="data_preview"></div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <script>
        $(document).ready(function () {
            // Daterange picker
            $('.input-daterange-datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-danger',
                cancelClass: 'btn-inverse'
            });

            function getReportData(call_type){
                employee_name     =       $('#employee_name').val();
                report_date       =       $('#report_date').val();
                project_id        =       $('#projects').val();
                work_type_id      =       $('#work_types').val();
                report_type       =       $('#report_type').val();
                $('#data_preview').empty();
                $('#printView').css('display','none');
                $('#excelView').css('display','none');
                $('#recordNotFound').css('display', 'none');
                $('.preloader').css('display','inline');
                $.ajax({
                    url: "{{url('admin-panel/report/data-view-employee')}}",
                    data: {
                        employee_name: employee_name, 
                        report_date: report_date,
                        project_id: project_id,
                        work_type_id: work_type_id,
                        report_type: report_type
                    },
                    type: 'get',
                    success: function (response) {
                        $('.preloader').css('display','none');
                        if(response.status == 'error'){
                            $('#printView').css('display','none');
                            $('#excelView').css('display','none');
                            $('#reportViewBlock').css('display', 'inline');
                            $('#recordNotFound').css('display', 'block');
                        }
                        if(response.status == 'success'){
                            $('#printView').css('display','inline');
                            $('#excelView').css('display','inline');
                            $('#recordNotFound').css('display', 'none');
                            $('#reportViewBlock').css('display', 'inline');

                            if(call_type === 'table')
                            {
                                $('#data_preview').html(response.data);
                            }
                            else if(call_type === 'print'){
                                // Fill Table Data in preview
                                $('#data_preview').html(response.data);
                                var content = response.data;
                                var frame1 = document.createElement('iframe');
                                frame1.name = "frame1";
                                frame1.style.position = "absolute";
                                frame1.style.top = "-1000000px";
                                document.body.appendChild(frame1);
                                var frameDoc = (frame1.contentWindow) ? frame1.contentWindow : (frame1.contentDocument.document) ? frame1.contentDocument.document : frame1.contentDocument;
                                frameDoc.document.open();
                                frameDoc.document.write('<html><head><title></title>');
                                frameDoc.document.write('<style>table {  border-collapse: collapse;  border-spacing: 0; width:100%; margin-top:20px;} .table td, .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{ padding:8px 18px;  } .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {     border: 1px solid #e2e2e2;} </style>');
                                frameDoc.document.title = "Employee Work Report";
                                frameDoc.document.write('</head><body>');
                                frameDoc.document.write('<h3>Employee Name: '+ $('#employee_name option:selected').html() +'</h3>')
                                frameDoc.document.write('<h3>Date Range: '+ $('#report_date').val() +'</h3>')
                                frameDoc.document.write(content);
                                frameDoc.document.write('</body></html>');
                                frameDoc.document.close();
                                setTimeout(function () {
                                    window.frames["frame1"].focus();
                                    window.frames["frame1"].print();
                                    document.body.removeChild(frame1);
                                }, 500);
                                return false;
                            }
                        }
                    }
                });
            }
            
            $('#search-form').on('submit', function (e) {
                var htmls = getReportData('table');
                e.preventDefault();
            });
            $('#clearBtn').click(function () {
                location.reload();
            });
            $(document).on('click','#printView',function(){
                getReportData('print');
            });
            $(document).on('click','#excelView',function(){
                var htmls = "";
                var uri = 'data:application/vnd.ms-excel;base64,';
                var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'; 
                var base64 = function(s) {
                    return window.btoa(unescape(encodeURIComponent(s)))
                };
                var format = function(s, c) {
                    return s.replace(/{(\w+)}/g, function(m, p) {
                        return c[p];
                    })
                };
                htmls  = '<h4>Employee Name: '+ $('#employee_name option:selected').html() +'</h4>';
                htmls += '<h4>Date Range: '+ $('#report_date').val() +'</h4>';
                htmls += $('#table-preview').html();
                var ctx = {
                    worksheet : 'Worksheet',
                    table : htmls
                }
                var link = document.createElement("a");
                link.download = "employee_report.xls";
                link.href = uri + base64(format(template, ctx));
                link.click();
            });
            $('#employee_name').change(function(){
                employee_id     =       $('#employee_name').val();
                $('#projects').empty();
                $('#work_types').empty();
                $('#projects').append('<option value="" selected>Select Project</option>');
                $('#work_types').append('<option value="" selected>Select Work Type</option>');
                if(employee_id != ''){
                    $('.preloader').css('display','inline');
                    $.ajax({
                            url: "{{url('admin-panel/report/get-employee-project')}}",
                            data: {employee_id: employee_id},
                            type: 'get',
                        success: function (response) {
                            $('.preloader').css('display','none');
                            if(response.status == 'success'){
                                $.each( response.projects, function( key, value ) {
                                    opt = '<option value="'+key+'">' + value + '</option>';
                                    $('#projects').append(opt);
                                });
                                $.each( response.work_types, function( key, value ) {
                                    opt = '<option value="'+key+'">' + value + '</option>';
                                    $('#work_types').append(opt);
                                });
                            }
                        }
                    });
                }
            });

            /*
            var table = $('#report-employee-table').DataTable({
                pageLength: 20,
                processing: true,
                serverSide: true,
                bLengthChange: false,
                dom: 'Blfrtip',
                buttons: [{
                        extend: 'excelHtml5',
                        "text": '<span class="glyphicons glyphicons-file_export"></span> &nbsp; Export',
                        "title": 'Employee Report',
                        "message": 'Employee Name: ' + $('#employee_id option:selected').text() + '      Work Duration: ' + $('#report_date').val(),
                        "filename": 'employee-report',
                        header: true,
                        footer: true,
                        exportOptions: {
                            modifier: {
                                selected: true
                            }
                        },
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            // s = for styling and 5 will make it left align with grey background
                            $('row:first c', sheet).attr('s', '5');
                        }
                    },
                    {
                        extend: 'print',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'Employee Report',
                        "message": 'Employee Name: ' + $('#employee_name option:selected').text() + '      Work Duration: ' + $('#report_date').val(),
                        "filename": 'employee-report',
                        header: true,
                        footer: true,
                        exportOptions: {
                            modifier: {
                                selected: true
                            }
                        }
                    }
                ],
                ajax: {
                    url: "{{url('admin-panel/report/data-view-employee')}}",
                    data: function (d) {
                        d.employee_name = $('#employee_name').val();
                        d.report_date = $('#report_date').val();
                    }
                },
                columns: [
                    //{data: 'DT_Row_Index', name: 'DT_Row_Index'},
                    {
                        data: 'project_name',
                        name: 'project_name'
                    },
                    {
                        data: 'work_type_name',
                        name: 'work_type_name'
                    },
                    {
                        data: 'total_hour',
                        name: 'total_hour'
                    },
                ],
                rowsGroup: ['project_name:name', 0],
                footerCallback: function (row, data, start, end, display) {
                    var api = this.api();

                    var aCol = api.column(2);

                    var foo = aCol.data(); //is empty!

                    $(aCol.footer()).html(

                    );
                },
            });
            */
        });
    </script>
    @endsection