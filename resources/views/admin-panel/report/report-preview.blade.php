@if(!empty($final_array))
<table class="table dataTable table-bordered" id="table-preview">
    <thead>
        <tr>
            <th>Sr.</th>
            <th>Project Name</th>
            <th>Work Type</th>
            <th>Working Hrs</th>
        </tr>
    </thead>
    <tbody>
    @php $i = 1; @endphp
    @foreach ($final_array as $data)
        @php $project_name = $data['project_name']; 
            $counter = 1;
        @endphp
        @foreach ($data['details'] as $details)
            <tr>
                @if($counter == 1) <td rowspan="{{ count($data['details']) }}">{!! $i!!}</td>@endif
                @if($counter == 1) <td rowspan="{{ count($data['details']) }}">{!! $project_name!!}</td>@endif
                <td>{!! $details['work_type_name'] !!}</td>
                <td>{!! $details['total_hour'] !!}</td>
            </tr>
            @php $counter++ ; @endphp
        @endforeach
        @php $i++ ; @endphp
    @endforeach
    @php $counter = 1;  @endphp
    @foreach ($summeryByWorkType as $key => $value)
        <tr>
            @if($counter == 1) <th rowspan="{{ count($summeryByWorkType) }}"></th> @endif
            @if($counter == 1) <th rowspan="{{ count($summeryByWorkType) }}">Summary By Work Type</th> @endif
            <th>{!! $value['work_type_name'] !!}</th>
            <th>{!! array_sum(array_column($value['time'],'total_hour')) !!}</th>
        </tr>
        @php $counter++;  @endphp
    @endforeach
    @php $counter = 1;  @endphp
    @foreach ($final_array as $key => $value)
        <tr>
            @if($counter == 1) <th rowspan="{{ count($final_array) }}"></th> @endif
            @if($counter == 1) <th rowspan="{{ count($final_array) }}">Summary By Project</th> @endif
            <th>{!! $value['project_name'] !!}</th>
            <th>{!! array_sum(array_column($value['details'],'total_hour')) !!}</th>
        </tr>
        @php $counter++;  @endphp
    @endforeach
</table>
@endif
