@if(!empty($final_array))
<table class="table dataTable table-bordered" id="table-preview">
    <thead>
        <tr>
            <th>Sr.</th>
            <th>Work Type</th>
            <th>Employee</th>
            <th>Involvement Days</th>
            <th>Working Hrs</th>
        </tr>
    </thead>
    <tbody>
    @php $i = 1; @endphp
    @foreach ($final_array as $data)
        @php $work_type_name = $data['work_type_name']; 
            $counter = 1;
        @endphp
        @foreach ($data['details'] as $details)
            <tr>
                @if($counter == 1) <td rowspan="{{ count($data['details']) }}">{!! $i!!}</td>@endif
                @if($counter == 1) <td rowspan="{{ count($data['details']) }}">{!! $work_type_name!!}</td>@endif
                <td>{!! $details['admin_name'] !!}</td>
                <td>{!! $details['involvement_days'] !!}</td>
                <td>{!! $details['total_hour'] !!}</td>
            </tr>
            @php $counter++ ; @endphp
        @endforeach
        @php $i++ ; @endphp
    @endforeach
    @php $counter = 1;  @endphp
    @foreach ($summeryByEmployee as $key => $value)
        <tr>
            @if($counter == 1) <th rowspan="{{ count($summeryByEmployee) }}"></th> @endif
            @if($counter == 1) <th rowspan="{{ count($summeryByEmployee) }}">Summary By Employee</th> @endif
            <th>{!! $value['admin_name'] !!}</th>
            <th>{!! $value['involvement_days'] !!}</th>
            <th>{!! $value['total_hour'] !!}</th>
        </tr>
        @php $counter++;  @endphp
    @endforeach
    @php $counter = 1;  @endphp
    @foreach ($summeryByWorkType as $key => $value)
        <tr>
            @if($counter == 1) <th rowspan="{{ count($final_array) }}"></th> @endif
            @if($counter == 1) <th rowspan="{{ count($final_array) }}">Summary By Work Type</th> @endif
            <th>{!! $value['work_type_name'] !!}</th>
            <th>{!! $value['involvement_days'] !!}</th>
            <th>{!! $value['total_hour'] !!}</th>
        </tr>
        @php $counter++;  @endphp
    @endforeach
</table>
@endif
