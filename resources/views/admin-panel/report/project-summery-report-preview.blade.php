@if(!empty($final_array))
<table class="table dataTable table-bordered" id="table-preview">
    <thead>
        <tr>
            <th>Sr.</th>
            <th>Project</th>
            <th>Work Type</th>
            <th>Working Hrs</th>
        </tr>
    </thead>
    <tbody>
    @php $i = 1; @endphp
    @foreach ($final_array as $data)
        @php $project_name = $data['project_name']; 
            $counter = 1;
        @endphp
        @foreach ($data['details'] as $details)
            <tr>
                @if($counter == 1) <td rowspan="{{ count($data['details']) }}">{!! $i!!}</td>@endif
                @if($counter == 1) <td rowspan="{{ count($data['details']) }}">{!! $project_name!!}</td>@endif
                <td>{!! $details['work_type_name'] !!}</td>
                <td>{!! number_format($details['total_hour'], 2) !!}</td>
            </tr>
            @php $counter++ ; @endphp
        @endforeach
        @php $i++ ; @endphp
    @endforeach
</table>
@endif
