@if(!empty($final_array))
<table class="table dataTable table-bordered" id="table-preview">
    <thead>
        <tr>
            <th>Sr.</th>
            <th>Project</th>
            <th>Code Reviewer</th>
            <th>Review Date</th>
            <th>Coder</th>
            <th>Review Check</th>
            <th>Comments</th>
            <th>Review Status</th>
            {{-- <th>Counter</th> --}}
        </tr>
    </thead>
    <tbody>
    @php $i = 1; @endphp
    @foreach ($final_array as  $p_id => $data)
        @php $project_name = $data['project_name'];
        $p = 1; 
        @endphp
        @foreach ($data['details'] as $c_key => $details)

            @php $code_reviewer_id = $details['code_reviewer_id']; 
                $c = 1;             
            @endphp
            @foreach ($details['code_reviewer_details'] as $d_key => $code_review_details)
                @php $review_date = $code_review_details['review_date']; 
                    $d = 1;
                @endphp
                @foreach ($code_review_details['review_date_details'] as $coder_details)
                    @php 
                        $dev = 1;
                    @endphp
                    @foreach ($coder_details['coder_details'] as $log)
                        <tr>
                            @if($p == 1)<td class="alignCenterMiddel" rowspan="{{ $p_row[$p_id] }}">{!! $i!!}</td>@endif
                            @if($p == 1)<td class="alignCenterMiddel" rowspan="{{ $p_row[$p_id] }}">{!! $project_name!!}</td>@endif
                            @if($c == 1)<td class="alignCenterMiddel" rowspan="{{ $c_row[$p_id][$c_key] }}">{!! $code_reviewer_arr[$code_reviewer_id] !!}</td>@endif
                            @if($d == 1)<td class="alignCenterMiddel" rowspan="{{ $d_row[$p_id][$c_key][$d_key] }}">{!! date_format(date_create($review_date), 'd-M-Y') !!}</td>@endif
                            @if($dev == 1)<td class="alignCenterMiddel" rowspan="{{ count($coder_details['coder_details']) }}">{!! $employee_arr[$coder_details['coder_id']] !!}@endif</td>
                            <td>{!! $review_check_list[$log['review_check_id']] !!}</td>
                            <td>{!! $log['comments'] !!}</td>
                            @php 
                                $className = '';
                                if($log['review_status'] == 1){
                                    $className = 'statusOkey';
                                }else if($log['review_status'] == 2){
                                    $className = 'statusNotOkey';
                                }else if($log['review_status'] == 3){
                                    $className = 'statusOkeyButClosed';
                                }
                            @endphp
                            <td class="{{ $className }}">{!! $code_review_status_arr[$log['review_status']] !!}</td>
                        </tr>
                        @php
                            $p++;
                            $c++;
                            $d++;
                            $dev++;
                        @endphp
                    @endforeach
                @endforeach
            @endforeach
            @php $i++ ; @endphp
        @endforeach
    @endforeach
</table>
@endif
