{!! Form::hidden('user_role_id',old('user_role_id',isset($user_role_id) ? $user_role_id : ''),['class' => 'gui-input', 'id' => 'user_role_id', 'readonly' => 'true']) !!}
@if($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{$errors->first()}}
    </div>
@endif
<div class="clearfix"></div>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-10">
    <div class="input-group">
        <label>Select Responsibility <span class="red-text">*</span></label>
        {!!Form::select('selected-responsibility[]', $responsibility_list_arr, isset($selected_responsibility) ? $selected_responsibility : '', ['multiple', 'class' => 'multiselect', 'required' =>'true' ,'id'=>'selected-responsibility'])!!}
    </div>
    <div class="button-box m-t-20"> 
        <a id="select-all" class="btn btn-success no-radius" href="#">select all</a> 
        <a id="deselect-all" class="btn btn-primary no-radius" href="#">deselect all</a> 
    </div>
</div>
<div class="clearfix"></div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-10">
    <div class="input-group">
        <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10 no-radius p-l-30 p-r-30">{!! $submit_button !!}</button>
    </div>
</div>
<div class="clearfix"></div>
<script>
    $(function() {
        // For select 2
        $(".select2").select2();
        $('.selectpicker').selectpicker();
        // For multiselect
        $('#pre-selected-options').multiSelect();
        $('#optgroup').multiSelect({
            selectableOptgroup: true
        });
        $('#selected-responsibility').multiSelect();
        $('#select-all').click(function() {
            $('#selected-responsibility').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function() {
            $('#selected-responsibility').multiSelect('deselect_all');
            return false;
        });
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#responsibility-assign-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                'selected-responsibility[]': 'required'
            },
            messages: { 
                'selected-responsibility[]': "At least one responsibility required for role assign.",
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
             highlight: function (element, errorClass, validClass) {
                $( element ).parents( ".input-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".input-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                console.log(error);
                console.log(element);
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
                if (element.hasClass('multiselect')) {
                    // custom placement for hidden select
                    error.insertAfter(element.next('.btn-group'))
                } else {
                    // message placement for everything else
                    error.insertAfter(element);
                }
            }
        }); 
    });
</script>