@extends('admin-panel.layout.header')

@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="{!! $redirect_url !!}">{!! trans('language.view_user_role') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box white-box-modify">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="input-group">
                            <label class="p-r-20" for="{!! trans('language.user_role_name') !!}">{!! trans('language.user_role_name') .':  '!!}</label>{!! isset($user_role['user_role_name']) ? $user_role['user_role_name'] : '----' !!}
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="input-group">
                            <label class="p-r-20" for="{!! trans('language.user_role_status') !!}">{!! trans('language.user_role_status') .':  '!!}</label>{!! isset($user_role['user_role_status']) ? $user_role_status[$user_role['user_role_status']] : '----' !!}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'responsibility-assign-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                        @include('admin-panel.user-role.responsibility-assign._form',['submit_button' => $submit_button])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

