@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="#">{!! trans('language.roles_responsibility'); !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    <div class="alert alert-success alert-dismissible" id="ChangeStatusMessage" style="display:none;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        Status Successfully Changed.
                    </div>
                    <div class="clearfix"></div>
                    
                    {!! Form::open(['files'=>TRUE,'id' => 'user-role-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                        @include('admin-panel.user-role._form',['submit_button' => $submit_button])
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table dataTable " id="user-role-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.user_role_name') !!}</th>
                                    <th>{!! trans('language.assign_responsibility') !!}</th>
                                    <th>{!! trans('language.isWorkTimeRequired') !!}</th>
                                    <th>{!! trans('language.action') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->        
    </div>
    <!-- /.container-fluid -->
</div>
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();

        var table = $('#user-role-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/userrole/data')}}',
                data: function (d) { 
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'user_role_name', name: 'user_role_name'},
                {data: 'assignResponsibility', name: 'assignResponsibility'},
                {data: 'isWorkTimeRequired', name: 'isWorkTimeRequired'},
                {data: 'action', name: 'action'},
            ],
            columnDefs: [
                { "targets": [2,4], "orderable": false }
            ]
        });
        $('#search-form').on('submit', function(e) {
           table.draw();
           e.preventDefault();
        });
        $('#clearBtn').click(function(){
           location.reload();
        });
    });
    function changeStatus(id,status){
        var token = '{!!csrf_token()!!}';
        $('.preloader').css('display','inline');
        $.ajax(
        {
            url: "{{ url('admin-panel/userrole/userrole-status') }}",
            type: 'GET',
            data: {
                'user_role_id': id,
                'user_role_status': status,
            },
            success: function (res)
            {
                $('.preloader').css('display','none');
                if (res == "Success")
                {
                    var table = $('#user-role-table').DataTable();
                    table.ajax.reload( null, false );
                    $('#ChangeStatusMessage').fadeIn(function() {
                        setTimeout(function() {
                            $('#ChangeStatusMessage').fadeOut();
                        }, 2500);
                    });
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }
        });
    }
</script>
@endsection






