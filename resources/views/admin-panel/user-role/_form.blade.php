@if(isset($user_role['user_role_id']) && !empty($user_role['user_role_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('user_role_id',old('user_role_id',isset($user_role['user_role_id']) ? $user_role['user_role_id'] :
''),['class' => 'gui-input', 'id' => 'user_role_id', 'readonly' => 'true']) !!}
@if($errors->any())
<div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    {{$errors->first()}}
</div>
@endif
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="{!! trans('language.user_role_name') !!}">{!! trans('language.user_role_name') !!} <span class="red-text">*</span></label>
        {!! Form::text('user_role_name', old('user_role_name',isset($user_role['user_role_name']) ?
        $user_role['user_role_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.user_role_name'),
        'id' => 'user_role_name','for'=>trans('language.user_role_name')]) !!}
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 m-t-30">
    <div class="input-group">
        <div class="checkbox checkbox-success">
            <input id="isWorkTimeRequired" name="isWorkTimeRequired" type="checkbox" @if(isset($user_role['isWorkTimeRequired']) && $user_role['isWorkTimeRequired'] == 1) checked @endif>
            <label for="isWorkTimeRequired"> Work Time Required </label>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-4 col-sm-12">
    <button type="submit" name="submit" class="btn btn-info btn-md waves-effect waves-light m-t-10 m-r-5 no-radius">{!!
        $submit_button !!}</button>
    <button type="reset" name="reset" id="reset" class="btn btn-inverse btn-md waves-effect waves-light m-t-10 no-radius">Clear</button>
</div>
<div class="clearfix"></div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#user-role-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                user_role_name: {
                    required: true,
                }
            },
            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Save');
                $(element).parents(".input-group").addClass("has-error").removeClass("has-success");
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Save');
                $(element).parents(".input-group").addClass("has-success").removeClass("has-error");
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                $(".submitFormButton").html('Save');
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
    });
    $(document).on('click', '#reset', function (e) {
        window.location = "{!! URL::to('admin-panel/userrole/view-userrole') !!}";
    })
</script>