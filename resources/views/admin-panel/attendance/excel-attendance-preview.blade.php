@if(!empty($data_emp))
<div class="col-sm-12">
    <div class="white-box" style="text-align: center;
    font-weight: 500;">
        <span style="font-size:20px;">Attendance of {!! $attendance_month !!}</span>
        <div class="table-responsive">
            <table class="table dataTable " id="preview-table">
                <thead>
                    <tr>
                        <th>Sr.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Full Leaves</th>
                        <th>Half Leaves</th>
                        <th>DHalf Leaves</th>
                        <th>Late Days</th>
                        <th>Extra Working Hrs</th>
                    </tr>
                </thead>
                <tbody>
                @php $i = 1; @endphp
                @foreach ($data_emp as $data)
                    <tr>
                        <th>{!! $i++ !!}</th>
                        <th>{!! $data['admin_name'] !!}</th>
                        <th>{!! $data['email'] !!}</th>
                        <th>{!! $data['full_leaves'] !!}</th>
                        <th>{!! $data['half_leaves'] !!}</th>
                        <th>{!! $data['dhalf_leaves'] !!}</th>
                        <th>{!! $data['late_days'] !!}</th>
                        <th>{!! $data['extra_working_hrs'] !!}</th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-sm-12 col-md-5">
    <a href="{!! $save_url !!}" role="button" class="btn btn-info waves-effect waves-light m-t-10 m-r-10 no-radius">Save Attendance</a>
    <a href="{!! $cancel_url !!}" role="button" class="btn btn-inverse waves-effect waves-light m-t-10 no-radius">Cancel</a>
</div>
@endif
<script>
$(document).ready(function () {
    var table = $('#preview-table').DataTable({
        "bPaginate": false,
        "bFilter": true,
        "bInfo": false,
        "bAutoWidth": false 
    });
});
</script>
