@extends('admin-panel.layout.header')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-md-4">
                <div class="white-box">
                    <h3 class="box-title">Attendance Data</h3>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <blockquote>
                                <span><h4>Calendar Days: <span id="calendar_days"></span></h4></span>
                                <span><h4>Sundays: <span id="sundays"></span></h4></span>
                                <span><h4>Holidays: <span id="holidays"></span></h4></span>
                                <span><h4>Working Days: <span id="company_working_days"></span></h4></span>
                            </blockquote>
                            <blockquote id="ifSaturdayNotOff" style="display:none;">
                                <span><h4>Saturday Off: <span id="saturdays"></span></h4></span>
                                <span><h4>Employee Working Days: <span id="employee_working_days"></span></h4></span>
                            </blockquote>
                            <blockquote class="error">
                                <span><h4>Full Day Leaves: <span id="full_leaves"></span></h4></span>
                                <span><h4>Half Day Leaves: <span id="half_leaves"></span></h4></span>
                                <span><h4>Direct Half Day Leaves: <span id="dhalf_leaves"></span></h4></span>
                                <span><h4>Late Days: <span id="late_days"></span></h4></span>
                                <span><h4>Release Leaves (Utliized): <span id="utilized"></span></h4></span>
                                <span><h4>Total Leaves (after less): <span id="leaves_after_less"></span></h4></span>
                            </blockquote>
                            <blockquote class="success">
                                <span><h4>Extra Working Hrs: <span id="extra_working_hrs"></span></h4></span>
                                <span><h4>Total Paid Days: <span id="total_paid_days"></span></h4></span>
                                <span><h4>Release Leaves (Unutilized): <span id="unutilized"></span></h4></span>
                            </blockquote>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                    <label for="{!! trans('language.base_salary') !!}">{!! trans('language.base_salary') !!} <span class="red-text">*</span></label>
                                        {!! Form::text('base_salary', '', ['class' => 'form-control','placeholder'=>trans('language.base_salary'), 'id' => 'base_salary','for'=>trans('language.base_salary')]) !!}
                                    </div>
                                </div>
                                <div class="clear-fix"></div>
                                <div class="col-md-12 m-t-10">
                                    <div class="col-md-3"></div> 
                                    <div class="col-md-6"> 
                                        <button type="button" class="btn btn-info calSalary no-radius"> Salary Calculator </button>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="clear-fix"></div>
                                <div class="col-md-12 m-t-10">
                                    <div class="input-group">
                                    <label for="{!! trans('language.calculate_salary') !!}">{!! trans('language.calculate_salary') !!}:</label>
                                    <span class="m-l-10" for="{!! trans('language.calculate_salary') !!}" id="calculate_salary" name="calculate_salary"></span> &#x20B9;                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="white-box">
                    <div id="calendar1"></div>
                </div>
                <div class="white-box">
                    <span><strong>Note: -</strong> <span id="description"></span></span>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<script>
    $(document).ready(function () {
        $(document).on('click', '.calSalary', function(e){
            var emp_working_days  =  parseFloat($('#employee_working_days').html());
            var total_paid_days   =  parseFloat($('#total_paid_days').html());
            var base_salary       =  parseFloat($('#base_salary').val());
            var perDaySal         =  base_salary / emp_working_days;
            if($.isNumeric( perDaySal )){
                var calculated_salary  =  perDaySal * total_paid_days;
                $('#calculate_salary').html(Math.round(calculated_salary));
            }
        });
        function callCalendarData() 
        {
            $('.preloader').css('display','inline');
            var moment = $('#calendar1').fullCalendar('getDate');
            console.log('Date:- ' + moment.format('YYYY-MM-DD'));
            $.ajax({
                url: '{{ url("admin-panel/my-attendance/data") }}',
                type: 'GET',
                data: { 'date' : moment.format('YYYY-MM-DD') } ,
                success: function (response) {
                    $('.preloader').css('display','none');
                    if(response.isSaturdayOff ==  true){
                        $('#ifSaturdayNotOff').css('display','block');
                    }
                    else{
                        $('#ifSaturdayNotOff').css('display','none');
                    }
                    if( response.emp_data.length != 0){
                        var data = response.emp_data;
                        $('#calendar_days').html(data['calendar_days']);
                        $('#sundays').html(data['sundays']);
                        $('#holidays').html(data['holidays']);
                        $('#company_working_days').html(data['company_working_days']);
                        $('#saturdays').html(data['saturdays']);
                        $('#employee_working_days').html(data['employee_working_days']);
                        $('#full_leaves').html(data['full_leaves']);
                        $('#half_leaves').html(data['half_leaves']);
                        $('#dhalf_leaves').html(data['dhalf_leaves']);
                        $('#late_days').html(data['late_days']);
                        $('#leaves_after_less').html(data['leaves_after_less']);
                        $('#extra_working_hrs').html(data['extra_working_hrs']);
                        $('#total_paid_days').html(data['total_paid_days']);
                        $('#utilized').html(data['utilized']);
                        $('#unutilized').html(data['unutilized']);
                        $('#description').html(data['log_description']);
                    }
                    else{
                        $('#calendar_days').html('--');
                        $('#sundays').html('--');
                        $('#holidays').html('--');
                        $('#company_working_days').html('--');
                        $('#saturdays').html('--');
                        $('#employee_working_days').html('--');
                        $('#full_leaves').html('--');
                        $('#half_leaves').html('--');
                        $('#dhalf_leaves').html('--');
                        $('#late_days').html('--');
                        $('#leaves_after_less').html('--');
                        $('#extra_working_hrs').html('--');
                        $('#total_paid_days').html('--');
                        $('#utilized').html('--');
                        $('#unutilized').html('--');
                        $('#description').html('Nothing');
                    }
                },
                error: function () {
                    alert("error");
                }
            });
        }
        callCalendarData();
        $('.fc-corner-left, .fc-corner-right').click( function () {
            callCalendarData();
        });
    });
</script>
@endsection






