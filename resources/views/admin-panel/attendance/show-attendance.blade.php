@extends('admin-panel.layout.header')
@section('content')
<style>
    td.details-control {
        background: url(" {{ URL::asset('public/assets/images/details_open.png') }} ") no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url(" {{ URL::asset('public/assets/images/details_close.png') }} ") no-repeat center center;
    }
    .dt-buttons{
        float:right !important;
    }
    .excelButton{
        margin-right:10px;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="#">{!! trans('language.attendance') !!}</a></li>
                    <li><a href="{!! URL::to('admin-panel/attendance/view-attendance') !!}">{!! trans('language.view_attendance') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0"> Calendar: {!! $company_config['month'] !!} </h3>
                    <table class="table p-b-10">
                        <thead>
                            <tr>
                                <th>Calendar Days</th>
                                <th>Sundays</th>
                                <th>Company Holidays</th>
                                <th>Working Days</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="active">
                                <td  scope="row">{!! $company_config['calendar_days'] !!}</td>
                                <td>{!! $company_config['sundays'] !!}</td>
                                <td>{!! $company_config['holidays'] !!}</td>
                                <td class="company-working-days">{!! $company_config['company_working_days'] !!}</td>
                            </tr>
                        </tbody>
                    </table>
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    <h3 class="box-title m-b-0">
                        <span> Employee Information </span>
                    </h3>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table dataTable " id="attendance-data-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Emp ID</th>
                                    <th>Name</th>
                                    <th>Full Leaves</th>
                                    <th>Half Leaves</th>
                                    <th>DHalf Leaves</th>
                                    <th>Late Days</th>
                                    <th>Extra W. Hours</th>
                                    <th>Base Salary</th>
                                    <th>Calculated Salary</th>
                                </tr>
                            </thead>
                            <tbody>
                                    
                            </tbody>
                        </table>
                    </div>
                    {{-- Model Start --}}
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="exampleModalLabel1">New message</h4> </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="recipient-name" class="control-label">Recipient:</label>
                                            <input type="text" class="form-control" id="recipient-name1"> </div>
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Message:</label>
                                            <textarea class="form-control" id="message-text1"></textarea>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Send message</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Model End --}}
                </div>
            </div>
        </div>
        <!-- /.row -->        
    </div>
    <!-- /.container-fluid -->
</div>
<script>
    $(document).ready(function () {
        drawDataTable();

        $(document).on('click','.calSalary',function(e){
            drawDataTable();          
        });
        $(document).on('keyup','.base-sal',function(e){
           $("#calSalary").attr('disabled',true); 
           var total_salary = 0;
            $('.base-sal').each(function( index, value ) {
                var sal = parseFloat($(this).val());
                if(isNaN(sal)){
                    sal = 0;
                }
                total_salary = parseFloat(total_salary) + parseFloat(sal); 
                
            });
            if(total_salary > 0) 
            {
                $("#calSalary").attr('disabled',false);   
            }
            else
            {
                $("#calSalary").attr('disabled',true);   
            }
        });
        $(document).on('keyup','.extraWorkingHrs', function(e){
            var extraHrs = parseFloat($(this).val());
            var id = $(this).attr('name').split('_')[0];
            if(isNaN(extraHrs)){
                extraHrs = 0;
            }
            if(extraHrs > {!! Config::get('custom.max_extra_working_hrs') !!}){
                $('#'+id+'_edit').attr('disabled',true);
                console.log('max-extra-hrs');
            }
            else{
                $('#'+id+'_edit').attr('disabled',false);
            }
        });
        $(document).on('keyup','.full-leave', function(e){
            var days = parseFloat($(this).val());
            var id = $(this).attr('name').split('_')[0];
            if(isNaN(days)){
                days = 0;
            }
            if(days > parseInt($('.company-working-days').html())){
                console.log('max-value-days-FL');
                $('#'+id+'_edit').attr('disabled',true);
            }
            else{
                $('#'+id+'_edit').attr('disabled',false);
            }
        });
        $(document).on('keyup','.half-leave',function(e){
            var days = parseFloat($(this).val());
            var id = $(this).attr('name').split('_')[0];
            if(isNaN(days)){
                days = 0;
            }
            if(days > parseInt($('.company-working-days').html())){
                console.log('max-value-days-HL');
                $('#'+id+'_edit').attr('disabled',true);
            }
            else{
                $('#'+id+'_edit').attr('disabled',false);
            }
        });
        $(document).on('keyup','.dhalf-leave',function(e){
            var days = parseFloat($(this).val());
            var id = $(this).attr('name').split('_')[0];
            if(isNaN(days)){
                days = 0;
            }
            if(days > parseInt($('.company-working-days').html())){
                console.log('max-value-days-DHL');
                $('#'+id+'_edit').attr('disabled',true);
            }
            else{
                $('#'+id+'_edit').attr('disabled',false);
            }
        });
        $(document).on('keyup','.late-days', function(e){
            var days = parseFloat($(this).val());
            var id = $(this).attr('name').split('_')[0];
            if(isNaN(days)){
                days = 0;
            }
            if(days > parseInt($('.company-working-days').html())){
                console.log('max-value-days-LD');
                $('#'+id+'_edit').attr('disabled',true);
            }
            else{
                $('#'+id+'_edit').attr('disabled',false);
            }
        });
        function drawDataTable() {
            $('#attendance-data-table').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                bLengthChange: false,
                paging: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        className: 'excelButton no-radius',
                        "text": '<span class="fas fa-file-excel"></span> &nbsp; Export',
                        "title": 'Employee Attendance Report',
                        "filename": 'employee-attendance-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9],
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        extend: 'print',
                        className: 'excelButton no-radius',
                        "text": '<span class="fa fa-print"></span> &nbsp; Print',
                        "title": 'Employee Attendance Report',
                        "filename": 'employee-attendance-report',
                        exportOptions: {
                            columns: [1, 2, 3, 4, 5, 6, 7, 8, 9],
                            modifier: {
                                selected: true
                            }
                        }
                    },
                    {
                        className: 'calSalary no-radius',
                        "text": '<span class="fas fa-calculator"></span> &nbsp; Salary Calculator',
                        "title": 'Salary Calculator',
                        attr:  {
                            disabled: 'true'
                        }
                    }
                ],
                ajax: {
                    url: '{{url('admin-panel/attendance/attendance-data')}}',
                    data: function (d) { 
                        d.attendance_id   = '{!! isset($attendance_id)? $attendance_id:''; !!}',
                        d.base_sal        =  $('td :input').serialize();
                    }
                },
                columns: [
                    {
                        "class":          "details-control",
                        "orderable":      false,
                        "searchable":     false,
                        "data":           null,
                        "defaultContent": "",
                    },
                    {data: 'emp_id', name: 'emp_id', orderable: false, searchable: false},
                    {data: 'name', name: 'name', orderable: false, searchable: false},
                    {data: 'full_leaves', name: 'full_leaves', orderable: false, searchable: false},
                    {data: 'half_leaves', name: 'half_leaves', orderable: false, searchable: false},
                    {data: 'dhalf_leaves', name: 'dhalf_leaves', orderable: false, searchable: false},
                    {data: 'late_days', name: 'late_days', orderable: false, searchable: false},
                    {data: 'extra_working_hrs', name: 'extra_working_hrs', orderable: false, searchable: false},
                    {data: 'base_salary', name: 'base_salary', orderable: false, searchable: false},
                    {data: 'calculate_salary', name: 'calculate_salary', orderable: false, searchable: false},
                ],
                order: [[1, 'asc']]
            });    
        }
        // Array to track the ids of the details displayed rows
        var detailRows = [];
        $('#attendance-data-table').on( 'click', 'tr td.details-control', function () {
            var tr = $(this).closest('tr');
            var table = $('#attendance-data-table').DataTable();
            var row = table.row( tr );
            var idx = $.inArray( tr.attr('id'), detailRows );
            if ( row.child.isShown() ) {
                tr.removeClass( 'details' );
                row.child.hide();
            }
            else {
                tr.addClass( 'details' );
                row.child( format( row.data() ) ).show();
            }
        });
        // On each draw, loop over the `detailRows` array and show any child rows
        $(document).on('click','.mylabel',function(e){
            var isUpdate = $(this).attr('updateDetail');
            if(isUpdate == 'update'){
                var button_id = $(this).attr('id');
                var id = button_id.split('_');

                $('body').addClass("loading");
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{url('admin-panel/attendance/update-attendance-data')}}",
                    type: 'GET',
                    data: {
                        'emp_id': id[0],
                        'attendance_data_id': $('#' + id[0] + '_ADI').val(),
                        'full_leaves': $('#' + id[0] + '_FL').val(),
                        'half_leaves': $('#' + id[0] + '_HL').val(),
                        'dhalf_leaves': $('#' + id[0] + '_DHL').val(),
                        'LD': $('#' + id[0] + '_LD').val(),
                        'EWD': $('#' + id[0] + '_EWD').val(),
                    },
                    success: function (data) {
                        $('body').removeClass("loading");
                        if(data['status'] == 0){
                            swal(data.message,'', "success");
                            $('#attendance-data-table').DataTable().ajax.reload( null, false );
                        }
                        else{
                            swal('Error',data.message);
                        }
                        $('#' + button_id).html('Edit');
                        $('#' + button_id).attr('updateDetail', 'edit');
                        $('#' + id[0] + '_FL').attr("readonly");
                        $('#' + id[0] + '_HL').attr("readonly");
                        $('#' + id[0] + '_DHL').attr("readonly");
                        $('#' + id[0] + '_LD').attr("readonly");
                        $('#' + id[0] + '_EWD').attr("readonly");
                        
                    }
                });
            }
            else{
                var button_id = $(this).attr('id');
                var id = button_id.split('_');
                $('#' + button_id).html('Update');
                $('#' + button_id).attr('updateDetail', 'update');
                $('#' + id[0] + '_FL').removeAttr("readonly");
                $('#' + id[0] + '_HL').removeAttr("readonly");
                $('#' + id[0] + '_DHL').removeAttr("readonly");
                $('#' + id[0] + '_LD').removeAttr("readonly");
                $('#' + id[0] + '_EWD').removeAttr("readonly");
            }
        });
        function format ( d ) {
            $my_var =      '<table class="table table-bordered" style="background:azure;">' +
                                '<tbody>' +
                                    '<tr>' +
                                        '<th>Off Saturdays</th>' +
                                        '<td>' + d.saturday_off + '</td>' +
                                        '<th>Employee Working Days</th>' +
                                        '<td>' + d.emp_working_days + '</td>' +
                                        '<th>Extra W. Hours <span class="mytooltip tooltip-effect-2"> <span class="fas fa-info-circle"></span> <span class="tooltip-content clearfix"> <span class="tooltip-text-1">Extra working hrs must be less then {!! Config::get('custom.max_extra_working_hrs') !!} hrs.</span> </span> </span></th>' +
                                        '<td class="editBoxAttendance"><input type="number" min="0" max="270" class="form-control month extraWorkingHrs" readonly name="' + d.admin_id + '_EWD' + '" id="' + d.admin_id + '_EWD' + '" value="' + d.extra_working_hrs + '" /></td>' +
                                        '<th>Extra W. Hours Paid Days</th>' +
                                        '<td>' + d.extra_working_hrs_paid_days + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<th>Full Leaves</th>' +
                                        '<td class="editBoxAttendance"><input type="number" min="0" max="' + d.month_days + '" class="form-control month full-leave" readonly name="' + d.admin_id + '_FL' + '" id="' + d.admin_id + '_FL' + '" value="' + d.full_leaves + '" /><input type="hidden" id="' + d.admin_id + '_ADI' + '" value="' + d.attendance_data_id + '" /></td>' +
                                        '<th>Half Leaves</th>' +
                                        '<td class="editBoxAttendance"><input type="number" min="0" max="' + d.month_days + '" class="form-control month half-leave" readonly name="' + d.admin_id + '_HL' + '" id="' + d.admin_id + '_HL' + '" value="' + d.half_leaves + '" /></td>' +
                                        '<th>DHalf Days</th>' +
                                        '<td class="editBoxAttendance"><input type="number" min="0" max="' + d.month_days + '" class="form-control month dhalf-leave" readonly name="' + d.admin_id + '_DHL' + '" id="' + d.admin_id + '_DHL' + '" value="' + d.dhalf_leaves + '" /></td>' +
                                        '<th>Late Days</th>' +
                                        '<td class="editBoxAttendance"><input type="number" min="0" max="' + d.month_days + '" class="form-control month late-days" readonly name="' + d.admin_id + '_LD' + '" id="' + d.admin_id + '_LD' + '" value="' + d.late_days + '" /></td>' +                                        
                                    '</tr>' +
                                    '<tr>' +
                                        '<th>Total Leaves After Less</th>' +
                                        '<td>' + d.leaves_after_less + '</td>' +
                                        '<th>Release Utilized</th>' +
                                        '<td>' + d.utilized + '</td>' +
                                        '<th>Release Unutilized</th>' +
                                        '<td>' + d.unutilized + '</td>' +
                                        '<th>Total Paid Days</th>' +
                                        '<td>' + d.total_paid_days + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                        '<td colspan="6"><span id="' + d.admin_id + '_message' + '" ></span></td>' +
                                        '<td colspan="2" style="text-align:right;"><button style="padding: 10px 20px 10px 20px;" class="btn btn-info mylabel" updateDetail="edit"  id="' + d.admin_id + '_edit' + '" >Edit</button></td>' +
                                    '</tr>' +
                                '</tbody>' + 
                            '</table>' 
            return $my_var;

        }
    });
</script>

@endsection






