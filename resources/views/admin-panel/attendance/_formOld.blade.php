@if(isset($attendance['attendance_id']) && !empty($attendance['attendance_id']))
<?php  $readonly = true; $disabled = 'disabled'; ?>
@else
<?php $readonly = false; $disabled=''; ?>
@endif

{!! Form::hidden('attendance_id',old('attendance_id',isset($attendance['attendance_id']) ? $attendance['attendance_id'] : ''),['class' => 'gui-input', 'id' => 'attendance_id', 'readonly' => 'true']) !!}
<div class="alert alert-danger alert-dismissible print-error-msg" style="display:none;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="Date">{!! trans('language.month_year') !!} <span class="red-text">*</span></label>
        {!! Form::text('attendance_month', old('attendance_month',isset($attendance['attendance_month']) ? $attendance['attendance_month']: ''), ['class' => 'date-picker form-control', 'id' => 'attendance_month','for'=>'attendance_month','autocomplete'=>'off']) !!}
    </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="File">{!! trans('language.attendance_file') !!} <span class="red-text">*</span></label>
        <input type="file" name="attendance_file" id="attendance_file" @if(!isset($attendance['attendance_id']))<?php echo 'required'; ?>@endif >  
    </div>
</div>
@if(isset($encrypted_id) && $encrypted_id != '')
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <div class="input-group">
        <label for="Date">{!! trans('language.download_file') !!}</label><br>
        <a href="{!! URL::to('admin-panel/attendance/download-attendance').'/'.$encrypted_id!!}"><i class="fas fa-download"></i></a>
    </div>
</div>
@endif
<div class=" col-sm-9 clear-fix">
    <span class="help-block">
        <small>Note: - <li>Make sure you are uploading correct file.</li>
                       <li>File Name should be look like this - Attendance-sheet-{month_name}</li>
        </small>
    </span>
    <button type="button" name="preview" class="btn btn-info waves-effect waves-light m-t-10 no-radius uploadAttendance">{!! $submit_button !!}</button>
</div>
<div id="excelPreview"></div>
<div class="clearfix"></div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        $("#attendance-upload-form").validate({
            /* @validation states + elements 
             ------------------------------------------- */
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            rules: {
                attendance_month: {
                    required: true,
                },
                attendance_file: {
                    extension: "xlsx|csv",
                },
            },
            /* @validation messages 
             ------------------------------------------ */
            messages:{
                attendance_month: {
                    required: 'Please select month and year.',
                },
                attendance_file: {
                    required: 'Please select file.',
                    extension: 'Invalid extension.'
                },
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        
        $('.date-picker').datepicker( {
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'MM yy',
            onClose: function(dateText, inst) { 
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            }
        });
    });
    $(function () {
        $('.uploadAttendance').click(function (e) {
            var formData = new FormData($("form")[0]);
            console.log($("form")[0]);
            e.preventDefault();          
            $('.preloader').css('display','inline');
            $.ajax({
                    url: "{!! $save_preview_url !!}",
                    data: formData,
                    type: 'post',
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false, // NEEDED, DON'T OMIT THIS
                success: function (response) {
                    $('.preloader').css('display','none');
                    if($.isEmptyObject(response.error)){
                        $('#excelPreview').html(response);
	                }else{
	                	printErrorMsg(response.error);
	                }
                }
            });
        });
        function printErrorMsg (msg) {
            $('.print-error-msg').fadeIn(function() {
                setTimeout(function() {
                    $('.print-error-msg').fadeOut();
                }, 1500);
            });
			$.each( msg, function( key, value ) {
				$(".print-error-msg").html(value);
			});
		}
      });
</script>