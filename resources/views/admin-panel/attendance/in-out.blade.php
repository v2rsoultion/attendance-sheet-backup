@extends('admin-panel.layout.header')
@section('content')
@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" />
@endpush
<style>
.caret-reversed{
  -ms-transform: rotate(180deg);
  -webkit-transform: rotate(180deg);
  transform: rotate(180deg);
}
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="row clearfix">
                        {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="col-lg-3 col-md-3">
                            <div class="input-group">
                                {!!Form::select('employee_name', $employee_list, '', ['class' => 'form-control
                                select2','id'=>'employee_name'])!!}
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="input-group">
                                <input class="form-control input-daterange-datepicker" type="text" name="in_out_date"
                                    id="in_out_date" value=""/>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            {!! Form::submit('Search', ['class' => 'btn btn-info no-radius m-r-10','name'=>'Search']) !!}
                            {!! Form::button('Clear', ['class' => 'btn btn-inverse no-radius','name'=>'Clear', 'id' => "clearBtn"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            {{-- Loader for Page --}}
            {{-- End - Loader for Page --}}
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="col-md-2 row pull-right">
                        <select id="sortingField" class="form-control input-sm m-b-10">
                            <option value="0">All</option>
                            <option value="1" @if(isset($pending_condition) && $pending_condition != '') selected @endif>Pending</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table dataTable " id="in-out-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.in_out_date') !!}</th>
                                    <th>{!! trans('language.in_out_emp_name') !!}</th>
                                    <th>{!! trans('language.in_out_intime') !!}</th>
                                    <th>{!! trans('language.in_out_outtime') !!}</th>
                                    <th>{!! trans('language.in_out_total') !!}</th>
                                    <th>{!! trans('language.in_out_key') !!}</th>
                                    <th>{!! trans('language.in_out_ip') !!}</th>
                                    <th>{!! trans('language.in_out_desc') !!}</th>
                                    <th>{!! trans('language.in_out_request') !!}</th>
                                    <th>{!! trans('language.action') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->       
    </div>
    <div id="attendance-register-model" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="attendance-register-model" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Update Attendance</h4> 
                </div>
                {!! Form::open(['files'=>TRUE,'id' => 'attendance-register-details' ,'autocomplete' => "off"]) !!}
                <input type="hidden" name="attendance_register_id" id="attendance_register_id" value=""/>
                <input type="hidden" name="admin_id" id="admin_id" value=""/>
                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible" id="InOutError" style="display:none;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span class="InOutErrorMessage"></span>
                    </div>
                    <div class="input-group">
                        <label for="leave_comment" class="control-label">In Time:</label>
                        <div class="input-group timepicker">
                            <input type="text" class="form-control" id="register_in_time" name="register_in_time" required> 
                             <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                    <div class="input-group">
                        <label for="leave_comment" class="control-label">Out Time:</label>
                        <div class="input-group timepicker">
                            <input type="text" class="form-control" id="register_out_time" name="register_out_time" required>
                             <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                    <div class="input-group">
                        <label for="attendance_key" class="control-label">Attendance Key:</label>
                        {!! Form::select('register_key', $attendance_key, '', ['class' => 'form-control selectpicker', 'data-style' => 'form-control', 'id' => 'register_key']) !!}
                        <span class="help-block1"> Note: - Full day means absent </span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" onClick="this.form.reset()" class="btn btn-default waves-effect no-radius" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light no-radius">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<script>   
    $(document).ready(function () {
        $('.timepicker').datetimepicker({
            format: 'LT'
        });
        $("#attendance-register-details").validate({
            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            rules: {
                register_in_time: {
                    required: true,
                },
                register_out_time: {
                    required: true,
                },
                register_key: {
                    required: true,
                },
            },
            highlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Save');
                $( element ).parents( ".input-group" ).addClass( "has-error" ).removeClass( "has-success" );
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(".submitFormButton").html('Save');
                $( element ).parents( ".input-group" ).addClass( "has-success" ).removeClass( "has-error" );
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function (error, element) {
                $(".submitFormButton").html('Save');
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });

        $(document).on('click','.editInOut', function(){
            var attendance_register = $(this).attr('rel');
            var admin = $(this).attr('admin-id');
            $.ajax(
            {
                url: "{{ url('admin-panel/attendance-register1/get-attendance-data') }}",
                type: 'GET',
                data: {
                    'admin_id': admin,
                    'attendance_register_id': attendance_register,
                },
                success: function (res)
                {
                    if (res.status == "success") {
                        $('#attendance_register_id').val(res.data.attendance_register_id);
                        $('#admin_id').val(res.data.admin_id);
                        $('#register_in_time').val(res.data.attendance_intime);
                        $('#register_out_time').val(res.data.attendance_outtime);
                        $('#register_key').val(null);
                        $('#attendance-register-model').modal({
                            show: 'false'
                        });
                    } else {
                        swal({
                            title: res.message,
                            text: '',
                            type: "error", 
                        }).then(function(){
                            location.reload();
                        });
                    }
                }
            });   
        });
        
        $(document).on('submit', '#attendance-register-details', function(e){
            if($('#attendance-register-details').valid()) {
                $('.preloader').css('display','inline');
                var token = '{!!csrf_token()!!}';
                $.ajax(
                {
                    url: "{{ url('admin-panel/attendance-register1/update-attendance') }}",
                    type: 'GET',
                    data: {
                        'admin_id': $('#admin_id').val(),
                        'attendance_register_id': $('#attendance_register_id').val(),
                        'register_in_time': $('#register_in_time').val(),
                        'register_out_time': $('#register_out_time').val(),
                        'register_key': $('#register_key').val(),
                    },
                    success: function (res)
                    {
                        $('.preloader').css('display','none');
                        if (res.status == "success")
                        {
                            var table = $('#in-out-table').DataTable();
                            table.ajax.reload( null, false );
                            swal({
                                title: res.message,
                                text: '',
                                type: "success", 
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            $('#InOutError').html(res.message);
                            $('#InOutErrorMessage').fadeIn(function() {
                                setTimeout(function() {
                                    $('#InOutErrorMessage').fadeOut();
                                }, 2500);
                            });
                        }
                    }
                });
                e.preventDefault();
            }
        });

        $('#sortingField').change(function(){
            var table = $('#in-out-table').DataTable();
            table.ajax.reload( null, false );
        });

        // Daterange picker
        $('.input-daterange-datepicker').daterangepicker({
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-danger',
            cancelClass: 'btn-inverse'
        });
        var table = $('#in-out-table').DataTable({
            pageLength: 40,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/attendanceregister/in-out-data')}}',
                data: function (d) { 
                    d.dates = $('#in_out_date').val();
                    d.emp = $('#employee_name').val();
                    d.isRequest = $('#sortingField').val();
                 }
            },
            columns: [
                {data: 'in_out_date', name: 'in_out_date'},
                {data: 'emp_name', name: 'emp_name'},
                {data: 'in_out_intime', name: 'in_out_intime'},
                {data: 'in_out_outtime', name: 'in_out_outtime'},
                {data: 'in_out_total', name: 'in_out_total'},
                {data: 'in_out_key', name: 'in_out_key'},
                {data: 'in_out_ip', name: 'in_out_ip'},
                {data: 'in_out_desc', name: 'in_out_desc'},
                {data: 'request', name: 'request'},
                {data: 'action', name: 'action'},
            ],
            columnDefs: [
                { "targets": [2,3,4,6,7,8,9], "orderable": false }
            ]
        });
        $('#search-form').on('submit', function(e) {
           table.draw();
           e.preventDefault();
        });
        $('#clearBtn').click(function(){
           location.reload();
        })
        $(document).on('change', '.changeRequest', function(e){
            $('.preloader').css('display','inline');
            $.ajax(
            {
                url: "{{ url('admin-panel/attendance/changerequest') }}",
                type: 'GET',
                data: {
                    'attendance-id': $(this).attr('condition-for'),
                    'condition': $(this).val(),
                },
                success: function (res)
                {
                    $('.preloader').css('display','none');
                    if (res == "success")
                    {
                        var table = $('#team-table').DataTable();
                        table.ajax.reload( null, false );
                        swal({
                            title: res.message,
                            text: '',
                            type: "success", 
                            closeOnClickOutside: false,
                        }).then(function(){
                            location.reload();
                        });
                    }
                    else{
                        swal({
                            title: res.message,
                            text: '',
                            type: "success", 
                            closeOnClickOutside: false,
                        }).then(function(){
                            location.reload();
                        });
                    }
                }
            });
        });
    });
</script>
@push('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>    
@endpush
@endsection






