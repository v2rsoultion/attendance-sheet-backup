@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">{!! trans('language.dashboard') !!}</a></li>
                    <li><a href="#">{!! trans('language.attendance') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{$errors->first()}}
                        </div>
                    @endif
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group" id="datepickerbox">
                                    {!! Form::text('attendance_month', old('attendance_month',isset($attendance['attendance_month']) ? $attendance['attendance_month']: ''), ['class' => 'form-control', 'id' => 'attendance_month','for'=>'attendance_month','autocomplete'=>'off']) !!}
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                {!! Form::submit('Search', ['class' => 'btn btn-info no-radius m-r-10','name'=>'Search']) !!}
                                {!! Form::button('Clear', ['class' => 'btn btn-inverse no-radius','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table dataTable " id="attendance-table">
                            <thead>
                                <tr>
                                    <th>{!! trans('language.serial_no') !!}</th>
                                    <th>{!! trans('language.month_year') !!}</th>
                                    <th>{!! trans('language.download_file') !!}</th>
                                    <th>{!! trans('language.show_attendance') !!}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->        
    </div>
    <!-- /.container-fluid -->
</div>
<script>
    $(document).ready(function () {
        var table = $('#attendance-table').DataTable({
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/attendance/data')}}',
                data: function (d) { 
                    d.attendance_month = $('input[name=attendance_month]').val();
                 }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'attendance_month', name: 'attendance_month'},   
                {data: 'download_attendance', name: 'download_attendance'},
                {data: 'show_attendance', name: 'show_attendance'},
            ],
            columnDefs: [
                { "targets": [2,3], "orderable": false }
            ]
        });
        $('#search-form').on('submit', function(e) {
           table.draw();
           e.preventDefault();
        });
        $('#clearBtn').click(function(){
           location.reload();
        });
    });
    jQuery('#attendance_month').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "MM yyyy",
        viewMode: "months", 
        minViewMode: "months",
        container:'#datepickerbox'
    });
</script>

@endsection






