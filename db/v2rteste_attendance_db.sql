-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2020 at 12:50 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `v2rteste_attendance_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `emp_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department_id` int(10) UNSIGNED DEFAULT NULL,
  `type` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2',
  `joining_date` date DEFAULT NULL,
  `leaving_date` date DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `is_terminate` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_resign` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `verification_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `time_slot_id` int(10) UNSIGNED DEFAULT NULL,
  `saturday_off` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `platforms` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_profile_img` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `emp_id`, `admin_name`, `email`, `phone`, `department_id`, `type`, `joining_date`, `leaving_date`, `status`, `is_terminate`, `is_resign`, `verification_status`, `time_slot_id`, `saturday_off`, `platforms`, `password`, `admin_profile_img`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Admin', 'admin@gmail.com', NULL, NULL, '1', NULL, NULL, '1', '0', '0', '0', NULL, NULL, NULL, '$2y$10$CRFA.bp9jE6bG9iYd0BcUugBhFSur3FEtfy7t2iHZbq2Uyp4dDgdW', NULL, 'bWmPfHgTyIwLlo7HqE6yzpZkXbUFD0BXkngWyL5Nuv34xxYZebIHZkFgJddG', '2019-04-08 17:50:08', '2019-04-08 17:50:08'),
(2, 'V2R19002', 'Rupam Mukherjee', 'rupam.v2rsolution@gmail.com', '7790965087', 2, '2', '2018-12-01', NULL, '1', '0', '0', '1', 1, '2', '1,4,2,5,3', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, '5gCGxWvFaLebIw3wYECeLLXgqXVjSu1hSlBcG0iYrnakC4gBoVbtvjrgZTIF', '2019-01-25 10:23:28', '2019-03-04 12:28:35'),
(3, 'V2R19003', 'Bhagya Shree', 'bhagyashree@v2rsolution.com', '8890149916', 3, '2', '2018-12-01', NULL, '1', '0', '0', '1', 2, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, 'AAwKjpbtcWWz1ylmRN2pETfDh8PPJxjU9P4tWfID8OoTynakofz2lWMqe4Gk', '2019-01-25 10:24:02', '2019-03-05 11:19:13'),
(4, 'V2R19004', 'Amba Patel', 'amba@v2rsolution.com', '9988776655', 3, '2', '2018-12-01', NULL, '1', '0', '0', '1', 1, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, 'ZR0K0m4ODPQuJBCFA7i0KteFMSivr4TVpptNjSR9MK7BzIhutgYBJlT7ZmqF', '2019-01-25 10:34:03', '2019-02-08 05:11:35'),
(5, 'V2R19005', 'Mangu Singh', 'mangu@wscubetech.com', '9988776655', 3, '2', '2018-12-01', NULL, '1', '0', '0', '1', 2, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, NULL, '2019-01-25 10:35:36', '2019-02-08 06:30:50'),
(6, 'V2R19006', 'Aaditya Soni', 'aadityasoni@v2rsolution.com', '9988776655', 2, '2', '2018-12-01', '2019-03-04', '0', '0', '1', '1', 2, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, NULL, '2019-01-25 10:40:43', '2019-03-04 12:30:16'),
(7, 'V2R19007', 'Reema Parakh', 'reema@v2rsolution.com', '9988776655', 1, '2', '2018-12-01', NULL, '1', '0', '0', '1', 1, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, 'OcAEaAWcbqpIhZBwURKUAs37vO8YyCJkOxEvxNFj4ACeIousIFhu287F31h7', '2019-01-25 10:44:34', '2019-02-28 19:06:11'),
(8, 'V2R19008', 'Bhuvanesh Soni', 'bhuvaneshsoni@v2rsolution.com', '8769728420', 3, '2', '2019-04-01', NULL, '1', '0', '0', '1', 1, '2', '', '$2y$10$XBxsgV4iil7AuMRao3tMq.XmWWnarXCzvLxpGGJ1FaQQnB13OU2XC', NULL, 'TjdUVKrcJNgAj71DPoBx3JCdRUFZiHEQcpZvQLUReZ38KTpg2aKTkqy9GGKO', '2019-01-25 10:44:59', '2019-04-16 16:36:56'),
(9, 'V2R19009', 'Sweta', 'sweta.v2rsolution@gmail.com', '9462525173', 2, '2', '2019-01-01', NULL, '1', '0', '0', '1', 1, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, 'WE6NuMKcoNPkQH88i3GfxgdQmC2VrzvReprLQfe1ZacSt4DJPiliEorml8wn', '2019-02-06 14:48:58', '2019-02-07 14:05:14'),
(10, 'V2R19010', 'Akib Raja', 'akibraja@v2rsolution.com', '8114485411', 2, '2', '2019-01-01', NULL, '1', '0', '0', '1', 3, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, NULL, '2019-02-06 14:49:46', '2019-03-02 11:51:30'),
(11, 'V2R19011', 'Ankita Mangal', 'amkitamangal@wscubetech.com', '9414166682', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 1, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, 'RftEOGvFfinDOunSFA1Ezf91ozEPCWcSGCN9u4SxabEbrW6N3D1VvMA6vq0I', '2019-02-06 14:50:41', '2019-03-02 10:13:08'),
(12, 'V2R19012', 'Pratyush Bharti', 'pratyush@v2rsolution.co.in', '7976115227', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 1, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, '6IGFb4XQeK2C2Pk558YP4gIhRzgVsD3ziWX5KLDCN1grgZNVArGBR1I5TEt6', '2019-02-06 14:51:22', '2019-02-06 14:51:22'),
(13, 'V2R19013', 'Pankaj', 'pankajkasotiya@wscubetech.com', '8094494523', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 1, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, 'm2qjlQB4Ck48uBMw72tSM7n0BwKD7LQbAQ1mgATSoc2ijFxT2DbbtXhwHd0p', '2019-02-06 14:52:50', '2019-02-08 05:53:56'),
(14, 'V2R19014', 'Brijpal', 'brijpal@wscubetech.com', '7742351323', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 1, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, 'mzSGlW8n27kxuI8lSAtDdYcKFEKikutu21AKD8JCyiZTxzfKyOPFQ6fGcBIS', '2019-02-06 14:53:32', '2019-02-16 14:38:10'),
(15, 'V2R19015', 'Khushbu Vaishnav', 'khushbuvaishnav@v2rsolution.com', '7073660684', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 1, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, 'Wk6JGLdyPeegD0Vn38YLSRUGCcJz0SWPnv15aiF7BO14LUpjDO8SZva3rr00', '2019-02-06 14:55:01', '2019-03-05 10:38:22'),
(16, 'V2R19016', 'Bhavana Gautam', 'bhavana@v2rsolution.com', '8005719078', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 1, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, NULL, '2019-02-06 14:55:45', '2019-02-08 05:47:12'),
(17, 'V2R19017', 'Goutam', 'goutamchoudhary@wscubetech.com', '9898989898', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 1, '0', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, NULL, '2019-02-06 14:56:37', '2019-02-08 15:27:33'),
(18, 'V2R19018', 'Mayank', 'mayankbhurani@v2rsolution.com', '8769635403', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 1, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, NULL, '2019-02-06 14:57:16', '2019-02-06 14:57:16'),
(19, 'V2R19019', 'Neelam', 'neelam@wscubetech.com', '9024447040', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 1, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, NULL, '2019-02-06 14:57:52', '2019-02-06 14:57:52'),
(20, 'V2R19020', 'Jitendra Jangid', 'jitendrajangid@v2rsolution.com', '9782608731', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 2, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, 'RC1WIKh9E4JHGSZKZFBVr4hZv47MllQq7DcLR1dXfTiRm95he5ENP8fIO06c', '2019-02-06 14:59:44', '2019-02-08 05:41:21'),
(21, 'V2R19021', 'Sumit Naruka', 'sumitsingh@v2rsolution.com', '9782833224', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 2, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, 'iUfQyBbvxvhXNoSr0sSTaynKxdCldFmszo5wZfmUIK6aUkrf9GFtrnmjsDCw', '2019-02-06 15:00:28', '2019-03-04 10:25:18'),
(22, 'V2R19022', 'Sandeep Bhati', 'sandeepbhati@v2rsolution.com', '9782371474', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 2, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, NULL, '2019-02-06 15:00:59', '2019-02-08 05:40:01'),
(23, 'V2R19023', 'Asif Khan', 'asif@v2rsolution.com', '8561843709', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 2, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, 'a1htZytU6YUGwXejf5BW7uE33k75sesqD7NHw0fh5cqprylVuYJfKWU8GIhk', '2019-02-06 15:01:28', '2019-03-04 10:33:29'),
(24, 'V2R19024', 'Mahendra Kumawat', 'mahikumawat@v2rsolution.com', '8764444996', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 3, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, '187M6N3gBYgKn4qNjAHuVaDPdcRlKI2fgsHvTLuOVzooWgxEEG4OZPAOVfnJ', '2019-02-06 15:01:58', '2019-02-08 05:57:26'),
(25, 'V2R19025', 'Pradeep Rai', 'pradeeprai@wscubetech.com', '8949254923', 3, '2', '2019-01-01', NULL, '1', '0', '0', '1', 1, '2', '', '$2y$10$ezYhfTuNGRTmA0PjnFWQy.s00nMLy5LOJ9AzAzIiHtNCgTZftiMFS', NULL, 'PIyEeyt6BFhzNqe0lvneMPeBMEEbetJPdu7ut8chSE7SDndITe9ELnmSZQEL', '2019-02-06 15:02:36', '2019-02-21 13:17:26');

-- --------------------------------------------------------

--
-- Table structure for table `admin_password_resets`
--

CREATE TABLE `admin_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_password_resets`
--

INSERT INTO `admin_password_resets` (`email`, `token`, `created_at`) VALUES
('bhuvaneshsoni@v2rsolution.com', '$2y$10$3Xr.WgJ4OST7a6BRXXloYOI2uxnMyZq77O.6mN88yvVzoKpG4Ol36', '2019-04-16 17:17:18');

-- --------------------------------------------------------

--
-- Table structure for table `assign_user_roles`
--

CREATE TABLE `assign_user_roles` (
  `assign_user_role_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `user_role_id` int(10) UNSIGNED NOT NULL,
  `assign_user_role_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assign_user_roles`
--

INSERT INTO `assign_user_roles` (`assign_user_role_id`, `admin_id`, `user_role_id`, `assign_user_role_status`, `created_at`, `updated_at`) VALUES
(1, 4, 3, '1', '2019-04-08 17:57:27', '2019-04-08 17:57:27'),
(2, 4, 4, '1', '2019-04-08 17:57:27', '2019-04-08 17:57:27'),
(3, 2, 2, '1', '2019-04-08 17:57:38', '2019-04-08 17:57:38'),
(4, 2, 1, '1', '2019-04-08 17:57:38', '2019-04-08 17:57:38'),
(5, 8, 4, '1', '2019-04-08 17:57:54', '2019-04-08 17:57:54'),
(6, 15, 4, '1', '2019-04-08 17:58:23', '2019-04-08 17:58:23'),
(7, 20, 3, '1', '2019-04-08 22:28:09', '2019-04-08 22:28:09');

-- --------------------------------------------------------

--
-- Table structure for table `attendancedatas`
--

CREATE TABLE `attendancedatas` (
  `attendancedata_id` int(10) UNSIGNED NOT NULL,
  `attendance_id` int(10) UNSIGNED DEFAULT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `month_year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `json_data` text COLLATE utf8mb4_unicode_ci,
  `wfh_json_data` text COLLATE utf8mb4_unicode_ci,
  `full_leaves` double(8,2) DEFAULT NULL,
  `half_leaves` double(8,2) DEFAULT NULL,
  `dhalf_leaves` double(8,2) DEFAULT NULL,
  `extra_working_hrs` double(8,2) DEFAULT NULL,
  `late_days` double(8,2) DEFAULT NULL,
  `leaves_after_less` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attendancedata_logs`
--

CREATE TABLE `attendancedata_logs` (
  `attendancedata_log_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `employee_id` int(10) UNSIGNED DEFAULT NULL,
  `attendancedata_id` int(10) UNSIGNED DEFAULT NULL,
  `month_year` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `attendance_id` int(10) UNSIGNED NOT NULL,
  `attendance_month` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attendance_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attendance_registers`
--

CREATE TABLE `attendance_registers` (
  `attendance_register_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `attendance_date` date DEFAULT NULL,
  `attendance_intime` datetime DEFAULT NULL,
  `attendance_outtime` datetime DEFAULT NULL,
  `attendance_total_time` int(11) DEFAULT NULL COMMENT 'Total time in minutes',
  `attendance_status` tinyint(4) NOT NULL DEFAULT '1',
  `attendance_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attendance_isRequest` tinyint(4) NOT NULL DEFAULT '0',
  `isRequest_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Request approved or not by admin. 0 - approved, 1 - not approved',
  `attendance_desc` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendance_registers`
--

INSERT INTO `attendance_registers` (`attendance_register_id`, `admin_id`, `attendance_date`, `attendance_intime`, `attendance_outtime`, `attendance_total_time`, `attendance_status`, `attendance_key`, `attendance_isRequest`, `isRequest_status`, `attendance_desc`, `created_at`, `updated_at`) VALUES
(1, 4, '2019-04-13', '2019-04-13 16:24:22', NULL, NULL, 1, 'dhalf_16:24', 0, 0, NULL, '2019-04-13 21:24:22', '2019-04-13 21:24:22');

-- --------------------------------------------------------

--
-- Table structure for table `code_reviews`
--

CREATE TABLE `code_reviews` (
  `code_review_id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  `coder_id` int(10) UNSIGNED DEFAULT NULL,
  `code_reviewer_id` int(10) UNSIGNED DEFAULT NULL,
  `frequently_code_review` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_start_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comments_for_reviewer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_points` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Ids of check points',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Active - 1, Inactive - 0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `code_reviews`
--

INSERT INTO `code_reviews` (`code_review_id`, `project_id`, `coder_id`, `code_reviewer_id`, `frequently_code_review`, `review_start_date`, `comments_for_reviewer`, `check_points`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 8, 4, '1', '2019-04-11', 'Please check in proper way. and also maintain report for this.', '1,2,3,4,5', 1, '2019-04-11 21:10:21', '2019-04-11 22:36:15'),
(2, 2, 8, 20, '2', '2019-04-11', 'Please maintain excel sheet for this. and share weekly with me.', '2,3', 1, '2019-04-11 21:11:18', '2019-04-11 21:11:18'),
(3, 2, 15, 4, '2', '2019-04-11', 'Please maintain excel sheet for this. and share weekly with me.', '1,4,5', 1, '2019-04-11 21:11:18', '2019-04-11 21:11:18'),
(4, 1, 4, 20, '1', '2019-04-11', 'Please check in proper way. and also maintain report for this.', '1,2,3,4,5', 1, '2019-04-11 22:36:15', '2019-04-11 22:36:15'),
(5, 3, 4, 20, '1', '2019-03-01', 'db & logic is the focus', '1,2,3,4,5', 1, '2019-04-13 21:00:58', '2019-04-13 21:02:05'),
(6, 3, 8, 4, '1', '2019-03-01', 'db & logic is the focus', '1,5', 1, '2019-04-13 21:02:05', '2019-04-13 21:02:05');

-- --------------------------------------------------------

--
-- Table structure for table `code_review_details`
--

CREATE TABLE `code_review_details` (
  `code_review_detail_id` int(10) UNSIGNED NOT NULL,
  `code_review_info_id` int(10) UNSIGNED DEFAULT NULL,
  `code_review_id` int(10) UNSIGNED DEFAULT NULL,
  `review_check_id` int(10) UNSIGNED DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `review_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `code_review_details`
--

INSERT INTO `code_review_details` (`code_review_detail_id`, `code_review_info_id`, `code_review_id`, `review_check_id`, `comments`, `review_status`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'Done', '1', '1', '2019-04-11 21:19:47', '2019-04-11 21:19:47'),
(2, 1, 1, 2, 'Not Done', '2', '1', '2019-04-11 21:19:47', '2019-04-11 21:19:47'),
(3, 1, 1, 3, 'Need improvement', '3', '1', '2019-04-11 21:19:47', '2019-04-11 21:19:47'),
(4, 1, 1, 4, 'All ok', '1', '1', '2019-04-11 21:19:47', '2019-04-11 21:19:47'),
(5, 1, 1, 5, 'Not Ok', '2', '1', '2019-04-11 21:19:47', '2019-04-11 21:19:47'),
(6, 3, 3, 1, 'All Done', '1', '1', '2019-04-11 21:22:33', '2019-04-11 21:22:33'),
(7, 3, 3, 4, 'Need Improvement', '3', '1', '2019-04-11 21:22:33', '2019-04-11 21:22:33'),
(8, 3, 3, 5, 'Not OK', '2', '1', '2019-04-11 21:22:33', '2019-04-11 21:22:33'),
(9, 2, 2, 2, 'All Done.', '1', '1', '2019-04-11 21:25:12', '2019-04-11 21:25:12'),
(10, 2, 2, 3, 'Need Improvement', '3', '1', '2019-04-11 21:25:12', '2019-04-11 21:25:12'),
(11, 7, 4, 1, 'sdfsdf', '1', '1', '2019-04-11 22:36:54', '2019-04-11 22:36:54'),
(12, 7, 4, 2, 'sadfsdf', '3', '1', '2019-04-11 22:36:54', '2019-04-11 22:36:54'),
(13, 7, 4, 3, 'sadf', '1', '1', '2019-04-11 22:36:54', '2019-04-11 22:36:54'),
(14, 7, 4, 4, 'sdfsdaf', '3', '1', '2019-04-11 22:36:54', '2019-04-11 22:36:54'),
(15, 7, 4, 5, 'fasdfsdf', '2', '1', '2019-04-11 22:36:54', '2019-04-11 22:36:54'),
(16, 4, 1, 1, 'All Right', '1', '1', '2019-04-12 23:48:02', '2019-04-12 23:48:02'),
(17, 4, 1, 2, '1. Use looping for fatch record\r\n2. Define function more then 40 lines', '3', '1', '2019-04-12 23:48:02', '2019-04-12 23:48:02'),
(18, 4, 1, 3, '1. Use table name sepration when having more then two words\r\n2. Define meaning full name for table and database either local or live server', '2', '1', '2019-04-12 23:48:02', '2019-04-12 23:48:02'),
(19, 4, 1, 4, 'All is good', '1', '1', '2019-04-12 23:48:02', '2019-04-12 23:48:02'),
(20, 4, 1, 5, 'Good', '1', '1', '2019-04-12 23:48:02', '2019-04-12 23:48:02'),
(21, 11, 6, 1, 'good', '1', '1', '2019-04-13 21:04:16', '2019-04-13 21:04:16'),
(22, 11, 6, 5, 'good', '1', '1', '2019-04-13 21:04:16', '2019-04-13 21:04:16'),
(23, 12, 6, 1, 'good', '1', '1', '2019-04-13 21:07:31', '2019-04-13 21:07:31'),
(24, 12, 6, 5, 'good', '1', '1', '2019-04-13 21:07:31', '2019-04-13 21:07:31'),
(25, 13, 6, 1, 'good', '1', '1', '2019-04-13 21:07:46', '2019-04-13 21:07:46'),
(26, 13, 6, 5, 'good', '1', '1', '2019-04-13 21:07:46', '2019-04-13 21:07:46'),
(27, 14, 6, 1, 'good', '1', '1', '2019-04-13 21:07:56', '2019-04-13 21:07:56'),
(28, 14, 6, 5, 'ok', '1', '1', '2019-04-13 21:07:56', '2019-04-13 21:07:56'),
(29, 15, 6, 1, 'ok', '1', '1', '2019-04-13 21:08:04', '2019-04-13 21:08:04'),
(30, 15, 6, 5, 'ok', '1', '1', '2019-04-13 21:08:04', '2019-04-13 21:08:04'),
(31, 16, 6, 1, 'ok', '1', '1', '2019-04-13 21:08:11', '2019-04-13 21:08:11'),
(32, 16, 6, 5, 'ok', '1', '1', '2019-04-13 21:08:11', '2019-04-13 21:08:11'),
(33, 17, 6, 1, 'improve', '2', '1', '2019-04-13 21:08:39', '2019-04-13 21:08:39'),
(34, 17, 6, 5, 'not ok', '3', '1', '2019-04-13 21:08:39', '2019-04-13 21:08:39'),
(35, 18, 6, 1, 're', '3', '1', '2019-04-13 21:09:03', '2019-04-13 21:09:03'),
(36, 18, 6, 5, 'repeat', '2', '1', '2019-04-13 21:09:03', '2019-04-13 21:09:03');

-- --------------------------------------------------------

--
-- Table structure for table `code_review_infos`
--

CREATE TABLE `code_review_infos` (
  `code_review_info_id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  `code_reviewer_id` int(10) UNSIGNED DEFAULT NULL,
  `review_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_review_status` tinyint(191) DEFAULT NULL COMMENT '1 - Ontime, 2 - Pending, 3 - Delay',
  `review_upcoming_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isSubmit` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - Save, 1 - Save & Submit',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `code_review_infos`
--

INSERT INTO `code_review_infos` (`code_review_info_id`, `project_id`, `code_reviewer_id`, `review_date`, `code_review_status`, `review_upcoming_date`, `isSubmit`, `created_at`, `updated_at`) VALUES
(1, 1, 4, '2019-04-11', 1, '2019-04-18', 1, '2019-04-11 21:10:21', '2019-04-11 21:21:11'),
(2, 2, 20, '2019-04-11', 1, '2019-04-26', 1, '2019-04-11 21:11:18', '2019-04-11 21:25:12'),
(3, 2, 4, '2019-04-11', 1, '2019-04-26', 1, '2019-04-11 21:11:18', '2019-04-11 22:33:26'),
(4, 1, 4, '2019-04-12', 1, '2019-04-25', 1, '2019-04-11 21:19:47', '2019-04-12 23:48:02'),
(5, 2, 4, '', 2, '2019-05-10', 0, '2019-04-11 21:22:33', '2019-04-11 21:22:33'),
(6, 2, 20, '', 2, '2019-05-10', 0, '2019-04-11 21:25:12', '2019-04-11 21:25:12'),
(7, 1, 20, '2019-04-11', 1, '2019-04-18', 1, '2019-04-11 22:36:15', '2019-04-11 22:36:54'),
(8, 1, 20, '', 2, '2019-04-25', 0, '2019-04-11 22:36:54', '2019-04-11 22:36:54'),
(9, 1, 4, '', 2, '2019-05-02', 0, '2019-04-12 23:48:02', '2019-04-12 23:48:02'),
(10, 3, 20, '', 2, '2019-03-01', 0, '2019-04-13 21:00:58', '2019-04-13 21:00:58'),
(11, 3, 4, '2019-04-13', 3, '2019-03-01', 1, '2019-04-13 21:02:05', '2019-04-13 21:04:16'),
(12, 3, 4, '2019-04-13', 3, '2019-03-08', 1, '2019-04-13 21:04:16', '2019-04-13 21:07:31'),
(13, 3, 4, '2019-04-13', 3, '2019-03-15', 1, '2019-04-13 21:07:31', '2019-04-13 21:07:46'),
(14, 3, 4, '2019-04-13', 3, '2019-03-22', 1, '2019-04-13 21:07:46', '2019-04-13 21:07:56'),
(15, 3, 4, '2019-04-13', 3, '2019-03-29', 1, '2019-04-13 21:07:56', '2019-04-13 21:08:04'),
(16, 3, 4, '2019-04-13', 3, '2019-04-05', 1, '2019-04-13 21:08:04', '2019-04-13 21:08:11'),
(17, 3, 4, '2019-04-13', 3, '2019-04-12', 1, '2019-04-13 21:08:11', '2019-04-13 21:08:39'),
(18, 3, 4, '2019-04-13', 1, '2019-04-19', 1, '2019-04-13 21:08:39', '2019-04-13 21:09:03'),
(19, 3, 4, '', 2, '2019-04-26', 0, '2019-04-13 21:09:03', '2019-04-13 21:09:03');

-- --------------------------------------------------------

--
-- Table structure for table `company_configs`
--

CREATE TABLE `company_configs` (
  `company_config_id` int(10) UNSIGNED NOT NULL,
  `office_hours` text COLLATE utf8mb4_unicode_ci,
  `lunch_hours` text COLLATE utf8mb4_unicode_ci,
  `direct_half_time` text COLLATE utf8mb4_unicode_ci,
  `late_half_days` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_configs`
--

INSERT INTO `company_configs` (`company_config_id`, `office_hours`, `lunch_hours`, `direct_half_time`, `late_half_days`, `created_at`, `updated_at`) VALUES
(1, '9', '0.5', '12:00 PM', '3', '2019-04-08 17:50:26', '2019-04-08 17:50:26');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `department_id` int(10) UNSIGNED NOT NULL,
  `department_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`department_id`, `department_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'App Department', '1', '2019-04-08 17:50:05', '2019-04-08 17:50:05'),
(2, 'Other', '1', '2019-04-08 17:50:05', '2019-04-08 17:50:05'),
(3, 'Web Department', '1', '2019-04-08 17:50:05', '2019-04-08 17:50:05');

-- --------------------------------------------------------

--
-- Table structure for table `early_leave_registers`
--

CREATE TABLE `early_leave_registers` (
  `early_leave_register_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `leave_date` date DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `leave_status` tinyint(4) NOT NULL DEFAULT '2' COMMENT 'Approved-1, Pending-2 or Cancel-3',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `leave_reason` text COLLATE utf8mb4_unicode_ci,
  `leave_comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `early_leave_registers`
--

INSERT INTO `early_leave_registers` (`early_leave_register_id`, `admin_id`, `leave_date`, `start_time`, `end_time`, `leave_status`, `status`, `leave_reason`, `leave_comment`, `created_at`, `updated_at`) VALUES
(1, 8, '2019-04-22', '2019-04-22 12:45:00', '2019-04-22 13:45:00', 1, 1, 'sdfsdfsdfsdf', NULL, '2019-04-22 17:43:32', '2019-04-22 17:43:32');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `event_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `eventType` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `className` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `title`, `eventType`, `start_date`, `end_date`, `className`, `created_at`, `updated_at`) VALUES
(1, 'Saturday', '0', '2019-04-06', '2019-04-06', 'bg-saturday', '2019-04-13 21:22:43', '2019-04-13 21:22:54'),
(2, 'Saturday', '0', '2019-04-20', '2019-04-20', 'bg-saturday', '2019-04-13 21:22:43', '2019-04-13 21:22:54'),
(3, 'Saturday', '0', '2019-05-04', '2019-05-04', 'bg-saturday', '2019-04-13 21:22:54', '2019-04-13 21:22:54'),
(4, 'Saturday', '0', '2019-05-18', '2019-05-18', 'bg-saturday', '2019-04-13 21:22:54', '2019-04-13 21:22:54');

-- --------------------------------------------------------

--
-- Table structure for table `holiday_calendars`
--

CREATE TABLE `holiday_calendars` (
  `holiday_calendar_id` int(10) UNSIGNED NOT NULL,
  `month_year` date DEFAULT NULL,
  `json` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leave_registers`
--

CREATE TABLE `leave_registers` (
  `leave_register_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `leave_category` tinyint(4) NOT NULL,
  `leave_start_date` date DEFAULT NULL,
  `leave_end_date` date DEFAULT NULL,
  `leave_status` tinyint(4) NOT NULL DEFAULT '2' COMMENT 'Approved-1, Pending-2 or Cancel-3',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `leave_reason` text COLLATE utf8mb4_unicode_ci,
  `leave_comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `log_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mac_address` text COLLATE utf8mb4_unicode_ci,
  `work_type` tinyint(4) NOT NULL COMMENT 'Login - 1, In - 2, Out - 3',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`log_id`, `admin_id`, `ip_address`, `mac_address`, `work_type`, `created_at`, `updated_at`) VALUES
(1, 1, '112.196.166.123', NULL, 1, '2019-04-11 21:06:29', '2019-04-11 21:06:29'),
(2, 4, '112.196.166.123', NULL, 1, '2019-04-11 21:11:36', '2019-04-11 21:11:36'),
(3, 1, '112.196.166.123', NULL, 1, '2019-04-11 21:16:39', '2019-04-11 21:16:39'),
(4, 4, '112.196.166.123', NULL, 1, '2019-04-11 21:18:19', '2019-04-11 21:18:19'),
(5, 20, '112.196.166.123', NULL, 1, '2019-04-11 21:24:35', '2019-04-11 21:24:35'),
(6, 8, '112.196.166.123', NULL, 1, '2019-04-11 21:25:51', '2019-04-11 21:25:51'),
(7, 1, '112.196.166.123', NULL, 1, '2019-04-11 21:26:56', '2019-04-11 21:26:56'),
(8, 8, '112.196.166.123', NULL, 1, '2019-04-11 22:32:42', '2019-04-11 22:32:42'),
(9, 4, '112.196.166.123', NULL, 1, '2019-04-11 22:33:09', '2019-04-11 22:33:09'),
(10, 1, '112.196.166.123', NULL, 1, '2019-04-11 22:35:50', '2019-04-11 22:35:50'),
(11, 20, '112.196.166.123', NULL, 1, '2019-04-11 22:36:27', '2019-04-11 22:36:27'),
(12, 4, '112.196.166.123', NULL, 1, '2019-04-11 22:37:10', '2019-04-11 22:37:10'),
(13, 1, '112.196.166.123', NULL, 1, '2019-04-11 23:46:52', '2019-04-11 23:46:52'),
(14, 8, '112.196.166.91', NULL, 1, '2019-04-12 23:42:50', '2019-04-12 23:42:50'),
(15, 4, '112.196.166.91', NULL, 1, '2019-04-12 23:43:48', '2019-04-12 23:43:48'),
(16, 8, '112.196.166.91', NULL, 1, '2019-04-12 23:48:11', '2019-04-12 23:48:11'),
(17, 1, '117.247.238.221', NULL, 1, '2019-04-13 20:44:14', '2019-04-13 20:44:14'),
(18, 1, '112.196.166.61', NULL, 1, '2019-04-13 20:51:28', '2019-04-13 20:51:28'),
(19, 4, '112.196.166.61', NULL, 1, '2019-04-13 20:52:39', '2019-04-13 20:52:39'),
(20, 8, '112.196.166.61', NULL, 1, '2019-04-13 21:11:04', '2019-04-13 21:11:04'),
(21, 4, '112.196.166.123', NULL, 2, '2019-04-13 21:24:22', '2019-04-13 21:24:22'),
(22, 1, '112.196.166.7', NULL, 1, '2019-04-16 17:33:58', '2019-04-16 17:33:58'),
(23, 1, '112.196.166.7', NULL, 1, '2019-04-16 17:53:16', '2019-04-16 17:53:16'),
(24, 4, '112.196.166.7', NULL, 1, '2019-04-16 17:53:39', '2019-04-16 17:53:39'),
(25, 1, '112.196.166.7', NULL, 1, '2019-04-16 20:42:57', '2019-04-16 20:42:57'),
(26, 1, '112.196.166.97', NULL, 1, '2019-04-22 17:42:57', '2019-04-22 17:42:57'),
(27, 4, '112.196.166.97', NULL, 1, '2019-04-22 22:07:06', '2019-04-22 22:07:06');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_12_11_065035_create_departments_table', 1),
(2, '2018_12_11_065123_create_time_slot', 1),
(3, '2018_12_11_065125_create_admins_table', 1),
(4, '2018_12_18_065534_create_attendances_table', 1),
(5, '2018_12_19_071725_create_attendancedatas_table', 1),
(6, '2018_12_21_042743_create_admin_reset_password_table', 1),
(7, '2018_12_22_082523_create_time_slot_history', 1),
(8, '2018_12_24_093130_create_project_table', 1),
(9, '2018_12_24_093629_create_platform_table', 1),
(10, '2018_12_24_094712_create_review_check_lists_table', 1),
(11, '2018_12_24_094713_create_code_review_table', 1),
(12, '2018_12_24_094714_create_code_review_infos_table', 1),
(13, '2018_12_24_100042_create_code_review_detail_table', 1),
(14, '2019_01_07_052020_company_config', 1),
(15, '2019_01_07_053047_holiday_calendar', 1),
(16, '2019_01_07_053749_release_plan', 1),
(17, '2019_01_11_071003_create_events_table', 1),
(18, '2019_01_16_042658_create_release_plan_log', 1),
(19, '2019_01_21_065312_create_table_attendance_update_log', 1),
(20, '2019_01_24_113151_create_work_types_table', 1),
(21, '2019_01_24_113826_create_project_assigns_table', 1),
(22, '2019_01_24_115825_create_work_times_table', 1),
(23, '2019_01_24_120549_create_work_time_details_table', 1),
(24, '2019_01_30_091540_create_user_roles_table', 1),
(25, '2019_01_30_091920_create_assign_user_roles_table', 1),
(26, '2019_01_30_131539_create_user_role_responsibility_trans_table', 1),
(27, '2019_02_11_064328_create_attendance_registers_table', 1),
(28, '2019_02_14_174442_create_leave_registers_table', 1),
(29, '2019_02_15_153309_create_early_leave_registers_table', 1),
(30, '2019_02_18_170120_create_logs_table', 1),
(31, '2019_02_20_125506_create_notifications_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` int(10) UNSIGNED NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `platforms`
--

CREATE TABLE `platforms` (
  `platform_id` int(10) UNSIGNED NOT NULL,
  `platform_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `platforms`
--

INSERT INTO `platforms` (`platform_id`, `platform_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Android', '1', '2019-04-08 17:50:17', '2019-04-08 17:50:17'),
(2, 'IOS', '1', '2019-04-08 17:50:17', '2019-04-08 17:50:17'),
(3, 'Web', '1', '2019-04-08 17:50:17', '2019-04-08 17:50:17'),
(4, 'API', '1', '2019-04-08 17:50:17', '2019-04-08 17:50:17'),
(5, 'MEAN Stack', '1', '2019-04-08 17:50:17', '2019-04-08 17:50:17');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(10) UNSIGNED NOT NULL,
  `project_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `platform` text COLLATE utf8mb4_unicode_ci,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_owner` int(10) UNSIGNED DEFAULT NULL,
  `project_manager` int(10) UNSIGNED DEFAULT NULL,
  `project_start_date` date DEFAULT NULL,
  `project_end_date` date DEFAULT NULL,
  `project_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `project_name`, `platform`, `status`, `project_owner`, `project_manager`, `project_start_date`, `project_end_date`, `project_description`, `created_at`, `updated_at`) VALUES
(1, 'Team Portal', NULL, '1', 2, 2, '2019-04-11', NULL, 'This is a attendance Project.', '2019-04-11 21:08:10', '2019-04-11 21:08:10'),
(2, 'School Management', NULL, '1', 2, 2, '2019-04-11', NULL, 'School Management project', '2019-04-11 21:08:43', '2019-04-11 21:08:43'),
(3, 'Review Management Testing', NULL, '1', 2, 2, '2019-02-01', '2019-05-31', NULL, '2019-04-13 20:55:43', '2019-04-13 20:55:43');

-- --------------------------------------------------------

--
-- Table structure for table `project_assigns`
--

CREATE TABLE `project_assigns` (
  `project_assign_id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `project_assign_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_assigns`
--

INSERT INTO `project_assigns` (`project_assign_id`, `project_id`, `admin_id`, `project_assign_status`, `created_at`, `updated_at`) VALUES
(1, 2, 8, '1', '2019-04-11 21:08:59', '2019-04-11 21:08:59'),
(2, 2, 15, '1', '2019-04-11 21:08:59', '2019-04-11 21:08:59'),
(3, 1, 8, '1', '2019-04-11 21:09:07', '2019-04-11 22:36:04'),
(4, 1, 4, '1', '2019-04-11 22:36:04', '2019-04-11 22:36:04'),
(5, 3, 4, '1', '2019-04-13 20:56:28', '2019-04-13 20:56:28'),
(6, 3, 8, '1', '2019-04-13 20:56:28', '2019-04-13 20:56:28'),
(7, 3, 2, '1', '2019-04-13 20:56:28', '2019-04-13 20:56:28');

-- --------------------------------------------------------

--
-- Table structure for table `project_details`
--

CREATE TABLE `project_details` (
  `project_detail_id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  `platform_id` int(10) UNSIGNED DEFAULT NULL,
  `coder_id` int(10) UNSIGNED DEFAULT NULL,
  `code_reviewer_id` int(10) UNSIGNED DEFAULT NULL,
  `frequently_code_review` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_review_day` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `release_plans`
--

CREATE TABLE `release_plans` (
  `release_plan_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `year` text COLLATE utf8mb4_unicode_ci,
  `json` text COLLATE utf8mb4_unicode_ci,
  `total` decimal(8,2) DEFAULT NULL,
  `jan` decimal(8,2) DEFAULT NULL,
  `feb` decimal(8,2) DEFAULT NULL,
  `mar` decimal(8,2) DEFAULT NULL,
  `apr` decimal(8,2) DEFAULT NULL,
  `may` decimal(8,2) DEFAULT NULL,
  `jun` decimal(8,2) DEFAULT NULL,
  `july` decimal(8,2) DEFAULT NULL,
  `aug` decimal(8,2) DEFAULT NULL,
  `sep` decimal(8,2) DEFAULT NULL,
  `oct` decimal(8,2) DEFAULT NULL,
  `nov` decimal(8,2) DEFAULT NULL,
  `dec` decimal(8,2) DEFAULT NULL,
  `utilized` double(8,2) NOT NULL DEFAULT '0.00',
  `unutilized` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `release_plan_logs`
--

CREATE TABLE `release_plan_logs` (
  `release_plan_log_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `month_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `utilized` double(8,2) NOT NULL,
  `unutilized` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `release_plan_logs`
--

INSERT INTO `release_plan_logs` (`release_plan_log_id`, `admin_id`, `month_year`, `utilized`, `unutilized`, `created_at`, `updated_at`) VALUES
(1, 5, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(2, 2, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(3, 9, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(4, 10, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(5, 11, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(6, 12, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(7, 13, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(8, 4, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(9, 14, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(10, 7, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(11, 15, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(12, 16, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(13, 17, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(14, 18, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(15, 19, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(16, 8, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(17, 3, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(18, 20, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(19, 21, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(20, 22, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(21, 23, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(22, 24, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48'),
(23, 25, 'January 2019', 0.00, 0.00, '2019-04-09 18:29:48', '2019-04-09 18:29:48');

-- --------------------------------------------------------

--
-- Table structure for table `review_check_lists`
--

CREATE TABLE `review_check_lists` (
  `review_check_id` int(10) UNSIGNED NOT NULL,
  `review_check` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weekly_time` int(11) NOT NULL DEFAULT '0',
  `fortnightly_time` int(11) NOT NULL DEFAULT '0',
  `monthly_time` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 - Active, 0 - Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `review_check_lists`
--

INSERT INTO `review_check_lists` (`review_check_id`, `review_check`, `weekly_time`, `fortnightly_time`, `monthly_time`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Code Comments', 5, 10, 15, 1, '2019-04-08 17:50:17', '2019-04-08 17:50:17'),
(2, 'Coding Standards', 7, 14, 21, 1, '2019-04-08 17:50:17', '2019-04-08 17:50:17'),
(3, 'Database', 10, 13, 15, 1, '2019-04-08 17:50:17', '2019-04-08 17:50:17'),
(4, 'Logic', 10, 15, 20, 1, '2019-04-08 17:50:17', '2019-04-08 17:50:17'),
(5, 'Syntax', 5, 8, 12, 1, '2019-04-08 17:50:17', '2019-04-08 17:50:17');

-- --------------------------------------------------------

--
-- Table structure for table `time_slot`
--

CREATE TABLE `time_slot` (
  `time_slot_id` int(10) UNSIGNED NOT NULL,
  `time_slot_time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `time_slot`
--

INSERT INTO `time_slot` (`time_slot_id`, `time_slot_time`, `created_at`, `updated_at`) VALUES
(1, '09:30:00', '2019-04-08 17:50:05', '2019-04-08 17:50:05'),
(2, '10:00:00', '2019-04-08 17:50:05', '2019-04-08 17:50:05'),
(3, '10:30:00', '2019-04-08 17:50:05', '2019-04-08 17:50:05');

-- --------------------------------------------------------

--
-- Table structure for table `time_slot_history`
--

CREATE TABLE `time_slot_history` (
  `time_slot_history_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `time_slot_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_role_id` int(10) UNSIGNED NOT NULL,
  `user_role_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_role_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `isWorkTimeRequired` tinyint(4) NOT NULL DEFAULT '0',
  `isfixed` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_role_id`, `user_role_name`, `user_role_status`, `isWorkTimeRequired`, `isfixed`, `created_at`, `updated_at`) VALUES
(1, 'Owner', '1', 0, '1', '2019-04-08 17:50:42', '2019-04-08 17:50:42'),
(2, 'Manager', '1', 0, '1', '2019-04-08 17:50:42', '2019-04-08 17:50:42'),
(3, 'Code Reviewer', '1', 0, '1', '2019-04-08 17:50:42', '2019-04-08 17:50:42'),
(4, 'Developer', '1', 1, '0', '2019-04-08 17:57:02', '2019-04-08 17:57:08');

-- --------------------------------------------------------

--
-- Table structure for table `user_role_responsibility_transs`
--

CREATE TABLE `user_role_responsibility_transs` (
  `user_Role_responsibility_trans_id` int(10) UNSIGNED NOT NULL,
  `user_role_id` int(10) UNSIGNED NOT NULL,
  `work_type_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `work_times`
--

CREATE TABLE `work_times` (
  `work_time_id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED DEFAULT NULL,
  `work_time_date` date DEFAULT NULL,
  `total_work_time_hr` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `work_time_details`
--

CREATE TABLE `work_time_details` (
  `work_time_detail_id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL,
  `work_time_id` int(10) UNSIGNED NOT NULL,
  `work_type_id` int(10) UNSIGNED NOT NULL,
  `work_time_detail_hrs` text COLLATE utf8mb4_unicode_ci,
  `work_time_detail_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `work_types`
--

CREATE TABLE `work_types` (
  `work_type_id` int(10) UNSIGNED NOT NULL,
  `work_type_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_type_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `work_type_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `work_types`
--

INSERT INTO `work_types` (`work_type_id`, `work_type_name`, `work_type_status`, `work_type_description`, `created_at`, `updated_at`) VALUES
(1, 'Client Meeting & Demo', '1', 'Client Meeting & Demo', '2019-04-08 17:50:34', '2019-04-08 17:50:34'),
(2, 'Change Request & Smoke Testing', '1', 'Change Request & Smoke Testing', '2019-04-08 17:50:34', '2019-04-08 17:50:34'),
(3, 'Design', '1', 'Design', '2019-04-08 17:50:34', '2019-04-08 17:50:34'),
(4, 'Development & Smoke Testing', '1', 'Development & Smoke Testing', '2019-04-08 17:50:34', '2019-04-08 17:50:34'),
(5, 'Support & Bug Fixing', '1', 'Support & Bug Fixing', '2019-04-08 17:50:34', '2019-04-08 17:50:34'),
(6, 'Testing', '1', 'Testing', '2019-04-08 17:50:34', '2019-04-08 17:50:34'),
(7, 'Team Meeting', '1', 'Team Meeting', '2019-04-08 17:50:34', '2019-04-08 17:50:34'),
(8, 'Work on Tester Report', '1', 'Work on Tester Report', '2019-04-08 17:50:34', '2019-04-08 17:50:34'),
(9, 'Code Review', '1', 'Code Review', '2019-04-08 17:50:34', '2019-04-08 17:50:34'),
(10, 'Review & Guideline', '1', 'Review & Guideline', '2019-04-08 17:50:34', '2019-04-08 17:50:34'),
(11, 'Reporting', '1', 'Reporting', '2019-04-08 17:50:34', '2019-04-08 17:50:34'),
(12, 'Documentation / Mockups', '1', 'Documentation / Mockups', '2019-04-08 17:50:35', '2019-04-08 17:50:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`),
  ADD UNIQUE KEY `admins_emp_id_unique` (`emp_id`),
  ADD KEY `admins_department_id_foreign` (`department_id`),
  ADD KEY `admins_time_slot_id_foreign` (`time_slot_id`);

--
-- Indexes for table `admin_password_resets`
--
ALTER TABLE `admin_password_resets`
  ADD KEY `admin_password_resets_email_index` (`email`),
  ADD KEY `admin_password_resets_token_index` (`token`);

--
-- Indexes for table `assign_user_roles`
--
ALTER TABLE `assign_user_roles`
  ADD PRIMARY KEY (`assign_user_role_id`),
  ADD KEY `assign_user_roles_admin_id_foreign` (`admin_id`),
  ADD KEY `assign_user_roles_user_role_id_foreign` (`user_role_id`);

--
-- Indexes for table `attendancedatas`
--
ALTER TABLE `attendancedatas`
  ADD PRIMARY KEY (`attendancedata_id`),
  ADD KEY `attendancedatas_attendance_id_foreign` (`attendance_id`),
  ADD KEY `attendancedatas_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `attendancedata_logs`
--
ALTER TABLE `attendancedata_logs`
  ADD PRIMARY KEY (`attendancedata_log_id`),
  ADD KEY `attendancedata_logs_admin_id_foreign` (`admin_id`),
  ADD KEY `attendancedata_logs_employee_id_foreign` (`employee_id`),
  ADD KEY `attendancedata_logs_attendancedata_id_foreign` (`attendancedata_id`);

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`attendance_id`);

--
-- Indexes for table `attendance_registers`
--
ALTER TABLE `attendance_registers`
  ADD PRIMARY KEY (`attendance_register_id`),
  ADD KEY `attendance_registers_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `code_reviews`
--
ALTER TABLE `code_reviews`
  ADD PRIMARY KEY (`code_review_id`),
  ADD KEY `code_reviews_project_id_foreign` (`project_id`),
  ADD KEY `code_reviews_coder_id_foreign` (`coder_id`),
  ADD KEY `code_reviews_code_reviewer_id_foreign` (`code_reviewer_id`);

--
-- Indexes for table `code_review_details`
--
ALTER TABLE `code_review_details`
  ADD PRIMARY KEY (`code_review_detail_id`),
  ADD KEY `code_review_details_code_review_info_id_foreign` (`code_review_info_id`),
  ADD KEY `code_review_details_code_review_id_foreign` (`code_review_id`),
  ADD KEY `code_review_details_review_check_id_foreign` (`review_check_id`);

--
-- Indexes for table `code_review_infos`
--
ALTER TABLE `code_review_infos`
  ADD PRIMARY KEY (`code_review_info_id`),
  ADD KEY `code_review_infos_project_id_foreign` (`project_id`),
  ADD KEY `code_review_infos_code_reviewer_id_foreign` (`code_reviewer_id`);

--
-- Indexes for table `company_configs`
--
ALTER TABLE `company_configs`
  ADD PRIMARY KEY (`company_config_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `early_leave_registers`
--
ALTER TABLE `early_leave_registers`
  ADD PRIMARY KEY (`early_leave_register_id`),
  ADD KEY `early_leave_registers_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `holiday_calendars`
--
ALTER TABLE `holiday_calendars`
  ADD PRIMARY KEY (`holiday_calendar_id`);

--
-- Indexes for table `leave_registers`
--
ALTER TABLE `leave_registers`
  ADD PRIMARY KEY (`leave_register_id`),
  ADD KEY `leave_registers_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `logs_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`);

--
-- Indexes for table `platforms`
--
ALTER TABLE `platforms`
  ADD PRIMARY KEY (`platform_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`),
  ADD KEY `projects_project_owner_foreign` (`project_owner`),
  ADD KEY `projects_project_manager_foreign` (`project_manager`);

--
-- Indexes for table `project_assigns`
--
ALTER TABLE `project_assigns`
  ADD PRIMARY KEY (`project_assign_id`),
  ADD KEY `project_assigns_project_id_foreign` (`project_id`),
  ADD KEY `project_assigns_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `project_details`
--
ALTER TABLE `project_details`
  ADD PRIMARY KEY (`project_detail_id`),
  ADD KEY `project_details_project_id_foreign` (`project_id`),
  ADD KEY `project_details_platform_id_foreign` (`platform_id`),
  ADD KEY `project_details_coder_id_foreign` (`coder_id`),
  ADD KEY `project_details_code_reviewer_id_foreign` (`code_reviewer_id`);

--
-- Indexes for table `release_plans`
--
ALTER TABLE `release_plans`
  ADD PRIMARY KEY (`release_plan_id`),
  ADD KEY `release_plans_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `release_plan_logs`
--
ALTER TABLE `release_plan_logs`
  ADD PRIMARY KEY (`release_plan_log_id`),
  ADD KEY `release_plan_logs_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `review_check_lists`
--
ALTER TABLE `review_check_lists`
  ADD PRIMARY KEY (`review_check_id`);

--
-- Indexes for table `time_slot`
--
ALTER TABLE `time_slot`
  ADD PRIMARY KEY (`time_slot_id`);

--
-- Indexes for table `time_slot_history`
--
ALTER TABLE `time_slot_history`
  ADD PRIMARY KEY (`time_slot_history_id`),
  ADD KEY `time_slot_history_admin_id_foreign` (`admin_id`),
  ADD KEY `time_slot_history_time_slot_id_foreign` (`time_slot_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_role_id`);

--
-- Indexes for table `user_role_responsibility_transs`
--
ALTER TABLE `user_role_responsibility_transs`
  ADD PRIMARY KEY (`user_Role_responsibility_trans_id`),
  ADD KEY `user_role_responsibility_transs_user_role_id_foreign` (`user_role_id`),
  ADD KEY `user_role_responsibility_transs_work_type_id_foreign` (`work_type_id`);

--
-- Indexes for table `work_times`
--
ALTER TABLE `work_times`
  ADD PRIMARY KEY (`work_time_id`),
  ADD KEY `work_times_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `work_time_details`
--
ALTER TABLE `work_time_details`
  ADD PRIMARY KEY (`work_time_detail_id`),
  ADD KEY `work_time_details_project_id_foreign` (`project_id`),
  ADD KEY `work_time_details_work_time_id_foreign` (`work_time_id`),
  ADD KEY `work_time_details_work_type_id_foreign` (`work_type_id`);

--
-- Indexes for table `work_types`
--
ALTER TABLE `work_types`
  ADD PRIMARY KEY (`work_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `assign_user_roles`
--
ALTER TABLE `assign_user_roles`
  MODIFY `assign_user_role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `attendancedatas`
--
ALTER TABLE `attendancedatas`
  MODIFY `attendancedata_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attendancedata_logs`
--
ALTER TABLE `attendancedata_logs`
  MODIFY `attendancedata_log_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `attendance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attendance_registers`
--
ALTER TABLE `attendance_registers`
  MODIFY `attendance_register_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `code_reviews`
--
ALTER TABLE `code_reviews`
  MODIFY `code_review_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `code_review_details`
--
ALTER TABLE `code_review_details`
  MODIFY `code_review_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `code_review_infos`
--
ALTER TABLE `code_review_infos`
  MODIFY `code_review_info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `company_configs`
--
ALTER TABLE `company_configs`
  MODIFY `company_config_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `department_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `early_leave_registers`
--
ALTER TABLE `early_leave_registers`
  MODIFY `early_leave_register_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `event_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `holiday_calendars`
--
ALTER TABLE `holiday_calendars`
  MODIFY `holiday_calendar_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leave_registers`
--
ALTER TABLE `leave_registers`
  MODIFY `leave_register_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `log_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `platforms`
--
ALTER TABLE `platforms`
  MODIFY `platform_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `project_assigns`
--
ALTER TABLE `project_assigns`
  MODIFY `project_assign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `project_details`
--
ALTER TABLE `project_details`
  MODIFY `project_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `release_plans`
--
ALTER TABLE `release_plans`
  MODIFY `release_plan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `release_plan_logs`
--
ALTER TABLE `release_plan_logs`
  MODIFY `release_plan_log_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `review_check_lists`
--
ALTER TABLE `review_check_lists`
  MODIFY `review_check_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `time_slot`
--
ALTER TABLE `time_slot`
  MODIFY `time_slot_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `time_slot_history`
--
ALTER TABLE `time_slot_history`
  MODIFY `time_slot_history_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `user_role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_role_responsibility_transs`
--
ALTER TABLE `user_role_responsibility_transs`
  MODIFY `user_Role_responsibility_trans_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `work_times`
--
ALTER TABLE `work_times`
  MODIFY `work_time_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `work_time_details`
--
ALTER TABLE `work_time_details`
  MODIFY `work_time_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `work_types`
--
ALTER TABLE `work_types`
  MODIFY `work_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `admins_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`department_id`),
  ADD CONSTRAINT `admins_time_slot_id_foreign` FOREIGN KEY (`time_slot_id`) REFERENCES `time_slot` (`time_slot_id`);

--
-- Constraints for table `assign_user_roles`
--
ALTER TABLE `assign_user_roles`
  ADD CONSTRAINT `assign_user_roles_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `assign_user_roles_user_role_id_foreign` FOREIGN KEY (`user_role_id`) REFERENCES `user_roles` (`user_role_id`);

--
-- Constraints for table `attendancedatas`
--
ALTER TABLE `attendancedatas`
  ADD CONSTRAINT `attendancedatas_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `attendancedatas_attendance_id_foreign` FOREIGN KEY (`attendance_id`) REFERENCES `attendances` (`attendance_id`) ON DELETE CASCADE;

--
-- Constraints for table `attendancedata_logs`
--
ALTER TABLE `attendancedata_logs`
  ADD CONSTRAINT `attendancedata_logs_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `attendancedata_logs_attendancedata_id_foreign` FOREIGN KEY (`attendancedata_id`) REFERENCES `attendancedatas` (`attendancedata_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `attendancedata_logs_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `attendance_registers`
--
ALTER TABLE `attendance_registers`
  ADD CONSTRAINT `attendance_registers_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `code_reviews`
--
ALTER TABLE `code_reviews`
  ADD CONSTRAINT `code_reviews_code_reviewer_id_foreign` FOREIGN KEY (`code_reviewer_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `code_reviews_coder_id_foreign` FOREIGN KEY (`coder_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `code_reviews_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE CASCADE;

--
-- Constraints for table `code_review_details`
--
ALTER TABLE `code_review_details`
  ADD CONSTRAINT `code_review_details_code_review_id_foreign` FOREIGN KEY (`code_review_id`) REFERENCES `code_reviews` (`code_review_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `code_review_details_code_review_info_id_foreign` FOREIGN KEY (`code_review_info_id`) REFERENCES `code_review_infos` (`code_review_info_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `code_review_details_review_check_id_foreign` FOREIGN KEY (`review_check_id`) REFERENCES `review_check_lists` (`review_check_id`) ON DELETE CASCADE;

--
-- Constraints for table `code_review_infos`
--
ALTER TABLE `code_review_infos`
  ADD CONSTRAINT `code_review_infos_code_reviewer_id_foreign` FOREIGN KEY (`code_reviewer_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `code_review_infos_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE CASCADE;

--
-- Constraints for table `early_leave_registers`
--
ALTER TABLE `early_leave_registers`
  ADD CONSTRAINT `early_leave_registers_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `leave_registers`
--
ALTER TABLE `leave_registers`
  ADD CONSTRAINT `leave_registers_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_project_manager_foreign` FOREIGN KEY (`project_manager`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `projects_project_owner_foreign` FOREIGN KEY (`project_owner`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `project_assigns`
--
ALTER TABLE `project_assigns`
  ADD CONSTRAINT `project_assigns_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `project_assigns_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`);

--
-- Constraints for table `project_details`
--
ALTER TABLE `project_details`
  ADD CONSTRAINT `project_details_code_reviewer_id_foreign` FOREIGN KEY (`code_reviewer_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `project_details_coder_id_foreign` FOREIGN KEY (`coder_id`) REFERENCES `admins` (`admin_id`),
  ADD CONSTRAINT `project_details_platform_id_foreign` FOREIGN KEY (`platform_id`) REFERENCES `platforms` (`platform_id`),
  ADD CONSTRAINT `project_details_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE CASCADE;

--
-- Constraints for table `release_plans`
--
ALTER TABLE `release_plans`
  ADD CONSTRAINT `release_plans_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `release_plan_logs`
--
ALTER TABLE `release_plan_logs`
  ADD CONSTRAINT `release_plan_logs_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE;

--
-- Constraints for table `time_slot_history`
--
ALTER TABLE `time_slot_history`
  ADD CONSTRAINT `time_slot_history_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `time_slot_history_time_slot_id_foreign` FOREIGN KEY (`time_slot_id`) REFERENCES `time_slot` (`time_slot_id`) ON DELETE CASCADE;

--
-- Constraints for table `user_role_responsibility_transs`
--
ALTER TABLE `user_role_responsibility_transs`
  ADD CONSTRAINT `user_role_responsibility_transs_user_role_id_foreign` FOREIGN KEY (`user_role_id`) REFERENCES `user_roles` (`user_role_id`),
  ADD CONSTRAINT `user_role_responsibility_transs_work_type_id_foreign` FOREIGN KEY (`work_type_id`) REFERENCES `work_types` (`work_type_id`);

--
-- Constraints for table `work_times`
--
ALTER TABLE `work_times`
  ADD CONSTRAINT `work_times_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`admin_id`);

--
-- Constraints for table `work_time_details`
--
ALTER TABLE `work_time_details`
  ADD CONSTRAINT `work_time_details_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`),
  ADD CONSTRAINT `work_time_details_work_time_id_foreign` FOREIGN KEY (`work_time_id`) REFERENCES `work_times` (`work_time_id`),
  ADD CONSTRAINT `work_time_details_work_type_id_foreign` FOREIGN KEY (`work_type_id`) REFERENCES `work_types` (`work_type_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
