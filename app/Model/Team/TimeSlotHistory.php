<?php

namespace App\Model\Team;

use Illuminate\Database\Eloquent\Model;

class TimeSlotHistory extends Model
{
    protected $table      = 'time_slot_history';
    protected $primaryKey = 'time_slot_history_id';
}
