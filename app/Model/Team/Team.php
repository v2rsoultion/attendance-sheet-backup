<?php

namespace App\Model\Team;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Team extends Model
{
    use Notifiable;

    protected $table = 'admins';
    protected $primaryKey = 'admin_id';

    public function getDepartment()
    {
        return $this->belongsTo('App\Model\Department\Department', 'department_id');
    }

    public function getTimeSlot()
    {
        return $this->belongsTo('App\Model\Team\TimeSlot', 'time_slot_id');
    }

    public function getTimeSlotHistory()
    {
        return $this->hasMany('App\Model\Team\TimeSlotHistory', 'admin_id');
    }

    public function getReleasePlan()
    {
        return $this->hasMany('App\Model\ReleasePlan\ReleasePlan', 'admin_id');
    }

    public function getReleasePlanLog()
    {
        return $this->hasMany('App\Model\ReleasePlan\ReleasePlanLog', 'admin_id');
    }

    public function getAttendanceData()
    {
        return $this->hasMany('App\Model\Attendancedata\Attendancedata', 'admin_id');
    }

    public function getUserRole()
    {
        return $this->hasMany('App\Model\UserRole\AssignUserRole', 'admin_id');
    }

    public function getAttendanceRegister()
    {
        return $this->hasMany('App\Model\Attendance\AttendanceRegister', 'admin_id');
    }
}