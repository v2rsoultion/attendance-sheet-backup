<?php

namespace App\Model\Team;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    protected $table      = 'platforms';
    protected $primaryKey = 'platform_id';
}
