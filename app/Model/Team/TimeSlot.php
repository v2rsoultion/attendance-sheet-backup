<?php

namespace App\Model\Team;

use Illuminate\Database\Eloquent\Model;

class TimeSlot extends Model
{
    protected $table      = 'time_slot';
    protected $primaryKey = 'time_slot_id';
}
