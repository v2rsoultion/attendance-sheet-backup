<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;

class CodeReviewDetail extends Model
{
    protected $tableName      = 'code_review_details';
    protected $primaryKey     = 'code_review_detail_id';

    public function getCodeReview()
    {
        return $this->belongsTo('App\Model\Project\CodeReview', 'code_review_id');
    }
    public function getCodeReviewInfo()
    {
        return $this->belongsTo('App\Model\Project\CodeReviewInfo', 'code_review_info_id');
    }
}
