<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectDetails extends Model
{
    protected $tableName      = 'project_details'; 
    protected $primaryKey     = 'project_detail_id';

    public function getAdmins()
    {
        return $this->hasOne('App\Model\Team\Team', 'admin_id');
    }
    
    public function getProject()
    {
        return $this->belongsTo('App\Model\Project\Project', 'project_id');
    }

    public function getCodeReviews()
    {
        return $this->hasMany('App\Model\Project\CodeReview', 'project_detail_id');
    }
}
