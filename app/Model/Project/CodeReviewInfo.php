<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;

class CodeReviewInfo extends Model
{
    //
    protected $tableName      = 'code_review_infos';
    protected $primaryKey     = 'code_review_info_id';

    public function getCodeReviewDetails()
    {
        return $this->hasMany('App\Model\Project\CodeReviewDetail', 'code_review_info_id');
    }
    public function getProject()
    {
        return $this->belongsTo('App\Model\Project\Project', 'project_id');
    }
}
