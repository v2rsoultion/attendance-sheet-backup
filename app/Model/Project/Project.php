<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $tableName      = 'projects';
    protected $primaryKey     = 'project_id';

    public function getCodeReviews()
    {
        return $this->hasMany('App\Model\Project\CodeReview', 'project_id');
    }

    public function getCodeReviewsActive()
    {
        return $this->hasMany('App\Model\Project\CodeReview', 'project_id')->where('status', '1');
    }

    public function getProjectAssign()
    {
        return $this->hasMany('App\Model\Project\ProjectAssign', 'project_id');
    }

    public function getProjectAssignActive()
    {
        return $this->hasMany('App\Model\Project\ProjectAssign', 'project_id')->where('project_assign_status', '1');
    }
}
