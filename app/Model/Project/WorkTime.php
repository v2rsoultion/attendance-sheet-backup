<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;

class WorkTime extends Model
{
    protected $tableName      = 'work_times'; 
    protected $primaryKey     = 'work_time_id';
    
    public function getWorkTimeDetails()
    {
        return $this->hasMany('App\Model\Project\WorkTimeDetail', 'work_time_id');
    }
}
