<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;

class CodeReview extends Model
{
    protected $tableName      = 'code_reviews';
    protected $primaryKey     = 'code_review_id';

    public function getProjectDetails()
    {
        return $this->belongsTo('App\Model\Project\Project', 'project_id');
    }

    public function getCoderDetails()
    {
        return $this->belongsTo('App\Model\Team\Team', 'coder_id');
    }

    public function getCodeReviewerDetails()
    {
        return $this->belongsTo('App\Model\Team\Team', 'code_reviewer_id');
    }

    public function getCodeReviewDetails()
    {
        return $this->hasMany('App\Model\Project\CodeReviewDetail', 'code_review_id');
    }
}
