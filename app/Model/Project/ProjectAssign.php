<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectAssign extends Model
{
    protected $tableName      = 'project_assigns'; 
    protected $primaryKey     = 'project_assign_id'; 

    protected $fillable = ['pathpath'];

    public function getDeveloper()
    {
        return $this->belongsTo('App\Model\Team\Team', 'admin_id');
    }

    public function getProject()
    {
        return $this->belongsTo('App\Model\Project\Project', 'project_id');
    }
}
