<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;

class WorkType extends Model
{
    protected $tableName      = 'work_types'; 
    protected $primaryKey     = 'work_type_id'; 
}
