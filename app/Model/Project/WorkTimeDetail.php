<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;

class WorkTimeDetail extends Model
{
    protected $tableName      = 'work_time_details'; 
    protected $primaryKey     = 'work_time_detail_id';
    
    public function getProjectDetails()
    {
        return $this->belongsTo('App\Model\Project\Project', 'project_id');
    }
}
