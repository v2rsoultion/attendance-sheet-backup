<?php

namespace App\Model\Project;

use Illuminate\Database\Eloquent\Model;

class ReviewCheckList extends Model
{
    protected $tableName      = 'review_check_lists';
    protected $primaryKey     = 'review_check_id';
}
