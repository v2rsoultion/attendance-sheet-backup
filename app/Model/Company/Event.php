<?php

namespace App\Model\Company;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $tableName      = 'events'; 
    protected $primaryKey     = 'event_id';
}
