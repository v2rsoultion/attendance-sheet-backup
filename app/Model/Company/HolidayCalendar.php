<?php

namespace App\Model\Company;

use Illuminate\Database\Eloquent\Model;

class HolidayCalendar extends Model
{
    protected $tableName      = 'holiday_calendars'; 
    protected $primaryKey     = 'holiday_calendar_id';
}
