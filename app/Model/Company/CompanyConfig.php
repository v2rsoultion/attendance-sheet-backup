<?php

namespace App\Model\Company;

use Illuminate\Database\Eloquent\Model;

class CompanyConfig extends Model
{
    protected $tableName      = 'company_configs'; 
    protected $primaryKey     = 'company_config_id';
}
