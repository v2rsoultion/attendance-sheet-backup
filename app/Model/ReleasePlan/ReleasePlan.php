<?php

namespace App\Model\ReleasePlan;

use Illuminate\Database\Eloquent\Model;

class ReleasePlan extends Model
{
    protected $tableName      = 'release_plans'; 
    protected $primaryKey     = 'release_plan_id';
}
