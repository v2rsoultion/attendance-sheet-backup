<?php

namespace App\Model\ReleasePlan;

use Illuminate\Database\Eloquent\Model;

class ReleasePlanLog extends Model
{
    protected $tableName      = 'release_plan_logs'; 
    protected $primaryKey     = 'release_plan_log_id';
}
