<?php

namespace App\Model\Attendancedata;

use Illuminate\Database\Eloquent\Model;

class AttendancedataLogs extends Model
{
    //
    protected $table      = 'attendancedata_logs';
    protected $primaryKey = 'attendancedata_log_id';
}
