<?php

namespace App\Model\Attendancedata;

use Illuminate\Database\Eloquent\Model;

class Attendancedata extends Model
{
    protected $primaryKey = 'attendancedata_id';

    public function getAttendance()
    {
        return $this->belongsTo('App\Model\Attendance\Attendance', 'attendance_id');
    }
    public function getEmployee()
    {
        return $this->belongsTo('App\Model\Team\Team', 'admin_id');
    }
}
