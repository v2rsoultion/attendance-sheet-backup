<?php

namespace App\Model\UserRole;

use Illuminate\Database\Eloquent\Model;

class AssignUserRole extends Model
{
    protected $table = 'assign_user_roles';
    protected $primaryKey = 'assign_user_role_id';

    public function getUserRole()
    {
        return $this->belongsTo('App\Model\UserRole\UserRole', 'user_role_id');
    }

}
