<?php

namespace App\Model\UserRole;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'user_roles';
    protected $primaryKey = 'user_role_id';

    public function getResponsibility()
    {
        return $this->hasMany('App\Model\UserRole\UserRoleResponsibilityTrans', 'user_role_id');
    }
}
