<?php

namespace App\Model\UserRole;

use Illuminate\Database\Eloquent\Model;

class UserRoleResponsibilityTrans extends Model
{
    protected $table = 'user_role_responsibility_transs';
    protected $primaryKey = 'user_role_responsibility_trans_id';

    public function getWorkType()
    {
        return $this->belongsTo('App\Model\Project\WorkType', 'work_type_id');
    }
}

