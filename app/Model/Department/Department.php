<?php

namespace App\Model\Department;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $tableName      = 'department'; 
    protected $primaryKey     = 'department_id';
}
