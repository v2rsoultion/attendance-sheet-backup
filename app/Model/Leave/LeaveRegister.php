<?php

namespace App\Model\Leave;

use Illuminate\Database\Eloquent\Model;

class LeaveRegister extends Model
{
    //
    protected $tableName = 'leave_registers';
    protected $primaryKey = 'leave_register_id';

    public function getEmployeeData()
    {
        return $this->belongsTo('App\Model\Team\Team', 'admin_id');
    }
}
