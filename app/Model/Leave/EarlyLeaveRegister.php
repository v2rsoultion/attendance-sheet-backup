<?php

namespace App\Model\Leave;

use Illuminate\Database\Eloquent\Model;

class EarlyLeaveRegister extends Model
{
    //
    protected $tableName = 'early_leave_registers';
    protected $primaryKey = 'early_leave_register_id';

    public function getEmployeeData()
    {
        return $this->belongsTo('App\Model\Team\Team', 'admin_id');
    }
}
