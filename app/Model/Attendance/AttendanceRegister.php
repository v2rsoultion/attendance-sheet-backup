<?php

namespace App\Model\Attendance;

use Illuminate\Database\Eloquent\Model;

class AttendanceRegister extends Model
{
    // Use Model for In Out Time
    protected $table = 'attendance_registers';
    protected $primaryKey = 'attendance_register_id';

    public function getEmployeeData()
    {
        return $this->belongsTo('App\Model\Team\Team', 'admin_id');
    }
}
