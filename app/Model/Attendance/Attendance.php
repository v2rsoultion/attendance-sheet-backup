<?php

namespace App\Model\Attendance;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $primaryKey = 'attendance_id';

    public function getAttendanceData()
    {
        return $this->hasMany('App\Model\Attendancedata\Attendancedata', 'attendance_id');
    }
}



