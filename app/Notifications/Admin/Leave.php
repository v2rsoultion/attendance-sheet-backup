<?php

namespace App\Notifications\Admin;

use View;
use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\DatabaseMessage;

class Leave extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    protected $arr;


    public function __construct(array $arr)
    {
        $this->arr = $arr;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        // In Case of multiple Notify
        return ['database', 'mail'];
        // return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $data = $this->arr;
        $email_type = '';
        $reason = '';
        $leave_date = '';
        $request_date = date('d-m-Y');
        $condition = Config::get('custom.notification_message');
        $condition_key = array_search($data['message'], $condition);
        $flag = 0;

        if ($condition_key == 'leave') {
            $reason = $data['reason'];
            $leave_date = $data['leave_date'];
            $email_type = 'Leave';
            $flag = 1;
        } else if ($condition_key == 'early_leave') {
            $email_type = 'Early Leave';
            $flag = 2;
        } else if ($condition_key == 'attendance_request') {
            $flag = 3;
            $email_type = 'Attendance';
        } else {
            $email_type = '--';
        }

        $to_name =  'Admin';
        $from_name =  $data['emp_name'];
        $subject = $email_type . ' request from ' . $data['emp_name'];

        return (new MailMessage)->view('admin-panel.auth.mail.request_mail', compact('to_name', 'from_name', 'email_type', 'flag', 'email_type', 'request_date', 'reason', 'leave_date'))->subject($subject);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\DatabaseMessage
     */

    public function toDatabase($notifiable)
    {
        return [
            'data' => $this->arr,
            // 'action' => url($this->post->slug)
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        //Return Contents
        return [
            'data' => $this->arr,
        ];
    }
}
