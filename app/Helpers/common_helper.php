<?php

//use File;

use Illuminate\Support\Facades\Crypt;
use Session as LSession;
use Carbon\Carbon;
use App\Admin;
use App\Model\Logs\Log;
use App\Model\Team\Team;
use App\Model\UserRole\AssignUserRole;
use App\Model\Team\Platform;
use App\Model\Team\TimeSlot;
use App\Model\Project\Project;
use App\Model\Project\ProjectDetails;
use App\Model\Department\Department;
use App\Model\Attendance\Attendance;
use App\Model\Company\Event;
use App\Model\Attendance\AttendanceRegister;
use App\Model\Leave\LeaveRegister;
use App\Model\Leave\EarlyLeaveRegister;
use App\Model\ReleasePlan\ReleasePlan;
use function GuzzleHttp\json_encode;

/**
 *  Print data in pre formate
 *  @Shree on 22 Aug 2018
 **/
function p($p, $exit = 1)
{
    echo '<pre>';
    print_r($p);
    echo '</pre>';
    if ($exit == 1) {
        exit;
    }
}

/**
 *  Get logged in user data
 *  @Shree on 22 Aug 2018
 **/
function get_loggedin_user_data()
{
    $user_data = array();
    $isCodeReviewer = 0;
    $admin_user = Auth::guard('admin')->user();
    $user_role  = AssignUserRole::where(['admin_id' => $admin_user['admin_id'], 'assign_user_role_status' => '1', 'user_role_id' => 3])->get()->toArray();
    if (!empty($user_role)) {
        $isCodeReviewer = 1;
    }
    if (!empty($admin_user['admin_id'])) {
        $user_data = array(
            'admin_id' => $admin_user['admin_id'],
            'admin_name' => $admin_user['admin_name'],
            'email' => $admin_user['email'],
            'type' => $admin_user['type'],
            'status' => $admin_user['status'],
            'code_reviewer' => $isCodeReviewer,
            'time_slot_id' => $admin_user['time_slot_id'],
            'saturday_off' => $admin_user['saturday_off']
        );
    }
    return $user_data;
}

/**
 *  Get last query
 *  @Shree on 22 Aug 2018
 **/
function last_query($raw = false, $last = false)
{
    DB::enableQueryLog();
    $log = DB::getQueryLog();

    if ($raw) {
        foreach ($log as $key => $query) {
            $log[$key] = vsprintf(str_replace('?', "'%s'", $query['query']), $query['bindings']);
        }
    }

    // return ($last) ? end($log) : $log; // return single table query
    return $log;  // return join tables queries 
}

/**
 *  Get custom date formate
 *  @Shree on 22 Aug 2018
 **/
function get_formatted_date($date)
{
    $return = array();
    $date_format = Config::get('custom.date_format');
    if (!empty($date)) {
        $return = date($date_format, strtotime($date));
    } else {
        $return = '';
    }
    return $return;
}

/**
 *  Get encrypted value
 *  @Shree on 22 Aug 2018
 **/
function get_encrypted_value($key, $encrypt = false)
{
    $encrypted_key = null;
    if (!empty($key)) {
        if ($encrypt == true) {
            $key = Crypt::encrypt($key);
        }
        $encrypted_key = $key;
    }
    return $encrypted_key;
}

/**
 *  Get decrypted value
 *  @Shree on 22 Aug 2018
 **/
function get_decrypted_value($key, $decrypt = false)
{
    $decrypted_key = null;
    if (!empty($key)) {
        if ($decrypt == true) {
            try {
                $key = Crypt::decrypt($key);
            } catch (\Exception $e) {
                return 0;
            }
        }
        $decrypted_key = $key;
    }
    return $decrypted_key;
}

/**
 *  Get total count
 *  @Bhuvanesh on 11 Dec 2018
 **/
function get_total_counter($key, $userId = null)
{
    $counter = 0;
    if ($key == 'teammember') {
        $counter = Admin::where('type', '=', '2')->get()->count();
    }
    if ($key == 'department') {
        $counter = Department::count();
    }
    if ($key == 'projects') {
        if ($userId != null) {
            $counter = ProjectDetails::where('coder_id', '=', $userId)->get()->groupBy('project_id')->count();
        } else {
            $counter = Project::count();
        }
    }
    if ($key == 'codereview-projects') {
        if ($userId != null) {
            $counter = ProjectDetails::where('code_reviewer_id', '=', $userId)->get()->groupBy('project_id')->count();
        } else {
            $counter = Project::count();
        }
    }
    if ($key == 'attendancePending') {
        $counter = AttendanceRegister::where('isRequest_status', '1')->get()->count();
    }
    if ($key == 'leavePending') {
        $counter = LeaveRegister::where('leave_status', 2)->get()->count();
    }
    if ($key == 'earlyLeavePending') {
        $counter = EarlyLeaveRegister::where('leave_status', 2)->get()->count();
    }
    return $counter;
}

/**
 *  Get all departments json for select box
 *  @Bhuvanesh on 11 Dec 2018
 **/
function get_all_departments()
{
    $arr_department = [];
    $arr_department_list = Department::where('status', '=', '1')->select('department_id', 'department_name')->orderBy('department_name', 'ASC')->get();
    if (!empty($arr_department_list)) {
        foreach ($arr_department_list as $arr_departments) {
            $arr_department[$arr_departments['department_id']] = $arr_departments['department_name'];
        }
    }
    return $arr_department;
}

/**
 * Get all time slots 
 * @Bhuvanesh on 22 Dec 2018
 **/
function get_all_time_slot($id = null)
{
    $arr_time_slot = [];
    $arr_time_slot_list = TimeSlot::select('time_slot_id', 'time_slot_time')->orderBy('time_slot_id', 'ASC')->get();
    if (!empty($arr_time_slot_list)) {
        foreach ($arr_time_slot_list as $arr_time_slots) {
            $startTime = substr($arr_time_slots['time_slot_time'], 0, 5);
            $endH = sprintf("%02d", ((substr($arr_time_slots['time_slot_time'], 0, 2) + 9) - 12));
            $endM = (substr($arr_time_slots['time_slot_time'], 3, 2) + 30);
            if ($endM == 60) {
                $endH += 1;
                $endH = sprintf("%02d", $endH);
                $endM = '00';
            }
            $arr_time_slot[$arr_time_slots['time_slot_id']] = $startTime . ' AM - ' . $endH . ':' . $endM . ' PM';
        }
    }
    if (!empty($id)) {
        return $arr_time_slot[$id];
    }
    return $arr_time_slot;
}

/**
 *  Add default blank option to select box
 *  @Shree on 22 Aug 2018
 **/
function add_blank_option($arr, $option)
{
    $arr_option = array();
    if (!empty($option)) {
        $arr_option[''] = $option;
    } else {
        $arr_option[''] = '';
    }
    // operator on array
    $result = $arr_option + $arr;

    return $result;
}

/**
 *  Send mail 
 *  @Bhuvanesh on 14 Dec 2018
 **/
function send_email($name, $email, $path)
{

    $data = array(
        'email' => $email,
        'path' => $path,
        'to_name' => $name,
        'subject' => 'Verification',
        'from' => 'v2rteam@gmail.com',
        'from_name' => 'V2R Solution'
    );
    Mail::send('admin-panel.auth.mail.email_verification', $data, function ($message) use ($data) {
        $message->to($data['email'])->from($data['from'], $data['from_name'])->subject($data['subject']);
    });
}

/**
 *  Get Monday days(like: - December 2018 | retun: - 31)
 *  @Bhuvanesh on 19 Dec 2018
 **/
function get_month_days($monthyear)
{

    $month = explode(" ", $monthyear)[0];
    $monthVal = 0;
    switch ($month) {
        case 'January':
            $monthVal = 1;
            break;
        case 'February':
            $monthVal = 2;
            break;
        case 'March':
            $monthVal = 3;
            break;
        case 'April':
            $monthVal = 4;
            break;
        case 'May':
            $monthVal = 5;
            break;
        case 'June':
            $monthVal = 6;
            break;
        case 'July':
            $monthVal = 7;
            break;
        case 'August':
            $monthVal = 8;
            break;
        case 'September':
            $monthVal = 9;
            break;
        case 'October':
            $monthVal = 10;
            break;
        case 'November':
            $monthVal = 11;
            break;
        case 'December':
            $monthVal = 12;
            break;
        default:
            $monthVal = 1;
            break;
    }
    $days = cal_days_in_month(CAL_GREGORIAN, $monthVal, explode(" ", $monthyear)[1]);
    return $days;
}

/**
 *  Get employee list 
 *  @Bhuvanesh on 25 Dec 2018
 **/
function get_employees($employee_id = null, $isTrue = false)
{
    $arr_name = [];
    if ($isTrue == true) {
        $arr_name_list = Team::where(function ($query) use ($employee_id) {
            if ($employee_id != '') {
                $query->where('admin_id', $employee_id);
            }
            $query->where(['status' => '1', 'is_terminate' => '0', 'is_resign' => '0']);
        })->orderBy('admin_name', 'ASC')->get()->toArray();
        if (!empty($arr_name_list)) {
            foreach ($arr_name_list as $arr_names) {
                $arr_name[$arr_names['admin_id']] = $arr_names['admin_name'];
            }
        }
        return $arr_name;
    }
    return $arr_name;
}

/**
 *  Get code_reviewer list 
 *  @Bhuvanesh on 25 Dec 2018
 **/
function get_coder_reviewer($employee_id = null, $isTrue = false)
{
    $arr_name = [];
    if ($isTrue == true) {
        $arr_name_list = DB::table('admins as admin')
            ->join('assign_user_roles as asr', 'admin.admin_id', 'asr.admin_id')
            ->where(['admin.status' => '1', 'admin.is_terminate' => '0', 'admin.is_resign' => '0', 'asr.assign_user_role_status' => '1', 'asr.user_role_id' => 10])->get()->toArray();
        if (!empty($arr_name_list)) {
            $arr_list = json_decode(json_encode($arr_name_list, true), true);
            foreach ($arr_list as $arr_names) {
                $arr_name[$arr_names['admin_id']] = $arr_names['admin_name'];
            }
        }
        return $arr_name;
    }
    return $arr_name;
}

/**
 *  Get all platforms list
 *  @Bhuvanesh on 25 Dec 2018
 **/
function get_all_platforms()
{
    $arr_platform = [];
    $arr_platform_list = Platform::where('status', '=', '1')->select('platform_id', 'platform_name')->orderBy('platform_name', 'ASC')->get();
    if (!empty($arr_platform_list)) {
        foreach ($arr_platform_list as $arr_platforms) {
            $arr_platform[$arr_platforms['platform_id']] = $arr_platforms['platform_name'];
        }
    }
    return $arr_platform;
}

/**
 *  Array to selectbox options
 *  @Bhuvanesh on 25 Dec 2018
 **/
function get_select_box_options($arr, $defaultOption)
{
    $options = '';
    $options .= '<option disabled selected>' . $defaultOption . '</option>';
    if (!empty($arr)) {
        foreach ($arr as $key => $value) {
            $options .= '<option value="' . $key . '">' . $value . '</option>';
        }
    }
    return $options;
}

/**
 *  Return given date, week dates from monday to saturday
 *  @Bhuvanesh on 31 Dec 2018
 **/
function rangeWeek($datestr)
{
    date_default_timezone_set(date_default_timezone_get());
    $dt = strtotime($datestr);
    return array(
        "start" => date('N', $dt) == 1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt)),
        "end" => date('N', $dt) == 7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next saturday', $dt))
    );
}

/**
 *  Send mail For Coder and Code Reviewer
 *  @Bhuvanesh on 14 Dec 2018
 **/
function send_email_for_project_info($details, $isCodeReviewer = null)
{
    if ($isCodeReviewer == true) {
        foreach ($details as $detail) {
            $team = Team::Find($detail[0]);
            $project = Project::Find($detail[1]);
            $platform = Platform::Find($detail[2]);
            $data = array(
                'email' => $team->email,
                'to_name' => $team->admin_name,
                'subject' => $project->project_name . ' Project Allocated for Code Review',
                'project' => $project->project_name,
                'isCodeReviewer' => 1,
                'platform' => $platform->platform_name,
                'from' => 'v2rteam@gmail.com',
                'from_name' => 'V2R Solution'
            );
            Mail::send('admin-panel.auth.mailForProjectInfo', $data, function ($message) use ($data) {
                $message->to($data['email'])->from($data['from'], $data['from_name'])->subject($data['subject']);
            });
        }
    } else {
        foreach ($details as $detail) {
            $team = Team::Find($detail[0]);
            $project = Project::Find($detail[1]);
            $platform = Platform::Find($detail[2]);
            $data = array(
                'email' => $team->email,
                'to_name' => $team->admin_name,
                'subject' => $project->project_name . ' Project Assigned',
                'project' => $project->project_name,
                'platform' => $platform->platform_name,
                'from' => 'v2rteam@gmail.com',
                'from_name' => 'V2R Solution'
            );
            Mail::send('admin-panel.auth.mailForProjectInfo', $data, function ($message) use ($data) {
                $message->to($data['email'])->from($data['from'], $data['from_name'])->subject($data['subject']);
            });
        }
    }
}

/**
 *  Year Options for select box
 *  @Bhuvanesh on 14 Dec 2018
 **/
function get_year_options($startYear, $endYear)
{
    $yearOption = [];
    for ($i = $startYear; $i <= $endYear; $i++) {
        $yearOption[$i] = $i;
    }
    return $yearOption;
}

/**
 *  Get all platforms list
 *  @Bhuvanesh on 13 Jan 2019
 **/
function get_arr_saturday_off()
{
    $arr_saturday = [];
    $arr_saturday_off_list = Config::get('custom.saturday_off');
    if (!empty($arr_saturday_off_list)) {
        foreach ($arr_saturday_off_list as $key => $value) {
            $arr_saturday[$key] = $value;
        }
    }
    return $arr_saturday;
}

// sat off from event table
function get_arr_saturday_off_from_event()
{
    $arr_saturday = [];
    $arr_saturday_off_list = DB::table('events')->where('className', 'bg-saturday')->select('event_id', 'start_date')->get();
    if (!empty($arr_saturday_off_list)) {
        foreach ($arr_saturday_off_list as $key => $value) {
            $arr_saturday[$value->event_id] = $value->start_date;
        }
    }
    return $arr_saturday;
}

/** 
 * Get Number of Sundays of given month year 
 * @Bhuvanesh on 13 Jan 2019
 **/
function get_month_sundays($month, $year)
{
    // $monthName = date("F", mktime(0, 0, 0, $month));
    $fromdt = date('Y-m-01 ', strtotime("First Day Of  $month $year"));
    $todt = date('Y-m-d ', strtotime("Last Day of $month $year"));
    $num_sundays = '';
    for ($i = 0; $i <= ((strtotime($todt) - strtotime($fromdt)) / 86400); $i++) {
        if (date('l', strtotime($fromdt) + ($i * 86400)) == 'Sunday') {
            $num_sundays++;
        }
    }
    return $num_sundays;
}

/** 
 * Get Number of Sundays of given month year 
 * @Bhuvanesh on 21 Jan 2019
 **/
function get_month_saturdays($month, $year)
{
    $fromdt = date('Y-m-01 ', strtotime("First Day Of  $month $year"));
    $todt = date('Y-m-d ', strtotime("Last Day of $month $year"));
    $num_saturdays = '';
    for ($i = 0; $i < ((strtotime($todt) - strtotime($fromdt)) / 86400); $i++) {
        if (date('l', strtotime($fromdt) + ($i * 86400)) == 'Saturday') {
            $num_saturdays++;
        }
    }
    return $num_saturdays;
}

/**
 *  Get Monday short name
 *  @Bhuvanesh on 13 Jan 2019
 **/
function get_month_short_name($month)
{
    $shortName = '';
    switch ($month) {
        case 'January':
            $shortName = 'jan';
            break;
        case 'February':
            $shortName = 'feb';
            break;
        case 'March':
            $shortName = 'mar';
            break;
        case 'April':
            $shortName = 'apr';
            break;
        case 'May':
            $shortName = 'may';
            break;
        case 'June':
            $shortName = 'jun';
            break;
        case 'July':
            $shortName = 'july';
            break;
        case 'August':
            $shortName = 'aug';
            break;
        case 'September':
            $shortName = 'sep';
            break;
        case 'October':
            $shortName = 'oct';
            break;
        case 'November':
            $shortName = 'nov';
            break;
        case 'December':
            $shortName = 'dec';
            break;
        default:
            $shortName = '';
            break;
    }
    return $shortName;
}

/**
 *  Get Month Details
 *  @Bhuvanesh on 15 Jan 2019
 **/
function get_month_details($decrypted_attendance_id)
{
    if ($decrypted_attendance_id != '') {
        $attendance = Attendance::where('attendance_id', '=', $decrypted_attendance_id)->get()->first();
        $monthYear = explode(' ', $attendance['attendance_month']);
        $month = date('m', strtotime($monthYear[0]));
        $sundays = get_month_sundays($monthYear[0], $monthYear[1]);
        $saturdays = Event::whereRaw('MONTH(start_date) = ?', [$month])->where('className', '=', 'bg-saturday')->get()->count();
        $holidays_arr = Event::whereRaw('MONTH(start_date) = ?', [$month])->where('className', '=', 'bg-companyHoliday')->get()->toArray();
        $holiday_counter = 0;
        foreach ($holidays_arr as $holiday) {
            if (strtotime($holiday['start_date']) == strtotime($holiday['end_date'])) {
                $date = strtotime($holiday['start_date']);
                $date = date("l", $date);
                $date = strtolower($date);
                if ($date != "sunday") {
                    $holiday_counter += 1;
                }
            } else {
                $start = new DateTime($holiday['start_date']);
                $end = new DateTime($holiday['end_date']);

                $days = $start->diff($end, true)->days;
                $holidaySundays = intval($days / 7) + ($start->format('N') + $days % 7 >= 7);
                $durationHoliday = $start->diff($end)->format('%a');
                $durationHoliday -= $holidaySundays;
                $holiday_counter += $durationHoliday;
            }
        }
        $calendar_days = get_month_days($attendance['attendance_month']);
        $company_config = array(
            'month' => $monthYear[0],
            'sundays' => $sundays,
            'saturdays' => $saturdays,
            'holidays' => $holiday_counter,
            'calendar_days' => $calendar_days,
            // 'company_working_days'  =>  $calendar_days-($sundays + $saturdays + $holiday_counter),
            'company_working_days' => $calendar_days - ($sundays + $holiday_counter),
        );
        return $company_config;
    } else {
        return false;
    }
}

/**
 * Get Total Paid Days
 * @Bhuvanesh on 15 Jan 2019
 */
function get_total_paid_days($emp_details, $company_config)
{
    $attendance_id = $emp_details['get_attendance_data'][0]['attendance_id'];
    $month_details = get_month_details($attendance_id);
    $company_working_days = $month_details['company_working_days'] - $emp_details['saturday_off'];
    $total_days = $company_working_days - $emp_details['get_attendance_data'][0]['leaves_after_less'];
    $extra_working_hr = number_format(($emp_details['get_attendance_data'][0]['extra_working_hrs'] / $company_config['office_hours']), 2, '.', '');
    $total_paid_days = $total_days + $extra_working_hr;
    // p($total_paid_days);
    return $total_paid_days;
}

/**
 *  Get Previous Month of given month year 
 *  @Bhuvanesh on 16 Jan 2019
 **/
function get_previous_month($monthyear)
{
    $month = explode(" ", $monthyear)[0];
    $previousMonth = '';
    switch ($month) {
        case 'jan':
            $previousMonth = 'January';
            break;
        case 'feb':
            $previousMonth = 'January';
            break;
        case 'mar':
            $previousMonth = 'February';
            break;
        case 'apr':
            $previousMonth = 'March';
            break;
        case 'may':
            $previousMonth = 'April';
            break;
        case 'jun':
            $previousMonth = 'May';
            break;
        case 'july':
            $previousMonth = 'June';
            break;
        case 'aug':
            $previousMonth = 'July';
            break;
        case 'sep':
            $previousMonth = 'August';
            break;
        case 'oct':
            $previousMonth = 'September';
            break;
        case 'nov':
            $previousMonth = 'October';
            break;
        case 'dec':
            $previousMonth = 'November';
            break;
        default:
            $previousMonth = 'January';
            break;
    }
    return $previousMonth;
}

/**
 * Get Total Paid Days
 * @Bhuvanesh on 15 Jan 2019
 */
function get_total_paid_days_for_employee($emp_details, $company_config)
{
    $attendance_id = $emp_details['getAttendanceData'][0]['attendance_id'];
    $month_details = get_month_details($attendance_id);
    $company_working_days = $month_details['company_working_days'] - $emp_details['saturday_off'];
    $total_days = $company_working_days - $emp_details['getAttendanceData'][0]['leaves_after_less'];
    $extra_working_hr = number_format(($emp_details['getAttendanceData'][0]['extra_working_hrs'] / $company_config['office_hours']), 2, '.', '');
    $total_paid_days = $total_days + $extra_working_hr;
    // p($total_paid_days);
    return $total_paid_days;
}

/**
 * Get Current Month Attendance Details From Attendance Register
 * @Bhuvanesh on 11 Feb 2019
 */
function get_current_month_details($emp_id, $month = null)
{
    $total_late = 0;
    $total_dhalf = 0;
    $total_na = 0;
    $total_half_day = 0;
    $total_full_day = 0;

    $month_name = [];
    for ($i = 1; $i <= 12; $i++) {
        if ($i == 7) {
            $month_name[sprintf("%02d", $i)] = 'july';
        } else {
            $month_name[sprintf("%02d", $i)] = strtolower(date('M', strtotime('2012-' . $i . '-01')));
        }
    }
    $emp = Team::where('admin_id', $emp_id)
        ->with(['getAttendanceRegister' => function ($query) use ($month) {
            if ($month != '') {
                $query->whereMonth('attendance_date', $month);
            } else {
                $query->whereMonth('attendance_date', date('m'));
            }
        }])
        ->select('admin_id', 'admin_name')
        ->first()
        ->toArray();
    foreach ($emp['get_attendance_register'] as $key => $value) {
        if (strpos($value['attendance_key'], 'late') !== false) {
            $total_late++;
        } else if (strpos($value['attendance_key'], 'dhalf') !== false) {
            $total_dhalf++;
        } else if (strpos($value['attendance_key'], 'na') !== false) {
            $total_na++;
        } else if (strpos($value['attendance_key'], 'half_day') !== false) {
            $total_half_day++;
        } else if (strpos($value['attendance_key'], 'full_day') !== false) {
            $total_full_day++;
        }
    }

    $early_leave_chance = 0;
    $early_leave_count = EarlyLeaveRegister::where(['admin_id' => $emp_id, 'leave_status' => '1'])->whereMonth('leave_date', date('m'))->get()->count();
    $early_leave_chance = \Config::get('custom.early_leave_chance');
    $release_unutilized = ReleasePlan::where(['admin_id' => $emp_id, 'year' => date('Y')])->select('unutilized', $month_name[date('m')])->first();
    if (!empty($release_unutilized)) {
        $release_unutilized = $release_unutilized['unutilized'] + $release_unutilized[$month_name[date('m')]];
    } else {
        $release_unutilized = 0;
    }

    if ($early_leave_count > $early_leave_chance) {
        $early_leave_chance = 0;
    } else {
        $early_leave_chance = $early_leave_chance - $early_leave_count;
    }
    $attendance_detail = array(
        'total_late' => $total_late,
        'total_dhalf' => $total_dhalf,
        'total_na' => $total_na,
        'total_half_day' => $total_half_day,
        'total_full_day' => $total_full_day,
        'release_unutilized' => isset($release_unutilized) ? $release_unutilized : 0,
        'early_leave_chance' => $early_leave_chance,
        'early_leave_count' => $early_leave_count,
    );
    return $attendance_detail;
}

/**
 * Get Time into decimal
 * @Bhuvanesh on 11 Feb 2019
 */
function get_time_decimal($time, $isTrue)
{
    if ($isTrue == true) {
        $hours = $time->format('%h');
        $minutes = $time->format('%i');
        return $hours + round($minutes / 60, 2);
    } else {
        return $time;
    }
}

/**
 * Get off Saturday Array of current month 
 * @Bhuvanesh on 11 Feb 2019
 */
function get_off_saturday($isTrue)
{
    $sat_off_arr = array(1, 3);
    $date_sat_off = [];
    if ($isTrue == true) {
        $date = date('M Y');
        $off = array(
            1 => date('Y-m-d', strtotime($date . ' first saturday')),
            2 => date('Y-m-d', strtotime($date . ' second saturday')),
            3 => date('Y-m-d', strtotime($date . ' third saturday')),
            4 => date('Y-m-d', strtotime($date . ' fourth saturday')),
        );
        foreach ($off as $key => $value) {
            if (in_array($key, $sat_off_arr)) {
                $date_sat_off[$key] = $value;
            }
        }
    }
    return $date_sat_off;
}

/**
 * Insert Log Entry In DB
 * @Bhuvanesh On 18 Feb 2019
 */
function insert_log_entry($work_type)
{
    $logged_info = get_loggedin_user_data();
    $log = new Log;
    DB::beginTransaction();
    try {
        $log->admin_id = $logged_info['admin_id'];
        $log->ip_address = \Request::ip();
        $log->work_type = $work_type;
        $log->save();
    } catch (\Exception $e) {
        DB::rollback();
        $error_message = $e->getMessage();
        return redirect()->back()->withErrors($error_message);
    }
    DB::commit();
    return true;
}

/**
 *  Send mail when leave approved of cancel
 *  @Bhuvanesh on 19 Fen 2019
 **/
function send_email_for_leave($leave_register_id)
{
    $from = 'v2rteam@gmail.com';
    $from_name = 'V2R Solution';

    $subject = '';
    $message_body = '';

    $leave_details = LeaveRegister::where('leave_register_id', $leave_register_id)->with('getEmployeeData')->first();
    $email = $leave_details['getEmployeeData']['email'];
    $to_name = $leave_details['getEmployeeData']['admin_name'];

    if ($leave_details['leave_status'] == 1) {
        $subject = "Leave Approved";
    } elseif ($leave_details['leave_status'] == 3) {
        $subject = "Leave Cancelled";
    }

    $data = array(
        'email' => $email,
        'to_name' => $to_name,
        'subject' => $subject,
        'from' => $from,
        'message_body' => $message_body,
        'from_name' => $from_name,
        'leave_status' => $leave_details['leave_status'],
        'leave_comment' => $leave_details['leave_comment'],
    );
    Mail::send('admin-panel.auth.mail.leave_response', $data, function ($message) use ($data) {
        $message->to($data['email'])->from($data['from'], $data['from_name'])->subject($data['subject']);
    });
}
function get_attendance_update()
{
    $isPending = false;
    $status = 0;
    // Status - 1 : No entry of two today
    // Status - 2 : Today's in but not out
    // Status - 3 : Last day in but not out
    $logged_info = get_loggedin_user_data();
    if ($logged_info['type'] == '2') {
        $attendance_register = AttendanceRegister::where(function ($query) use ($logged_info) {
            $query->where('admin_id', $logged_info['admin_id']);
        })
            ->select('*', DB::raw('(CASE WHEN attendance_date = "' . date('Y-m-d') . '" THEN 1 ELSE 0 END) AS is_today'))
            ->orderBy('attendance_date', 'DESC')->first();
        // p($attendance_register);
        // if($attendance_register['attendance_intime'] != ''){
        //     p($attendance_register['is_today']);
        // }
        if (!empty($attendance_register)) {
            if ($attendance_register['is_today'] == 0) {
                if ($attendance_register['attendance_intime'] == '' || $attendance_register['attendance_outtime'] == '') {
                    $isPending = true;
                    $status = 3;
                } else {
                    $isPending = true;
                    $status = 2;
                }
            } else if ($attendance_register['is_today'] == 1) {
                if ($attendance_register['attendance_intime'] == '' || $attendance_register['attendance_outtime'] == '') {
                    $isPending = true;
                    $status = 2;
                } else {
                    $isPending = false;
                    $status = 0;
                }
            }
            // dd(['isPending' => $isPending, 'status' => $status]);
        }
        return ['isPending' => $isPending, 'status' => $status];
    }
    // p(['isPending' => $isPending, 'status' => $status]);
    return ['isPending' => $isPending, 'status' => $status];
}

function convert_date($variable, $flag)
{
    $date = '';
    if ($flag == 'display') {
        $date = date('d M Y', strtotime($variable));
    } else if ($flag == 'database') {
        $date = date('Y-m-d', strtotime($variable));
    } else {
        $date = $variable;
    }
    return $date;
}

// function mailForAdminRequest($condition, $employeeName, $email)
// {
//     $data = array(
//         'email' => 'bhuvaneshsoni88@gmail.com',
//         'to_name' => 'Admin',
//         'from' => $email,
//         'from_name' => $employeeName
//     );

//     if ($condition == 'attendance') {
//         $data = [
//             'subject' => 'Attendance Request',
//             'message' => '----',
//         ];
//     } else if ($condition == 'leave') {
//         $data = [
//             'subject' => 'Leave Request',
//             'message' => '----',
//         ];
//     } else if ($condition == 'early-leave') {
//         $data = [
//             'subject' => 'Early Leave Request',
//             'message' => '----',
//         ];
//     } else {
//         return false;
//     }
//     p($data);
//     Mail::send('admin-panel.auth.mailRequest', $data, function ($message) use ($data) {
//         $message->to($data['email'])->from($data['from'], $data['from_name'])->subject($data['subject']);
//     });
//     return true;
// }

// function weeks_between_two_date($start_date = null, $end_date = null, $isTrue = false)
// {
//     if ($isTrue && $start_date != null && $end_date != null) {
//         $weeks = array();
//         $startTime = strtotime($start_date);
//         $endTime = strtotime($end_date);
//         $counter = 0;
//         $temp = [];
//         while ($startTime < $endTime) {
//             // $weeks[] = date('W', $startTime);
//             $temp[$counter]['start_date'] =  date('Y-m-d', $startTime);
//             $startTime += strtotime('+7 day', 0);
//             $temp[$counter]['end_date'] =  date('Y-m-d', $startTime);
//             $startTime += strtotime('+1 day', 0);
//             $counter++;
//         }

//         // $week_with_dates = [];
//         // foreach ($weeks as $key => $value) {
//         //     $week_with_dates[$key]['week'] = $value;
//         //     $week_with_dates[$key]['dates'] = getStartAndEndDate($value, date('Y'));
//         // }

//         return  array('start_date' => $start_date, 'end_date' => $end_date, 'weeks' => $temp);
//     } else {
//         return  array('start_date' => $start_date, 'end_date' => $end_date);
//     }
// }
// function getStartAndEndDate($week, $year)
// {
//     $dto = new DateTime();
//     $dto->setISODate($year, $week);
//     $ret['week_start'] = $dto->format('Y-m-d');
//     $dto->modify('+6 days');
//     $ret['week_end'] = $dto->format('Y-m-d');
//     return $ret;
// }
