<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class MemberMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admin')
    {
        $log_user = Auth::guard($guard)->getUser();
        if ($log_user['type'] != 2) {
	        return redirect('admin-panel/dashboard');
	    }
        return $next($request);
    }
}
