<?php

namespace App\Helpers;

namespace App\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAdmin
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'admin')
	{
		$log_info = Auth::guard($guard)->user();
		// p($log_info, 0);


		if (!Auth::guard($guard)->check()) {

			if ($request->ajax()) {
				return response()->json(['status' => 'not_login']);
			} else {
				return redirect('admin-panel');
			}
		}
		if ($log_info['status'] != 1) {
			Auth::guard($guard)->logout();
			return redirect('admin-panel')->withErrors('Your account blocked by admin.');
		}
		return $next($request);
	}
}
