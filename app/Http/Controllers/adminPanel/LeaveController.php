<?php

namespace App\Http\Controllers\adminPanel;

use Config;
use DateTime;
use DateInterval;
use DatePeriod;
use Validator;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Model\Team\Team;
use App\Model\Leave\LeaveRegister;
use App\Model\Leave\EarlyLeaveRegister;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use App\Notifications\Admin\Leave;
use Illuminate\Support\Facades\Notification;

class LeaveController extends Controller
{
    /**
     * Constructor for Leave Controller 
     **/
    public function __construct()
    {
        $this->middleware('isadmin')->except('applyLeave', 'applyEarlyLeave', 'checkLeaveDate', 'viewMyLeave', 'anyDataMyLeave', 'destroy', 'getModifyLeave');
        $this->middleware('ismember')->only('applyLeave', 'checkLeaveDate', 'viewMyLeave', 'anyDataMyLeave', 'destroy', 'getModifyLeave');
    }

    /**
     *  View page for Leave Manage
     *  @Bhuvanesh on 14 Feb 2019
     **/
    public function index($condition = null)
    {
        $employee_list = [];
        $loginInfo = get_loggedin_user_data();
        $employee_arr = Team::select('admin_id', 'admin_name')->where(['type' => '2', 'status' => '1', 'is_terminate' => '0', 'is_resign' => '0'])->get()->toArray();
        if (!empty($employee_arr)) {
            foreach ($employee_arr as $key => $value) {
                $employee_list[$value['admin_id']] = $value['admin_name'];
            }
        }
        $employee_list = add_blank_option($employee_list, 'Select Employee');

        $leave_category = \Config::get('custom.leave_category');
        $leave_category = add_blank_option($leave_category, 'Select Category');

        $leave_status = \Config::get('custom.leave_status');
        $leave_status = add_blank_option($leave_status, 'All');

        if ($condition == 'pending') {
            $condition = 2;
        } else {
            $condition = null;
        }
        $data = array(
            'page_title' => trans('language.view_leaves'),
            'employee_list' => $employee_list,
            'condition' => $condition,
            'leave_status' => $leave_status,
            'leave_category' => $leave_category,
            'login_info' => $loginInfo
        );
        // p($data);
        return view('admin-panel.leave.index')->with($data);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Bhuvanesh on 14 Feb 2019
     **/

    public function anyData(Request $request)
    {
        $leaves = [];
        $searchKey = null;
        $leave_category = \Config::get('custom.leave_category');
        $leave_category = add_blank_option($leave_category, 'Select Category');
        $leave_status = \Config::get('custom.leave_status');

        $leaves = LeaveRegister::where(function ($query) use ($request) {
            if (!empty($request->get('condition'))) {
                $query->where('leave_status', $request->get('condition'));
            }
            if (!empty($request->get('employee'))) {
                $query->where('admin_id', $request->get('employee'));
            }
            if (!empty($request->get('leave_category'))) {
                $query->where('leave_category', $request->get('leave_category'));
            }
        })->with('getEmployeeData')->orderBy('leave_start_date')->get()->toArray();
        return Datatables::of($leaves)
            ->addColumn('employee_name', function ($leaves) {
                return $leaves['get_employee_data']['admin_name'];
            })
            ->addColumn('leave_date', function ($leaves) {
                if ($leaves['leave_start_date'] == $leaves['leave_end_date']) {
                    return date_create($leaves['leave_start_date'])->format('d M Y');
                } else {
                    return date_create($leaves['leave_start_date'])->format('d M Y') . ' to ' . date_create($leaves['leave_end_date'])->format('d M Y');
                }
            })
            ->addColumn('leave_details', function ($leaves) use ($leave_category) {
                return $leave_category[$leaves['leave_category']] . '<br/><span class="text-muted">' . $leaves['leave_reason'] . '</span>';
            })
            ->addColumn('leave_apply_date', function ($leaves) {
                return date_create($leaves['created_at'])->format('d M Y');
            })
            ->addColumn('leave_status', function ($leaves) use ($leave_status) {

                $content = '<span class="label ';
                if ($leaves['leave_status'] == 1) {
                    $content .= 'label-success';
                } else if ($leaves['leave_status'] == 3) {
                    $content .= 'label-danger';
                } else if ($leaves['leave_status'] == 2) {
                    $content .= 'label-info';
                }

                $content .= ' label-rouded">' . $leave_status[$leaves['leave_status']] . '</span>';
                return $content;
            })
            ->addColumn('leave_comment', function ($leaves) {
                if ($leaves['leave_status'] == 3) {
                    return $leaves['leave_comment'];
                } else {
                    return '--';
                }
            })
            ->addColumn('action', function ($leaves) {
                $approve = '<button type="button" title="Approve" class="btn btn-success btn-circle btn-md" onClick = "changeStatus(' . $leaves['leave_register_id'] . ', 1,)" ><i class="fa fa-check"></i> </button> &nbsp';

                $pending = '<button type="button" title="Pending" class="btn btn-info btn-circle btn-md" onClick = "changeStatus(' . $leaves['leave_register_id'] . ', 2,)" ><i class="fas fa-undo-alt"></i> </button> &nbsp';

                $cancel = '<button type="button" title="Cancel" class="btn btn-danger btn-circle btn-md cancelRequest" leave-register-id="' . $leaves['leave_register_id'] . '"><i class="fa fa-times"></i> </button>';

                if ($leaves['leave_status'] == '1') {
                    return $pending . $cancel;
                }
                if ($leaves['leave_status'] == '2') {
                    return $approve . $cancel;
                }
                if ($leaves['leave_status'] == '3') {
                    return $approve . $pending;
                }
            })->rawColumns(['leave_status' => 'leave_status', 'leave_details' => 'leave_details', 'action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Apply Leave for Employee
     *  @Bhuvanesh on 13 Feb 2019
     **/
    public function applyLeave(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $decrypted_leave_register_id = null;
        $decrypted_leave_register_id = get_decrypted_value($request->get('leave_register_id'), true);
        if (!empty($request->get('leave_register_id'))) {
            $leave = LeaveRegister::find($decrypted_leave_register_id);
            if (!$leave) {
                return array('status' => 'error', 'message' => 'Leave not found!');
            }
            $success_msg = 'Leave updated successfully!';
        } else {
            $leave = new LeaveRegister;
            $success_msg = 'Leave applied Successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'leave_category' => 'required',
            'leave_date' => 'required'
        ]);
        if ($validatior->fails()) {
            return array('status' => 'error-validate', 'message' => $validator->getMessageBag()->toArray());
        } else {
            $dates = explode('-', Input::get('leave_date'));
            $start_date = trim($dates[0]);
            $end_date = trim($dates[1]);

            $isLeaveDateValid = $this->checkLeaveDate($loginInfo, $start_date, $end_date, $decrypted_leave_register_id);
            if ($isLeaveDateValid == false) {
                return array('status' => 'error', 'message' => 'Leave already applied on this date');
            }
            DB::beginTransaction();
            try {
                $leave->leave_category = Input::get('leave_category');
                $leave->admin_id = $loginInfo['admin_id'];

                $leave->leave_start_date = date_create($start_date)->format('Y-m-d');
                $leave->leave_end_date = date_create($end_date)->format('Y-m-d');

                $leave->leave_reason = Input::get('leave_reason');
                $leave->save();

                $msg_notify = \Config::get('custom.notification_message');
                $data_notif = [
                    'emp_name' => $loginInfo['admin_name'],
                    'message' => $msg_notify['leave'],
                    'reason' => $leave->leave_reason,
                    'leave_date' => $leave->leave_start_date . ' to ' . $leave->leave_end_date
                ];
                // Notification for Admin
                Team::find(1)->notify(new Leave($data_notif));
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return array('status' => 'error', 'message' => $error_message);
            }
            DB::commit();
            return array('status' => 'success', 'message' => $success_msg);
        }
    }

    /**
     * Check Leave date is exist or not
     * @Bhuvanesh on 14 Feb 2019
     */
    public function checkLeaveDate($loginInfo, $start_date, $end_date, $id)
    {

        $start_date = date_create($start_date)->format('Y-m-d');
        $end_date = date_create($end_date)->format('Y-m-d');

        $leaveRegister = LeaveRegister::where(function ($query) use ($loginInfo, $id) {
            $query->where('admin_id', $loginInfo['admin_id']);
            if (!empty($id)) {
                $query->where('leave_register_id', '!=', $id);
            }
        })->get()->toArray();

        $previous_leave_dates = [];
        foreach ($leaveRegister as $key => $value) {
            if ($value['leave_start_date'] == $value['leave_end_date']) {
                array_push($previous_leave_dates, $value['leave_start_date']);
            } else {
                $begin = new DateTime($value['leave_start_date']);
                $end = new DateTime($value['leave_end_date']);
                $end = $end->modify('+1 day');
                $interval_day = new DateInterval('P1D');
                $date_range = new DatePeriod($begin, $interval_day, $end);
                foreach ($date_range as $date_key => $date_value) {
                    array_push($previous_leave_dates, $date_value->format('Y-m-d'));
                }
            }
        }

        if ($start_date == $end_date) {
            if (in_array($start_date, $previous_leave_dates)) {
                return false;
            } else {
                return true;
            }
        } else {
            $leave_flag = true;
            $applied_leave_dates = [];
            $begin = new DateTime($start_date);
            $end = new DateTime($end_date);
            $end = $end->modify('+1 day');
            $interval_day = new DateInterval('P1D');
            $date_range = new DatePeriod($begin, $interval_day, $end);
            foreach ($date_range as $date_key => $date_value) {
                array_push($applied_leave_dates, $date_value->format('Y-m-d'));
            }
            foreach ($applied_leave_dates as $key => $value) {
                if (in_array($value, $previous_leave_dates)) {
                    $leave_flag = false;
                }
            }
            return $leave_flag;
        }
    }

    /**
     *  Employee Leave status change (Aproved, Pending, or cancel)
     *  @Bhuvanesh on 14 Jan 2019
     **/
    public function changeStatusLeave(Request $request)
    {
        $id = $request->get('leave_register_id');
        $status = $request->get('leave_status');
        $leave = LeaveRegister::find($id);
        $comment = '';
        $message = '';
        if ($leave) {
            if ($status == 1) {
                $message = 'Leave Approved Successfully';
            } else if ($status == 2) {
                $message = 'Leave Pending Successfully';
            } else if ($status == 3) {
                $message = 'Leave Cancel Successfully';
                if (isset($request) && $request->get('leave_comment') != '') {
                    $comment = $request->get('leave_comment');
                } else {
                    return array(
                        'status' => 'error',
                        'message' => 'Please give your comment',
                    );
                }
            } else {
                return array(
                    'status' => 'error',
                    'message' => 'Invalid argument',
                );
            }

            $leave->leave_status = $status;
            if ($status == 3) {
                $leave->leave_comment = $comment;
            }
            $leave->save();
            if ($status == 1 || $status == 3) {
                send_email_for_leave($leave->leave_register_id);
            }
            return array(
                'status' => 'success',
                'message' => $message,
            );
        } else {
            return array(
                'status' => 'error',
                'message' => 'Record Not Found.',
            );
        }
    }


    /**
     *  View page for Leave Manage
     *  @Bhuvanesh on 14 Feb 2019
     **/
    public function indexEarlyLeave($condition = null)
    {
        $employee_list = [];
        $loginInfo = get_loggedin_user_data();
        $apply_short_leave_url = url('admin-panel/early-leave/apply-early-leave');
        $employee_arr = Team::select('admin_id', 'admin_name')->where(['type' => '2', 'status' => '1', 'is_terminate' => '0', 'is_resign' => '0'])->get()->toArray();
        if (!empty($employee_arr)) {
            foreach ($employee_arr as $key => $value) {
                $employee_list[$value['admin_id']] = $value['admin_name'];
            }
        }

        $employee_list = add_blank_option($employee_list, 'Select Employee');

        $leave_status = \Config::get('custom.leave_status');
        $leave_status = add_blank_option($leave_status, 'All');

        if ($condition == 'pending') {
            $condition = 2;
        } else {
            $condition = null;
        }
        $data = array(
            'page_title' => trans('language.view_early_leaves'),
            'employee_list' => $employee_list,
            'condition' => $condition,
            'leave_status' => $leave_status,
            'apply_short_leave_url' => $apply_short_leave_url,
            'login_info' => $loginInfo
        );
        return view('admin-panel.leave.early-leave')->with($data);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Bhuvanesh on 14 Feb 2019
     **/

    public function anyDataEarlyLeave(Request $request)
    {
        $early_leaves = [];
        $searchKey = null;
        $leave_status = \Config::get('custom.leave_status');

        $early_leaves = EarlyLeaveRegister::where(function ($query) use ($request) {
            if (!empty($request->get('condition'))) {
                $query->where('leave_status', $request->get('condition'));
            }
            if (!empty($request->get('employee'))) {
                $query->where('admin_id', $request->get('employee'));
            }
        })->with('getEmployeeData')->orderBy('leave_date', 'DESC')->get()->toArray();
        return Datatables::of($early_leaves)
            ->addColumn('employee_name', function ($early_leaves) {
                return $early_leaves['get_employee_data']['admin_name'];
            })
            ->addColumn('leave_time', function ($early_leaves) {
                return date_create($early_leaves['start_time'])->format('H:i A') . ' to ' . date_create($early_leaves['end_time'])->format('H:i A');
            })
            ->addColumn('leave_details', function ($early_leaves) {
                return $early_leaves['leave_reason'];
            })
            ->addColumn('leave_apply_date', function ($early_leaves) {
                return date_create($early_leaves['leave_date'])->format('d M Y');
            })
            ->addColumn('leave_status', function ($early_leaves) use ($leave_status) {
                $content = '<span class="label ';
                if ($early_leaves['leave_status'] == 1) {
                    $content .= 'label-success';
                } else if ($early_leaves['leave_status'] == 3) {
                    $content .= 'label-danger';
                } else if ($early_leaves['leave_status'] == 2) {
                    $content .= 'label-info';
                }

                $content .= ' label-rouded">' . $leave_status[$early_leaves['leave_status']] . '</span>';
                return $content;
            })
            ->addColumn('action', function ($early_leaves) {
                $approve = '<button type="button" title="Approve" class="btn btn-success btn-circle btn-md" onClick = "changeStatus(' . $early_leaves['early_leave_register_id'] . ', 1)" ><i class="fa fa-check"></i> </button> &nbsp';

                $pending = '<button type="button" title="Pending" class="btn btn-info btn-circle btn-md" onClick = "changeStatus(' . $early_leaves['early_leave_register_id'] . ', 2)" ><i class="fas fa-undo-alt"></i> </button> &nbsp';

                $cancel = '<button type="button" title="Cancel" class="btn btn-danger btn-circle btn-md" onClick="changeStatus(' . $early_leaves['early_leave_register_id'] . ', 3)" ><i class="fa fa-times"></i> </button>';

                if ($early_leaves['leave_status'] == '1') {
                    return $pending . $cancel;
                }
                if ($early_leaves['leave_status'] == '2') {
                    return $approve . $cancel;
                }
                if ($early_leaves['leave_status'] == '3') {
                    return $approve . $pending;
                }
            })->rawColumns(['leave_time' => 'leave_time', 'leave_status' => 'leave_status', 'leave_details' => 'leave_details', 'action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Apply Leave for Employee
     *  @Bhuvanesh on 13 Feb 2019
     **/
    public function applyEarlyLeave(Request $request, $id = null)
    {
        $loginInfo = get_loggedin_user_data();
        $decrypted_early_leave_register_id = null;
        $decrypted_early_leave_register_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $early_leave = EarlyLeaveRegister::find($decrypted_early_leave_register_id);
            if (!$early_leave) {
                return array('status' => 'error', 'message' => 'Record not found!');
            }
            $success_msg = 'Early Leave updated successfully!';
        } else {
            $early_leave = new EarlyLeaveRegister;
            $success_msg = 'Early leave applied successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'early_leave_reason' => 'required',
            'leave_start_time' => 'required',
            'leave_end_time' => 'required'
        ]);
        if ($validatior->fails()) {
            return array('status' => 'error-validate', 'message' => $validatior->getMessageBag()->toArray());
        } else {
            DB::beginTransaction();
            try {
                $admin_id = '';
                if ($request->has('early_leave_employee')) {
                    $admin_id = $request->get('early_leave_employee');
                    $early_leave->leave_status = 1;
                } else {
                    $admin_id =  $loginInfo['admin_id'];
                }
                $early_leave->admin_id = $admin_id;
                $early_leave->leave_date = date('Y-m-d');
                $early_leave->start_time = date_create(Input::get('leave_start_time'))->format('Y-m-d H:i:s');
                $early_leave->end_time = date_create(Input::get('leave_end_time'))->format('Y-m-d H:i:s');
                $early_leave->leave_reason = Input::get('early_leave_reason');
                $early_leave->save();

                $msg_notify = \Config::get('custom.notification_message');
                $data_notif = [
                    'emp_name' => $loginInfo['admin_name'],
                    'message' => $msg_notify['early_leave']
                ];
                if (!$request->has('early_leave_employee')) {
                    // Notification for Admin
                    Team::find(1)->notify(new Leave($data_notif));
                }
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return array('status' => 'error', 'message' => $error_message);
            }
            DB::commit();
            return array('status' => 'success', 'message' => $success_msg);
        }
    }

    /**
     *  Employee Leave status change (Aproved, Pending, or cancel)
     *  @Bhuvanesh on 14 Jan 2019
     **/
    public function changeEarlyLeaveStatus(Request $request)
    {
        $id = $request->get('early_leave_register_id');
        $status = $request->get('leave_status');
        $early_leave = EarlyLeaveRegister::find($id);
        $message = '';
        if ($early_leave) {
            if ($status == 1) {
                $message = 'Leave Approved Successfully';
            } else if ($status == 2) {
                $message = 'Leave Pending Successfully';
            } else if ($status == 3) {
                $message = 'Leave Cancelled Successfully';
            } else {
                return array(
                    'status' => 'error',
                    'message' => 'Invalid argument',
                );
            }
            $early_leave->leave_status = $status;
            $early_leave->save();
            return array(
                'status' => 'success',
                'message' => $message,
            );
        } else {
            return array(
                'status' => 'error',
                'message' => 'Record Not Found.',
            );
        }
    }

    /**
     *  View Leave Manage for Employee
     *  @Bhuvanesh on 14 Feb 2019
     **/
    public function viewMyLeave()
    {
        $loginInfo = get_loggedin_user_data();
        $leave_category = \Config::get('custom.leave_category');
        $leave_category = add_blank_option($leave_category, 'Select Category');
        $leave_status = \Config::get('custom.leave_status');
        $leave_status = add_blank_option($leave_status, 'All');
        $apply_leave_url = url('admin-panel/leave/apply-leave');
        $data = array(
            'page_title' => trans('language.view_leaves'),
            'leave_status' => $leave_status,
            'leave_category' => $leave_category,
            'login_info' => $loginInfo,
            'apply_leave_url' => $apply_leave_url,
        );
        return view('admin-panel.leave.my-leave')->with($data);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Bhuvanesh on 14 Feb 2019
     **/

    public function anyDataMyLeave(Request $request)
    {
        $leaves = [];
        $searchKey = null;
        $loginInfo = get_loggedin_user_data();
        $leave_category = \Config::get('custom.leave_category');
        $leave_category = add_blank_option($leave_category, 'Select Category');
        $leave_status = \Config::get('custom.leave_status');

        $leaves = LeaveRegister::where(function ($query) use ($request, $loginInfo) {
            $query->where('admin_id', $loginInfo['admin_id']);
            if (!empty($request->get('condition'))) {
                $query->where('leave_status', $request->get('condition'));
            }
            if (!empty($request->get('employee'))) {
                $query->where('admin_id', $request->get('employee'));
            }
            if (!empty($request->get('leave_category'))) {
                $query->where('leave_category', $request->get('leave_category'));
            }
        })->orderBy('updated_at', 'desc')->get()->toArray();
        return Datatables::of($leaves)
            ->addColumn('leave_date', function ($leaves) {
                if ($leaves['leave_start_date'] == $leaves['leave_end_date']) {
                    return date_create($leaves['leave_start_date'])->format('d M Y');
                } else {
                    return date_create($leaves['leave_start_date'])->format('d M Y') . ' to ' . date_create($leaves['leave_end_date'])->format('d M Y');
                }
            })
            ->addColumn('leave_details', function ($leaves) use ($leave_category) {
                return $leave_category[$leaves['leave_category']] . '<br/><span class="text-muted">' . $leaves['leave_reason'] . '</span>';
            })
            ->addColumn('leave_apply_date', function ($leaves) {
                return date_create($leaves['created_at'])->format('d M Y');
            })
            ->addColumn('leave_status', function ($leaves) use ($leave_status) {

                $content = '<span class="label ';
                if ($leaves['leave_status'] == 1) {
                    $content .= 'label-success';
                } else if ($leaves['leave_status'] == 3) {
                    $content .= 'label-danger';
                } else if ($leaves['leave_status'] == 2) {
                    $content .= 'label-info';
                }

                $content .= ' label-rouded">' . $leave_status[$leaves['leave_status']] . '</span>';
                return $content;
            })
            ->addColumn('leave_comments', function ($leaves) {
                if ($leaves['leave_status'] == 3) {
                    return $leaves['leave_comment'];
                } else {
                    return '--';
                }
            })
            ->addColumn('action', function ($leaves) {
                $encrypted_leave_register_id = get_encrypted_value($leaves['leave_register_id'], true);
                $edit = '&nbsp; &nbsp; <button type="button" title="Edit" class="btn btn-warning btn-circle btn-md modifyRequest" leave-register-id="' . $encrypted_leave_register_id . '"><i class="fa fa-edit"></i> </button>';

                $delete = '&nbsp; <button type="button" title="Delete" class="btn btn-danger btn-circle btn-md deleteRequest" leave-register-id="' . $encrypted_leave_register_id . '"><i class="fa fa-trash"></i> </button>';

                if ($leaves['leave_status'] == '1') {
                    if (strtotime($leaves['leave_start_date']) > strtotime(date('Y-m-d'))) {
                        return '&nbsp;' . $delete;
                    } else {
                        return '--';
                    }
                }
                if ($leaves['leave_status'] == '2') {
                    return $edit . $delete;
                }
                if ($leaves['leave_status'] == '3') {
                    return '--';
                }
            })->rawColumns(['leave_comments' => 'leave_comments', 'leave_status' => 'leave_status', 'leave_details' => 'leave_details', 'action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Destroy Leave data
     *  @Bhuvanesh on 18 Feb 2019
     **/
    public function destroy(Request $request)
    {
        $leave_register_id = get_decrypted_value($request->get('id'), true);
        $leave = LeaveRegister::find($leave_register_id);
        if ($leave) {
            DB::beginTransaction();
            try {
                $leave->delete();
                $success_msg = "Leave deleted successfully!";
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return array('status' => 'error', 'message' => "It's already in use. We can't delete this.");
            }
            return array('status' => 'success', 'message' => $success_msg);
        } else {
            $error_message = "Leave not found!";
            return array('status' => 'error', 'message' => $error_message);
        }
    }

    /**
     * Find Leave for Update 
     * @Bhuvanesh on 18 Feb 2019
     */
    public function getModifyLeave(Request $request)
    {
        if ($request->get('id') != '') {
            $decrypted_leave_register_id = get_decrypted_value($request->get('id'), true);
            $leave = LeaveRegister::find($decrypted_leave_register_id);
            if ($leave) {
                if ($leave['leave_status'] == 1 || $leave['leave_status'] == 3) {
                    return array('status' => 'error', 'message' => 'You can\'t modify this record');
                } else {
                    $start_date = date_create($leave['leave_start_date'])->format('Y/m/d');
                    $end_date = date_create($leave['leave_end_date'])->format('Y/m/d');
                    $leave_data = array(
                        'leave_date' => $start_date . ' - ' . $end_date,
                        'leave_category' => $leave['leave_category'],
                        'leave_reason' => $leave['leave_reason'],
                        'leave_register_id' => get_encrypted_value($leave['leave_register_id'], true),
                    );
                    return array('status' => 'success', 'message' => 'Record Found Successfully', 'data' => $leave_data);
                }
            } else {
                return array('status' => 'error', 'message' => 'Record Not Found');
            }
        } else {
            return array('status' => 'error', 'message' => 'Invalid Argument');
        }
    }

    /**
     *  Get Data for early leave employee count(Datatables)
     *  @Bhuvanesh on 14 Feb 2019
     **/

    public function anyDataEarlyLeaveCount(Request $request)
    {
        $early_leaves = [];
        $early_leaves = DB::table('admins as ad')
            ->where(['ad.type' => '2', 'ad.status' => '1'])
            ->select(
                'ad.admin_id',
                'ad.admin_name',
                DB::raw('(select MONTH(elr.leave_date) from early_leave_registers as elr where ad.admin_id = elr.admin_id 
                AND MONTH(elr.leave_date) = "' . date("m") . '"  AND YEAR(elr.leave_date) = "' . date("Y") . '" AND elr.leave_status = 1
                 GROUP BY MONTH(elr.leave_date), elr.admin_id) as leave_month'),
                DB::raw('(select COUNT(elr.leave_date) from early_leave_registers as elr where ad.admin_id = elr.admin_id 
                AND MONTH(elr.leave_date) = "' . date("m") . '"  AND YEAR(elr.leave_date) = "' . date("Y") . '" AND elr.leave_status = 1
                 GROUP BY MONTH(elr.leave_date), elr.admin_id) as leave_count')
            )
            ->orderBy('admin_name')
            ->get();
        return Datatables::of($early_leaves)->addColumn('leave_count', function ($early_leaves) {
            if ($early_leaves->leave_count != '') {
                return $early_leaves->leave_count;
            } else {
                return 0;
            }
        })->addIndexColumn()->make(true);
    }
}
