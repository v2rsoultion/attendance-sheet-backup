<?php

namespace App\Http\Controllers\adminPanel;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use App\Model\ReleasePlan\ReleasePlan;
use App\Model\Team\Team;
use function GuzzleHttp\json_decode;

class ReleasePlanController extends Controller
{
    /**
     * Constructor for Department Controller 
     **/
    public function __construct()
    {
        $this->middleware('isadmin');
        // $this->middleware('user')->only('index');
        // $this->middleware('subscribed')->except('store');
    }

    /**
     *  View page for Release Plan
     *  @Bhuvanesh on 07 Jan 2018
     **/
    public function index()
    {
        $lastYear = ReleasePlan::orderBy('year', 'ASC')->get()->first();
        $loginInfo = get_loggedin_user_data();
        if (!empty($lastYear)) {
            $yearOption = get_year_options($lastYear['year'], date('Y'));
        } else {
            $yearOption = get_year_options(date('Y'), date('Y'));
        }
        $yearOption = add_blank_option($yearOption, 'Select Year');
        $data = array(
            'page_title' => trans('language.view_release_plans'),
            'login_info' => $loginInfo,
            'yearOption' => $yearOption,
            'save_url' => url('admin-panel/release-plan/save')
        );
        return view('admin-panel.release-plan.show-release-plan')->with($data);
    }

    /**
     *  Save department data
     *  @Bhuvanesh on 11 Dec 2018
     **/
    public function save(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $success_msg = '';
        $status = 0;
        $isUpdate = false;
        if (!empty($request->get('admin_year'))) {
            $release_plan = ReleasePlan::where(['admin_id' => $request->get('admin_year'), 'admin_id' => $request->get('admin_id')])->first();
            if (!$release_plan) {
                $release_plan = new ReleasePlan;
                $success_msg = 'Release plan saved successfully!';
            } else {
                $success_msg = "Release plan updated successfully";
            }
        }
        // p($release_plan);
        DB::beginTransaction();
        try {
            $release_plan->year = Input::get('admin_year');
            $release_plan->admin_id = Input::get('admin_id');
            $tempTotal = 0;
            $map = Input::get('map');
            foreach ($map as $key => $value) {
                $release_plan->$key = $value;
                $tempTotal += $value;
            }
            $release_plan->total = $tempTotal; 
            // p($release_plan);  
            $release_plan->save();
        } catch (\Exception $e) {
            DB::rollback();
            $status = 1;
            $success_msg = $e->getMessage();
            // return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        return ['status' => $status, 'message' => $success_msg];
    }

    /**
     *  Release Plan datatable data
     *  @Bhuvanesh on 07 Jan 2018
     **/
    public function showReleasePlanData(Request $request)
    {
        $release_plan = [];
        $release_plan = Team::where(function ($query) use ($request) {
            $query->where(['type' => '2', 'is_terminate' => '0', 'is_resign' => '0']);
            if (!empty($request) && $request->get('team_name') != '') {
                $query->where('admin_name', 'LIKE', '%' . $request->get('team_name') . '%');
            }
        })
            ->with(['getReleasePlan' => function ($q) use ($request) {
                $q->where('year', '=', $request->get('year'));
            }])
            ->orderBy('admin_id', 'desc')
            ->get()
            ->toArray();

        if (date('Y') > $request->get('year') && $release_plan[0]['get_release_plan'] == []) {
            $release_plan = [];
        }
        return Datatables::of($release_plan)
            ->addColumn('total', function ($release_plan) {
                if (!empty($release_plan['get_release_plan'])) {
                    return $release_plan['get_release_plan'][0]['total'];
                } else {
                    return '--';
                }
            })
            ->addColumn('show_month_wise', function ($release_plan) use ($request) {
                if (!empty($release_plan['get_release_plan'])) {
                    $year = $release_plan['get_release_plan'][0]['year'];
                } else {
                    $year = $request->get('year');
                }
                $dec_id = get_encrypted_value($release_plan['admin_id'], true);
                return '<div class="col-md-2 pull-left">
                    <button type="button" class="btn btn-info no-radius mylabel" year-id=' . $year . '_' . $dec_id . '>Monthly</button>
                </div>';
            })->rawColumns(['total' => 'total', 'show_month_wise' => 'show_month_wise', 'emp_id' => 'emp_id'])->addIndexColumn()->make(true);
    }

    /**
     *  Release Plan datatable data
     *  @Bhuvanesh on 07 Jan 2018
     **/
    public function getReleasePlanMonthWiseData(Request $request)
    {
        if (!empty($request) && $request->get('year_id') != '') {
            $year = explode('_', $request->get('year_id'))[0];
            $id = explode('_', $request->get('year_id'))[1];
            $release_plan = [];
            $decrypted_id = get_decrypted_value($id, true);
            $release_plan = Team::where(function ($query) use ($decrypted_id) {
                $query->where(['type' => '2', 'admin_id' => $decrypted_id]);
            })
                ->with(['getReleasePlan' => function ($q) use ($year) {
                    $q->where('year', '=', $year);
                }])
                ->orderBy('admin_id', 'desc')
                ->get()
                ->first();

            $utilized = DB::table('release_plan_logs')->where(function ($query) use ($release_plan, $year) {
                $query->where('admin_id', '=', $release_plan['admin_id']);
                $query->where('month_year', 'LIKE', '%' . $year . '%');
            })->orderBy('updated_at', 'desc')->get(['month_year', 'utilized', 'unutilized']);
            $utilized_month = json_decode(json_encode($utilized), true);
            $utilized_month = array_column($utilized_month, null, 'month_year');
            $data['name'] = $release_plan['admin_name'];
            $data['emp_id'] = $release_plan['emp_id'];
            $data['admin_id'] = $release_plan['admin_id'];
            $data['status'] = $release_plan['status'];
            $data['year'] = isset($release_plan['getReleasePlan'][0]['year']) ? $release_plan['getReleasePlan'][0]['year'] : $year;
            $data['total'] = isset($release_plan['getReleasePlan'][0]['total']) ? $release_plan['getReleasePlan'][0]['total'] : 15;

            $jan = isset($release_plan['getReleasePlan'][0]['jan']) ? $release_plan['getReleasePlan'][0]['jan'] : 1;
            $feb = isset($release_plan['getReleasePlan'][0]['feb']) ? $release_plan['getReleasePlan'][0]['feb'] : 1;
            $mar = isset($release_plan['getReleasePlan'][0]['mar']) ? $release_plan['getReleasePlan'][0]['mar'] : 1.5;
            $apr = isset($release_plan['getReleasePlan'][0]['apr']) ? $release_plan['getReleasePlan'][0]['apr'] : 1;
            $may = isset($release_plan['getReleasePlan'][0]['may']) ? $release_plan['getReleasePlan'][0]['may'] : 1;
            $jun = isset($release_plan['getReleasePlan'][0]['jun']) ? $release_plan['getReleasePlan'][0]['jun'] : 2;
            $july = isset($release_plan['getReleasePlan'][0]['july']) ? $release_plan['getReleasePlan'][0]['july'] : 1;
            $aug = isset($release_plan['getReleasePlan'][0]['aug']) ? $release_plan['getReleasePlan'][0]['aug'] : 1;
            $sep = isset($release_plan['getReleasePlan'][0]['sep']) ? $release_plan['getReleasePlan'][0]['sep'] : 1.5;
            $oct = isset($release_plan['getReleasePlan'][0]['oct']) ? $release_plan['getReleasePlan'][0]['oct'] : 1;
            $nov = isset($release_plan['getReleasePlan'][0]['nov']) ? $release_plan['getReleasePlan'][0]['nov'] : 1;
            $dec = isset($release_plan['getReleasePlan'][0]['dec']) ? $release_plan['getReleasePlan'][0]['dec'] : 2;

            $jan_util = array_key_exists('January ' . $year, $utilized_month) ? isset($utilized_month['January ' . $year]['utilized']) ? $utilized_month['January ' . $year]['utilized'] : 0 : 0;
            $feb_util = array_key_exists('February ' . $year, $utilized_month) ? isset($utilized_month['February ' . $year]['utilized']) ? $utilized_month['February ' . $year]['utilized'] : 0 : 0;
            $mar_util = array_key_exists('March ' . $year, $utilized_month) ? isset($utilized_month['March ' . $year]['utilized']) ? $utilized_month['March ' . $year]['utilized'] : 0 : 0;
            $apr_util = array_key_exists('April ' . $year, $utilized_month) ? isset($utilized_month['April ' . $year]['utilized']) ? $utilized_month['April ' . $year]['utilized'] : 0 : 0;
            $may_util = array_key_exists('May ' . $year, $utilized_month) ? isset($utilized_month['May ' . $year]['utilized']) ? $utilized_month['May ' . $year]['utilized'] : 0 : 0;
            $jun_util = array_key_exists('June ' . $year, $utilized_month) ? isset($utilized_month['June ' . $year]['utilized']) ? $utilized_month['June ' . $year]['utilized'] : 0 : 0;
            $july_util = array_key_exists('July ' . $year, $utilized_month) ? isset($utilized_month['July ' . $year]['utilized']) ? $utilized_month['July ' . $year]['utilized'] : 0 : 0;
            $aug_util = array_key_exists('August ' . $year, $utilized_month) ? isset($utilized_month['August ' . $year]['utilized']) ? $utilized_month['August ' . $year]['utilized'] : 0 : 0;
            $sep_util = array_key_exists('September ' . $year, $utilized_month) ? isset($utilized_month['September ' . $year]['utilized']) ? $utilized_month['September ' . $year]['utilized'] : 0 : 0;
            $oct_util = array_key_exists('October ' . $year, $utilized_month) ? isset($utilized_month['October ' . $year]['utilized']) ? $utilized_month['October ' . $year]['utilized'] : 0 : 0;
            $nov_util = array_key_exists('November ' . $year, $utilized_month) ? isset($utilized_month['November ' . $year]['utilized']) ? $utilized_month['November ' . $year]['utilized'] : 0 : 0;
            $dec_util = array_key_exists('December ' . $year, $utilized_month) ? isset($utilized_month['December ' . $year]['utilized']) ? $utilized_month['December ' . $year]['utilized'] : 0 : 0;

            $content =
                '<div class="col-md-12">
                                <table class="table-bordered table table-responsive">
                                    <tr>
                                        <th class="colorTD">Month</th>
                                        <th class="colorTD">Leaves</th>
                                        <th class="colorTD">Utilized</th>
                                        <th class="colorTD">Month</th>
                                        <th class="colorTD">Leaves</th>
                                        <th class="colorTD">Utilized</th>
                                        </tr>
                                    <tr>
                                        <td class="colorTD">' . ucwords('January') . '</td>
                                        <td class="td_box"><input type="number" class="td_input_box month" team-id="' . $release_plan['admin_id'] . '" name="map[jan]" id="team_' . $release_plan['admin_id'] . '_1" step="0.25" min="0" max="31" pattern="^\d+(\.\d{1,2})?$" value="' . $jan . '" /></td>
                                        <td>' . $jan_util . '</td>                                        
                                        <td class="colorTD">' . ucwords('July') . '</td>
                                        <td class="td_box"><input type="number" class="td_input_box month" team-id="' . $release_plan['admin_id'] . '" name="map[july]" id="team_' . $release_plan['admin_id'] . '_7" step="0.25" min="0" max="31" pattern="^\d+(\.\d{1,2})?$" value="' . $july . '" /></td>
                                        <td>' . $july_util . '</td>
                                    </tr>
                                    <tr>
                                        <td class="colorTD">' . ucwords('February') . '</td>
                                        <td class="td_box"><input type="number" class="td_input_box month" team-id="' . $release_plan['admin_id'] . '"name="map[feb]" id="team_' . $release_plan['admin_id'] . '_2" step="0.25" min="0" max="28" pattern="^\d+(\.\d{1,2})?$" value="' . $feb . '" /></td>
                                        <td>' . $feb_util . '</td>
                                        <td class="colorTD">' . ucwords('August') . '</td>
                                        <td class="td_box"><input type="number" class="td_input_box month" team-id="' . $release_plan['admin_id'] . '"name="map[aug]" id="team_' . $release_plan['admin_id'] . '_8" step="0.25" min="0" max="31" pattern="^\d+(\.\d{1,2})?$" value="' . $aug . '" /></td>
                                        <td>' . $aug_util . '</td>
                                    </tr>
                                    <tr>
                                        <td class="colorTD text-left ">' . ucwords('March') . '</td>
                                        <td class="td_box"><input type="number" class="td_input_box month" team-id="' . $release_plan['admin_id'] . '"name="map[mar]" id="team_' . $release_plan['admin_id'] . '_3" step="0.25" min="0" max="31" pattern="^\d+(\.\d{1,2})?$" value="' . $mar . '" /></td>
                                        <td>' . $mar_util . '</td>
                                        <td class="colorTD">' . ucwords('September') . '</td>
                                        <td class="td_box"><input type="number" class="td_input_box month" team-id="' . $release_plan['admin_id'] . '"name="map[sep]" id="team_' . $release_plan['admin_id'] . '_9" step="0.25" min="0" max="30" pattern="^\d+(\.\d{1,2})?$" value="' . $sep . '" /></td>
                                        <td>' . $sep_util . '</td>
                                    </tr>
                                    <tr>
                                        <td class="colorTD">' . ucwords('April') . '</td>
                                        <td class="td_box"><input type="number" class="td_input_box month" team-id="' . $release_plan['admin_id'] . '"name="map[apr]" id="team_' . $release_plan['admin_id'] . '_4" step="0.25" min="0" max="30" pattern="^\d+(\.\d{1,2})?$" value="' . $apr . '" /></td>
                                        <td>' . $apr_util . '</td>
                                        <td class="colorTD">' . ucwords('October') . '</td>
                                        <td class="td_box"><input type="number" class="td_input_box month" team-id="' . $release_plan['admin_id'] . '"name="map[oct]" id="team_' . $release_plan['admin_id'] . '_10" step="0.25" min="0" max="31" pattern="^\d+(\.\d{1,2})?$" value="' . $oct . '" /></td>
                                        <td>' . $oct_util . '</td>
                                    </tr>
                                    <tr>
                                        <td class="colorTD">' . ucwords('May') . '</td>
                                        <td class="td_box"><input type="number" class="td_input_box month" team-id="' . $release_plan['admin_id'] . '"name="map[may]" id="team_' . $release_plan['admin_id'] . '_5" step="0.25" min="0" max="31" pattern="^\d+(\.\d{1,2})?$" value="' . $may . '" /></td>
                                        <td>' . $may_util . '</td>
                                        <td class="colorTD">' . ucwords('November') . '</td>
                                        <td class="td_box"><input type="number" class="td_input_box month" team-id="' . $release_plan['admin_id'] . '"name="map[nov]" id="team_' . $release_plan['admin_id'] . '_11" step="0.25" min="0" max="30" pattern="^\d+(\.\d{1,2})?$" value="' . $nov . '" /></td>
                                        <td>' . $nov_util . '</td>
                                    </tr>
                                    <tr>
                                        <td class="colorTD">' . ucwords('June') . '</td>
                                        <td class="td_box"><input type="number" class="td_input_box month" team-id="' . $release_plan['admin_id'] . '"name="map[jun]" id="team_' . $release_plan['admin_id'] . '_6" step="0.25" min="0" max="30" pattern="^\d+(\.\d{1,2})?$" value="' . $jun . '" /></td>
                                        <td>' . $jun_util . '</td>
                                        <td class="colorTD">' . ucwords('December') . '</td>
                                        <td class="td_box"><input type="number" class="td_input_box month" team-id="' . $release_plan['admin_id'] . '" name="map[dec]" id="team_' . $release_plan['admin_id'] . '_12" step="0.25" min="0" max="31" pattern="^\d+(\.\d{1,2})?$" value="' . $dec . '" /></td>
                                        <td>' . $dec_util . '</td>
                                    </tr>
                                </table>
                             </div>';
            if ($data['year'] === date('Y')) {
                $data['isUpdate'] = true;
            } else {
                $data['isUpdate'] = false;
            }
            if ($data['status'] == '0') {
                $data['isUpdate'] = false;
            }
            $data['json'] = $content;
            return $data;
        } else {
            return [];
        }
    }

    // /**
    //  *  Get Data for view page(Datatables)
    //  *  @Bhuvanesh on 07 Jan 2018
    // **/
    // public function anyData(Request $request)
    // {
    //     $searchKey    = NULL;
    //     $release_plan = ReleasePlan::where(function($query) use ($request) 
    //     {
    //         if (!empty($request->get('release_plan_year')))
    //         {
    //             $query->where('year', "LIKE", '%'.$request->get('release_plan_year').'%');
    //         }    
    //     })->orderBy('release_plan_id', 'desc')->get()->groupBy('year')->toArray();
    //     return Datatables::of($release_plan)
    //         ->addColumn('year', function($release_plan){
    //             return  $release_plan[0]['year'];
    //         })
    //         ->addColumn('show_release_plan', function($release_plan){
    //             $encrypted_release_plan_year = get_encrypted_value($release_plan[0]['year'], true);
    //             return  '<a href="'. url('admin-panel/release-plan/show-release-plan/' . $encrypted_release_plan_year ) . '" "><div class="label label-table label-info">Show Release Plan</div></a>';
    //         })
    //         ->addColumn('action', function ($release_plan)
    //         {
    //             $encrypted_release_plan_year = get_encrypted_value($release_plan[0]['year'], true);
    //             return '
    //             &nbsp;
    //             <a href="'. url('admin-panel/release-plan/add-release-plan/' . $encrypted_release_plan_year ) .'"data-toggle="tooltip" title="Edit Release Plan"><i class="fas fa-pencil-alt"></i></a>
    //             &nbsp;&nbsp; 
    //             <a href="'. url('admin-panel/release-plan/release-plan-delete/' . $encrypted_release_plan_year ) .'" onclick="return confirm('."'Are you sure?'".')" " data-toggle="tooltip" title="Delete Release Plan"><i class="fas fa-trash"></i></a> ';       
    //         })->rawColumns(['year' => 'year','show_release_plan' => 'show_release_plan', 'action' => 'action'])->addIndexColumn()->make(true);
    // }

    /**
     *  Add page for Release Plan
     *  @Bhuvanesh on 11 Dec 2018
     **/
    // public function add(Request $request, $year = NULL)
    // {
    //     $data    		             =  [];
    //     $release_plan                =  [];
    //     $yearOption                  =  get_year_options(1999,2035);
    //     $yearOption                  =  add_blank_option($yearOption, 'Select Year');
    //     $loginInfo 		             =  get_loggedin_user_data();
    //     $decrypted_release_plan_year =  NULL;
    //     $isUpdate                    =  FALSE;
    //     if (!empty($year)) {
    //         $decrypted_release_plan_year    = get_decrypted_value($year, true);
    //         $release_plan      			    = ReleasePlan::where('year','=',$decrypted_release_plan_year);
    //         if (!$release_plan)
    //         {
    //             return redirect('admin-panel/release-plan/add-release-plan')->withError('Release plan not found!');
    //         }
    //         $page_title      = trans('language.edit_release_plan');
    //         $save_url        = url('admin-panel/release-plan/save/' . $year);
    //         $submit_button   = 'Update';
    //         $isUpdate        = TRUE;
    //     }
    //     else {
    //         $page_title      = trans('language.add_release_plan');
    //         $save_url        = url('admin-panel/release-plan/save');
    //         $submit_button   = 'Save';
    //     }
    //     $data   = array(
    //         'page_title'    	=> $page_title,
    //         'save_url'      	=> $save_url,
    //         'submit_button' 	=> $submit_button,
    //         'year'              => $decrypted_release_plan_year,
    //         'enc_year'          => $year,
    //         'yearOption'        => $yearOption,
    //         'is_update'         => $isUpdate,  
    //         'login_info'    	=> $loginInfo,
    //     );
    //     return view('admin-panel.release-plan.add')->with($data);
    // }

    /**
     *  Validate year for Release Plan
     *  @Bhuvanesh on 07 Jan 2018
     **/
    // public function yearValidate(Request $request)
    // {
    //     $isValid = 'notValid';
    //     if(!empty($request) && $request->get('year') != '')
    //     $release_plan  =  ReleasePlan::where('year','=',$request->get('year'))->get()->count();
    //     if($release_plan == 0){
    //         $isValid = 'valid';
    //     }
    //     return $isValid;
    // }

    /**
     *  Team List for Release Plan
     *  @Bhuvanesh on 07 Jan 2018
     **/
    // public function teamList(Request $request)
    // {
    //     // p($request->all());
    //     $release_plan = [];
    //     $release_plan = Team::where(function($query) 
    //     {
    //         $query->where('type','=','2');    
    //     })->with(['getReleasePlan' => function($q) use ($request){
    //         $decrypted_release_plan_year = get_decrypted_value($request->get('year'), true);
    //         $q->where('year', '=', $decrypted_release_plan_year);
    //         $q->orderBy('year', 'desc');
    //     }])->orderBy('admin_id', 'desc')->get()->toArray();
    //     return Datatables::of($release_plan)
    //         ->addColumn('total', function($release_plan){
    //             $release_plan_id   = NULL;
    //             $fillValue = 0;
    //             if(!empty($release_plan['get_release_plan']) && $release_plan['get_release_plan'][0]['json']){
    //                 $json = $release_plan['get_release_plan'][0]['json'];
    //                 $json_arr = json_decode($json);
    //                 foreach($json_arr as $key => $value)
    //                 {
    //                     $fillValue = $fillValue + $json_arr->$key;    
    //                 }
    //             }
    //             else{
    //                 $fillValue = 12;
    //             }
    //             return '<input type="number" readonly class="form-control" name="map['.$release_plan['admin_id'].'][total]" id="map_'.$release_plan['admin_id'].'_total" value="'.$fillValue.'" team-id='.$release_plan['admin_id'].'>';
    //         })
    //         ->addColumn('jan', function($release_plan){
    //             $release_plan_id  = NULL;
    //             $fillValue = 0;
    //             if(!empty($release_plan['get_release_plan']) && $release_plan['get_release_plan'][0]['json']){
    //                 $release_plan_id = $release_plan['get_release_plan'][0]['release_plan_id'];
    //                 $json = $release_plan['get_release_plan'][0]['json'];
    //                 $json_arr = json_decode($json);
    //                 $fillValue = $json_arr->jan;   
    //             }
    //             else{
    //                 $fillValue = 1;
    //             }
    //             $content   =  '<input type="hidden" name="map['.$release_plan['admin_id'].'][release_plan_id]" id="map_'.$release_plan['admin_id'].'_release_plan_id" value="'.$release_plan_id.'">';
    //             $content  .=  '<input type="hidden" name="map['.$release_plan['admin_id'].'][admin_id]" id="map_'.$release_plan['admin_id'].'_admin_id" value="'.$release_plan['admin_id'].'">';                
    //             $json      =  isset($release_plan_arr['json']) ? $release_plan_arr['json'] : '';
    //             return $content.'<input type="number" class="form-control month" name="map['.$release_plan['admin_id'].'][json][jan]" id="map_'.$release_plan['admin_id'].'_1" value="'.$fillValue.'" team-id='.$release_plan['admin_id'].'  min="0" >';
    //         })
    //         ->addColumn('feb', function($release_plan){
    //             $fillValue = 0;
    //             if(!empty($release_plan['get_release_plan']) && $release_plan['get_release_plan'][0]['json']){
    //                 $release_plan_id = $release_plan['get_release_plan'][0]['release_plan_id'];
    //                 $json = $release_plan['get_release_plan'][0]['json'];
    //                 $json_arr = json_decode($json);
    //                 $fillValue = $json_arr->feb;   
    //             }
    //             else{
    //                 $fillValue = 1;
    //             }
    //             return '<input type="number" class="form-control month" name="map['.$release_plan['admin_id'].'][json][feb]" id="map_'.$release_plan['admin_id'].'_2" value="'.$fillValue.'" team-id='.$release_plan['admin_id'].' min="0" >';
    //         })
    //         ->addColumn('mar', function($release_plan){
    //             $fillValue = 0;
    //             if(!empty($release_plan['get_release_plan']) && $release_plan['get_release_plan'][0]['json']){
    //                 $release_plan_id = $release_plan['get_release_plan'][0]['release_plan_id'];
    //                 $json = $release_plan['get_release_plan'][0]['json'];
    //                 $json_arr = json_decode($json);
    //                 $fillValue = $json_arr->mar;   
    //             }
    //             else{
    //                 $fillValue = 1;
    //             }
    //             return '<input type="number" class="form-control month" name="map['.$release_plan['admin_id'].'][json][mar]" id="map_'.$release_plan['admin_id'].'_3" value="'.$fillValue.'" team-id='.$release_plan['admin_id'].' min="0" >';
    //         })
    //         ->addColumn('apr', function($release_plan){
    //             $fillValue = 0;
    //             if(!empty($release_plan['get_release_plan']) && $release_plan['get_release_plan'][0]['json']){
    //                 $release_plan_id = $release_plan['get_release_plan'][0]['release_plan_id'];
    //                 $json = $release_plan['get_release_plan'][0]['json'];
    //                 $json_arr = json_decode($json);
    //                 $fillValue = $json_arr->apr;   
    //             }
    //             else{
    //                 $fillValue = 1;
    //             }
    //             return '<input type="number" class="form-control month" name="map['.$release_plan['admin_id'].'][json][apr]" id="map_'.$release_plan['admin_id'].'_4" value="'.$fillValue.'" team-id='.$release_plan['admin_id'].' min="0" >';
    //         })
    //         ->addColumn('may', function($release_plan){
    //             $fillValue = 0;
    //             if(!empty($release_plan['get_release_plan']) && $release_plan['get_release_plan'][0]['json']){
    //                 $release_plan_id = $release_plan['get_release_plan'][0]['release_plan_id'];
    //                 $json = $release_plan['get_release_plan'][0]['json'];
    //                 $json_arr = json_decode($json);
    //                 $fillValue = $json_arr->may;   
    //             }
    //             else{
    //                 $fillValue = 1;
    //             }
    //             return '<input type="number" class="form-control month" name="map['.$release_plan['admin_id'].'][json][may]" id="map_'.$release_plan['admin_id'].'_5" value="'.$fillValue.'" team-id='.$release_plan['admin_id'].' min="0" >';
    //         })
    //         ->addColumn('jun', function($release_plan){
    //             $fillValue = 0;
    //             if(!empty($release_plan['get_release_plan']) && $release_plan['get_release_plan'][0]['json']){
    //                 $release_plan_id = $release_plan['get_release_plan'][0]['release_plan_id'];
    //                 $json = $release_plan['get_release_plan'][0]['json'];
    //                 $json_arr = json_decode($json);
    //                 $fillValue = $json_arr->jun;   
    //             }
    //             else{
    //                 $fillValue = 1;
    //             }
    //             return '<input type="number" class="form-control month" name="map['.$release_plan['admin_id'].'][json][jun]" id="map_'.$release_plan['admin_id'].'_6" value="'.$fillValue.'" team-id='.$release_plan['admin_id'].' min="0" >';
    //         })
    //         ->addColumn('july', function($release_plan){
    //             $fillValue = 0;
    //             if(!empty($release_plan['get_release_plan']) && $release_plan['get_release_plan'][0]['json']){
    //                 $release_plan_id = $release_plan['get_release_plan'][0]['release_plan_id'];
    //                 $json = $release_plan['get_release_plan'][0]['json'];
    //                 $json_arr = json_decode($json);
    //                 $fillValue = $json_arr->july;   
    //             }
    //             else{
    //                 $fillValue = 1;
    //             }
    //             return '<input type="number" class="form-control month" name="map['.$release_plan['admin_id'].'][json][july]" id="map_'.$release_plan['admin_id'].'_7" value="'.$fillValue.'" team-id='.$release_plan['admin_id'].' min="0" >';
    //         })
    //         ->addColumn('aug', function($release_plan){
    //             $fillValue = 0;
    //             if(!empty($release_plan['get_release_plan']) && $release_plan['get_release_plan'][0]['json']){
    //                 $release_plan_id = $release_plan['get_release_plan'][0]['release_plan_id'];
    //                 $json = $release_plan['get_release_plan'][0]['json'];
    //                 $json_arr = json_decode($json);
    //                 $fillValue = $json_arr->aug;   
    //             }
    //             else{
    //                 $fillValue = 1;
    //             }
    //             return '<input type="number" class="form-control month" name="map['.$release_plan['admin_id'].'][json][aug]" id="map_'.$release_plan['admin_id'].'_8" value="'.$fillValue.'" team-id='.$release_plan['admin_id'].' min="0" >';
    //         })
    //         ->addColumn('sep', function($release_plan){
    //             $fillValue = 0;
    //             if(!empty($release_plan['get_release_plan']) && $release_plan['get_release_plan'][0]['json']){
    //                 $release_plan_id = $release_plan['get_release_plan'][0]['release_plan_id'];
    //                 $json = $release_plan['get_release_plan'][0]['json'];
    //                 $json_arr = json_decode($json);
    //                 $fillValue = $json_arr->sep;   
    //             }
    //             else{
    //                 $fillValue = 1;
    //             }
    //             return '<input type="number" class="form-control month" name="map['.$release_plan['admin_id'].'][json][sep]" id="map_'.$release_plan['admin_id'].'_9" value="'.$fillValue.'" team-id='.$release_plan['admin_id'].' min="0" >';
    //         })
    //         ->addColumn('oct', function($release_plan){
    //             $fillValue = 0;
    //             if(!empty($release_plan['get_release_plan']) && $release_plan['get_release_plan'][0]['json']){
    //                 $release_plan_id = $release_plan['get_release_plan'][0]['release_plan_id'];
    //                 $json = $release_plan['get_release_plan'][0]['json'];
    //                 $json_arr = json_decode($json);
    //                 $fillValue = $json_arr->oct;   
    //             }
    //             else{
    //                 $fillValue = 1;
    //             }
    //             return '<input type="number" class="form-control month" name="map['.$release_plan['admin_id'].'][json][oct]" id="map_'.$release_plan['admin_id'].'_10" value="'.$fillValue.'" team-id='.$release_plan['admin_id'].' min="0" >';
    //         })
    //         ->addColumn('nov', function($release_plan){
    //             $fillValue = 0;
    //             if(!empty($release_plan['get_release_plan']) && $release_plan['get_release_plan'][0]['json']){
    //                 $release_plan_id = $release_plan['get_release_plan'][0]['release_plan_id'];
    //                 $json = $release_plan['get_release_plan'][0]['json'];
    //                 $json_arr = json_decode($json);
    //                 $fillValue = $json_arr->nov;   
    //             }
    //             else{
    //                 $fillValue = 1;
    //             }
    //             return '<input type="number" class="form-control month" name="map['.$release_plan['admin_id'].'][json][nov]" id="map_'.$release_plan['admin_id'].'_11" value="'.$fillValue.'" team-id='.$release_plan['admin_id'].' min="0" >';
    //         })
    //         ->addColumn('dec', function($release_plan){
    //             $fillValue = 0;
    //             if(!empty($release_plan['get_release_plan']) && $release_plan['get_release_plan'][0]['json']){
    //                 $release_plan_id = $release_plan['get_release_plan'][0]['release_plan_id'];
    //                 $json = $release_plan['get_release_plan'][0]['json'];
    //                 $json_arr = json_decode($json);
    //                 $fillValue = $json_arr->dec;   
    //             }
    //             else{
    //                 $fillValue = 1;
    //             }
    //             return '<input type="number" class="form-control month" name="map['.$release_plan['admin_id'].'][json][dec]" id="map_'.$release_plan['admin_id'].'_12" value="'.$fillValue.'" team-id='.$release_plan['admin_id'].' min="0" >';
    //         })
    //         ->rawColumns(['total' => 'total', 'jan' => 'jan', 'feb' => 'feb','mar' => 'mar','apr' => 'apr','may' => 'may','jun' => 'jun','july' => 'july','aug' => 'aug','sep' => 'sep','oct' => 'oct','nov' => 'nov','dec' => 'dec'])->addIndexColumn()->make(true);
    // }

    //  /**
    //  *  Destroy department data
    //  *  @Bhuvanesh on 11 Dec 2018
    // **/
    // public function destroy($id)
    // {
    //     $year = get_decrypted_value($id, true);
    //     $release_plan    = ReleasePlan::where('year','=', $year);
    //     if($release_plan){
    //         DB::beginTransaction();
    //         try {
    //             $release_plan->delete();
    //             $success_msg = "Release plan deleted successfully!";
    //             DB::commit();
    //         }
    //         catch(\Exception $e) {
    //             DB::rollback();
    //             $error_message = $e->getMessage();
    //             return redirect()->back()->withErrors("It's already in use. We can't delete this.");
    //         }   
    //         return redirect('admin-panel/release-plan/view-release-plan')->withSuccess($success_msg);
    //     }
    //     else {
    //         $error_message = "Release plan not found!";
    //         return redirect()->back()->withErrors($error_message);
    //     }
    // }

    // /**
    //  *  Team List for Release Plan
    //  *  @Bhuvanesh on 07 Jan 2018
    // **/
    // public function showReleasePlan(Request $request, $year = NULL)
    // {
    //     if(empty($year)){
    //         return redirect()->back()->withErrors('Release plan year not found.');
    //     }
    //     $loginInfo          = get_loggedin_user_data();
    //     $data = array(
    //         'page_title'    => trans('language.show_release_plans'),
    //         'redirect_url'  => url('admin-panel/release-plan/view-release-plan'),
    //         'login_info'    => $loginInfo,
    //         'year'          => $year,
    //     );
    //     return view('admin-panel.release-plan.show-release-plan')->with($data);
    // }
}