<?php
namespace App\Http\Controllers\adminPanel;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Model\Project\WorkType;
use Yajra\Datatables\Datatables;

class WorkTypeController extends Controller
{
    /**
     * Constructor for WorkType Controller 
    **/
    public function __construct()
    {
        $this->middleware('isadmin');
        // $this->middleware('user')->only('index');
        // $this->middleware('subscribed')->except('store');
    }

    /**
     *  View page for WorkType
     *  @Bhuvanesh on 26 Jan 2019
    **/
    public function index(Request $request, $id = NULL)
    {
        $data    		      =   [];
        $work_type            =   [];
        $loginInfo 		      =   get_loggedin_user_data();
        if (!empty($id)) {
            $decrypted_work_type_id 	    = get_decrypted_value($id, true);
            $work_type      			    = WorkType::Find($decrypted_work_type_id);
            if (!$work_type)
            {
                return redirect('admin-panel/work-type/view-work-type')->withError('Work Type not found!');
            }
            $page_title             	= trans('language.edit_work_type');
            $encrypted_work_type_id     = get_encrypted_value($work_type->work_type_id, true);
            $save_url               	= url('admin-panel/work-type/save/' . $encrypted_work_type_id);
            $submit_button          	= 'Update';
        }
        else {
            $page_title      = trans('language.add_work_type');
            $save_url        = url('admin-panel/work-type/save');
            $submit_button   = 'Save';
        }

        $data = array(
            'page_title'      => trans('language.view_responsibility'),
            'save_url'        => $save_url,
            'submit_button'   => $submit_button,
            'work_type' 	  => $work_type,
            'redirect_url'    => url('admin-panel/work-type/view-work-type'),
            'login_info'      => $loginInfo
        );
        return view('admin-panel.worktype.index')->with($data);
    }
    /**
     *  Get Data for view page(Datatables)
     *  @Bhuvanesh on 26 Jan 2019
    **/

    public function anyData(Request $request)
    {
        $work_type = [];
        $searchKey = NULL;
        $work_type = WorkType::where(function($query) use ($request) 
        {
            if (!empty($request->get('work_type_name')))
            {
                $query->where('work_type_name', "LIKE", '%'.$request->get('work_type_name').'%');
            }    
        })->orderBy('work_type_id', 'desc')->get()->toArray();
        return Datatables::of($work_type)
            ->addColumn('action', function ($work_type)
            {
                $encrypted_work_type_id = get_encrypted_value($work_type['work_type_id'], true);
                if($work_type['work_type_status'] == 0) {
                    $status = 1;
                    $statusVal = '<div class="radio radio-danger custom_radiobox"  data-toggle="tooltip" title="Change Status">
                    <input type="radio" name="radio'.$work_type['work_type_id'].'" onClick="changeStatus('.$work_type['work_type_id'].','.$status.')" id="radio'.$work_type['work_type_id'].'" value="option4" checked="">
                    <label for="radio'.$work_type['work_type_id'].'"> </label>
                </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="radio radio-success custom_radiobox" data-toggle="tooltip" title="Change Status">
                        <input type="radio" name="radio'.$work_type['work_type_id'].'" onClick="changeStatus('.$work_type['work_type_id'].','.$status.')" id="radio'.$work_type['work_type_id'].'" value="option4" checked="">
                        <label for="radio'.$work_type['work_type_id'].'"> </label>
                    </div>';
                }
                return '
                '.$statusVal.'
                &nbsp;
                <a href="'. url('admin-panel/work-type/view-work-type/' . $encrypted_work_type_id ) .'"data-toggle="tooltip" title="Edit Work Type"><i class="fas fa-pencil-alt"></i></a>
                &nbsp;&nbsp; 
                <a href="'. url('admin-panel/work-type/work-type-delete/' . $encrypted_work_type_id ) .'" onclick="return confirm('."'Are you sure?'".')" " data-toggle="tooltip" title="Delete Work Type"><i class="fas fa-trash"></i></a> ';       
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    // /**
    //  *  Add page for WorkType
    //  *  @Bhuvanesh on 26 Jan 2019
    // **/
    // public function add(Request $request, $id = NULL)
    // {
    //     $data    		            = [];
    //     $work_type                  = [];
    //     $loginInfo 		            = get_loggedin_user_data();
    //     if (!empty($id)) {
    //         $decrypted_work_type_id 	    = get_decrypted_value($id, true);
    //         $work_type      			    = WorkType::Find($decrypted_work_type_id);
    //         if (!$work_type)
    //         {
    //             return redirect('admin-panel/work-type/add-work-type')->withError('Work Type not found!');
    //         }
    //         $page_title             	= trans('language.edit_work_type');
    //         $encrypted_work_type_id     = get_encrypted_value($work_type->work_type_id, true);
    //         $save_url               	= url('admin-panel/work-type/save/' . $encrypted_work_type_id);
    //         $submit_button          	= 'Update';
    //     }
    //     else {
    //         $page_title      = trans('language.add_work_type');
    //         $save_url        = url('admin-panel/work-type/save');
    //         $submit_button   = 'Save';
    //     }
    //     $data   = array(
    //         'page_title'    	=> $page_title,
    //         'save_url'      	=> $save_url,
    //         'submit_button' 	=> $submit_button,
    //         'work_type' 	    => $work_type,
    //         'login_info'    	=> $loginInfo,
    //         'redirect_url'  	=> url('admin-panel/work-type/view-work-type'),
    //     );
    //     return view('admin-panel.worktype.add')->with($data);
    // }

    /**
     *  Save WorkType data
     *  @Bhuvanesh on 26 Jan 2019
    **/
    public function save(Request $request, $id = NULL)
    {
        $loginInfo              = get_loggedin_user_data();
        $decrypted_work_type_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $work_type = WorkType::find($decrypted_work_type_id);
            if (!$work_type)
            {
                return redirect('admin-panel/work-type/view-work-type/')->withError('Work Type not found!');
            }
            $success_msg    = 'Work Type updated successfully!';
        }
        else
        {
            $work_type      = New WorkType;
            $success_msg    = 'Work Type saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
                'work_type_name'          => 'required|unique:work_types,work_type_name,' . $decrypted_work_type_id . ',work_type_id|max:255',
                'work_type_description'   => 'required',
        ]);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try  {
                $work_type->work_type_name           = Input::get('work_type_name');
                $work_type->work_type_description    = Input::get('work_type_description'); 
                $work_type->save();
            }
            catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/work-type/view-work-type')->withSuccess( $success_msg);
    }

    /**
     *  Change WorkType's status
     *  @Bhuvanesh on 26 Jan 2019
    **/
    public function changeStatus(Request $request)
    {
        $id          =  $request->get('work_type_id');
        $status      =  $request->get('work_type_status');
        $work_type   =  WorkType::find($id);
        if ($work_type) {
            $work_type->work_type_status = $status;
            $work_type->save();
            echo "Success";
        }
        else {
            echo "Fail";
        }
    }

     /**
     *  Destroy WorkType data
     *  @Bhuvanesh on 26 Jan 2019
    **/
    public function destroy($id)
    {
        $work_type_id  = get_decrypted_value($id, true);
        $work_type    = WorkType::find($work_type_id);
        if ($work_type) {
            DB::beginTransaction();
            try {
                $work_type->delete();
                $success_msg = "Work Type deleted successfully!";
                DB::commit();
            }
            catch(\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors("It's already in use. We can't delete this.");
            }   
            return redirect('admin-panel/work-type/view-work-type')->withSuccess($success_msg);
        }
        else {
            $error_message = "Work Type not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}