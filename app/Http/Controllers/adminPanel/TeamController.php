<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Model\Department\Department;
use App\Model\Team\Team;
use App\Model\Team\TimeSlot;
use App\Model\Team\TimeSlotHistory;
use App\Model\UserRole\UserRole;
use App\Model\UserRole\AssignUserRole;
use Yajra\Datatables\Datatables;
use App\Model\Attendance\AttendanceRegister;
//use Illuminate\Support\Facades\Request;

class TeamController extends Controller
{
    /**
     * Constructor for Department Controller 
     **/
    public function __construct()
    {
        // $this->middleware('isadmin');
        // $this->middleware('user')->only('index');
        $this->middleware('isadmin')->except('verification', 'createPassword', 'inOutData', 'viewInOut');
        $this->middleware('ismember')->only('verification', 'createPassword', 'inOutData', 'viewInOut');
    }

    /**
     *  View page for Team Members
     *  @Bhuvanesh on 12 Dec 2018
     **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title' => trans('language.view_team'),
            'redirect_url' => url('admin-panel/team/view-team'),
            'resign_url' => url('admin-panel/team/team-delete'),
            'login_info' => $loginInfo,
        );
        return view('admin-panel.team.index')->with($data);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Bhuvanesh on 12 Dec 2018
     **/
    public function anyData(Request $request)
    {
        $team = [];
        $team = Team::where(function ($query) use ($request) {
            if (!empty($request->get('team_name'))) {
                $query->where('admin_name', 'LIKE', '%' . $request->get('team_name') . '%');
                $query->orWhere('email', 'LIKE', '%' . $request->get('team_name') . '%');
                $query->orWhere('phone', 'LIKE', '%' . $request->get('team_name') . '%');
            }
            $query->Where(['type' => '2', 'is_terminate' => '0', 'is_resign' => '0']);
        })
            ->with(['getDepartment', 'getTimeSlot'])
            ->orderBy('admin_id', 'desc')
            ->get()->toArray();
        return Datatables::of($team)
            ->addColumn('admin_name', function ($team) {
                $time_slot = '----';
                if (!empty($team['get_time_slot'])) {
                    $time_slot = get_all_time_slot($team['get_time_slot']['time_slot_id']);
                }
                $department_name = '----';
                if (!empty($team['get_department'])) {
                    $department_name = $team['get_department']['department_name'];
                }
                return $team['admin_name'] . '<br/><span class="text-muted">' . $department_name . '</span><br/><span class="text-muted">' . $time_slot . '</span>';
            })
            ->addColumn('email', function ($team) {
                return $team['email'] . '<br/><span class="text-muted">' . $team['phone'] . '</span>';
            })
            ->addColumn('user_roles', function ($team) {
                $selected_roles = [];
                $assign_roles = AssignUserRole::where(['admin_id' => $team['admin_id'], 'assign_user_role_status' => '1'])->with('getUserRole')->get()->toArray();
                if (!empty($assign_roles)) {
                    foreach ($assign_roles as $value) {
                        $selected_roles[] = $value['get_user_role']['user_role_name'];
                    }
                }
                return implode(', ', $selected_roles);
            })
            ->addColumn('VerificationProcess', function ($team) {
                if ($team['verification_status'] == 0) {
                    return '<div class=""><p class="text-danger">Pending</p></div>';
                } else {
                    return '<div class=""><p class="text-success">Complete</p></div>';
                }
            })
            ->addColumn('action', function ($team) {
                $encrypted_team_id = get_encrypted_value($team['admin_id'], true);
                if ($team['status'] == 0) {
                    $status = 1;
                    $statusVal = '<div class="radio radio-danger custom_radiobox">
                    <input type="radio" name="radio' . $team['admin_id'] . '" onClick="changeStatus(' . $team['admin_id'] . ',' . $status . ')" id="radio' . $team['admin_id'] . '" value="option4" checked="">
                    <label for="radio' . $team['admin_id'] . '"> </label>
                </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="radio radio-success custom_radiobox">
                        <input type="radio" name="radio' . $team['admin_id'] . '" onClick="changeStatus(' . $team['admin_id'] . ',' . $status . ')" id="radio' . $team['admin_id'] . '" value="option4" checked="">
                        <label for="radio' . $team['admin_id'] . '"> </label>
                    </div>';
                }
                return '
                ' . $statusVal . '
                &nbsp;
                <a href="' . url('admin-panel/team/add-team/' . $encrypted_team_id) . '"><i class="fas fa-pencil-alt"></i></a>
                &nbsp;&nbsp; 
                <div class="btn-group dropdown m-r-10">    
                    <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" style="color:red;" aria-expanded="false" data-toggle="dropdown"><i class="fas fa-tag"></i><b class="caret-icon fas fa-caret-down"></b></a>
                        <ul role="menu" class="dropdown-menu animated flipInY" style="right:0;left:unset;">
                            <li><a href="#" class="isCondition" data-toggle="modal" data-target="#isDeatils" condition="isResign" condition-for="' . $encrypted_team_id . '">Resign</a></li>
                            <li><a href="#" class="isCondition" data-toggle="modal" data-target="#isDeatils" condition="isTerminate" condition-for="' . $encrypted_team_id . '">Terminate</a></li>
                        </ul>
                </div>';
            })->rawColumns(['admin_name' => 'admin_name', 'email' => 'email', 'user_roles' => 'user_roles', 'VerificationProcess' => 'VerificationProcess'])->addIndexColumn()->make(true);
    }

    /**
     *  Add page for Team Member
     *  @Bhuvanesh on 12 Dec 2018
     **/
    public function add(Request $request, $id = null)
    {
        $data = [];
        $team = [];
        $user_roles = [];
        $selected_roles = [];
        $arr_saturday_off = get_arr_saturday_off();
        $arr_department = get_all_departments();
        $arr_platform = get_all_platforms();
        $arr_time_slot = get_all_time_slot();
        $loginInfo = get_loggedin_user_data();
        $user_role_obj = UserRole::where('user_role_status', '1')->orderBy('user_role_name', 'ASC')->get();
        if (!empty($user_role_obj)) {
            foreach ($user_role_obj as $value) {
                $user_roles[$value->user_role_id] = $value->user_role_name;
            }
        }
        if (!empty($id)) {
            $decrypted_team_id = get_decrypted_value($id, true);
            $team = Team::Find($decrypted_team_id);
            if (!$team) {
                return redirect('admin-panel/team/add-team')->withErrors('Team member not found!');
            }
            $assign_roles = AssignUserRole::where(['admin_id' => $team->admin_id, 'assign_user_role_status' => '1'])->get()->toArray();
            if (!empty($assign_roles)) {
                foreach ($assign_roles as $value) {
                    $selected_roles[] = $value['user_role_id'];
                }
            }
            $page_title = trans('language.edit_team');
            $encrypted_team_id = get_encrypted_value($team->admin_id, true);
            $save_url = url('admin-panel/team/save/' . $encrypted_team_id);
            $submit_button = 'Update';
        } else {
            $page_title = trans('language.add_team');
            $save_url = url('admin-panel/team/save');
            $submit_button = 'Save';
        }
        $team['arr_saturday_off'] = add_blank_option($arr_saturday_off, 'Select Saturday Off');
        $team['arr_department'] = add_blank_option($arr_department, 'Select Department');
        $team['arr_platform'] = $arr_platform;
        $team['arr_time_slot'] = add_blank_option($arr_time_slot, 'Select Time Slot');
        $data = array(
            'team' => $team,
            'save_url' => $save_url,
            'login_info' => $loginInfo,
            'user_roles' => $user_roles,
            'page_title' => $page_title,
            'submit_button' => $submit_button,
            'selected_roles' => $selected_roles,
            'redirect_url' => url('admin-panel/team/view-team'),
        );
        return view('admin-panel.team.add')->with($data);
    }

    /**
     *  Save Team member data
     *  @Bhuvanesh on 12 Dec 2018
     **/
    public function save(Request $request, $id = null)
    {
        $loginInfo = get_loggedin_user_data();
        $mailFlag = false;
        $decrypted_team_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $team = Team::find($decrypted_team_id);
            if (!$team) {
                return redirect('admin-panel/tean/view-team/')->withErrors('Team member not found!');
            }
            $success_msg = 'Team member updated successfully!';
            $emailValidator = 'required|unique:admins,email,' . $decrypted_team_id . ',admin_id|max:255';
        } else {
            $team = new Team;
            $success_msg = 'Team Member saved successfully!';
            $emailValidator = 'required|unique:admins,email|max:255';
            $emailVarification = '';
            $mailFlag = true;

            ///Logic for add Employee Number
            $emp_prefix = \Config::get('custom.emp_id_prefix');
            $year = date('y');
            $lastrecord = Team::select('admin_id')->orderBy('admin_id', 'desc')->first();
            $lastnumber = $lastrecord['admin_id'] + 1;
            $a = sprintf("%03d", $lastnumber);
            $emp_id = $emp_prefix . $year . $a;
            $team->emp_id = $emp_id;
        }

        $validatior = Validator::make($request->all(), [
            'team_name' => 'required|regex:/^[A-Za-z0-9\s\-\_]+$/',
            'team_email' => $emailValidator,
            // 'team_phone'      =>  'required|regex:/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[6789]\d{9}$/',
            'team_department' => 'required',
            'saturday_off' => 'required',
            'joining_date' => 'required',
            'team_roles' => 'required',
        ]);
        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction();
            try {
                $team->admin_name = Input::get('team_name');
                $team->email = Input::get('team_email');
                $team->phone = Input::get('team_phone');
                $team->department_id = Input::get('team_department');
                $team->saturday_off = Input::get('saturday_off');
                $team->joining_date = Input::get('joining_date');

                if (Input::get('team_platform') != '') {
                    $team->platforms = implode(',', Input::get('team_platform'));
                } else {
                    $team->platforms = '';
                }
                // if(!empty(Input::get('team_time_slot'))){
                $team->time_slot_id = Input::get('team_time_slot');
                $team->save();
                $team_roles = Input::get('team_roles');
                if (!empty($team_roles)) {
                    AssignUserRole::where('admin_id', '=', $team->admin_id)->update(['assign_user_role_status' => '0']);
                    foreach ($team_roles as $value) {
                        $role_assign = AssignUserRole::where(['admin_id' => $team->admin_id, 'user_role_id' => $value])->first();
                        if (!empty($role_assign)) {
                            $role_assign->assign_user_role_status = '1';
                        } else {
                            $role_assign = new AssignUserRole();
                            $role_assign->admin_id = $team->admin_id;
                            $role_assign->user_role_id = $value;
                        }
                        $role_assign->save();
                    }
                }
                // $time_slot_history = TimeSlotHistory::where('admin_id','=',$team->admin_id)->whereDate('created_at', Carbon::today())->first();
                // if(!empty($time_slot_history)){
                //     $time_slot_history->admin_id     = $team->admin_id;
                //     $time_slot_history->time_slot_id = Input::get('team_time_slot');
                //     $time_slot_history->save();    
                // }
                // else{
                //     $time_slot_history           = new TimeSlotHistory;
                // }
                // $time_slot_history->admin_id     = $team->admin_id;
                // $time_slot_history->time_slot_id = Input::get('team_time_slot');
                // $time_slot_history->save();
                // }
                // else{
                //     $team->save();                    
                // }
                if ($mailFlag) {
                    $encryp_id = get_encrypted_value($team->admin_id, true);
                    $verificationPath = url("/activate/{$encryp_id}");
                    send_email(Input::get('team_name'), Input::get('team_email'), $verificationPath);
                }
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/team/view-team')->withSuccess($success_msg);
    }

    /**
     *  Change Team member status
     *  @Bhuvanesh on 12 Dec 2018
     **/
    public function changeStatus(Request $request)
    {
        $id = $request->get('team_id');
        $status = $request->get('team_status');
        $team = Team::find($id);
        if ($team) {
            $team->status = $status;
            $team->save();
            echo "Success";
        } else {
            echo "Fail";
        }
    }

    /**
     *  Destroy Team member data
     *  @Bhuvanesh on 12 Dec 2018
     **/
    public function destroy(Request $request)
    {
        $request_arr = $request->all();
        $condition = '';
        $leaving_date = '';
        $joining_date = '';
        $team = [];
        if (!empty($request_arr)) {
            $team_id = get_decrypted_value($request_arr['team_id'], true);
            $condition = $request_arr['condtionIs'];
            if (isset($request['leaving_date'])) {
                $leaving_date = $request_arr['leaving_date'];
            }
            if (isset($request['joining_date'])) {
                $joining_date = $request_arr['joining_date'];
            }
            $team = Team::find($team_id);
        }
        if ($team) {
            DB::beginTransaction();
            try {
                if ($condition == 'isTerminate') {
                    $team->status = '0';
                    $team->is_terminate = '1';
                    $team->leaving_date = $leaving_date;
                    $team->save();
                    $success_msg = "Employee terminated successfully!";
                } else if ($condition == 'isResign') {
                    $team->status = '0';
                    $team->is_resign = '1';
                    $team->leaving_date = $leaving_date;
                    $team->save();
                    $success_msg = "Employee resign successfully!";
                } else if ($condition == 'isRejoin') {
                    $team->status = '0';
                    $team->is_terminate = '0';
                    $team->is_resign = '0';
                    $team->joining_date = $joining_date;
                    $team->leaving_date = null;
                    $team->save();
                    $success_msg = "Employee joined successfully!";
                } else {
                    return redirect('admin-panel/team/view-resign-team')->withErrors('Invalid Condition');
                }
            } catch (\Exception $e) {
                DB::rollback();
                // dd($e);
                $error_message = "Member Record is used for other record. Not deleted!";
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
            return redirect('admin-panel/team/view-team')->withSuccess($success_msg);
        } else {
            $error_message = "Member Record not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /*
     *  Call for Email Verification
     *  @Bhuvanesh on 12 Dec 2018
     */
    public function verification($id)
    {
        $team_id = get_decrypted_value($id, true);
        if ($team_id == 0) {
            return redirect('admin-panel/')->withErrors('Something wrong goes with url!');
        }
        $team = Team::find($team_id);
        if (!$team) {
            return redirect('admin-panel/')->withErrors('Invalid User!');
        } else {
            $verification_status = $team->verification_status;
            if ($verification_status == 0) {
                $data = array(
                    'page_title' => trans('language.password_create'),
                    'redirect_url' => url('admin-panel/auth/createpassword'),
                    'team_id' => $id
                );
                return view('admin-panel.auth.createpassword')->with($data);
            } else {
                $error_message = 'Your verification already done. Please Login.';
                return redirect('admin-panel')->withErrors($error_message);
            }
        }
    }

    /*
     *  Set password for team members
     *  @Bhuvanesh on 12 Dec 2018
     */
    public function createPassword(Request $request)
    {
        $passwordValidator = null;
        $decrypted_team_id = get_decrypted_value($request->get('team_id'), true);
        if (!empty($request->has('team_id'))) {
            $team = Team::find($decrypted_team_id);
            if (!$team) {
                return redirect('admin-panel/')->withErrors('Team member not found!');
            } else {
                if ($team->verification_status == 1) {
                    return redirect()->back()->withErrors('Your verification process already done. Please Login.');
                }
            }
            $success_msg = 'Verification proecss successfully complete.';
            $passwordValidator = 'required|regex:/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/|max:255';
        }
        $validatior = Validator::make($request->all(), [
            'team_password' => $passwordValidator,
        ]);
        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction();
            try {
                $team->password = Hash::make($request->get('team_password'));
                $team->verification_status = '1';
                $team->save();
                // send_thankyou_email($team->admin_name, $team->email);
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withInput()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/')->withSuccess($success_msg);
    }


    /**
     *  View page for Team Members those are resign and terminate
     *  @Bhuvanesh on 12 Dec 2018
     **/
    public function resignView()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title' => trans('language.resign_team'),
            'rejoin_url' => url('admin-panel/team/team-delete'),
            'redirect_url' => url('admin-panel/team/view-team'),
            'login_info' => $loginInfo,
        );
        return view('admin-panel.team.resign')->with($data);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Bhuvanesh on 12 Dec 2018
     **/
    public function anyDataResign(Request $request)
    {
        $team = [];
        $team = Team::where(function ($query) use ($request) {
            if (!empty($request->get('team_name'))) {
                $query->where('admin_name', 'LIKE', '%' . $request->get('team_name') . '%');
            }
            $query->Where('is_terminate', '=', '1');
            $query->orWhere('is_resign', '=', '1');
        })
            ->with(['getDepartment', 'getTimeSlot'])
            ->orderBy('admin_id', 'desc')
            ->get()->toArray();
        return Datatables::of($team)
            ->addColumn('admin_name', function ($team) {
                $department_name = '----';
                if (!empty($team['get_department'])) {
                    $department_name = $team['get_department']['department_name'];
                }
                return $team['admin_name'] . '<br/><span class="text-muted">' . $department_name . '</span>';
            })
            ->addColumn('email', function ($team) {
                return $team['email'] . '<br/><span class="text-muted">' . $team['phone'] . '</span>';
            })
            ->addColumn('leaving_date', function ($team) {
                $cause = '----';
                if ($team['is_resign'] == 1) {
                    $cause = 'Resign';
                }
                if ($team['is_terminate'] == 1) {
                    $cause = 'Terminate';
                }
                return $team['leaving_date'] . '<br/><span class="text-muted">' . $cause . '</span>';
            })
            ->addColumn('action', function ($team) {
                $encrypted_team_id = get_encrypted_value($team['admin_id'], true);
                return ' 
                <div class="btn-group dropdown m-r-10">    
                    <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" style="color:red;" aria-expanded="false" data-toggle="dropdown"><i class="fas fa-tag"></i><b class="caret-icon fas fa-caret-down"></b></a>
                        <ul role="menu" class="dropdown-menu animated flipInY" style="right:0;left:unset;">
                            <li><a href="#" class="isCondition" data-toggle="modal" data-target="#isDeatils" condition="isRejoin" condition-for="' . $encrypted_team_id . '">Rejoin</a></li>
                        </ul>
                </div>';
            })->rawColumns(['admin_name' => 'admin_name', 'email' => 'email', 'leaving_date' => 'leaving_date', 'action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     * View In - Out for Employee
     * @Bhuvanesh on 13 Feb 2019
     */
    public function viewInOut()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title' => trans('language.view_in_out'),
            'login_info' => $loginInfo,
        );
        return view('admin-panel.team.in-out')->with($data);
    }

    /**
     * View In - Out for Employee
     * @Bhuvanesh on 13 Feb 2019
     */
    public function inOutData(Request $request)
    {
        $in_out = [];
        $loginInfo = get_loggedin_user_data();
        $in_out = AttendanceRegister::where(function ($query) use ($loginInfo, $request) {
            $query->where('admin_id', $loginInfo['admin_id']);
            if (isset($request) && $request->get('dates') != '') {
                $dates = explode('-', $request->get('dates'));
                $start_date = date_create($dates[0])->format('Y-m-d');
                $end_date = date_create($dates[1])->format('Y-m-d');
                $query->whereBetween('attendance_date', [$start_date, $end_date]);
            }
        })
            ->orderBy('attendance_date', 'desc')
            ->get()->toArray();
        return Datatables::of($in_out)
            ->addColumn('in_out_date', function ($in_out) {
                return date_create($in_out['attendance_date'])->format('d-m-Y');
            })
            ->addColumn('in_out_intime', function ($in_out) {
                if ($in_out['attendance_intime'] != '') {
                    return date_create($in_out['attendance_intime'])->format('h:i A');
                } else {
                    return '--';
                }
            })
            ->addColumn('in_out_outtime', function ($in_out) {
                if ($in_out['attendance_outtime']) {
                    return date_create($in_out['attendance_outtime'])->format('h:i A');
                } else {
                    return '-:- -';
                }
            })
            ->addColumn('in_out_total', function ($in_out) {
                $total_time = $in_out['attendance_total_time'];
                $time = intdiv($total_time, 60) . ' hrs ' . ($total_time % 60) . ' minutes';
                return $time;
            })
            ->addColumn('in_out_key', function ($in_out) {
                if (strpos($in_out['attendance_key'], 'full_day') !== false) {
                    return 'Absent';
                } else if (strpos($in_out['attendance_key'], 'half_day') !== false) {
                    return 'Half Day';
                } else if (strpos($in_out['attendance_key'], 'CH') !== false) {
                    return 'Company Holiday';
                } else if (strpos($in_out['attendance_key'], 'dhalf') !== false) {
                    return 'Direct Half';
                } else if (strpos($in_out['attendance_key'], 'na') !== false) {
                    return 'Request';
                } else if (strpos($in_out['attendance_key'], 'late') !== false) {
                    return 'Late';
                } else if (strpos($in_out['attendance_key'], 'saturday') !== false) {
                    return 'Saturday Extra Work';
                } else if (strpos($in_out['attendance_key'], 'sunday') !== false) {
                    return 'Sunday Extra Work';
                } else {
                    return 'Present';
                }
            })
            ->addColumn('in_out_desc', function ($in_out) {
                return $in_out['attendance_desc'];
            })
            ->rawColumns(['in_out_total' => 'in_out_total', 'in_out_desc' => 'in_out_desc', 'in_out_date' => 'in_out_date'])->addIndexColumn()->make(true);
    }
}
