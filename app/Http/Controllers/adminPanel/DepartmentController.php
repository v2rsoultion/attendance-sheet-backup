<?php
namespace App\Http\Controllers\adminPanel;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Model\Department\Department;
use Yajra\Datatables\Datatables;

class DepartmentController extends Controller
{
    /**
     * Constructor for Department Controller 
     **/
    public function __construct()
    {
        $this->middleware('isadmin');
        // $this->middleware('user')->only('index');
        // $this->middleware('subscribed')->except('store');
    }

    /**
     *  View page for Department
     *  @Bhuvanesh on 11 Dec 2018
     **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title' => trans('language.view_departments'),
            'redirect_url' => url('admin-panel/department/view-department'),
            'login_info' => $loginInfo
        );
        return view('admin-panel.department.index')->with($data);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Bhuvanesh on 11 Dec 2018
     **/

    public function anyData(Request $request)
    {
        $department = [];
        $searchKey = null;
        $department = Department::where(function ($query) use ($request) {
            if (!empty($request->get('department_name'))) {
                $query->where('department_name', "LIKE", '%' . $request->get('department_name') . '%');
            }
        })->orderBy('department_id', 'desc')->get()->toArray();
        return Datatables::of($department)
            ->addColumn('action', function ($department) {
                $encrypted_department_id = get_encrypted_value($department['department_id'], true);
                if ($department['status'] == 0) {
                    $status = 1;
                    $statusVal = '<div class="radio radio-danger custom_radiobox"  data-toggle="tooltip" title="Change Status">
                    <input type="radio" name="radio' . $department['department_id'] . '" onClick="changeStatus(' . $department['department_id'] . ',' . $status . ')" id="radio' . $department['department_id'] . '" value="option4" checked="">
                    <label for="radio' . $department['department_id'] . '"> </label>
                </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="radio radio-success custom_radiobox" data-toggle="tooltip" title="Change Status">
                        <input type="radio" name="radio' . $department['department_id'] . '" onClick="changeStatus(' . $department['department_id'] . ',' . $status . ')" id="radio' . $department['department_id'] . '" value="option4" checked="">
                        <label for="radio' . $department['department_id'] . '"> </label>
                    </div>';
                }
                return '
                ' . $statusVal . '
                &nbsp;
                <a href="' . url('admin-panel/department/add-department/' . $encrypted_department_id) . '"data-toggle="tooltip" title="Edit Department"><i class="fas fa-pencil-alt"></i></a>
                &nbsp;&nbsp; 
                <a href="' . url('admin-panel/department/department-delete/' . $encrypted_department_id) . '" onclick="return confirm(' . "'Are you sure?'" . ')" " data-toggle="tooltip" title="Delete Department"><i class="fas fa-trash"></i></a> ';
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Add page for Department
     *  @Bhuvanesh on 11 Dec 2018
     **/
    public function add(Request $request, $id = null)
    {
        $data = [];
        $department = [];
        $loginInfo = get_loggedin_user_data();
        if (!empty($id)) {
            $decrypted_department_id = get_decrypted_value($id, true);
            $department = Department::Find($decrypted_department_id);
            if (!$department) {
                return redirect('admin-panel/department/add-department')->withError('Department not found!');
            }
            $page_title = trans('language.edit_department');
            $encrypted_department_id = get_encrypted_value($department->department_id, true);
            $save_url = url('admin-panel/department/save/' . $encrypted_department_id);
            $submit_button = 'Update';
        } else {
            $page_title = trans('language.add_department');
            $save_url = url('admin-panel/department/save');
            $submit_button = 'Save';
        }
        $data = array(
            'page_title' => $page_title,
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'department' => $department,
            'login_info' => $loginInfo,
            'redirect_url' => url('admin-panel/department/view-department'),
        );
        return view('admin-panel.department.add')->with($data);
    }

    /**
     *  Save department data
     *  @Bhuvanesh on 11 Dec 2018
     **/
    public function save(Request $request, $id = null)
    {
        $loginInfo = get_loggedin_user_data();
        $decrypted_department_id = null;
        $decrypted_department_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $department = Department::find($decrypted_department_id);
            if (!$department) {
                return redirect('admin-panel/department/view-department/')->withError('Department not found!');
            }
            $success_msg = 'Department updated successfully!';
        } else {
            $department = new Department;
            $success_msg = 'Department saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'department_name' => 'required|unique:departments,department_name,' . $decrypted_department_id . ',department_id|max:255|regex:/^[A-Za-z\s-_]+$/',
        ], [
            'department_name.required' => 'Department name required.',
            'department_name.regex' => 'Department name should be alphabetic.',
        ]);
        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction();
            try {
                $department->department_name = Input::get('department_name');
                $department->save();
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/department/view-department')->withSuccess($success_msg);
    }

    /**
     *  Change department's status
     *  @Bhuvanesh on 11 Dec 2018
     **/
    public function changeStatus(Request $request)
    {
        $id = $request->get('department_id');
        $status = $request->get('department_status');
        $department = Department::find($id);
        if ($department) {
            $department->status = $status;
            $department->save();
            echo "Success";
        } else {
            echo "Fail";
        }
    }

    /**
     *  Destroy department data
     *  @Bhuvanesh on 11 Dec 2018
     **/
    public function destroy($id)
    {
        $department_id = get_decrypted_value($id, true);
        $department = Department::find($department_id);
        if ($department) {
            DB::beginTransaction();
            try {
                $department->delete();
                $success_msg = "Department deleted successfully!";
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors("It's already in use. We can't delete this.");
            }
            return redirect('admin-panel/department/view-department')->withSuccess($success_msg);
        } else {
            $error_message = "Department not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
