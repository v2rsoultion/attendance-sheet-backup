<?php

namespace App\Http\Controllers\adminPanel;

use File;
use DateTime;
use DateInterval;
use Validator;
use Session;
use View;
use Form;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Model\Logs\Log;
use App\Model\Team\Team;
use App\Model\Team\TimeSlot;
use App\Model\Attendance\Attendance;
use App\Model\Attendance\AttendanceRegister;
use App\Model\Attendancedata\Attendancedata;
use App\Model\Attendancedata\AttendancedataLogs;
use App\Model\Company\CompanyConfig;
use App\Model\Company\Event;
use Yajra\Datatables\Datatables;
use Maatwebsite\Excel\Facades\Excel;
use App\Model\ReleasePlan\ReleasePlan;
use App\Model\ReleasePlan\ReleasePlanLog;
// use Symfony\Component\HttpFoundation\Request;

class AttendanceController extends Controller
{
    /**
     * Constructor for Attendance Controller 
     **/
    public function __construct()
    {
        $this->middleware('isadmin')->except('myAttendanceIndex', 'myAttendanceAnyData');
        $this->middleware('ismember')->only('myAttendanceIndex', 'myAttendanceAnyData');
    }

    /**
     *  View page for Attendance
     *  @Bhuvanesh on 15 Dec 2018
     **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title' => trans('language.view_attendance'),
            'redirect_url' => url('admin-panel/attendance/view-attendance'),
            'login_info' => $loginInfo
        );
        return view('admin-panel.attendance.index')->with($data);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Bhuvanesh on 15 Dec 2018
     **/
    public function anyData(Request $request)
    {
        $attendance = [];
        $searchKey = null;
        $attendance = Attendance::where(function ($query) use ($request) {
            if (!empty($request->get('attendance_month'))) {
                $query->where('attendance_month', 'LIKE', '%' . $request->get('attendance_month') . '%');
            }
        })
            ->orderBy('attendance_id', 'desc')
            ->get()->toArray();
        return Datatables::of($attendance)
            ->addColumn('download_attendance', function ($attendance) {
                $encrypted_attendance_id = get_encrypted_value($attendance['attendance_id'], true);
                return ' 
                    <a href="download-attendance/' . $encrypted_attendance_id . '" "><i class="fas fa-download"></i></a>';
            })
            ->addColumn('show_attendance', function ($attendance) {
                $encrypted_attendance_id = get_encrypted_value($attendance['attendance_id'], true);
                return ' 
                    <a href="show-attendance/' . $encrypted_attendance_id . '" "><div class="label label-table label-info">Show Attendance</div></a>';
            })->rawColumns(['download_attendance' => 'download_attendance', 'show_attendance' => 'show_attendance'])->addIndexColumn()->make(true);
    }

    /**
     *  Add page for Attendance
     *  @Bhuvanesh on 15 Dec 2018
     **/
    public function add(Request $request, $id = null)
    {
        $data = [];
        $attendance = [];
        $loginInfo = get_loggedin_user_data();
        $encrypted_attendance_id = '';
        if (!empty($id)) {
            $decrypted_attendance_id = get_decrypted_value($id, true);
            $attendance = Attendance::Find($decrypted_attendance_id);
            if (!$attendance) {
                return redirect('admin-panel/attendance/add-attendance')->withError('Attendance record not found!');
            }
            $page_title = trans('language.edit_attendance');
            $encrypted_attendance_id = get_encrypted_value($attendance->attendance_id, true);
            $save_url = url('admin-panel/attendance/save/' . $encrypted_attendance_id);
            $submit_button = 'Update';
        } else {
            $page_title = trans('language.add_attendance');
            $save_preview_url = url('admin-panel/attendance/preview');
            $submit_button = 'Upload';
        }
        $lastSession = Session::get('previewEmpAttendance');
        if ($lastSession != '') {
            $file_path = public_path() . '/uploads/' . $lastSession['file_name'];
            try {
                if (File::exists($file_path)) {
                    File::delete($file_path);
                }
            } catch (\Exception $e) { }
        }
        Session::put('previewEmpAttendance', '');
        $data = array(
            'page_title' => $page_title,
            'save_preview_url' => $save_preview_url,
            'submit_button' => $submit_button,
            'attendance' => $attendance,
            'encrypted_id' => $encrypted_attendance_id,
            'login_info' => $loginInfo,
            'redirect_url' => url('admin-panel/attendance/view-attendance'),
        );
        return view('admin-panel.attendance.add')->with($data);
    }

    /**
     *  Save attendance data
     *  @Bhuvanesh on 15 Dec 2018
     **/
    public function savePreview(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $filename = '';
        $monthYear = 'required|unique:attendances';
        $fileValid = 'required|file';
        $validatior = Validator::make($request->all(), [
            'attendance_month' => $monthYear,
            'attendance_file' => $fileValid
        ]);
        if ($validatior->fails()) {
            return response()->json(['error' => $validatior->errors()->all()]);
        } else {
            try {
                if (Input::file('attendance_file') != '') {
                    if ($request->hasFile('attendance_file')) {
                        $file = $request->File('attendance_file');
                        $destinationPath = public_path() . '/uploads/';
                        $ext = substr($file->getClientOriginalName(), -4);
                        $name = substr($file->getClientOriginalName(), 0, -4);
                        if (!isset(explode('-', explode('.', $name)[0])[2])) {
                            return response()->json(['error' => array('Invalid file name.')]);
                        }
                        $fileMonthName = explode('-', explode('.', $name)[0])[2];
                        $monthName = explode(' ', Input::get('attendance_month'))[0];
                        if (strcmp($fileMonthName, $monthName) !== 0) {
                            return response()->json(['error' => array('Selected month and attendance file month are different. Please upload correct file.')]);
                        }

                        $filename = time() . $name . $ext;
                        $file->move($destinationPath, $filename);
                        $final_name = $destinationPath . $filename;
                        chmod($final_name, 0777);

                        $file_info = array(
                            'path' => 'public/uploads/' . $filename,
                            'month_year' => Input::get('attendance_month'),
                        );
                        $data_emp = $this->save_attendance_data($file_info);
                        if (empty($data_emp)) {
                            return response()->json(['error' => array('Record not found.')]);
                        }
                        $session_info = array(
                            'data_emp' => $data_emp,
                            'file_name' => $filename,
                            'attendance_month' => Input::get('attendance_month'),
                        );
                        Session::put('previewEmpAttendance', $session_info);
                        $attd_data = Session::get('previewEmpAttendance');
                        $attd_data['save_url'] = url('admin-panel/attendance/save');
                        $attd_data['cancel_url'] = url('admin-panel/attendance/cancel');
                        $preview = View::make('admin-panel.attendance.excel-attendance-preview')->with($attd_data)->render();
                        return response()->json($preview);
                    }
                }
            } catch (\Exception $e) {
                $error_message = $e->getMessage();
                return response()->json(['error' => array($error_message)]);
            }
        }
    }

    /**
     *  Save attendance data
     *  @Bhuvanesh on 15 Dec 2018
     **/
    public function save(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $attendance = new Attendance;
        $success_msg = 'Attendance saved successfully!';
        DB::beginTransaction();
        try {
            $previewData = Session::get('previewEmpAttendance');
            if (!empty($previewData)) {
                $attendance->attendance_month = $previewData['attendance_month'];
                $attendance->attendance_file = $previewData['file_name'];
                $attendance->save();
                $leaves_after_less = $this->save_used_unsed_leaves($previewData['data_emp'], $previewData['attendance_month']);
                if (!empty($previewData['data_emp'])) {
                    foreach ($previewData['data_emp'] as $employee) {
                        $emp = new Attendancedata;

                        $emp->attendance_id = $attendance->attendance_id;
                        $emp->admin_id = $employee['admin_id'];
                        $emp->month_year = $attendance->attendance_month;
                        $emp->json_data = $employee['json_data'];
                        $emp->wfh_json_data = $employee['wfh_json_data'];
                        $emp->late_days = $employee['late_days'];
                        $emp->extra_working_hrs = $employee['extra_working_hrs'];
                        $emp->dhalf_leaves = $employee['dhalf_leaves'];
                        $emp->half_leaves = $employee['half_leaves'];
                        $emp->full_leaves = $employee['full_leaves'];
                        $emp->leaves_after_less = $leaves_after_less[$employee['admin_id']]['leaves_after_less'];
                        $emp->save();
                    }
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            $error_message = $e->getMessage();
            return $error_message;
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        return redirect('admin-panel/attendance/view-attendance')->withSuccess($success_msg);
    }

    /**
     *  Save attendance data
     *  @Bhuvanesh on 15 Dec 2018
     **/
    public function cancel(Request $request)
    {
        $lastSession = Session::get('previewEmpAttendance');
        if ($lastSession != '') {
            $file_path = public_path() . '/uploads/' . $lastSession['file_name'];
            try {
                if (File::exists($file_path)) {
                    File::delete($file_path);
                }
            } catch (\Exception $e) {
                return redirect('admin-panel/attendance/add-attendance')->withError("Data not Canceled.");
            }
            Session::put('previewEmpAttendance', '');
            return redirect('admin-panel/attendance/add-attendance');
        }
        Session::put('previewEmpAttendance', '');
        return redirect('admin-panel/attendance/add-attendance');
    }

    /**
     *  Store data in attendace data
     *  @Bhuvanesh on 15 Dec 2018
     **/
    public function save_attendance_data($file_info)
    {
        $WFHFlag = false;
        $data = Excel::load($file_info['path'], function ($reader) { })->get();
        $data = $data[0];
        $team_list = Team::select('admin_id', 'emp_id', 'admin_name', 'email', 'time_slot_id')->where('type', '=', '2')->with('getTimeSlot')->get()->toArray();
        // $team_list  =  array_column($team_list,NULL, 'email');
        $team_list = array_column($team_list, null, 'emp_id');
        // if(!empty($data) && $data->count()){
        //     $arr_email_key=[];
        //     foreach ($data as $key => $value) {
        //         if(isset($value['email']) && $value['email'] !='') {
        //             if(isset($team_list[$value['email']])){
        //                 $arr_email_key[$value['email']][] = $value;
        //                 $arr_email_key[$value['email']]['admin_id']  = $team_list[$value['email']]['admin_id'];
        //                 $arr_email_key[$value['email']]['email']     = $team_list[$value['email']]['email'];
        //                 $arr_email_key[$value['email']]['admin_name']      = $team_list[$value['email']]['admin_name'];
        //                 $arr_email_key[$value['email']]['time_slot'] = $team_list[$value['email']]['get_time_slot']['time_slot_time'];
        //             }
        //         }
        //     }
        //     $cal_info        =  array(
        //         'json'       => $arr_email_key,
        //         'month_year' =>  $file_info['month_year'],
        //     );
        //     return $this->returnAttendanceData($cal_info);
        // }
        if (!empty($data) && $data->count()) {
            $arr_email_key = [];
            foreach ($data as $key => $value) {
                if (isset($value['emp_id']) && $value['emp_id'] != '') {
                    if (isset($team_list[$value['emp_id']])) {
                        $arr_email_key[$value['emp_id']][] = $value;
                        $arr_email_key[$value['emp_id']]['admin_id'] = $team_list[$value['emp_id']]['admin_id'];
                        $arr_email_key[$value['emp_id']]['email'] = $team_list[$value['emp_id']]['email'];
                        $arr_email_key[$value['emp_id']]['admin_name'] = $team_list[$value['emp_id']]['admin_name'];
                        $arr_email_key[$value['emp_id']]['time_slot'] = $team_list[$value['emp_id']]['get_time_slot']['time_slot_time'];
                    }
                }
            }
            $cal_info = array(
                'json' => $arr_email_key,
                'month_year' => $file_info['month_year'],
            );
            return $this->returnAttendanceData($cal_info);
        }
    }

    /**
     *  return array for attendancedata save with (Full leaves, Half leaves, Extra Working Hr, Late Days)
     *  @Bhuvanesh on 15 Dec 2018
     **/
    public function returnAttendanceData($cal_info)
    {
        $monthdays = 0;
        $employees = [];
        if (!empty($cal_info)) {
            $monthdays = get_month_days($cal_info['month_year']);
            $temp = 0;
            foreach ($cal_info['json'] as $data) {
                $employee_json = $data[0];
                $sigleton = [];
                $wfh_hr = 0;  //// Hours for Work from home
                $wfh_minute = 0;
                $total_wfh_hr = '';
                $total_wfh_days = 0;
                $sunday_hr = 0; // Hours for sunday
                $sunday_i = 0;
                $saturday_hr = 0; // Hours for saturday
                $saturday_i = 0;
                $company_leave_hr = 0; // Hours for company leave
                $company_leave_i = 0;
                $total_late_hr = '';
                $total_late_days = 0;
                $total_sunday = 0;
                $total_saturday = 0;
                $total_working_day = 0;
                $total_company_leave = 0;
                $total_company_leave_extra_working_hr = '';
                $total_employee_leave = 0;
                $total_employee_half_day = 0;
                $total_employee_DHalf_day = 0;
                $total_employee_working_day = 0;
                $total_employee_na = 0;
                $total_sunday_extra_working_hr = '';
                $total_saturday_extra_working_hr = '';

                $totalInterval = new DateTime('00:00');
                $totalIntervalClone = clone $totalInterval;
                $totalLateIntervel = new DateTime('00:00');
                $totalLateIntervelClone = clone $totalLateIntervel;
                $total_wfh_Interval = new DateTime('00:00');
                $total_wfh_IntervalClone = clone $total_wfh_Interval;

                if (!empty($data[0])) {
                    for ($i = 1; $i <= $monthdays; $i++) {
                        if (strtolower($employee_json[$i]) == 'sunday') {
                            $total_sunday += 1;
                        } else if (strtolower($employee_json[$i]) == 'saturday') {
                            $total_saturday += 1;
                        } else if (strpos(strtolower($employee_json[$i]), 'sunday_') !== false) {
                            $decimalHr = explode("_", $employee_json[$i])[1];
                            $sunday_hr += explode(".", $decimalHr)[0];
                            $m = isset(explode(".", $decimalHr)[1]) ? ((explode(".", $decimalHr)[1]) * 60) : 0;
                            $sunday_i += substr($m, 0, 2);
                            $total_sunday += 1;
                        } else if (strpos(strtolower($employee_json[$i]), 'late_') !== false) {
                            $timeString = $employee_json[$i];
                            $time = explode("_", $timeString)[1];
                            $officeTime = new DateTime('09:30:00');
                            $lateTime = new DateTime($time . ':00');
                            $interval = date_diff($lateTime, $officeTime);
                            $totalLateIntervel->add($interval);
                            $total_late_days += 1;
                            $total_employee_working_day += 1;
                        } else if (strpos(strtolower($employee_json[$i]), 'saturday_') !== false) {
                            $decimalHr = explode("_", $employee_json[$i])[1];
                            $saturday_hr += explode(".", $decimalHr)[0];
                            $m = isset(explode(".", $decimalHr)[1]) ? ((explode(".", $decimalHr)[1]) * 60) : 0;
                            $saturday_i += substr($m, 0, 2);
                            $total_saturday += 1;
                        } else if (strtolower($employee_json[$i]) == 'half_day') {
                            $total_employee_half_day += 1;
                        } else if (strtolower($employee_json[$i]) == 'full_day') {
                            $total_employee_leave += 1;;
                        } else if ($employee_json[$i] == '') {
                            $total_employee_working_day += 1;
                        } else if (strtolower($employee_json[$i]) == 'na') {
                            $total_employee_na += 1;
                        } else if (strpos(strtolower($employee_json[$i]), 'dhalf_') !== false) {
                            $timeString = $employee_json[$i];
                            $time = explode("_", $timeString)[1];
                            $officeTime = new DateTime('09:30:00');
                            $lateTime = new DateTime($time . ':00');
                            $interval = date_diff($lateTime, $officeTime);
                            $totalInterval->add($interval);
                            $total_employee_DHalf_day += 1;
                        } else if (strpos(strtolower($employee_json[$i]), 'ch_') !== false) {
                            $decimalHr = isset(explode("_", $employee_json[$i])[2]) ? explode("_", $employee_json[$i])[2] : 0;
                            $company_leave_hr += explode(".", $decimalHr)[0];
                            $m = isset(explode(".", $decimalHr)[1]) ? ((explode(".", $decimalHr)[1]) * 60) : 0;
                            $company_leave_i += substr($m, 0, 2);
                            $total_company_leave += 1;
                        }
                    }
                }
                if (!empty($data[1]) != '') {
                    $employee_wfh_json = json_decode($data[1], true);
                    for ($j = 1; $j <= $monthdays; $j++) {
                        if (strpos(strtolower($employee_wfh_json[$j]), 'hr') !== false) {
                            $wfh_hrString = explode(" ", $employee_wfh_json[$j])[0];
                            $wfh_hr += explode(":", $wfh_hrString)[0];
                            $wfh_minute += isset(explode(":", $wfh_hrString)[1]) ? explode(":", $wfh_hrString)[1] : 0;
                            if ($wfh_minute > 60) {
                                $wfh_hr += 1;
                                $wfh_minute = 0;
                            }
                            $total_wfh_days += 1;
                        }
                    }
                    $total_wfh_hr = $wfh_hr . ':' . $wfh_minute;
                }
                $total_extra_working_hr = $sunday_hr + $saturday_hr + $company_leave_hr + $wfh_hr;
                $total_extra_working_i = $sunday_i + $saturday_i + $company_leave_i + $wfh_minute;
                $total_extra_working_time = $total_extra_working_hr . ':' . $total_extra_working_i;
                $total_sunday_extra_working_hr = $sunday_hr . ':' . $sunday_i;
                $total_saturday_extra_working_hr = $saturday_hr . ':' . $saturday_i;
                $total_company_leave_extra_working_hr = $company_leave_hr . ':' . $company_leave_i;
                $total_working_day = $monthdays - ($total_sunday + $total_saturday + $total_company_leave);
                $total_employee_working_day = $total_employee_working_day + (($total_employee_half_day / 2) + ($total_employee_DHalf_day / 2));
                $sigleton = array(
                    // 'total_working_day' => $total_working_day, 
                    // 'total_sunday' => $total_sunday,
                    // 'total_sunday_extra_working_hr' => $total_sunday_extra_working_hr,
                    // 'total_saturday' => $total_saturday,
                    // 'total_saturday_extra_working_hr' => $total_saturday_extra_working_hr,
                    // 'total_employee_half_day' => $total_employee_half_day + $total_employee_DHalf_day, 
                    // 'total_employee_leave' => $total_employee_leave,
                    // 'attendance_id' => $data['attendance_id'],
                    'admin_id' => $data['admin_id'],
                    'admin_name' => $data['admin_name'],
                    'email' => $data['email'],
                    'json_data' => json_encode($data[0], true),
                    'wfh_json_data' => isset($data[1]) ? json_encode($data[1], true) : '',
                    'full_leaves' => $total_employee_leave,
                    'half_leaves' => $total_employee_half_day,
                    'dhalf_leaves' => $total_employee_DHalf_day,
                    // 'total_employee_working_day' => $total_employee_working_day,
                    // 'total_late_days' => $total_late_days,
                    // 'total_late_hr' => date_diff($totalLateIntervelClone, $totalLateIntervel),
                    // 'total_late_hr' => date_diff($totalLateIntervelClone, $totalLateIntervel)->h.' hr '.date_diff($totalLateIntervelClone, $totalLateIntervel)->i.' min',
                    // 'total_company_leave' => $total_company_leave,
                    // 'total_company_leave_extra_working_hr' => $total_company_leave_extra_working_hr,
                    // 'total_employee_DHalf_day' => $total_employee_DHalf_day,
                    // 'total_employee_DHalf_hr' => date_diff($totalIntervalClone, $totalInterval),
                    // 'total_wfh_days'=>$total_wfh_days,
                    // 'total_wfh_hr'=>$total_wfh_hr,
                    // 'total_extra_working_time' => $total_extra_working_hr.' hr '.$total_extra_working_i.' min', 
                    'extra_working_hrs' => $total_extra_working_hr + round($total_extra_working_i / 60, 2),
                    'late_days' => $total_late_days,
                );
                $employees[] = $sigleton;
                $sigleton = [];
            }
        }
        return $employees;
    }

    /**
     *  Save Utilized and Unutilized leaves
     *  @Bhuvanesh on 14 Jan 2019
     **/
    public function save_used_unsed_leaves($employee_data, $month_year)
    {
        $month = get_month_short_name(explode(' ', $month_year)[0]);
        $year = explode(' ', $month_year)[1];
        $company_config = CompanyConfig::get()->first();
        if (!empty($employee_data)) {
            foreach ($employee_data as $value) {
                $releasePlan = ReleasePlan::where(function ($query) use ($year, $value) {
                    $query->where(['admin_id' => $value['admin_id'], 'year' => $year])->orderBy('year', 'desc');
                })->get()->first();
                if (!empty($releasePlan)) {
                    $releasePlanObject = ReleasePlan::find($releasePlan['release_plan_id']);
                    if ($month == 'jan') {
                        $releasePlanLogObject = new ReleasePlanLog;
                        $releasePlanLogObject->admin_id = $value['admin_id'];
                        $releasePlanLogObject->month_year = $month_year;
                        $releasePlanLogObject->utilized = 0;
                        $releasePlanLogObject->unutilized = $releasePlan->$month;
                        $releasePlanLogObject->save();
                    } else {
                        $previousMonthYear = get_previous_month($month) . ' ' . $year;
                        $releasePlanLogObject = ReleasePlanLog::where('month_year', '=', $previousMonthYear)->first();
                        $releasePlanLogObject->utilized = $releasePlanObject->utilized;
                        $releasePlanLogObject->unutilized = $releasePlanObject->unutilized;
                        $releasePlanLogObject->save();

                        $newReleasePlanLogObject = new ReleasePlanLog;
                        $newReleasePlanLogObject->admin_id = $value['admin_id'];
                        $newReleasePlanLogObject->month_year = $month_year;
                        $newReleasePlanLogObject->utilized = $releasePlanObject->utilized;
                        $newReleasePlanLogObject->unutilized = $releasePlanObject->unutilized + $releasePlan->$month;
                        $newReleasePlanLogObject->save();
                    }

                    $releaseLeaves['used'] = $releasePlanObject->utilized;
                    $releaseLeaves['unused'] = $releasePlanObject->unutilized;
                    $lateDays = floor($value['late_days'] / $company_config['late_half_days']) * 0.5;
                    $halfDays = (integer)$value['half_leaves'] * 0.5;
                    $dhalfDays = (integer)$value['dhalf_leaves'] * 0.5;
                    $totalLeaves = (integer)$value['full_leaves'] + $halfDays + $lateDays + $dhalfDays;

                    if ($totalLeaves > 0) {
                        $diff = 0;
                        if ($releasePlan->$month != 0) {
                            if ($totalLeaves > $releasePlan->$month) {
                                $diff = $totalLeaves - $releasePlan->$month;
                                $totalLeaves -= $releasePlan->$month;
                                $releaseLeaves['used'] += $releasePlan->$month;
                            } else {
                                $diff = $releasePlan->$month - $totalLeaves;
                                $releaseLeaves['used'] += $totalLeaves;
                                $releaseLeaves['unused'] += $diff;
                                $totalLeaves = 0;
                            }
                        }
                        if ($totalLeaves > 0 && $releaseLeaves['unused'] > 0) {
                            if ($totalLeaves > $releaseLeaves['unused']) {
                                $totalLeaves -= $releaseLeaves['unused'];
                                $releaseLeaves['used'] += $releaseLeaves['unused'];
                            } else {
                                $releaseLeaves['unused'] -= $totalLeaves;
                                $releaseLeaves['used'] += $totalLeaves;
                            }
                        }
                    } else {
                        $releaseLeaves['unused'] += $releasePlan->$month;
                    }
                    $releasePlanObject->utilized = $releaseLeaves['used'];
                    $releasePlanObject->unutilized = $releaseLeaves['unused'];
                    $releasePlanObject->save();
                } else {
                    $releasePlanLogObject = new ReleasePlanLog;
                    $releasePlanLogObject->admin_id = $value['admin_id'];
                    $releasePlanLogObject->month_year = $month_year;
                    $releasePlanLogObject->utilized = 0;
                    $releasePlanLogObject->unutilized = 0;
                    $releasePlanLogObject->save();
                    $lateDays = floor($value['late_days'] / $company_config['late_half_days']) * 0.5;
                    $halfDays = (integer)$value['half_leaves'] * 0.5;
                    $dhalfDays = (integer)$value['dhalf_leaves'] * 0.5;
                    $totalLeaves = (integer)$value['full_leaves'] + $halfDays + $lateDays + $dhalfDays;
                }
                $newArr[$value['admin_id']]['leaves_after_less'] = $totalLeaves;
            }
        }
        return $newArr;
    }

    /**
     *  Destroy attendance data
     *  @Bhuvanesh on 15 Dec 2018
     **/
    public function destroy($id)
    {
        $attendance_id = get_decrypted_value($id, true);
        $attendance = Attendance::find($attendance_id);
        if ($attendance) {
            DB::beginTransaction();
            try {
                $file_path = public_path() . '/uploads/' . $attendance->attendance_file;
                if (File::exists($file_path)) {
                    File::delete($file_path);
                }
                $release_plan_log = ReleasePlanLog::where('month_year', '=', $attendance->attendance_month);
                $release_plan_log->delete();
                $attendance->delete();
                DB::commit();
                $success_msg = "Attendance record deleted successfully!";
                return redirect('admin-panel/attendance/view-attendance')->withSuccess($success_msg);
            } catch (\Exception $e) {
                $error_message = "Attendance not deleted!";
                return redirect()->back()->withErrors($error_message);
            }
        } else {
            $error_message = "Attendance not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     * Download Attendance file
     * @Bhuvanesh on 19 Dec 2018
     **/
    public function downloadfile($id)
    {
        $attendance_id = get_decrypted_value($id, true);
        $attendance = Attendance::find($attendance_id);
        if ($attendance) {
            return response()->download(public_path() . '/uploads/' . $attendance->attendance_file);
        } else {
            $error_message = "Attendance not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  View page for Attendance
     *  @Bhuvanesh on 15 Dec 2018
     **/

    public function showAttendance($id)
    {
        $loginInfo = get_loggedin_user_data();
        $decrypted_attendance_id = get_decrypted_value($id, true);
        $company_config_details = get_month_details($decrypted_attendance_id);
        $data = array(
            'page_title' => trans('language.show_attendance'),
            'redirect_url' => url('admin-panel/attendance/show-attendance'),
            'login_info' => $loginInfo,
            'company_config' => $company_config_details,
            'attendance_id' => $id,
        );
        return view('admin-panel.attendance.show-attendance')->with($data);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Bhuvanesh on 15 Dec 2018
     **/
    public function showAttendanceAnyData(Request $request)
    {
        $data = [];
        if ($request->get('base_sal') != '') {
            $data = array();
            foreach (explode('&', $request->get('base_sal')) as $value) {
                $value1 = explode('=', $value);
                $data[$value1[0]] = $value1[1];
            }
        }
        $decrypted_attendance_id = get_decrypted_value($request->get('attendance_id'), true);
        $company_config_details = get_month_details($decrypted_attendance_id);
        $company_config = CompanyConfig::get()->first();
        $getYear = Attendance::where('attendance_id', '=', $decrypted_attendance_id)->get()->first();

        $newArr = Team::where(['type' => '2', 'status' => '1', 'is_terminate' => '0', 'is_resign' => '0'])
            ->with(['getAttendanceData' => function ($q) use ($decrypted_attendance_id) {
                $q->where('attendance_id', '=', $decrypted_attendance_id);
            }])
            ->with(['getReleasePlan' => function ($q) use ($getYear) {
                $q->where('year', '=', explode(' ', $getYear->attendance_month)[1]);
            }])
            ->get()
            ->toArray();
        $fullEmployeeRecord = [];
        for ($i = 0; $i < count($newArr); $i++) {
            if (!empty($newArr[$i]['get_attendance_data'])) {
                array_push($fullEmployeeRecord, $newArr[$i]);
            }
        }
        return Datatables::of($fullEmployeeRecord)
            ->addColumn('emp_id', function ($fullEmployeeRecord) {
                return $fullEmployeeRecord['emp_id'];
            })
            ->addColumn('month_days', function ($fullEmployeeRecord) use ($company_config_details) {
                return $company_config_details['calendar_days'];
            })
            ->addColumn('base_salary', function ($fullEmployeeRecord) use ($data) {
                if (isset($data['base_' . $fullEmployeeRecord['admin_id']])) {
                    return '<input type="number" class="form-control base-sal" value="' . $data['base_' . $fullEmployeeRecord['admin_id']] . '" id="base_' . $fullEmployeeRecord['admin_id'] . '" name="base_' . $fullEmployeeRecord['admin_id'] . '"/><span id="base_' . $fullEmployeeRecord['admin_id'] . '" style="display:none;">' . $data['base_' . $fullEmployeeRecord['admin_id']] . '</span>';
                } else {
                    return '<input type="number" id="base_' . $fullEmployeeRecord['admin_id'] . '" name="base_' . $fullEmployeeRecord['admin_id'] . '" class="form-control base-sal"/>';
                }
            })
            ->addColumn('calculate_salary', function ($fullEmployeeRecord) use ($data, $company_config_details, $company_config) {
                $total_paid_days = get_total_paid_days($fullEmployeeRecord, $company_config);
                $emp_working_days = $company_config_details['company_working_days'] - $fullEmployeeRecord['saturday_off'];
                if (isset($data['base_' . $fullEmployeeRecord['admin_id']]) && $data['base_' . $fullEmployeeRecord['admin_id']] != '') {
                    $per_day_sal = $data['base_' . $fullEmployeeRecord['admin_id']] / $emp_working_days;
                    $paid_sal = (float)$per_day_sal * $total_paid_days;
                    return '<span id="cal_' . $fullEmployeeRecord['admin_id'] . '">' . round($paid_sal) . '</span>';
                } else {
                    return '<span id="cal_' . $fullEmployeeRecord['admin_id'] . '"></span>';
                }
            })
            ->addColumn('emp_working_days', function ($fullEmployeeRecord) use ($company_config_details) {
                return $company_config_details['company_working_days'] - $fullEmployeeRecord['saturday_off'];
            })
            ->addColumn('name', function ($fullEmployeeRecord) {
                return $fullEmployeeRecord['admin_name'];
            })
            ->addColumn('saturday_off', function ($fullEmployeeRecord) {
                return $fullEmployeeRecord['saturday_off'];
            })
            ->addColumn('full_leaves', function ($fullEmployeeRecord) {
                return isset($fullEmployeeRecord['get_attendance_data'][0]) ? $fullEmployeeRecord['get_attendance_data'][0]['full_leaves'] : '';
            })
            ->addColumn('half_leaves', function ($fullEmployeeRecord) {
                return isset($fullEmployeeRecord['get_attendance_data'][0]) ? $fullEmployeeRecord['get_attendance_data'][0]['half_leaves'] : '';
            })
            ->addColumn('dhalf_leaves', function ($fullEmployeeRecord) {
                return isset($fullEmployeeRecord['get_attendance_data'][0]) ? $fullEmployeeRecord['get_attendance_data'][0]['dhalf_leaves'] : '';
            })
            ->addColumn('late_days', function ($fullEmployeeRecord) {
                return isset($fullEmployeeRecord['get_attendance_data'][0]) ? $fullEmployeeRecord['get_attendance_data'][0]['late_days'] : '';
            })
            ->addColumn('extra_working_hrs', function ($fullEmployeeRecord) {
                return isset($fullEmployeeRecord['get_attendance_data'][0]) ? $fullEmployeeRecord['get_attendance_data'][0]['extra_working_hrs'] : '';
            })
            ->addColumn('extra_working_hrs_paid_days', function ($fullEmployeeRecord) use ($company_config) {
                if (isset($fullEmployeeRecord['get_attendance_data'][0])) {
                    return number_format(($fullEmployeeRecord['get_attendance_data'][0]['extra_working_hrs'] / $company_config['office_hours']), 2, '.', '');
                } else {
                    return '';
                }
            })
            ->addColumn('leaves_after_less', function ($fullEmployeeRecord) {
                return isset($fullEmployeeRecord['get_attendance_data'][0]) ? $fullEmployeeRecord['get_attendance_data'][0]['leaves_after_less'] : '';
            })
            ->addColumn('utilized', function ($fullEmployeeRecord) {
                return isset($fullEmployeeRecord['get_release_plan'][0]) ? $fullEmployeeRecord['get_release_plan'][0]['utilized'] : '';
            })
            ->addColumn('unutilized', function ($fullEmployeeRecord) {
                return isset($fullEmployeeRecord['get_release_plan'][0]) ? $fullEmployeeRecord['get_release_plan'][0]['unutilized'] : '';
            })
            ->addColumn('total_paid_days', function ($fullEmployeeRecord) use ($company_config) {
                $total_paid_days = get_total_paid_days($fullEmployeeRecord, $company_config);
                return $total_paid_days;
            })
            ->addColumn('attendance_data_id', function ($fullEmployeeRecord) use ($company_config) {
                return isset($fullEmployeeRecord['get_attendance_data'][0]) ? $fullEmployeeRecord['get_attendance_data'][0]['attendancedata_id'] : '';
            })
            ->addIndexColumn(['month_days' => 'month_days', 'base_salary' => 'base_salary', 'calculate_salary' => 'calculate_salary', 'dhalf_leaves' => 'dhalf_leaves', 'emp_working_days' => 'emp_working_days', 'emp_id' => 'emp_id', 'name' => 'name', 'saturday_off' => 'saturday_off', 'full_leaves' => 'full_leaves', 'half_leaves' => 'half_leaves', 'late_days' => 'late_days', 'extra_working_hrs' => 'extra_working_hrs', 'extra_working_hrs_paid_days' => 'extra_working_hrs_paid_days', 'leaves_after_less' => 'leaves_after_less', 'utilized' => 'utilized', 'unutilized' => 'unutilized', 'total_paid_days' => 'total_paid_days', 'attendance_data_id' => 'attendance_data_id'])
            ->rawColumns(['base_salary', 'calculate_salary'])
            ->make(true);
    }

    /**
     * Update Attendance Data in Show Attendance
     * @Bhuvanesh on 15 Jan 2018
     */
    public function updateAttendanceData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $updateDetail = $request->all();
        if (!empty($updateDetail)) {
            DB::beginTransaction();
            try {
                $attendance_data = Attendancedata::where(['admin_id' => $updateDetail['emp_id'], 'attendancedata_id' => $updateDetail['attendance_data_id']])->first();
                $releasePlanLog = ReleasePlanLog::where(['admin_id' => $updateDetail['emp_id'], 'month_year' => $attendance_data->month_year])->first();
                $month = explode(' ', $attendance_data->month_year)[0];
                $year = explode(' ', $attendance_data->month_year)[1];
                $releasePlan = ReleasePlan::where(['admin_id' => $updateDetail['emp_id'], 'year' => $year])->first();
                $company_config = CompanyConfig::get()->first();

                //p($attendance_data, 0);
                $releaseLeaves['used'] = $releasePlanLog->utilized;
                $releaseLeaves['unused'] = $releasePlanLog->unutilized;

                $lateDays = floor($updateDetail['LD'] / $company_config['late_half_days']) * 0.5;
                $halfDays = (integer)$updateDetail['half_leaves'] * 0.5;
                $dhalfDays = (integer)$updateDetail['dhalf_leaves'] * 0.5;
                $totalLeaves = (integer)$updateDetail['full_leaves'] + $halfDays + $lateDays + $dhalfDays;

                // p($lateDays, 0);
                // p($halfDays, 0);
                // p($dhalfDays, 0);
                // p($totalLeaves, 0);

                if ($totalLeaves > 0) {
                    if ($totalLeaves > 0 && $releaseLeaves['unused'] > 0) {
                        if ($totalLeaves >= $releaseLeaves['unused']) {
                            $totalLeaves -= $releaseLeaves['unused'];
                            $releaseLeaves['used'] += $releaseLeaves['unused'];
                            $releaseLeaves['unused'] = 0;
                        } else {
                            $releaseLeaves['unused'] -= $totalLeaves;
                            $releaseLeaves['used'] += $totalLeaves;
                            $totalLeaves = 0;
                        }
                    }
                } else {
                    if (!empty($releasePlan)) {
                        $releaseLeaves['unused'] += $releasePlan->$month;
                    }
                }

                $description = 'Changes Fields are:';
                if ($attendance_data->full_leaves != $updateDetail['full_leaves']) {
                    $description .= '<br>Full Days from <b>' . $attendance_data->full_leaves . '</b> to <b>' . $updateDetail['full_leaves'] . '</b>';
                }
                if ($attendance_data->half_leaves != $updateDetail['half_leaves']) {
                    $description .= '<br>Half Days from <b>' . $attendance_data->half_leaves . '</b> to <b>' . $updateDetail['half_leaves'] . '</b>';
                }
                if ($attendance_data->dhalf_leaves != $updateDetail['dhalf_leaves']) {
                    $description .= '<br>DHalf Days from <b>' . $attendance_data->dhalf_leaves . '</b> to <b>' . $updateDetail['dhalf_leaves'] . '</b>';
                }
                if ($attendance_data->late_days != $updateDetail['LD']) {
                    $description .= '<br>Late Days from <b>' . $attendance_data->late_days . '</b> to <b>' . $updateDetail['LD'] . '</b>';
                }
                if ($attendance_data->extra_working_hrs != $updateDetail['EWD']) {
                    $description .= '<br>Extra Working Hrs from <b>' . $attendance_data->extra_working_hrs . '</b> to <b>' . $updateDetail['EWD'] . '</b>';
                }
                // p($totalLeaves);
                // Update log for changes
                $attendance_data_log = new AttendancedataLogs();
                $attendance_data_log->admin_id = $loginInfo['admin_id'];
                $attendance_data_log->employee_id = $updateDetail['emp_id'];
                $attendance_data_log->attendancedata_id = $attendance_data->attendancedata_id;
                $attendance_data_log->month_year = $attendance_data->month_year;
                $attendance_data_log->description = $description;
                $attendance_data_log->save();

                // Update data in attendance data
                $attendance_data->full_leaves = $updateDetail['full_leaves'];
                $attendance_data->half_leaves = $updateDetail['half_leaves'];
                $attendance_data->dhalf_leaves = $updateDetail['dhalf_leaves'];
                $attendance_data->late_Days = $updateDetail['LD'];
                $attendance_data->extra_working_hrs = $updateDetail['EWD'];
                $attendance_data->leaves_after_less = $totalLeaves;
                $attendance_data->save();

                if (!empty($releasePlan)) {
                    $releasePlan->utilized = $releaseLeaves['used'];
                    $releasePlan->unutilized = $releaseLeaves['unused'];
                    $releasePlan->save();
                }
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return ['status' => 1, 'message' => $error_message];
            }
            DB::commit();
        }
        return ['status' => 0, 'message' => 'Attendance Successfully Updated.'];
    }

    /**
     *  View page of My attendance
     *  @Bhuvanesh on 02 Jan 2019
     **/
    public function myAttendanceIndex()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title' => trans('language.view_attendance'),
            'redirect_url' => url('admin-panel/attendance/view-attendance'),
            'login_info' => $loginInfo
        );
        return view('admin-panel.attendance.show-myattendance')->with($data);
    }

    /**
     *  Get Data for view page of My attendance(Datatables)
     *  @Bhuvanesh on 02 Jan 2019
     **/
    public function myAttendanceAnyData(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $month_year = date('F Y', strtotime($request->get('date')));
        $year = explode(' ', $month_year)[1];
        $company_config = CompanyConfig::first();
        $emp_full_details = Team::where('admin_id', '=', $loginInfo['admin_id'])->with(['getAttendanceData' => function ($query) use ($month_year) {
            $query->where('month_year', '=', $month_year);
        }])
            ->with(['getReleasePlan' => function ($q) use ($year) {
                $q->where('year', '=', $year);
            }])->first();
        $emp_data = [];
        $isSaturdayOff = false;
        // If Saturday off then true otherwise false
        if ($emp_full_details['saturday_off'] > 0) {
            $isSaturdayOff = true;
        } else {
            $isSaturdayOff = false;
        }
        if (!$emp_full_details['getAttendanceData']->isEmpty()) {
            $total_paid_days = get_total_paid_days_for_employee($emp_full_details, $company_config);
            $attendance_data = $emp_full_details['getAttendanceData'][0];
            if (!$emp_full_details['getReleasePlan']->isEmpty()) {
                $release_plan = $emp_full_details['getReleasePlan'][0];
            }
            $attendance_data_log = AttendancedataLogs::where(['attendancedata_id' => $attendance_data['attendancedata_id'], 'employee_id' => $loginInfo['admin_id'], 'month_year' => $attendance_data['month_year']])->get()->toArray();
            $content = '';
            $content = 'There may be inequality in attendance and calendar data because admin updated attendance.';
            // if(!empty($attendance_data_log)){
            //     $content = 'Calendar record may not be same as calculation because admin changed in attendance record.';
            //     // foreach ($attendance_data_log as $value) {
            //     //     $date = date('d-m-Y', strtotime($value['created_at']));
            //     //     $content .= '<br>Date: '. $date .'<br>'.$value['description'].'<br><br>';
            //     // }
            // }
            $month_details = get_month_details($attendance_data['attendance_id']);
            $emp_data['month'] = $month_details['month'];
            $emp_data['calendar_days'] = $month_details['calendar_days'];
            $emp_data['sundays'] = $month_details['sundays'];
            $emp_data['holidays'] = $month_details['holidays'];
            $emp_data['company_working_days'] = $month_details['company_working_days'];
            $emp_data['saturdays'] = (float)$emp_full_details['saturday_off'];
            $emp_data['employee_working_days'] = $month_details['company_working_days'] - (float)$emp_full_details['saturday_off'];
            $emp_data['full_leaves'] = $attendance_data['full_leaves'];
            $emp_data['half_leaves'] = $attendance_data['half_leaves'];
            $emp_data['dhalf_leaves'] = $attendance_data['dhalf_leaves'];
            $emp_data['late_days'] = $attendance_data['late_days'];
            $emp_data['leaves_after_less'] = $attendance_data['leaves_after_less'];
            $emp_data['extra_working_hrs'] = $attendance_data['extra_working_hrs'];
            $emp_data['total_paid_days'] = $total_paid_days;

            if (!$emp_full_details['getReleasePlan']->isEmpty()) {
                $emp_data['utilized'] = $release_plan['utilized'];
                $emp_data['unutilized'] = $release_plan['unutilized'];
            }
            $emp_data['log_description'] = $content;
        }
        return array('emp_data' => $emp_data, 'isSaturdayOff' => $isSaturdayOff);
    }

    /**
     * View In - Out for Employee
     * @Bhuvanesh on 13 Feb 2019
     */
    public function viewInOut(Request $request, $condition = null)
    {
        $loginInfo = get_loggedin_user_data();
        $employee_list = [];
        $attendance_key = \Config::get('custom.attendance_key');
        $attendance_key = add_blank_option($attendance_key, 'Select Attendance Key');
        $employee_arr = Team::select('admin_id', 'admin_name')->where(['type' => '2', 'status' => '1', 'is_terminate' => '0', 'is_resign' => '0'])->get()->toArray();
        if (!empty($employee_arr)) {
            foreach ($employee_arr as $key => $value) {
                $employee_list[$value['admin_id']] = $value['admin_name'];
            }
        }
        $employee_list = add_blank_option($employee_list, 'Select Employee');
        $pendingCondition = '';
        if ($condition != null) {
            $pendingCondition = 'na';
        }
        $data = array(
            'page_title' => trans('language.view_in_out'),
            'employee_list' => $employee_list,
            'attendance_key' => $attendance_key,
            'pending_condition' => $pendingCondition,
            'login_info' => $loginInfo,
        );
        return view('admin-panel.attendance.in-out')->with($data);
    }

    /**
     * View In - Out for Employee
     * @Bhuvanesh on 13 Feb 2019
     */
    public function inOutData(Request $request)
    {
        $in_out = [];
        $loginInfo = get_loggedin_user_data();
        $in_out = AttendanceRegister::where(function ($query) use ($request) {
            if (isset($request) && $request->get('dates') != '') {
                $dates = explode('-', $request->get('dates'));
                $start_date = date_create($dates[0])->format('Y-m-d');
                $end_date = date_create($dates[1])->format('Y-m-d');
                if (isset($request) && $request->get('isRequest') != '1') {
                    $query->whereBetween('attendance_date', [$start_date, $end_date]);
                }
            }
            if (isset($request) && $request->get('emp') != '') {
                $query->where('admin_id', $request->get('emp'));
            }
            if (isset($request) && $request->get('isRequest') == '1') {

                $query->where('isRequest_status', '1');
            }
        })
            ->with('getEmployeeData')
            ->orderBy('attendance_date', 'desc')
            ->get()->toArray();
        return Datatables::of($in_out)
            ->addColumn('emp_name', function ($in_out) {
                return $in_out['get_employee_data']['admin_name'];
            })
            ->addColumn('in_out_date', function ($in_out) {
                return date_create($in_out['attendance_date'])->format('d-m-Y');
            })
            ->addColumn('in_out_intime', function ($in_out) {
                if ($in_out['attendance_intime'] != '') {
                    return date_create($in_out['attendance_intime'])->format('h:i A');
                } else {
                    return '--';
                }
            })
            ->addColumn('in_out_outtime', function ($in_out) {
                if ($in_out['attendance_outtime']) {
                    return date_create($in_out['attendance_outtime'])->format('h:i A');
                } else {
                    return '-:- -';
                }
            })
            ->addColumn('in_out_ip', function ($in_out) {
                $ip_log = Log::where(['admin_id' => $in_out['admin_id'], 'work_type' => '2'])->whereDate('created_at', $in_out['attendance_date'])->first();
                if (!empty($ip_log)) {
                    return $ip_log['ip_address'];
                } else {
                    return '--:--:--:--';
                }
            })
            ->addColumn('in_out_key', function ($in_out) {
                if (strpos($in_out['attendance_key'], 'full_day') !== false) {
                    return 'Absent';
                } else if (strpos($in_out['attendance_key'], 'half_day') !== false) {
                    return 'Half Day';
                } else if (strpos($in_out['attendance_key'], 'CH') !== false) {
                    return 'Company Holiday';
                } else if (strpos($in_out['attendance_key'], 'late') !== false) {
                    return 'Late';
                } else if (strpos($in_out['attendance_key'], 'dhalf') !== false) {
                    return 'Direct Half';
                }
                // else if (strpos($in_out['attendance_key'], 'na') !== false) {
                //     return 'Request';
                // } 
                else if (strpos($in_out['attendance_key'], 'saturday') !== false) {
                    return 'Saturday Extra Work';
                } else if (strpos($in_out['attendance_key'], 'sunday') !== false) {
                    return 'Sunday Extra Work';
                } else {
                    return 'Present';
                }
            })
            ->addColumn('in_out_total', function ($in_out) {
                $total_time = $in_out['attendance_total_time'];
                $time = intdiv($total_time, 60) . ' hrs ' . ($total_time % 60) . ' minutes';
                return $time;
            })
            ->addColumn('edit', function ($in_out) {
                $encrypted_register_id = get_encrypted_value($in_out['attendance_register_id'], true);
                $encrypted_admin_id = get_encrypted_value($in_out['admin_id'], true);
                return '<a href="#" data-toggle="tooltip" role="button" admin-id="' . $encrypted_admin_id . '" rel="' . $encrypted_register_id . '" title="Edit Attendance" class="editInOut"><i class="fas fa-pencil-alt"></i></a>';
            })
            ->addColumn('request', function ($in_out) {
                if ($in_out['attendance_isRequest'] == 1) {
                    if ($in_out['isRequest_status'] == 1) {
                        $encrypted_register_id = get_encrypted_value($in_out['attendance_register_id'], true);
                        return Form::select('changeRequest', array('' => 'Select Option', 'leave' => 'Leave', 'half_day' => 'Half Day', 'full_day' => 'Full Day'), '', ['class' => 'changeRequest form-control', 'condition-for' => $encrypted_register_id]);
                    } else {
                        $key = $in_out['attendance_key'];
                        $val = '';
                        if (strpos($key, 'full_day') !== false) {
                            $val = 'leave';
                        } else if (strpos($key, 'half_day') !== false) {
                            $val = 'half_day';
                        } else {
                            $val = 'full_day';
                        }
                        $encrypted_register_id = get_encrypted_value($in_out['attendance_register_id'], true);
                        return Form::select('changeRequest', array('' => 'Select Option', 'leave' => 'Leave', 'half_day' => 'Half Day', 'full_day' => 'Full Day'), $val, ['class' => 'changeRequest form-control', 'condition-for' => $encrypted_register_id]);
                    }
                }
            })
            ->addColumn('action', function ($in_out) {
                // p($in_out, 0);
                $register_id = get_encrypted_value($in_out['attendance_register_id'], true);
                $admin_id = get_encrypted_value($in_out['admin_id'], true);
                return '<a role="button" class="editInOut" rel="' . $register_id . '" admin-id="' . $admin_id . '" ><i class="fas fa-pencil-alt"></i></a>';
            })
            ->addColumn('in_out_desc', function ($in_out) {
                return $in_out['attendance_desc'];
            })
            ->rawColumns(['in_out_total' => 'in_out_total', 'in_out_ip' => 'in_out_ip', 'request' => 'request', 'edit' => 'edit', 'action' => 'action', 'in_out_desc' => 'in_out_desc', 'in_out_date' => 'in_out_date'])->addIndexColumn()->make(true);
    }

    /**
     * Update Employee Request for Half Day,  Full Day or leave
     * @Bhuvanesh on 13 Feb 2019
     */
    public function changeRequest(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $isRequest = false;
        if (isset($request) && $request->get('attendance-id') != '') {
            $decrypted_attendance_register_id = get_decrypted_value($request->get('attendance-id'), true);
            $attendance_register = AttendanceRegister::find($decrypted_attendance_register_id);
            if (!empty($attendance_register)) {
                $condition = $request->get('condition');
                if ($condition == 'leave') {
                    $key = 'full_day';
                } else if ($condition == 'half_day') {
                    $key = 'half_day';
                } else if ($condition == 'full_day') {
                    $key = $attendance_register->attendance_key;
                }
                // else if ($condition == 'na') {
                //     $isRequest = true;
                //     $key = 'na';
                // } 
                else {
                    return array('status' => 'error', 'message' => 'Condition Not Found.');
                }
                DB::beginTransaction();
                try {
                    $attendance_register->attendance_key = $key;
                    $attendance_register->isRequest_status = 0;
                    $attendance_register->save();
                } catch (\Exception $e) {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return array('status' => 'error', 'message' => $error_message);
                }
                DB::commit();
                return array('status' => 'success', 'message' => 'Request Updated Successfully');
            } else {
                return array('status' => 'error', 'message' => 'Record Not Found.');
            }
        } else {
            return array('status' => 'error', 'message' => 'Invalid Parameter');
        }
    }

    /**
     * Get Employee Attendance register in - out time
     * @Bhuvanesh on 13 Mar 2019
     */
    public function getAttendanceRegister(Request $request)
    {
        if (isset($request) && $request->has('admin_id') && $request->has('attendance_register_id')) {
            $decrypted_attendance_register_id = get_decrypted_value($request->get('attendance_register_id'), true);
            $attendance_register  = AttendanceRegister::where('attendance_register_id', $decrypted_attendance_register_id)->first();
            if (!empty($attendance_register)) {
                $data = array(
                    'attendance_register_id' => $attendance_register['attendance_register_id'],
                    'admin_id' => $attendance_register['admin_id'],
                    'attendance_intime' => date_format(date_create($attendance_register['attendance_intime']), 'h:i A'),
                    'attendance_outtime' => date_format(date_create($attendance_register['attendance_outtime']), 'h:i A'),
                    'attendance_key' => $attendance_register['attendance_key']
                );
                return array('status' => 'success', 'data' => $data);
            } else {
                return array('status' => 'error', 'message' => 'Attendance not found');
            }
        } else {
            return array('status' => 'error', 'message' => 'Invalid Parameter');
        }
    }

    /**
     * Update Employee Attendance register in - out time
     * @Bhuvanesh on 27 Feb 2018
     */
    public function updateAttendanceRegister(Request $request)
    {
        $response = [];
        if (!empty($request)) {
            $decrypted_id = get_decrypted_value($request['attendance_register_id']);
            $attendance_register = AttendanceRegister::find($decrypted_id);
            if (!empty($attendance_register)) {
                $key = '';
                $in_time = new DateTime($attendance_register['attendance_date'] . $request->get('register_in_time'));
                $out_time = new DateTime($attendance_register['attendance_date'] . $request->get('register_out_time'));
                $attendance_key = $request['register_key'];

                // Get Difference of In Time or Out Time
                $diff = date_diff($in_time, $out_time, true);
                $hours = $diff->format('%h');
                $minutes = $diff->format('%i');

                // Convert time into minutes
                $total_minute = ($hours * 60 + $minutes);

                if (strpos($attendance_key, 'late') !== false) {
                    $key = 'late_' . date_format($in_time, 'h:i');
                } else if (strpos($attendance_key, 'dhalf') !== false) {
                    $key = 'dhalf_' . date_format($in_time, 'h:i');
                } else if (strpos($attendance_key, 'full_day') !== false) {
                    $key = 'full_day';
                } else if (strpos($attendance_key, 'half_day') !== false) {
                    $key = 'half_day';
                } else if (strpos($attendance_key, 'na') !== false) {
                    $key = 'na';
                } else if (strpos($attendance_key, 'sunday') !== false) {
                    $key = 'sunday_' . get_time_decimal($diff, true);;
                } else if (strpos($attendance_key, 'saturday') !== false) {
                    $key = 'saturday_' . get_time_decimal($diff, true);;
                } else if (strpos($attendance_key, 'ch') !== false) {
                    $key = 'ch_' . get_time_decimal($diff, true);;
                } else if (strpos($attendance_key, 'present') !== false) {
                    $key = '';
                }
                DB::beginTransaction();
                try {
                    $attendance_register->attendance_intime = $in_time;
                    $attendance_register->attendance_outtime = $out_time;
                    $attendance_register->attendance_key = $key;
                    $attendance_register->attendance_total_time = $total_minute;
                    $attendance_register->save();
                } catch (\Exception $e) {
                    $error_message = $e->get_message();
                    DB::rollback();
                    return array('status' => 'error', 'message' => $error_message);
                }
                DB::commit();
                return array('status' => 'success', 'message' => 'Attendance Updated Successfully.');
            } else {
                return array('status' => 'error', 'message' => 'Record not found.');
            }
        } else {
            return array('status' => 'error', 'message' => 'Invalid Parameter');
        }
    }
}
