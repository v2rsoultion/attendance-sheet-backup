<?php

namespace App\Http\Controllers\adminPanel;

use View;
use Config;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Model\Team\Team;
use App\Model\Project\Project;
use App\Model\Project\CodeReview;
use App\Model\Project\CodeReviewInfo;
use App\Model\Project\CodeReviewDetail;
use App\Model\Project\ReviewCheckList;
use Yajra\Datatables\Datatables;
use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;
//use Illuminate\Support\Facades\Request;

class CodeReviewController extends Controller
{
    /**
     * Constructor for Project Controller 
     **/
    public function __construct()
    {
        $this->middleware('isadmin')->except('CodeReviewProjects', 'CodeReviewProjects', 'CodeReviewProjectsData', 'add', 'save', 'anyData');
        $this->middleware('ismember')->only('CodeReviewProjects', 'CodeReviewProjects', 'CodeReviewProjectsData', 'add', 'save', 'anyData');
    }


    /**
     *  view assign code reviewer
     *  @Bhuvanesh on 28 Dec 2018
     **/
    public function assignCodeReviewer($id = null)
    {
        $decrypted_project_id = get_decrypted_value($id, true);
        if ($id != null) {
            $project            = Project::where('project_id', $decrypted_project_id)->with('getProjectAssignActive')->with('getCodeReviewsActive')->first();
            // Check Condition project assign or not
            if (count($project['getProjectAssignActive']) == 0) {
                return redirect('admin-panel/project/view-project')->withError('Please assign project first!');
            }

            $code_review        =  [];
            $employee_list      = get_employees(null, true);
            $code_reviewer_list = get_coder_reviewer(null, true);
            $code_reviewer_list = add_blank_option($code_reviewer_list, 'Select Code Reviewer');
            $loginInfo          = get_loggedin_user_data();
            $project_status     = Config::get('custom.project_status');
            $review_frequence   = Config::get('custom.frequently_code_review');
            $review_frequence   = add_blank_option($review_frequence, 'Review Frequently');
            $review_check_list  = ReviewCheckList::get()->toArray();

            if (!empty($project['getCodeReviewsActive'])) {
                foreach ($project['getCodeReviewsActive'] as $value) {
                    $code_review['frequently_code_review'] = $value['frequently_code_review'];
                    $code_review['review_start_date'] = $value['review_start_date'];
                    $code_review['comments_for_reviewer'] = $value['comments_for_reviewer'];
                    $code_review['details'][$value['coder_id']]['code_review_id'] = $value['code_review_id'];
                    $code_review['details'][$value['coder_id']]['coder_id'] = $value['coder_id'];
                    $code_review['details'][$value['coder_id']]['code_reviewer_id'] = $value['code_reviewer_id'];
                    $code_review['details'][$value['coder_id']]['check_points'] = explode(',', $value['check_points']);
                }
                $submit_button      = 'Update';
            } else {
                $submit_button      = 'Save';
            }
            $save_url           = url('admin-panel/assign-codereviewer-save');
            $data = array(
                'page_title'        => trans('language.codereviewer_assign'),
                'redirect_url'      => url('admin-panel/project/my-project'),
                'login_info'        => $loginInfo,
                'project'           => $project,
                'code_review'       => $code_review,
                'employee_list'     => $employee_list,
                'project_status'    => $project_status,
                'code_reviewer_list' => $code_reviewer_list,
                'frequently_code_review'  => $review_frequence,
                'review_check_list' => $review_check_list,
                'save_url'          => $save_url,
                'submit_button'     => $submit_button,
            );
            return view('admin-panel.project.codereviewer-assign.add')->with($data);
        }
        return redirect()->back()->withInput()->withErrors('Invalid Project');
    }

    /**
     * Save Code Reviewer
     * @Bhuvanesh on 03 Apr 2019
     */
    public function saveCodeReviewer(Request $request, $id = null)
    {
        $loginInfo = get_loggedin_user_data();
        $project = [];
        $project_id = $request->get('project_id');
        if (!empty($project_id)) {
            $project = Project::find($project_id);
            if (!$project) {
                return redirect('admin-panel/project/view-project/')->withError('Project not found!');
            }
            $success_msg = 'Code Reviewer assign updated successfully!';

            $validatior = Validator::make($request->all(), [
                'frequently_code_review' => 'required',
                'review_start_date'      => 'required',
            ]);
            if ($validatior->fails()) {
                return redirect()->back()->withInput()->withErrors($validatior);
            } else {
                DB::beginTransaction();
                DB::table('code_reviews')
                    ->where('project_id', $project_id)
                    ->update(['status' => 0]);
                try {
                    $code_review_details = Input::get('map');
                    foreach ($code_review_details as $value) {
                        if (isset($value['check_list']) && $value['code_reviewer'] != '') {

                            $code_review    = CodeReview::where(['project_id' => $project_id, 'coder_id' => $value['coder'], 'code_reviewer_id' => $value['code_reviewer']])->first();
                            if (empty($code_review)) {
                                $code_review    = new CodeReview();
                            }
                            $code_review_info = CodeReviewInfo::where(['project_id' => $project_id, 'code_reviewer_id' => $value['code_reviewer']])->first();
                            if (empty($code_review_info)) {
                                $code_review_info = new CodeReviewInfo();
                                $code_review_info->project_id = $project_id;
                                $code_review_info->code_reviewer_id = $value['code_reviewer'];
                                $code_review_info->review_date = '';

                                if (Input::get('review_start_date') >= date('Y-m-d')) {
                                    $date   =     strtotime(Input::get('review_start_date'));
                                    if (Input::get('frequently_code_review') == 1) {
                                        $date  +=     strtotime('+7 day', 0);
                                    } else if (Input::get('frequently_code_review') == 2) {
                                        $date  +=     strtotime('+15 day', 0);
                                    } else if (Input::get('frequently_code_review') == 3) {
                                        $date  +=     strtotime('+30 day', 0);
                                    }
                                    $code_review_info->review_upcoming_date = date('Y-m-d', $date);
                                } else {
                                    $code_review_info->review_upcoming_date = Input::get('review_start_date');
                                }
                                $code_review_info->code_review_status = 2;
                                $code_review_info->save();
                            }

                            $code_review->project_id = $project_id;
                            $code_review->coder_id   = $value['coder'];
                            $code_review->code_reviewer_id   = $value['code_reviewer'];
                            $code_review->frequently_code_review   = Input::get('frequently_code_review');
                            $code_review->review_start_date = Input::get('review_start_date');
                            $code_review->comments_for_reviewer = Input::get('reviewer_comments');
                            $code_review->check_points   = implode(',', array_keys($value['check_list']));
                            $code_review->status = 1;
                            $code_review->save();
                        }
                    }
                } catch (\Exception $e) {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }
                DB::commit();
            }
            return redirect('admin-panel/project/view-project')->withSuccess($success_msg);
        } else {
            return redirect('admin-panel/project/view-project/')->withError('Project not found!');
        }
    }

    /**
     *  Project list view for codeReviewer
     *  @Bhuvanesh on 03 Apr 2019
     **/
    public function CodeReviewProjects()
    {
        $loginInfo          = get_loggedin_user_data();
        $data = array(
            'page_title'    => trans('language.view_codereview_project'),
            'redirect_url'  => url('admin-panel/codereview-projects/view-codereview'),
            'login_info'    => $loginInfo
        );
        return view('admin-panel.project.codereview-project')->with($data);
    }

    /**
     *  Project list view data for codeReviewer
     *  @Bhuvanesh on 03 Apr 2019
     **/
    public function CodeReviewProjectsData(Request $request)
    {
        $project    =  [];
        $loginInfo  =  get_loggedin_user_data();
        $status_array = Config::get('custom.project_status');
        $frequently_code_review_arr = Config::get('custom.frequently_code_review');
        $employee_list   = get_employees(null, true);
        $arr_project_data    =  DB::table('code_review_infos as cri')
            ->where('cri.code_reviewer_id', $loginInfo['admin_id'])
            ->where('cri.code_review_status', 2)
            ->groupBy('cri.review_upcoming_date')
            ->groupBy('cri.project_id')
            ->orderBy('cri.review_upcoming_date', 'DESC')
            ->select(
                'cri.*',
                DB::raw('(select p.project_name from projects as p where cri.project_id = p.project_id and p.status = "1") as project_name'),
                DB::raw('(select cr.frequently_code_review from code_reviews as cr where cri.project_id = cr.project_id and cr.code_reviewer_id = ' . $loginInfo['admin_id'] . ' Group By cr.code_reviewer_id,cr.project_id) as frequently_code_review'),
                DB::raw('(select cr.comments_for_reviewer from code_reviews as cr where cri.project_id = cr.project_id and cr.code_reviewer_id = ' . $loginInfo['admin_id'] . ' Group By cr.code_reviewer_id,cr.project_id) as comments_for_reviewer')
            )
            ->get();
        $arr_project_data = json_decode(json_encode($arr_project_data, true), true);
        $project = [];
        foreach ($arr_project_data as $key => $value) {
            $project[$value['project_id']] = (object)array(
                'project_id' => $value['project_id'],
                'project_name'   => $value['project_name'],
                'frequently_code_review' => $frequently_code_review_arr[$value['frequently_code_review']],
                'comments_for_reviewer'   => $value['comments_for_reviewer'],
                'review_upcoming_date'   => $value['review_upcoming_date'],
            );
        }
        return Datatables::of($project)
            ->addColumn('add_view_code_review', function ($project) {
                $encrypted_project_id   =  get_encrypted_value($project->project_id, true);
                return ' 
                    <a href="' . url('admin-panel/codereview/add-codereview/' . $encrypted_project_id) . '"><div class="label label-table label-info">Manage Review</div></a>';
            })
            ->rawColumns(['add_view_code_review' => 'add_view_code_review'])->addIndexColumn()->make(true);
    }

    /** 
     * 
     *  These Functions Used for Code Review to add, edit, delete, and view review list 
     *  Date:- 27 Dec 2018 
     *  @Bhuvanesh 
     * 
     **/


    /**
     *  Add page for CodeReview
     *  @Bhuvanesh on 27 Dec 2018
     **/
    public function add(Request $request, $project_id = null, $code_review_info_id = null, $isSubmitted = null)
    {
        $data                          = [];
        $code_review                   = [];
        $project_details               = [];
        $code_review_details           = [];
        $code_review_info              = [];
        $form_submitted               = false;
        $form_flag                     = false;
        $loginInfo                     = get_loggedin_user_data();
        $employee_list                 = get_employees(null, true);
        $decrypted_project_id          = get_decrypted_value($project_id, true);
        $status_array                  = Config::get('custom.project_status');
        $code_review_status_arr        = Config::get('custom.code_review_status');
        $frequently_code_review_arr    = Config::get('custom.frequently_code_review');
        $review_check_list             = ReviewCheckList::where('status', 1)->get()->pluck('review_check', 'review_check_id');

        $code_review                   = CodeReview::where(['code_reviewer_id' => $loginInfo['admin_id'], 'project_id' => $decrypted_project_id])->get()->toArray();
        if ($code_review_info_id != null) {
            if ($isSubmitted == 1) {
                $form_submitted = true;
            }
            $code_review_info_id   =  \get_decrypted_value($code_review_info_id, true);
            $project_details       =  DB::table('projects')
                ->select('projects.project_id', 'projects.project_name', 'projects.status as project_status', 'code_review.frequently_code_review', 'code_review.review_start_date')
                ->join('code_reviews as code_review', 'projects.project_id', '=', 'code_review.project_id')
                ->where('code_review.code_reviewer_id', $loginInfo['admin_id'])
                ->where('projects.project_id', $decrypted_project_id)
                ->groupBy('projects.project_id')
                ->get();
            $project_details = json_decode(json_encode($project_details, true), true);

            $code_review_info   =   CodeReview::where(['project_id' => $decrypted_project_id, 'code_reviewer_id' => $loginInfo['admin_id'], 'status' => '1'])->with(['getCodeReviewDetails' => function ($query) use ($code_review_info_id) {
                $query->where('code_review_info_id', $code_review_info_id);
            }])->get()->toArray();

            if (empty($code_review_info)) {
                return redirect('admin-panel/codereview/add-codereview/' . $project_id)->withError('Project not found!');
            }

            $code_review_info       = array_column($code_review_info, null, 'coder_id');
            foreach ($code_review_info as $key => $value) {
                $details         = array_column($value['get_code_review_details'], null, 'review_check_id');
                $code_review_details[$key]['code_review_id']  =   $value['code_review_id'];
                $code_review_details[$key]['project_id']  =   $value['project_id'];
                $code_review_details[$key]['coder_id']  =   $value['coder_id'];
                $code_review_details[$key]['code_reviewer_id']  =   $value['code_reviewer_id'];
                $code_review_details[$key]['frequently_code_review']  =   $value['frequently_code_review'];
                $code_review_details[$key]['get_code_review_details']  =   $details;
            }

            $form_flag       = true;
            $page_title      =  trans('language.edit_codereview');
            $save_url        =  url('admin-panel/codereview/save/' . $project_id . '/' . get_encrypted_value($code_review_info_id, true));
            $submit_button   =  'Update';
        } else {
            $page_title      =  trans('language.add_codereview');
            $save_url        =  url('admin-panel/codereview/save/' . $project_id);
            $submit_button   =  'Save';
        }

        $data   = array(
            'page_title'          => $page_title,
            'save_url'            => $save_url,
            'submit_button'       => $submit_button,
            'employee_list'       => $employee_list,
            'review_check_list'   => $review_check_list,
            'form_flag'           => $form_flag,
            'form_submitted'     => $form_submitted,
            'login_info'          => $loginInfo,
            'project_id'          => $project_id,
            'project'             => $project_details,
            'code_review'         => $code_review,
            'code_review_details' => $code_review_details,
            'code_review_info'    => $code_review_info,
            'code_review_info_id' => $code_review_info_id,
            'code_review_status_arr'  => $code_review_status_arr,
            'frequently_code_review_arr' => $frequently_code_review_arr,
            'redirect_url'        => url('admin-panel/my-project/codereview-project-list'),
        );
        return view('admin-panel.project.codereview.add')->with($data);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Bhuvanesh on 27 Dec 2018
     **/

    public function anyData(Request $request)
    {
        $code_review           = [];
        $code_review_logs      = [];
        $upcoming_review_date  = '';
        $pre_code_review_arr   = [];
        $searchKey             = null;
        $developer_arr         = [];
        $employee_list         = get_employees(null, true);
        $loginInfo             = get_loggedin_user_data();
        $review_status_arr = Config::get('custom.review_status');
        $project_id            = get_decrypted_value($request->get('project_id'), true);

        $code_reviews      = CodeReviewInfo::where(['project_id' => $project_id, 'code_reviewer_id' => $loginInfo['admin_id']])->with('getCodeReviewDetails')->orderBy('created_at', 'desc')->get()->toArray();
        $code_review           = CodeReview::where(['project_id' => $project_id, 'code_reviewer_id' => $loginInfo['admin_id']])->select('coder_id', 'status')->get()->toArray();
        foreach ($code_review as $value) {
            array_push($developer_arr, $employee_list[$value['coder_id']]);
        }
        $code_review = array_column($code_review, null, 'coder_id');
        return Datatables::of($code_reviews)
            ->addColumn('code_review_coder', function ($code_reviews) use ($developer_arr) {
                return $developer_arr;
            })
            ->addColumn('code_review_status', function ($code_reviews) use ($review_status_arr) {
                return $review_status_arr[$code_reviews['code_review_status']];
            })
            ->addColumn('code_review_date', function ($code_reviews) {
                return $code_reviews['review_date'];
            })
            ->addColumn('review_duration', function ($code_reviews) {
                if ($code_reviews['review_date'] == '') {
                    // Add 7 Days buffer in review date
                    $last_date = date('Y-m-d', strtotime("+7 day", strtotime($code_reviews['review_upcoming_date'])));
                    return $code_reviews['review_upcoming_date'] . ' to ' . $last_date;
                } else {
                    return '----';
                }
            })
            ->addColumn('edit_code_review', function ($code_reviews) use ($code_review) {
                $isReviewAllow = false;
                $review_date = $code_reviews['review_upcoming_date'];
                if ($review_date <= date('Y-m-d')) {
                    $encrypted_code_review_info_id = get_encrypted_value($code_reviews['code_review_info_id'], true);
                    $encrypted_project_id = get_encrypted_value($code_reviews['project_id'], true);
                    return '<a href="' . url('admin-panel/codereview/add-codereview/' . $encrypted_project_id . '/' . $encrypted_code_review_info_id . '/' . $code_reviews['isSubmit']) . '"><i class="fas fa-pencil-alt"></i></a>';
                }
            })
            ->rawColumns(['code_review_status' => 'code_review_status', 'review_duration' => 'review_duration', 'code_review_date' => 'code_review_date', 'edit_code_review' => 'edit_code_review', 'code_review_coder' => 'code_review_coder'])->addIndexColumn()->make(true);
    }

    /**
     *  Save CodeReview data
     *  @Bhuvanesh on 27 Dec 2018
     **/
    public function save(Request $request, $project_id = null, $code_review_info_id = null)
    {
        $code_review             =  [];
        $success_msg             =  '';
        $code_review_info        =  [];
        $isSaveRecord            =  false;
        $loginInfo               =  get_loggedin_user_data();
        $decrypted_project_id              =  get_decrypted_value($project_id, true);
        $decrypted_code_review_info_id     =  get_decrypted_value($code_review_info_id, true);
        $code_review = CodeReview::where(['project_id' => $decrypted_project_id, 'code_reviewer_id' => $loginInfo['admin_id']])->get()->toArray();
        if (empty($code_review)) {
            return redirect()->back()->withInput()->withErrors('Code review not found!');
        }
        if ($project_id != null) {
            DB::beginTransaction();
            try {
                $map = Input::get('map');
                if ($decrypted_code_review_info_id != '') {
                    $code_review_info = CodeReviewInfo::where(['code_review_info_id' => $decrypted_code_review_info_id])->first();
                } else {
                    return redirect()->back()->withInput()->withErrors('Invalid Code Review information');
                }
                if ($code_review_info->review_date == '') {
                    $code_review_info->review_date = date('Y-m-d');
                }
                if ($code_review_info->review_upcoming_date >= date('Y-m-d')) {
                    // Mark status as Ontime
                    $code_review_info->code_review_status = 1;
                } else {
                    // Mark status as Delay
                    $code_review_info->code_review_status = 3;
                }

                if ($request->get('submit') != '' && $request->get('submit') == 'submit') {
                    $code_review_info->isSubmit = 1;
                }

                $code_review_info->save();
                foreach ($map as $value) {
                    foreach ($value['details'] as $review_check) {
                        if (isset($review_check['code_review_detail_id']) && $review_check['code_review_detail_id'] != '') {
                            $success_msg         =  'Code review updated successfully!';
                            $code_review_detail  = CodeReviewDetail::find($review_check['code_review_detail_id']);
                        } else {
                            $success_msg         =  'Code review saved successfully!';
                            $code_review_detail  =  new CodeReviewDetail();
                            $isSaveRecord        =  true;
                        }
                        $code_review_detail->code_review_info_id   =  $code_review_info->code_review_info_id;
                        $code_review_detail->code_review_id        =  $value['code_review_id'];
                        $code_review_detail->review_check_id       =  $review_check['review_check_id'];
                        $code_review_detail->comments              =  $review_check['comment'];
                        $code_review_detail->review_status         =  $review_check['review_status'];
                        $code_review_detail->save();
                    }
                }

                if ($isSaveRecord) {
                    $new_upcoming_review_date = strtotime(date('Y-m-d'));
                    // Change Upcoming Review Date
                    $frequently_code_review = isset($code_review[0]['frequently_code_review']) ? $code_review[0]['frequently_code_review'] : '';
                    if ($frequently_code_review == 1) {
                        $new_upcoming_review_date += strtotime('+7 days', 0);
                    } else if ($frequently_code_review == 2) {
                        $new_upcoming_review_date += strtotime('+14 days', 0);
                    } else if ($frequently_code_review == 3) {
                        $new_upcoming_review_date += strtotime('+30 days', 0);
                    }
                    $new_code_review_info = new CodeReviewInfo();
                    $new_code_review_info->project_id = $decrypted_project_id;
                    $new_code_review_info->code_reviewer_id = $loginInfo['admin_id'];
                    $new_code_review_info->review_date = '';
                    // Mark status as pending
                    $new_code_review_info->code_review_status = 2;
                    $new_code_review_info->review_upcoming_date = date('Y-m-d', $new_upcoming_review_date);
                    $new_code_review_info->save();
                }
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return  redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/codereview/add-codereview/' . $project_id)->withSuccess($success_msg);
    }

    /**
     *  My Project list view for member
     *  @Bhuvanesh on 28 Dec 2018
     **/
    // public function myProjectList()
    // {
    //     $loginInfo          = get_loggedin_user_data();
    //     $data = array(
    //         'page_title'    => trans('language.view_my_project'),
    //         'redirect_url'  => url('admin-panel/project/my-project'),
    //         'login_info'    => $loginInfo
    //     );
    //     return view('admin-panel.project.my-projects')->with($data);
    // }

    /**
     *  My Project list view data for member
     *  @Bhuvanesh on 28 Dec 2018
     **/
    // public function myProjectListData(Request $request)
    // {
    //     $loginInfo  =  get_loggedin_user_data();
    //     $project    =  [];
    //     $project    =  DB::table('projects')->select('projects.project_id', 'projects.status', 'projects.project_name', 'platforms.platform_name', 'platforms.platform_id')
    //         ->join('project_details', 'projects.project_id', '=', 'project_details.project_id')
    //         ->join('platforms', 'project_details.platform_id', '=', 'platforms.platform_id')
    //         ->where('project_details.coder_id', $loginInfo['admin_id'])
    //         ->get()->toArray();
    //     $project    =  json_decode(json_encode($project), true);
    //     return Datatables::of($project)
    //         ->addColumn('project_status', function ($project) {
    //             $status_name = '----';
    //             if (!empty($project['status'])) {
    //                 $status_array = Config::get('custom.project_status');
    //                 $status_name  = $status_array[$project['status']];
    //             }
    //             return $status_name;
    //         })
    //         ->addColumn('view_review', function ($project) {
    //             return ' <a href="' . url('admin-panel/my-project/myproject-review/' . get_encrypted_value($project['project_id'], true) . '/' . get_encrypted_value($project['platform_id'], true)) . '"><div class="label label-table label-info">Reviews</div></a>';
    //         })
    //         ->rawColumns(['view_review' => 'view_review', 'project_status' => 'project_status'])->addIndexColumn()->make(true);
    // }

    /**
     *  My Project Code review for member
     *  @Bhuvanesh on 29 Dec 2018
     **/
    // public function myProjectCodeReview($project_id = null, $platform_id = null)
    // {
    //     $loginInfo             =  get_loggedin_user_data();
    //     $decrypted_project_id  =  get_decrypted_value($project_id, true);
    //     if (!empty($project_id) && !empty($platform_id)) {
    //         $page_sub_title    =  Project::Find($decrypted_project_id);
    //         $data = array(
    //             'page_title'     => trans('language.view_my_project_review'),
    //             'page_sub_title' => $page_sub_title->project_name,
    //             'redirect_url'   => url('admin-panel/my-project/myproject-review'),
    //             'login_info'     => $loginInfo,
    //             'project_id'     => $project_id,
    //             'platform_id'    => $platform_id,
    //         );
    //         return view('admin-panel.project.my-projects-codereview')->with($data);
    //     }
    //     return redirect()->back()->withInput()->withErrors('Invalid Project');
    // }

    /**
     *  My Project Code review data 
     *  @Bhuvanesh on 29 Dec 2018
     **/
    // public function myProjectCodeReviewData(Request $request)
    // {
    //     $decrypted_project_id   =  get_decrypted_value($request->get('project_id'), true);
    //     $decrypted_platform_id  =  get_decrypted_value($request->get('platform_id'), true);
    //     if (!empty($decrypted_project_id)) {
    //         $loginInfo  =   get_loggedin_user_data();
    //         $project    =   [];
    //         $project    =   ProjectDetails::where(['project_id' => $decrypted_project_id, 'platform_id' => $decrypted_platform_id, 'coder_id' => $loginInfo['admin_id']])
    //             ->with('getCodeReviews')
    //             ->get()->first();
    //         $project    =    json_decode(json_encode($project), true);
    //         return Datatables::of($project['get_code_reviews'])
    //             ->addIndexColumn()->make(true);
    //     }
    // }


    // /**
    //  *  Code Review log for admin
    //  *  @Bhuvanesh on 01 Jan 2018
    // **/
    // public function codeReviewLog($id = null)
    // {
    //     $decrypted_project_id   = get_decrypted_value($id, true);
    //     if (!empty($id)) {
    //         $project                    = Project::where('project_id', '=', $decrypted_project_id)->get()->first();
    //         $project_platforms          = Project::select('platform')->where('project_id', '=', $decrypted_project_id)->get()->first()->toArray();
    //         $project_platforms          = explode(',', $project_platforms['platform']);
    //         $project_platforms_arr      = [];
    //         foreach ($project_platforms as $project_platform) {
    //             $project_platform_arr   = Platform::where('platform_id', '=', $project_platform)->get()->first();
    //             $project_platforms_arr[$project_platform_arr->platform_id]   =  $project_platform_arr->platform_name;
    //         }
    //         $platforms          = add_blank_option($project_platforms_arr, 'Select Platform');
    //         $team_arr      = ProjectDetails::where('project_id', '=', $decrypted_project_id)->get()->toArray();
    //         foreach ($team_arr as $code_reviewer) {
    //             $code_reviewer_arr      = Team::select('admin_id', 'admin_name')->where('admin_id', '=', $code_reviewer['code_reviewer_id'])->get()->first();
    //             $code_reviewers_arr[$code_reviewer_arr->admin_id]     =  $code_reviewer_arr->admin_name;

    //             $coder_arr      = Team::select('admin_id', 'admin_name')->where('admin_id', '=', $code_reviewer['coder_id'])->get()->first();
    //             $coders_arr[$coder_arr->admin_id]     =  $coder_arr->admin_name;
    //         }

    //         $code_reviewers_arr      = add_blank_option($code_reviewers_arr, 'Select Code Reviewer');
    //         $coders_arr              = add_blank_option($coders_arr, 'Select Developer');
    //         $loginInfo          = get_loggedin_user_data();
    //         $data = array(
    //             'page_title'    => trans('language.view_codereview_log'),
    //             'login_info'    => $loginInfo,
    //             'platform'      => $platforms,
    //             'code_reviewer' => $code_reviewers_arr,
    //             'coder'         => $coders_arr,
    //             'project'       => $project,
    //             'project_id'    => $id,
    //         );
    //         return view('admin-panel.project.codereview-log.index')->with($data);
    //     }
    // }

    // /**
    //  *  Code Review data for admin
    //  *  @Bhuvanesh on 01 Jan 2018
    // **/
    // public function codeReviewLogData(Request $request)
    // {
    //     $code_reviews = [];
    //     if (!empty($request) && $request->get('project_id') != '') {
    //         $decrypted_project_id   = get_decrypted_value($request->get('project_id'), true);
    //         $code_reviews  =  CodeReview::where(function ($query) use ($request, $decrypted_project_id) {
    //             $query->where('project_id', '=', $decrypted_project_id);
    //             if ($request->has('coder_id') && $request->get('coder_id') != '') {
    //                 $query->where('coder_id', '=', $request->get('coder_id'));
    //             }
    //             if ($request->has('code_reviewer_id') && $request->get('code_reviewer_id') != '') {
    //                 $query->where('code_reviewer_id', '=', $request->get('code_reviewer_id'));
    //             }
    //         })->with(['getProjectDetails' => function ($q) use ($request) {
    //             if ($request->has('platform_id') && $request->get('platform_id') != '') {
    //                 $q->where('platform_id', '=', $request->get('platform_id'));
    //             }
    //         }])
    //             ->with(['getCoder', 'getCodeReviewer'])->get()->toArray();
    //         foreach ($code_reviews as $key => $value) {
    //             if ($value['get_project_details'] == '') {
    //                 unset($code_reviews[$key]);
    //             }
    //         }
    //     }
    //     return Datatables::of($code_reviews)
    //         ->addColumn('coder', function ($code_reviews) {
    //             $coder = '------';
    //             if (!empty($code_reviews['get_coder'])) {
    //                 $coder =  $code_reviews['get_coder']['admin_name'];
    //             }
    //             return $coder;
    //         })
    //         ->addColumn('review_date', function ($code_reviews) {
    //             $code_review_date = '------';
    //             if (!empty($code_reviews['created_at'])) {
    //                 $code_review_date = date("d-m-Y", strtotime($code_reviews['created_at']));
    //             }
    //             return $code_review_date;
    //         })
    //         ->addColumn('code_reviewer', function ($code_reviews) {
    //             $code_reviewer = '------';
    //             if (!empty($code_reviews['get_code_reviewer'])) {
    //                 $code_reviewer =  $code_reviews['get_code_reviewer']['admin_name'];
    //             }
    //             return $code_reviewer;
    //         })
    //         ->addColumn('platform', function ($code_reviews) {
    //             $platform = '------';
    //             if (!empty($code_reviews['get_project_details'])) {
    //                 $platform_id  =  $code_reviews['get_project_details']['platform_id'];
    //                 $platform     =  Platform::where('platform_id', '=', $platform_id)->get()->first();
    //                 $platform     =  $platform->platform_name;
    //             }
    //             return $platform;
    //         })
    //         ->addColumn('code_review_feedback', function ($code_reviews) {
    //             $encrypted_code_review_id  =  get_encrypted_value($code_reviews['code_review_id'], true);
    //             return "<div onclick=readMore('" . $encrypted_code_review_id . "') class='label label-table label-info'>Reviews</div>";
    //             // '<input type="button" value="click" data-toggle="modal" data-target=".bs-example-modal-lg" />';
    //         })
    //         ->rawColumns(['review_date' => 'review_date', 'code_review_feedback' => 'code_review_feedback', 'coder' => 'coder', 'platform' => 'platform', 'code_reviewer' => 'code_reviewer'])
    //         ->addIndexColumn()
    //         ->make(true);
    // }

    /**
     *  Code Review feedback on more click
     *  @Bhuvanesh on 01 Jan 2018
     **/
    // public function codeReviewFeeback(Request $request)
    // {

    //     if (isset($request) && $request->get('code_review_id') != '') {
    //         $decrypted_code_review_id = get_decrypted_value($request->get('code_review_id'), true);
    //         $code_review = CodeReview::Find($decrypted_code_review_id);
    //         return $code_review->code_review_feedback;
    //     }
    //     return 'Not Found!';
    // }
}
