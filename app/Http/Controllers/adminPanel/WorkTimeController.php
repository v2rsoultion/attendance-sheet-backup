<?php
namespace App\Http\Controllers\adminPanel;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Model\Team\Team;
use App\Model\Project\Project;
use App\Model\Project\WorkTime;
use App\Model\Project\WorkType;
use App\Model\Project\ProjectAssign;
use App\Model\UserRole\AssignUserRole;
use Yajra\Datatables\Datatables;
use App\Model\Project\WorkTimeDetail;
// use Illuminate\Support\Facades\Request;

class WorkTimeController extends Controller
{
    /**
     * Constructor for WorkTime Controller 
     **/
    public function __construct()
    {
        $this->middleware('isadmin')->except('index', 'anyData', 'add', 'save', 'removeWorkTimeDetail', 'checkWorkTimeDate');
        $this->middleware('ismember')->only('index', 'anyData', 'add', 'save', 'removeWorkTimeDetail', 'checkWorkTimeDate');
    }

    /**
     *  View page for WorkTime
     *  @Bhuvanesh on 26 Jan 2019
     **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title' => trans('language.view_work_time'),
            'redirect_url' => url('admin-panel/project-work-time/view-project-work-time'),
            'login_info' => $loginInfo,
        );
        return view('admin-panel.worktime.index')->with($data);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Bhuvanesh on 26 Jan 2019
     **/

    public function anyData(Request $request)
    {
        $work_type = [];
        $start_date = null;
        $end_date = null;
        $loginInfo = get_loggedin_user_data();
        if ($request->get('work_time_date') != '') {
            $date_range = explode('-', $request->get('work_time_date'));
            $date_create = date_create($date_range[0]);
            $start_date = date_format($date_create, 'Y-m-d');
            $date_create = date_create($date_range[1]);
            $end_date = date_format($date_create, 'Y-m-d');
        }
        $work_time = WorkTime::where(function ($query) use ($loginInfo, $start_date, $end_date) {
            $query->where('admin_id', '=', $loginInfo['admin_id']);
            if ($start_date != null && $end_date != null) {
                $query->whereBetween('work_time_date', [$start_date, $end_date]);
            }
        })
            ->with(['getWorkTimeDetails.getProjectDetails' => function ($q) { }])
            ->orderBy('work_time_id', 'desc')
            ->get()
            ->toArray();
        return Datatables::of($work_time)
            ->addColumn('work_time_date', function ($work_time) {
                $date = date_create($work_time['work_time_date']);
                return date_format($date, 'd-m-Y');
            })
            ->addColumn('work_time_projects', function ($work_time) {
                $work_time_details = isset($work_time['get_work_time_details']) ? $work_time['get_work_time_details'] : '';
                if (!empty($work_time_details)) {
                    $project_name_arr = [];
                    foreach ($work_time_details as $key => $value) {
                        array_push($project_name_arr, $value['get_project_details']['project_name']);
                    }
                    if (!empty($project_name_arr)) {
                        return implode(', ', array_unique($project_name_arr));
                    } else {
                        return '------';
                    }
                }
            })
            ->addColumn('action', function ($work_time) {
                $encrypted_work_time_id = get_encrypted_value($work_time['work_time_id'], true);
                return ' &nbsp; &nbsp;
                <a href="' . url('admin-panel/project-work-time/add-project-work-time/' . $encrypted_work_time_id) . '"data-toggle="tooltip" title="Edit Work Time"><i class="fas fa-pencil-alt"></i></a>';
                // '<a href="'. url('admin-panel/project-work-time/project-work-time-delete/' . $encrypted_work_time_id ) .'" onclick="return confirm('."'Are you sure?'".')" " data-toggle="tooltip" title="Delete Work Time"><i class="fas fa-trash"></i></a> ';       
            })->rawColumns(['work_time_date' => 'work_time_date', 'work_time_projects' => 'work_time_projects', 'action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Add page for WorkTime
     *  @Bhuvanesh on 26 Jan 2019
     **/
    public function add(Request $request, $id = null)
    {
        $data = [];
        $work_type = [];
        $work_time = [];
        $work_type_arr = [];
        $work_time_date = '';
        $project_assigns = [];
        $work_time_details = [];
        $loginInfo = get_loggedin_user_data();
        $project_status = \Config::get('custom.project_status');

        if (!empty($id)) {
            $decrypted_work_time_id = get_decrypted_value($id, true);
            $work_time = WorkTime::Find($decrypted_work_time_id);
            if (!$work_time) {
                return redirect('admin-panel/project-work-time/add-project-work-time')->withError('Work not found!');
            }
            $date = date_create($work_time['work_time_date']);
            $work_time_date = date_format($date, 'Y/m/d');
            $work_time_details = WorkTimeDetail::where('work_time_id', '=', $work_time->work_time_id)->get()->toArray();
            // p($work_time_details);
            // if (!$work_time_details) {
            //     return redirect('admin-panel/project-work-time/add-project-work-time')->withError('Work Details not found!');
            // }
            $page_title = trans('language.edit_work_time');
            $encrypted_work_time_id = get_encrypted_value($work_time->work_time_id, true);
            $save_url = url('admin-panel/project-work-time/save/' . $encrypted_work_time_id);
            $submit_button = 'Update';
        } else {
            $page_title = trans('language.add_work_time');
            $save_url = url('admin-panel/project-work-time/save');
            $submit_button = 'Save';
        }

        // Work Type List
        $assignRoles = AssignUserRole::where(['admin_id' => $loginInfo['admin_id'], 'assign_user_role_status' => '1'])->with('getUserRole.getResponsibility.getWorkType')->get()->toArray();
        foreach ($assignRoles as $getRole) {
            if (!empty($getRole['get_user_role'])) {
                if (!empty($getRole['get_user_role']['get_responsibility'])) {
                    foreach ($getRole['get_user_role']['get_responsibility'] as $respo) {
                        if (!empty($respo)) {
                            $work_type_arr[$respo['get_work_type']['work_type_id']] = $respo['get_work_type']['work_type_name'];
                        }
                    }
                }
            }
        }
        $work_type_arr = add_blank_option($work_type_arr, 'Select Work Type');

        // Assigned Project List
        $project_assigns = ProjectAssign::where([
            'admin_id' => $loginInfo['admin_id'],
            'project_assign_status' => '1'
        ])
            ->with(['getProject', 'getDeveloper'])
            ->get()
            ->toArray();

        $data = array(
            'page_title' => $page_title,
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'work_time' => $work_time,  //Work Time with date and admin_id
            'work_time_details' => $work_time_details, //Work time details
            'work_time_date' => $work_time_date,  // Converted date of work time 
            'work_type' => $work_type_arr, // Work time array
            'project_assigns' => $project_assigns,  //Assigned project Details
            'login_info' => $loginInfo,
            'project_status' => $project_status,
            'redirect_url' => url('admin-panel/project-work-time/view-project-work-time'),
        );
        $data['project_existing_data'] = array_count_values(array_column($data['work_time_details'], 'project_id'));
        // p($data);
        return view('admin-panel.worktime.add')->with($data);
    }

    /**
     *  Save WorkTime data
     *  @Bhuvanesh on 26 Jan 2019
     **/
    public function save(Request $request, $id = null)
    {
        // p($request->all());
        $loginInfo = get_loggedin_user_data();
        $decrypted_work_time_id = get_decrypted_value($id, true);
        $work_time = [];
        if (!empty($id)) {
            $work_time = WorkTime::find($decrypted_work_time_id);
            if (!$work_time) {
                return redirect('admin-panel/work-type/view-work-type/')->withError('Work Type not found!');
            }
            $success_msg = 'Work Time updated successfully!';
        } else {
            $work_time = new WorkTime();
            $success_msg = 'Work Time saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'work_date' => 'required',
            'total_work_time_hrs' => 'required'
        ]);
        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction();
            try {
                $total_working_hrs = 0;
                $work_time->admin_id = $loginInfo['admin_id'];
                $date = date_create(Input::get('work_date'));
                $work_time->work_time_date = date_format($date, 'Y-m-d');
                $work_time->total_work_time_hr = Input::get('total_work_time_hrs');
                $work_time->save();
                $work_detail = Input::get('map');
                if (!empty($work_detail)) {
                    foreach ($work_detail as $key => $value) {
                        $project_id = $key;
                        foreach ($value as $innerObject) {
                            if (isset($innerObject['work_type']) && $innerObject['work_type'] != '') {
                                if (isset($innerObject['work_time_detail_id']) && $innerObject['work_time_detail_id'] != '') {
                                    $work_detail_object = WorkTimeDetail::Find($innerObject['work_time_detail_id']);
                                } else {
                                    $work_detail_object = new WorkTimeDetail();
                                }
                                $work_detail_object->project_id = $project_id;
                                $work_detail_object->work_time_id = $work_time->work_time_id;
                                $work_detail_object->work_type_id = $innerObject['work_type'];
                                $work_detail_object->work_time_detail_hrs = $innerObject['work_time'];
                                $work_detail_object->work_time_detail_description = $innerObject['work_desc'];
                                $work_detail_object->save();
                            }
                        }
                    }
                }
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/project-work-time/view-project-work-time')->withSuccess($success_msg);
    }

    /**
     * Remove WorkTime Detail
     * @Bhuvanesh on 29 Jan 2019
     */
    public function removeWorkTimeDetail(Request $request)
    {
        if (!empty($request) && $request->get('id') != '') {
            $msg = '';
            $work_time_details = WorkTimeDetail::find($request->get('id'));
            if ($work_time_details) {
                DB::beginTransaction();
                try {
                    $work_time_details->delete();
                    $msg = "success";
                    $work_time_details_records = WorkTimeDetail::where('work_time_id', $request->get('work_time_id'))->get();
                    if (count($work_time_details_records) == 0) {
                        $work_time = WorkTime::find($request->get('work_time_id'));
                        $work_time->delete();
                    }
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollback();
                    $msg = "error";
                }
            } else {
                $msg = "not found!";
            }
            return $msg;
        }
    }

    /**
     * Check Date of Work Time
     * @Bhuvanesh on 29 Jan 2019
     */
    public function checkWorkTimeDate(Request $request)
    {
        if (!empty($request) && $request->get('work_date') != '') {
            $loginInfo = get_loggedin_user_data();
            $date = date_create($request->get('work_date'));
            $date_formate = date_format($date, 'Y-m-d');
            $work_time_details = WorkTime::where(function ($query) use ($date_formate, $request, $loginInfo) {
                $query->where(['work_time_date' => $date_formate, 'admin_id' => $loginInfo['admin_id']]);
                if (isset($request) && $request->get('work_time_id') != '') {
                    $query->where('work_time_id', '!=', $request->get('work_time_id'));
                }
            })->get()->count();
            if ($work_time_details > 0) {
                return response()->json(false);
            } else {
                return response()->json(true);
            }
        }
    }
}
