<?php
namespace App\Http\Controllers\adminPanel;

use DateTime;
use DateInterval;
use DatePeriod;
use Validator;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Model\Team\Team;
use App\Model\Team\TimeSlot;
use App\Model\Company\Event;
use App\Model\Company\CompanyConfig;
use App\Model\Project\WorkTime;
use App\Model\UserRole\AssignUserRole;
use App\Model\Project\CodeReviewInfo;
use App\Model\Attendance\AttendanceRegister;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Model\Leave\LeaveRegister;
use App\Notifications\Admin\Leave;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Notification;

class DashboardController extends Controller
{
    /**
     *  View page for Dashboard
     *  @Bhuvanesh on 22 Dec 2018
     **/
    public function index(Request $request)
    {
        // $data = [];
        // return view('admin-panel.auth.mail.code_review_mail')->with($data);
        $data = [];
        $teamCounter = 0;
        $departmentCounter = 0;
        $projectCounter = 0;
        $leave_category = [];
        $leaveRequestCounter = 0;
        $earlyLeaveRequestCounter = 0;
        $attendanceRequestCounter = 0;
        $attendance_detail = [];
        $loginInfo = get_loggedin_user_data();
        $in_out = [];
        $apply_leave_url = '';
        $apply_short_leave_url = '';
        $applied_leave = [];
        $current_month = date('M');
        $total_work_time = '';
        if ($loginInfo['type'] == 1) {
            $teamCounter = get_total_counter('teammember');
            $departmentCounter = get_total_counter('department');
            $projectCounter = get_total_counter('projects');
            $leaveRequestCounter = get_total_counter('leavePending');
            $earlyLeaveRequestCounter = get_total_counter('earlyLeavePending');
            $attendanceRequestCounter = get_total_counter('attendancePending');
        } else {
            $inTime = '';
            $outTime = '';
            $leave_category = \Config::get('custom.leave_category');
            $leave_status = \Config::get('custom.leave_status');
            $leave_category = add_blank_option($leave_category, 'Select Category');
            $attendance_detail = get_current_month_details($loginInfo['admin_id']);

            $todayAttendance = AttendanceRegister::where(['attendance_date' => date('Y-m-d'), 'admin_id' => $loginInfo['admin_id']])->orderBy('attendance_date')->first();
            if (!empty($todayAttendance) && $todayAttendance->attendance_intime != '') {
                $inTime = new DateTime($todayAttendance->attendance_intime);
                $in_out['in'] = date('h:i A', strtotime($todayAttendance->attendance_intime));
            } else {
                $in_out['in'] = '-:- -';
            }
            if (!empty($todayAttendance) && $todayAttendance->attendance_outtime != '') {
                $outTime = new DateTime($todayAttendance->attendance_outtime);
                $in_out['out'] = date('h:i A', strtotime($todayAttendance->attendance_outtime));
            } else {
                $in_out['out'] = '-:- -';
            }

            if (!empty($inTime)) {
                if (empty($outTime)) {
                    $outTime = new DateTime();
                }
                $diff = date_diff($inTime, $outTime, true);
                $hours = $diff->format('%h');
                $minutes = $diff->format('%i');
                $total_work_time = $hours . ' hrs ' . $minutes . ' minutes';
            }
            $apply_leave_url = url('admin-panel/leave/apply-leave');
            $apply_short_leave_url = url('admin-panel/early-leave/apply-early-leave');

            $applied_leave_arr = LeaveRegister::where(['admin_id' => $loginInfo['admin_id']])->select('leave_category', 'leave_start_date', 'leave_end_date', 'leave_status')->orderBy('updated_at', 'desc')->take(4)->get()->toArray();
            foreach ($applied_leave_arr as $key => $value) {
                if ($value['leave_start_date'] == $value['leave_end_date']) {
                    array_push($applied_leave, [date_create($value['leave_start_date'])->format('d M Y'), $leave_category[$value['leave_category']], $leave_status[$value['leave_status']]]);
                } else {
                    array_push($applied_leave, [date_create($value['leave_start_date'])->format('d M Y') . ' to ' . date_create($value['leave_end_date'])->format('d M Y'), $leave_category[$value['leave_category']], $leave_status[$value['leave_status']]]);
                }
            }
        }
        $data = array(
            'page_title' => trans('language.dashboard'),
            'login_info' => $loginInfo,
            'in_out' => $in_out,
            'teamCounter' => $teamCounter,
            'departmentCounter' => $departmentCounter,
            'projectCounter' => $projectCounter,
            'leave_category' => $leave_category,
            'attendance_detail' => $attendance_detail,
            'applied_leave' => $applied_leave,
            'apply_leave_url' => $apply_leave_url,
            'total_work_time' => $total_work_time,
            'apply_short_leave_url' => $apply_short_leave_url,
            'leaveRequestCounter' => $leaveRequestCounter,
            'earlyLeaveRequestCounter' => $earlyLeaveRequestCounter,
            'attendanceRequestCounter' => $attendanceRequestCounter,
            'month' => $current_month,
        );
        return view('admin-panel.dashboard.index')->with($data);
    }

    /**
     * Insert In Time for employee
     * @Bhuvanesh on 11 Feb 2018
     */
    public function registerTimeIn(Request $request)
    {
        if (isset($request) && $request->get('attendance_type') == 'in') {
            $loginInfo = get_loggedin_user_data();
            $type = $request->get('attendance_type');
            $toDay_Record = AttendanceRegister::where(['admin_id' => $loginInfo['admin_id'], 'attendance_date' => date('Y-m-d')])->get()->toArray();
            if ($toDay_Record) {
                return array(
                    'status' => 'error',
                    'message' => 'You are already In.'
                );
            } else {
                $attendance_register = new AttendanceRegister();
            }
            $key = '';
            $time_slot_time = TimeSlot::where('time_slot_id', $loginInfo['time_slot_id'])->pluck('time_slot_time');
            $office_time = new DateTime($time_slot_time[0]);
            $current_time = new DateTime(date('H:i:s'));
            // Get Relaxation time from custom file and add it to office time
            $time_relaxation = \Config::get('custom.time_relaxation');
            $time_relaxation += 1;
            $office_time->add(new DateInterval('PT' . $time_relaxation . 'M'));
            // Get Difference of Current or Office Time
            $diff = date_diff($current_time, $office_time, true);

            // current_date 
            $current_date = date('Y-m-d');
            $hours = $diff->format('%h');
            $minutes = $diff->format('%i');
            // Convert time into minutes

            $total_minute_late = ($hours * 60 + $minutes);
            $emp_off_saturday = Team::where(['admin_id' => $loginInfo])->pluck('saturday_off')->first();
            $arr_event_sat = get_arr_saturday_off_from_event();
            // In Time Register
            if (date_format($current_time, 'l') == 'Sunday') {
                $key = 'sunday';
            } else if ((date_format($current_time, 'l') == 'Saturday' && in_array($current_date, $arr_event_sat)) && $emp_off_saturday > 0) {
                $key = 'saturday';
            } else {
                $directHalfTime = CompanyConfig::pluck('direct_half_time')->first();
                $time = new DateTime(explode(' ', $directHalfTime)[0]);
                if ($office_time >= $current_time) {
                    $key = '';
                } else {
                    if (strtotime($current_time->format('H:i:s')) > strtotime($time->format('H:i:s'))) {
                        $key = 'dhalf_' . date_format($current_time, 'H:i');
                    } else {
                        $key = 'late_' . date_format($current_time, 'H:i');
                    }
                }
            }

            DB::beginTransaction();
            try {
                $attendance_register->admin_id = $loginInfo['admin_id'];
                $attendance_register->attendance_date = date('Y-m-d');
                $attendance_register->attendance_intime = $current_time;
                $attendance_register->attendance_key = $key;
                $attendance_register->save();
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return array(
                    'status' => 'error',
                    'message' => $error_message
                );
            }
            // Insert Log for In entry
            insert_log_entry(2);
            DB::commit();
            return array(
                'status' => 'success',
                'message' => 'You are In.'
            );
        }
        return array(
            'status' => 'error',
            'message' => 'Invalid Type.'
        );
    }

    /**
     * Insert out or Check Out Time for employee
     * @Bhuvanesh on 11 Feb 2018
     */
    public function registerTimeOut(Request $request)
    {
        if (isset($request) && $request->get('attendance_type') == 'out') {
            $loginInfo = get_loggedin_user_data();

            // Check for Current out date work time. If Exist then logout otherwise redirect to work time
            $assign_user_roles = AssignUserRole::leftJoin('user_roles', 'assign_user_roles.user_role_id', '=', 'user_roles.user_role_id')
                ->where(['admin_id' => $loginInfo['admin_id'], 'assign_user_role_status' => '1'])
                ->select('user_roles.user_role_id', 'user_roles.isWorkTimeRequired')
                ->get()
                ->toArray();
            foreach ($assign_user_roles as $key => $value) {
                if ($value['isWorkTimeRequired'] == 1) {
                    $work_time_count = WorkTime::where(['work_time_date' => date('Y-m-d'), 'admin_id' => $loginInfo['admin_id']])->count();
                    if ($work_time_count <= 0) {
                        return array(
                            'status' => 'error_work_time',
                            'url' => url('admin-panel/project-work-time/add-project-work-time'),
                            'message' => 'Work time not found.',
                        );
                    }
                }
            }

            $oldKey = '';
            $type = $request->get('attendance_type');
            $attendance_register = AttendanceRegister::where(['admin_id' => $loginInfo['admin_id'], 'attendance_date' => date('Y-m-d')])->first();
            if (empty($attendance_register)) {
                return array(
                    'status' => 'error',
                    'message' => 'Your in record not found'
                );
            } else if ($attendance_register->attendance_intime == '') {
                return array(
                    'status' => 'error',
                    'message' => 'You are Not In. Please First Do In.'
                );
            } else if ($attendance_register->attendance_outtime != '') {
                return array(
                    'status' => 'error',
                    'message' => 'You are already out.'
                );
            } else {
                $oldKey = $attendance_register->attendance_key;
            }

            $key = '';
            $total_minute = 0;

            // Get Time Slot time
            $time_slot_time = TimeSlot::where('time_slot_id', $loginInfo['time_slot_id'])->pluck('time_slot_time');
            $office_time = new DateTime($time_slot_time[0]);
            $current_time = new DateTime(date('H:i:s'));
            // Get Time Relaxation value from custom file
            // $minutes_to_add = \Config::get('custom.time_relaxation');
            // $office_time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
            // $diff = date_diff($current_time, $office_time, true);
            // $hours = $diff->format('%h');
            // $minutes = $diff->format('%i');
            //$total_minute_late = ($hours * 60 + $minutes);
            // Get Employee Off Saturdays
            $emp_off_saturday = Team::where(['admin_id' => $loginInfo])->pluck('saturday_off')->first();

            // Out Time Register
            $inTime = $attendance_register->attendance_intime;
            $inTime = new DateTime($inTime);
            // Get Difference between current time and In time
            $diff = date_diff($current_time, $inTime, true);

            // Get Month Holidays
            $holi_arr = [];
            $CompanyHoliday = Event::whereMonth('start_date', date_format($current_time, 'm'))->where('className', 'bg-companyHoliday')->select('title', 'start_date', 'end_date')->get()->toArray();
            foreach ($CompanyHoliday as $key => $value) {
                if (strtotime($value['start_date']) == strtotime($value['end_date'])) {
                    array_push($holi_arr, $value['start_date']);
                } else {
                    $begin = new DateTime($value['start_date']);
                    $end = new DateTime($value['end_date']);
                    $end = $end->modify('+1 day');
                    $interval_day = new DateInterval('P1D');
                    $date_range = new DatePeriod($begin, $interval_day, $end);
                    foreach ($date_range as $date_key => $date_value) {
                        array_push($holi_arr, $date_value->format('Y-m-d'));
                    }
                    array_pop($holi_arr);
                }
            }

            $arr_event_sat = get_arr_saturday_off_from_event();

            if (date_format($current_time, 'l') == 'Sunday') {
                if (!empty($inTime)) {
                    $key = 'sunday_' . get_time_decimal($diff, true);
                }
            } else if (in_array(date_format($current_time, 'Y-m-d'), $holi_arr)) {
                if (!empty($inTime)) {
                    $key = 'CH_' . get_time_decimal($diff, true);
                }
            } else if ((date_format($current_time, 'l') == 'Saturday' && in_array(date_format($current_time, 'Y-m-d'), $arr_event_sat)) && $emp_off_saturday > 0) {
                //$key = 'saturday';
                //} else if (date_format($current_time, 'l') == 'Saturday' && $emp_off_saturday > 0) {
                // $CompanyOffSaturday = Event::whereMonth('start_date', date_format($current_time, 'm'))->where('className', 'bg-saturday')->pluck('start_date')->toArray();
                // if (in_array(, $CompanyOffSaturday)) {
                // if (!empty($inTime)) {
                $key = 'saturday_' . get_time_decimal($diff, true);
                // }
                // } else {
                //     $key = '';
                // }
            } else {
                $office_hours = CompanyConfig::pluck('office_hours')->first();

                // Quarter Day Hours
                $quarter_day = $office_hours * .25;
                $quarter_hour = floor($quarter_day);
                $quarter_day_time = $quarter_hour . ':' . round(60 * ($quarter_day - $quarter_hour));
                //echo "<br>qtr" . $quarter_day_time;
                // Quarter Day Hours
                $third_quarter_day = $office_hours * .75;
                $third_quarter_hour = floor($third_quarter_day);
                $third_quarter_day_time = $third_quarter_hour . ':' . round(60 * ($third_quarter_day - $third_quarter_hour));
                //echo "<br>third-qtr" . $third_quarter_day_time;

                // Half Day Hours                
                $half_day = $office_hours * .50;
                $half_hour = floor($half_day);
                $half_day_time = $half_hour . ':' . round(60 * ($half_day - $half_hour));
                //echo "<br>half" . $half_day_time;

                // Full Day Hours
                $full_hour = floor($office_hours);
                $full_day_time = $full_hour . ':' . round(60 * ($office_hours - $full_hour));
                //echo "<br>full" . $full_day_time;
                // p('end');
                // echo "<br> In - Time<Br>";
                // print_r($inTime);
                // echo "<br> Out- Time<Br>";
                // print_r($current_time);
                // echo "<br> Time Diff" . $diff->h . ':' . $diff->i . ':' . $diff->s;

                if (strtotime($diff->h . ':' . $diff->i . ':' . $diff->s) > strtotime($full_day_time)) {
                    $key = $oldKey;
                } else {
                    if (strtotime($half_day_time) < strtotime($diff->h . ':' . $diff->i . ':' . $diff->s) && strtotime($full_day_time) > strtotime($diff->h . ':' . $diff->i . ':' . $diff->s)) {
                        if (strtotime($third_quarter_day_time) < strtotime($diff->h . ':' . $diff->i . ':' . $diff->s) && strtotime($full_day_time) > strtotime($diff->h . ':' . $diff->i . ':' . $diff->s)) {
                            //$key = "Request for Full Day";
                            return array(
                                'status' => 'success',
                                'message' => 'request_full_day',
                                'optArray' => array(
                                    'half_day' => 'Mark as Half Day',
                                    'request_full' => 'Request for Full Day'
                                )
                            );
                        } else {
                            $key = 'half_day';
                        }
                    } else if (strtotime($quarter_day_time) < strtotime($diff->h . ':' . $diff->i . ':' . $diff->s) && strtotime($half_day_time) > strtotime($diff->h . ':' . $diff->i . ':' . $diff->s)) {
                        //$key = "Request for Half Day";
                        return array(
                            'status' => 'success',
                            'message' => 'request_half_day',
                            'optArray' => array(
                                'full_day' => 'Mark as Absent',
                                'request_half' => 'Request for Half Day'
                            )
                        );
                    } else {
                        $key = "full_day";
                    }
                }
            }
            $hours = $diff->format('%h');
            $minutes = $diff->format('%i');
            $total_minute = ($hours * 60 + $minutes);

            DB::beginTransaction();
            try {
                $attendance_register->admin_id = $loginInfo['admin_id'];
                $attendance_register->attendance_outtime = $current_time;
                $attendance_register->attendance_total_time = $total_minute;
                $attendance_register->attendance_key = $key;
                $attendance_register->save();
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return array(
                    'status' => 'error',
                    'message' => $error_message
                );
            }
            // Insert Log for Out entry
            insert_log_entry(3);
            DB::commit();
            return array(
                'status' => 'success',
                'message' => 'You are Out.'
            );
        }
        return array(
            'status' => 'error',
            'message' => 'Invalid Type.'
        );
    }

    /**
     * Insert Out Time for employee
     * @Bhuvanesh on 11 Feb 2018
     */
    public function registerTimeOutSubmit(Request $request)
    {
        // p($request->all());
        if (isset($request) && $request->get('atten-option') != '') {
            $loginInfo = get_loggedin_user_data();
            $isRequest = false;
            $attendance_register = AttendanceRegister::where(['admin_id' => $loginInfo['admin_id'], 'attendance_date' => date('Y-m-d')])->first();
            if (empty($attendance_register)) {
                return array(
                    'status' => 'error',
                    'message' => 'In record not found'
                );
            } else if ($attendance_register->attendance_intime == '') {
                return array(
                    'status' => 'error',
                    'message' => 'You are Not In. Please First Mark In.'
                );
            } else if ($attendance_register->attendance_outtime != '') {
                return array(
                    'status' => 'error',
                    'message' => 'You are already out.'
                );
            } else {
                $key = '';
                if ($request->get('atten-option') == 'half_day') {
                    $key = 'half_day';
                } else if ($request->get('atten-option') == 'full_day') {
                    $key = 'full_day';
                } else {
                    $key = $attendance_register->attendance_key;
                    $isRequest = true;
                }

                if ($request->get('atten-option') == 'request_half') {
                    $description = '<b>Requested for half day</b><br>' . $request->get('atten-desc');
                } else if ($request->get('atten-option') == 'request_full') {
                    $description = '<b>Requested for full day</b><br>' . $request->get('atten-desc');
                } else {
                    $description = $request->get('atten-desc');
                }
                $total_minute = 0;
                $current_time = new DateTime(date('H:i:s'));

                $inTime = $attendance_register->attendance_intime;
                $inTime = new DateTime($inTime);
                $diff = date_diff($current_time, $inTime, true);

                $hours = $diff->format('%h');
                $minutes = $diff->format('%i');
                $total_minute = ($hours * 60 + $minutes);
            }
            DB::beginTransaction();
            try {
                $attendance_register->attendance_outtime = $current_time;
                $attendance_register->attendance_total_time = $total_minute;
                $attendance_register->attendance_key = $key;
                if ($isRequest == true) {
                    $attendance_register->attendance_isRequest = 1;
                    $attendance_register->isRequest_status = 1;
                }
                $attendance_register->attendance_desc = $description;
                $attendance_register->save();

                $meg_notify = \Config::get('custom.notification_message');
                $data_notif = [
                    'emp_name' => $loginInfo['admin_name'],
                    'message' => $meg_notify['attendance_request']
                ];
                // Notification for Admin
                Team::find(1)->notify(new Leave($data_notif));
                // if ($isRequest == true) {
                //     mailForAdminRequest('attendance', $loginInfo['admin_name'], $loginInfo['email']);
                // }
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return array(
                    'status' => 'error',
                    'message' => $error_message
                );
            }
            // Insert Log for Out entry
            insert_log_entry(3);
            DB::commit();
            return array(
                'status' => 'success',
                'message' => 'You are Out.'
            );
        } else {
            return array(
                'status' => 'error',
                'message' => 'Invalid Type.'
            );
        }
    }

    /**
     *  View page for Team Member Profile
     *  @Bhuvanesh on 22 Dec 2018
     **/
    public function profile()
    {
        $loginInfo = get_loggedin_user_data();
        $team_data = Team::where(function ($query) use ($loginInfo) {
            $query->Where('email', '=', $loginInfo['email']);
        })
            ->with(['getDepartment', 'getTimeSlot'])
            ->first()->toArray();
        $data = array(
            'page_title' => trans('language.profile'),
            'login_info' => $loginInfo,
            'team_data' => $team_data,
        );
        return view('admin-panel.dashboard.profile')->with($data);
    }

    /**
     *  Change password for team member
     *  @Bhuvanesh on 22 Dec 2018
     **/
    public function changePassword()
    {
        $loginInfo = get_loggedin_user_data();
        // p($loginInfo);
        $data = array(
            'page_title' => trans('language.change_password'),
            'login_info' => $loginInfo,
            'save_url' => url('admin-panel/setpassword/'),
            'encrypted_team_id' => get_encrypted_value($loginInfo['admin_id'], true),
            'submit_button' => 'Change Password',
        );
        return view('admin-panel.dashboard.changepassword')->with($data);
    }

    /**
     *  Save password for change request
     *  @Bhuvanesh on 22 Dec 2018
     **/
    public function setPassword(Request $request)
    {
        $passwordValidator = null;
        $team_id = get_loggedin_user_data()['admin_id'];
        $team = Team::where('admin_id', '=', $team_id)->first();

        if (Hash::check($request->get('current_password'), $team['password'])) {
            $values = array(
                'password' => Hash::make($request->get('new_password')),
            );

            $validatior = Validator::make(
                $request->all(),
                [
                    'current_password' => 'required',
                    'new_password' => 'required|different:current_password',
                    'confirm_password' => 'required|same:new_password'
                ],
                [
                    'confirm_password.same' => 'The new password and new confirm password are not the same.',
                    'new_password.different' => "New Password can't be same as current password",
                ]
            );

            if ($validatior->fails()) {
                return redirect()->back()->withInput()->withErrors($validatior);
            } else {
                Team::where('admin_id', $team_id)->update($values);
                $success_msg = 'Password changed successfully!';
                return redirect('admin-panel/profile')->withSuccess($success_msg);
            }
        } else {
            return redirect('admin-panel/changepassword')->withErrors("Incorrect current password. Please try again.");
        }
    }


    public function markAsRead()
    {
        $log_user = get_loggedin_user_data();
        $admin = Team::find($log_user['admin_id']);
        if ($admin->unreadNotifications->markAsRead()) {
            echo true;
        } else {
            echo false;
        }
    }

    public function pendingTask()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title' => trans('language.view_pending_task'),
            'redirect_url' => url('admin-panel/dashboard'),
            'login_info' => $loginInfo,
        );
        return view('admin-panel.dashboard.pending-task')->with($data);
    }

    public function pendingTaskData()
    {
        $loginInfo = get_loggedin_user_data();
        $pending_task = [];
        $pending_reviews = [];
        $pending_reviews = CodeReviewInfo::where(['code_reviewer_id' => $loginInfo['admin_id'], 'code_review_status' => 2])->orderBy('review_upcoming_date')->get()->toArray();
        if (!empty($pending_reviews)) {
            array_push($pending_task, ['task_type' => 'code_review', 'task_name' => 'Code Review', 'pending_date' => $pending_reviews[0]['review_upcoming_date']]);
        }
        return Datatables::of($pending_task)
            ->addColumn('action', function ($pending_task) {
                if ($pending_task['task_type'] == 'code_review') {
                    $restUrl = 'codereview-projects/view-codereview';
                } else {
                    $restUrl = 'dashboard';
                }
                return '<a href="' . url('admin-panel') . '/' . $restUrl . '"><div class="label label-table label-info">Action</div></a>';
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }
}
