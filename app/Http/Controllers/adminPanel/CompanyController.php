<?php

namespace App\Http\Controllers\adminPanel;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Model\Company\CompanyConfig;
use App\Model\Company\HolidayCalendar;
use App\Model\Company\Event;
use App\Model\Attendance\AttendanceRegister;
use Yajra\Datatables\Datatables;
use function GuzzleHttp\json_encode;

class CompanyController extends Controller
{
    /**
     * Constructor for Company Config Controller 
     **/
    public function __construct()
    {
        // $this->middleware('isadmin');
        // $this->middleware('user')->only('index');
        $this->middleware('isadmin')->except('getCalendarEvents');
    }

    /**
     *  View page for Save Company Details
     *  @Bhuvanesh on 2 Jan 2019
     **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $company_config = CompanyConfig::first();
        if (!$company_config) {
            return redirect('admin-panel/configuartion/view-configuartion')->withError('Configuration not found!');
        }
        $page_title = trans('language.configuration');
        $encrypted_company_config_id = get_encrypted_value($company_config->company_config_id, true);
        $save_url = url('admin-panel/configuartion/save/' . $encrypted_company_config_id);
        $submit_button = 'Update';

        $data = array(
            'page_title' => $page_title,
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'company_config' => $company_config,
            'login_info' => $loginInfo,
        );
        return view('admin-panel.company.edit')->with($data);
    }

    /**
     *  Save Company Config data
     *  @Bhuvanesh on 2 Jan 2019
     **/
    public function save(Request $request, $id = null)
    {
        $loginInfo = get_loggedin_user_data();
        $decrypted_company_config_id = null;
        $decrypted_company_config_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $company_config = CompanyConfig::find($decrypted_company_config_id);
            if (!$company_config) {
                return redirect('admin-panel/configuartion/view-configuartion/')->withError('Configuration not found!');
            }
            $success_msg = 'Configuration updated successfully!';
        } else {
            return redirect('admin-panel/configuartion/view-configuartion/')->withError('Invalid Configuration!');
        }
        DB::beginTransaction();
        try {
            $company_config->office_hours = Input::get('office_hours');
            $company_config->lunch_hours = Input::get('lunch_hours');
            $company_config->direct_half_time = Input::get('direct_half_time');
            $company_config->late_half_days = Input::get('late_half_days');
            $company_config->save();
        } catch (\Exception $e) {
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        return redirect('admin-panel/configuartion/view-configuartion')->withSuccess($success_msg);
    }

    /**
     *  Data for Calendar events
     *  @Bhuvanesh on 12 Jan 2019
     **/
    public function getCalendarEvents(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $data = [];
        // $event_counter = 1;
        // $attendance_register = [];
        $events = Event::where(function ($query) use ($loginInfo) {
            if ($loginInfo['type'] == '2' && $loginInfo['saturday_off'] == 0) {
                $query->where('className', '!=', 'bg-saturday');
            }
        })->get();

        if (!$events) {
            return $data;
        } else {
            // $event_counter = 0;
            foreach ($events as $event) {
                // $event_counter++;
                array_push(
                    $data,
                    [
                        'event_id' => $event->event_id,
                        'title' => $event['title'],
                        'start' => $event['start_date'],
                        'end' => $event['end_date'],
                        'className' => $event['className']
                    ]
                );
            }
        }
        // if ($loginInfo['type'] == '2') {
        //     $attendance_register = AttendanceRegister::where(function ($query) use ($loginInfo) {
        //         if ($loginInfo['type'] == '2' && $loginInfo['saturday_off'] == 0) {
        //             $query->where('admin_id', $loginInfo['admin_id']);
        //         }
        //     })->select()->get()->toArray();
        //     foreach ($attendance_register as $key => $value) {
        //         array_push(
        //             $data,
        //             [
        //                 'event_id' => $event_counter++,
        //                 'title' => '',
        //                 'start' => $value['attendance_date'],
        //                 'end' => $value['attendance_date'],
        //                 'className' => 'bg-saturday',
        //                 'desc' => $value['attendance_key']
        //             ]
        //         );
        //     }
        // }
        return $data;
    }

    /*
     * Save calendar
     * @Bhuvanesh on 12 Jan 2019
     */
    public function saveCalendar(Request $request)
    {
        $error_message = '';
        if (!empty($request->get('events'))) {
            $events = $request->get('events');
            foreach ($events as $event) {
                if (isset($event['eventId'])) {
                    $get_event = Event::Find($event['eventId']);
                    if (!$get_event) {
                        continue;
                    }
                } else {
                    $get_event = new Event;
                }
                DB::beginTransaction();
                try {
                    $get_event->title = $event['title'];
                    $get_event->eventType = 0;
                    $get_event->start_date = $event['start'];
                    if ($event['end'] != 'Invalid date') {
                        $get_event->end_date = $event['end'];
                    } else {
                        $get_event->end_date = $event['start'];
                    }
                    $get_event->className = $event['className'][0];
                    $get_event->save();
                } catch (\Exception $e) {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                    return ['status' => 1, 'error' => $error_message];
                }
                DB::commit();
            }
            return ['status' => 0, 'success' => 'Calendar Updated Successfully'];
        }
        return ['status' => 0, 'success' => 'No events found'];
    }

    /**
     *  Destroy Event from Calendar
     *  @Bhuvanesh on 12 jan 2019
     **/
    public function destroyCalendar(Request $request)
    {
        $event = Event::find($request->get('event_id'));
        if ($event) {
            DB::beginTransaction();
            try {
                $event->delete();
                $success_msg = "Event deleted successfully!";
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
            }
            return ['status' => 0, 'success' => $success_msg];
        } else {
            $error_message = "Event not found!";
            return ['status' => 1, 'error' => $error_message];
        }
    }
}
