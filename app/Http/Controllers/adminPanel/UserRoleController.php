<?php

namespace App\Http\Controllers\adminPanel;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Model\UserRole\UserRole;
use App\Model\UserRole\UserRoleResponsibilityTrans;
use App\Model\Project\WorkType;
use Yajra\Datatables\Datatables;

class UserRoleController extends Controller
{
    /**
     * Constructor for UserRole Controller 
    **/
    public function __construct()
    {
        $this->middleware('isadmin');
        // $this->middleware('user')->only('index');
        // $this->middleware('subscribed')->except('store');
    }

    /**
     *  View page for userrole
     *  @Bhuvanesh on 30 Jan 2019
    **/
    public function index(Request $request, $id = null)
    {
        $data                  =   [];
        $user_role            =   [];
        $loginInfo               =   get_loggedin_user_data();
        if (!empty($id)) {
            $decrypted_user_role_id         = get_decrypted_value($id, true);
            $user_role                      = UserRole::Find($decrypted_user_role_id);
            if (!$user_role) {
                return redirect('admin-panel/userrole/view-userrole')->withError('User Role not found!');
            }
            $page_title                 = trans('language.edit_user_role');
            $encrypted_user_role_id     = get_encrypted_value($user_role->user_role_id, true);
            $save_url                   = url('admin-panel/userrole/save/' . $encrypted_user_role_id);
            $submit_button              = 'Update';
        } else {
            $page_title      = trans('language.add_user_role');
            $save_url        = url('admin-panel/userrole/save');
            $submit_button   = 'Save';
        }

        $data = array(
            'page_title'      => trans('language.view_user_role'),
            'save_url'        => $save_url,
            'submit_button'   => $submit_button,
            'user_role'       => $user_role,
            'redirect_url'    => url('admin-panel/userrole/view-userrole'),
            'login_info'      => $loginInfo
        );
        return view('admin-panel.user-role.index')->with($data);
    }
    /**
     *  Get Data for view page(Datatables)
     *  @Bhuvanesh on 30 Jan 2019
    **/

    public function anyData(Request $request)
    {
        $user_role     =    [];
        $searchKey     =    null;
        $user_role     =    UserRole::orderBy('user_role_id', 'desc')->get()->toArray();
        return Datatables::of($user_role)
            ->addColumn('assignResponsibility', function ($user_role) {
                $encrypted_user_role_id = get_encrypted_value($user_role['user_role_id'], true);
                if ($user_role['user_role_status'] == '1') {
                    return '<a href="' . url('admin-panel/userrole/assign-responsibility/' . $encrypted_user_role_id) . '"><div class="label label-table label-info">Assign Responsibility</div></a>';
                } else {
                    return '<button class="btn btn-default disable-url label label-table label-info disabled">Assign Responsibility</button>';
                }
            })
            ->addColumn('isWorkTimeRequired', function ($user_role) {
                if ($user_role['isWorkTimeRequired'] == 1) {
                    return 'Yes';
                } else {
                    return 'No';
                }
            })
            ->addColumn('action', function ($user_role) {
                $encrypted_user_role_id = get_encrypted_value($user_role['user_role_id'], true);
                if ($user_role['user_role_status'] == 0) {
                    $status = 1;
                    $statusVal = '<div class="radio radio-danger custom_radiobox"  data-toggle="tooltip" title="Change Status">
                    <input type="radio" name="radio' . $user_role['user_role_id'] . '" onClick="changeStatus(' . $user_role['user_role_id'] . ',' . $status . ')" id="radio' . $user_role['user_role_id'] . '" value="option4" checked="">
                    <label for="radio' . $user_role['user_role_id'] . '"> </label>
                </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="radio radio-success custom_radiobox" data-toggle="tooltip" title="Change Status">
                        <input type="radio" name="radio' . $user_role['user_role_id'] . '" onClick="changeStatus(' . $user_role['user_role_id'] . ',' . $status . ')" id="radio' . $user_role['user_role_id'] . '" value="option4" checked="">
                        <label for="radio' . $user_role['user_role_id'] . '"> </label>
                    </div>';
                }
                $statusVal .= '
                &nbsp;
                <a href="' . url('admin-panel/userrole/view-userrole/' . $encrypted_user_role_id) . '"data-toggle="tooltip" title="Edit User Role"><i class="fas fa-pencil-alt"></i></a>
                &nbsp;&nbsp;';
                if ($user_role['isfixed'] == 0) {
                    $statusVal .=
                        '<a href="' . url('admin-panel/userrole/userrole-delete/' . $encrypted_user_role_id) . '" onclick="return confirm(' . "'Are you sure?'" . ')" " data-toggle="tooltip" title="Delete User Role"><i class="fas fa-trash"></i></a> ';
                }
                return $statusVal;
            })->rawColumns(['assignResponsibility' => 'assignResponsibility', 'action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Save User Role data
     *  @Bhuvanesh on 30 Jan 2019
    **/
    public function save(Request $request, $id = null)
    {
        $loginInfo              = get_loggedin_user_data();
        $decrypted_user_role_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $user_role      =   UserRole::find($decrypted_user_role_id);
            if (!$user_role) {
                return redirect('admin-panel/userrole/view-userrole/')->withError('User Role not found!');
            }
            $success_msg    = 'User Role updated successfully!';
        } else {
            $user_role      =   new UserRole;
            $success_msg    =   'User Role saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'user_role_name'          => 'required|unique:user_roles,user_role_name,' . $decrypted_user_role_id . ',user_role_id|max:255',
        ]);
        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction();
            try {
                $user_role->user_role_name           = Input::get('user_role_name');
                if ($request->has('isWorkTimeRequired')) {
                    $user_role->isWorkTimeRequired = 1;
                } else {
                    $user_role->isWorkTimeRequired = 0;
                }
                $user_role->save();
                // p($user_role);
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/userrole/view-userrole')->withSuccess($success_msg);
    }

    /**
     *  Change User Role status
     *  @Bhuvanesh on 30 Jan 2019
    **/
    public function changeStatus(Request $request)
    {
        $id          =  $request->get('user_role_id');
        $status      =  $request->get('user_role_status');
        $user_role   =  UserRole::find($id);
        if ($user_role) {
            $user_role->user_role_status = $status;
            $user_role->save();
            echo "Success";
        } else {
            echo "Fail";
        }
    }

    /**
     *  Destroy User Role data
     *  @Bhuvanesh on 30 Jan 2019
    **/
    public function destroy($id)
    {
        $user_role_id  = get_decrypted_value($id, true);
        $user_role     = WorkType::find($user_role_id);
        if ($user_role) {
            DB::beginTransaction();
            try {
                $user_role->delete();
                $success_msg = "User Role deleted successfully!";
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors("It's already in use. We can't delete this.");
            }
            return redirect('admin-panel/userrole/view-userrole')->withSuccess($success_msg);
        } else {
            $error_message = "User Role not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Assign Responsibility 
     *  @Bhuvanesh on 30 Jan 2019
    **/
    public function assignResponsibility($id = null)
    {
        $user_role          =  [];
        $user_role_status   =  \Config::get('custom.user_role_status');
        $loginInfo             =  get_loggedin_user_data();
        $user_role_id       =  get_decrypted_value($id, true);
        $selected_responsibility   =  [];
        if ($user_role_id != '') {
            $user_role    = UserRole::where('user_role_id', $user_role_id)->with('getResponsibility')->first();
            $AssignList = isset($user_role['getResponsibility'][0]) ? $user_role['getResponsibility'] : [];
            if (!empty($AssignList)) {
                foreach ($AssignList as $value) {
                    $selected_responsibility[]  =  $value['work_type_id'];
                }
            }
        } else {
            return redirect('admin-panel/userrole/view-userrole')->withError('User Role not found!');
        }
        $responsibility_list       =   WorkType::get();
        $responsibility_list_arr   =   [];
        if (!empty($responsibility_list)) {
            foreach ($responsibility_list as $value) {
                $responsibility_list_arr[$value['work_type_id']] = $value['work_type_name'];
            }
        }
        $page_title       =   trans('language.assign_responsibility');
        $submit_button    =   'Save';
        $save_url         =   url('admin-panel/userrole/assign-responsibility-save');
        $data   = array(
            'page_title'         => $page_title,
            'save_url'           => $save_url,
            'submit_button'      => $submit_button,
            'user_role_status'   => $user_role_status,
            'user_role_id'       => $id,
            'user_role'          => $user_role,
            'login_info'         => $loginInfo,
            'redirect_url'       => url('admin-panel/userrole/view-userrole'),
            'selected_responsibility'   =>   $selected_responsibility,
            'responsibility_list_arr'   => $responsibility_list_arr,
        );
        return view('admin-panel.user-role.responsibility-assign.add')->with($data);
    }

    /**
     *  Assign Responsibility Save 
     *  @Bhuvanesh on 30 Jan 2019
    **/
    public function assignResponsibilitySave(Request $request)
    {
        $loginInfo       =  get_loggedin_user_data();
        $user_role       =  [];
        $user_role_id    =  get_decrypted_value($request->get('user_role_id'), true);
        if (!empty($user_role_id)) {
            $user_role              =  UserRole::find($user_role_id);
            if (!$user_role) {
                return redirect('admin-panel/userrole/view-userrole/')->withError('User Role not found!');
            }
            $success_msg = 'Responsibility assign updated successfully!';
        } else {
            return redirect('admin-panel/userrole/view-userrole/')->withError('User Role not found!');
        }
        $validatior = Validator::make($request->all(), [
            'selected-responsibility'    => 'required',
        ]);
        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction();
            try {
                $respo_arr = Input::get('selected-responsibility');
                UserRoleResponsibilityTrans::where('user_role_id', $user_role->user_role_id)->delete();
                foreach ($respo_arr as $value) {
                    $respo_assign                =  new UserRoleResponsibilityTrans();
                    $respo_assign->user_role_id  =  $user_role->user_role_id;
                    $respo_assign->work_type_id  =  $value;
                    $respo_assign->save();
                }
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/userrole/view-userrole')->withSuccess($success_msg);
    }
}
