<?php

namespace App\Http\Controllers\adminPanel;

use View;
use Config;
use Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Model\Team\Team;
use App\Model\Project\Project;
use App\Model\Project\ProjectAssign;
use App\Model\Project\ReviewCheckList;
use App\Model\Project\WorkTime;
use App\Model\Project\WorkType;
use App\Model\UserRole\AssignUserRole;
use App\Model\UserRole\UserRoleResponsibilityTrans;
//use Illuminate\Support\Facades\Request;

class ProjectController extends Controller
{
    /**
     * Constructor for Project Controller 
     **/
    public function __construct()
    {
        $this->middleware('isadmin')->except('projectList', 'projectData');
        $this->middleware('ismember')->only('projectList', 'projectData');
    }

    /**
     *  View page for Project
     *  @Bhuvanesh on 24 Dec 2018
     **/
    public function index()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title' => trans('language.view_project'),
            'redirect_url' => url('admin-panel/project/view-project'),
            'login_info' => $loginInfo
        );
        return view('admin-panel.project.index')->with($data);
    }
    /**
     *  Get Data for view page(Datatables)
     *  @Bhuvanesh on 24 Dec 2018
     **/

    public function anyData(Request $request)
    {
        $project = [];
        $searchKey = null;
        $project = Project::where(function ($query) use ($request) {
            if (!empty($request->get('project_name'))) {
                $query->where('project_name', "LIKE", '%' . $request->get('project_name') . '%');
            }
            if ($request->get('project_status') != 2) {
                $query->where('status', $request->get('project_status'));
            }
        })->orderBy('project_id', 'desc')->get()->toArray();
        return Datatables::of($project)
            ->addColumn('assign_codereviewer', function ($project) {
                $encrypted_project_id = get_encrypted_value($project['project_id'], true);
                return '
                <a href="' . url('admin-panel/assign-codereviewer/' . $encrypted_project_id) . '" data-toggle="tooltip" title="Assign Code Reviewer"><div class="label label-table label-info">Assign Reviewer</div></a>';
            })
            ->addColumn('assign_project', function ($project) {
                $encrypted_project_id = get_encrypted_value($project['project_id'], true);
                if ($project['status'] == '1') {
                    return '<a href="' . url('admin-panel/project/assign-project/' . $encrypted_project_id) . '"><div class="label label-table label-info">Assign Developer</div></a>';
                } else {
                    return '<button class="btn btn-default disable-url label label-table label-info disabled">Assign Developer</button>';
                }
            })
            ->addColumn('action', function ($project) {
                $encrypted_project_id = get_encrypted_value($project['project_id'], true);
                if ($project['status'] == 0) {
                    $status = 1;
                    $statusVal = '<div class="radio radio-danger custom_radiobox">
                    <input type="radio" name="radio' . $project['project_id'] . '" onClick="changeStatus(' . $project['project_id'] . ',' . $status . ')" id="radio' . $project['project_id'] . '" value="option4" checked="">
                    <label for="radio' . $project['project_id'] . '"> </label>
                </div>';
                } else {
                    $status = 0;
                    $statusVal = '<div class="radio radio-success custom_radiobox">
                        <input type="radio" name="radio' . $project['project_id'] . '" onClick="changeStatus(' . $project['project_id'] . ',' . $status . ')" id="radio' . $project['project_id'] . '" value="option4" checked="">
                        <label for="radio' . $project['project_id'] . '"> </label>
                    </div>';
                }
                return '
                ' . $statusVal .
                    '&nbsp;
                <a href="' . url('admin-panel/project/add-project/' . $encrypted_project_id) . '"data-toggle="tooltip" title="Edit Project"><i class="fas fa-pencil-alt"></i></a>
                &nbsp;&nbsp;';
                // <a href="'. url('admin-panel/project/project-delete/' . $encrypted_project_id ) .'" onclick="return confirm('."'Are you sure?'".')" " data-toggle="tooltip" title="Delete Project"><i class="fas fa-trash"></i></a> ';       
            })->rawColumns(['assign_project' => 'assign_project', 'assign_codereviewer' => 'assign_codereviewer', 'project_status' => 'project_status', 'action' => 'action'])->addIndexColumn()->make(true);
    }

    /**
     *  Add page for Project
     *  @Bhuvanesh on 24 Dec 2018
     **/
    public function add(Request $request, $id = null)
    {
        $project = [];
        $loginInfo = get_loggedin_user_data();
        $owner_list = [];
        $manager_list = [];
        if (!empty($id)) {
            $decrypted_project_id = get_decrypted_value($id, true);
            $project = Project::Find($decrypted_project_id);
            if (!$project) {
                return redirect('admin-panel/project/add-project')->withError('Project not found!');
            }
            $page_title = trans('language.edit_project');
            $encrypted_project_id = get_encrypted_value($project->project_id, true);
            $save_url = url('admin-panel/project/save/' . $encrypted_project_id);
            $submit_button = 'Update';
        } else {
            $page_title = trans('language.add_project');
            $save_url = url('admin-panel/project/save');
            $submit_button = 'Save';
        }

        $emp_list = Team::where(['status' => '1', 'is_terminate' => '0', 'is_resign' => '0'])->whereHas('getUserRole', function ($query) {
            $query->where('user_role_id', '1');
        })->orderBy('admin_name', 'ASC')->get()->toArray();
        if (!empty($emp_list)) {
            foreach ($emp_list as $value) {
                $owner_list[$value['admin_id']] = $value['admin_name'];
            }
        }
        $owner_list = add_blank_option($owner_list, 'Select Owner');
        $emp_list = Team::where(['status' => '1', 'is_terminate' => '0', 'is_resign' => '0'])->whereHas('getUserRole', function ($query) {
            $query->where('user_role_id', '2');
        })->orderBy('admin_name', 'ASC')->get()->toArray();
        if (!empty($emp_list)) {
            foreach ($emp_list as $value) {
                $manager_list[$value['admin_id']] = $value['admin_name'];
            }
        }
        $manager_list = add_blank_option($manager_list, 'Select Manager');

        $data = array(
            'page_title' => $page_title,
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'owner_list' => $owner_list,
            'manager_list' => $manager_list,
            'project' => $project,
            'login_info' => $loginInfo,
            'redirect_url' => url('admin-panel/project/view-project'),
        );

        return view('admin-panel.project.add')->with($data);
    }

    /**
     *  Save Project data
     *  @Bhuvanesh on 24 Dec 2018
     **/
    public function save(Request $request, $id = null)
    {
        $loginInfo = get_loggedin_user_data();
        $decrypted_project_id = get_decrypted_value($id, true);
        if (!empty($id)) {
            $project = Project::find($decrypted_project_id);
            $project_details_arr = $project['get_project_details'];
            if (!$project) {
                return redirect('admin-panel/project/view-project/')->withError('Project not found!');
            }
            $success_msg = 'Project updated successfully!';
        } else {
            $project = new Project;
            $success_msg = 'Project saved successfully!';
        }
        $validatior = Validator::make($request->all(), [
            'project_name' => 'required|unique:projects,project_name,' . $decrypted_project_id . ',project_id|max:255',
            'project_owner' => 'required',
            'project_status' => 'required',
            'project_start_date' => 'required',
        ]);
        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction();
            try {
                $project->project_name = Input::get('project_name');
                $project->project_owner = Input::get('project_owner');
                $project->project_manager = Input::get('project_manager');
                $project->status = Input::get('project_status');
                $project->project_start_date = Input::get('project_start_date');
                $project->project_end_date = Input::get('project_end_date');
                $project->project_description = Input::get('project_description');
                $project->save();
                $platform_arr = Input::get('project_platform');
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/project/view-project')->withSuccess($success_msg);
    }

    /**
     *  Assign Project 
     *  @Bhuvanesh on 25 Jan 2018
     **/
    public function assignProject($id = null)
    {
        $project = [];
        $loginInfo = get_loggedin_user_data();
        $project_id = get_decrypted_value($id, true);
        $project_status = Config::get('custom.project_status');
        $selected_developer = [];
        if ($project_id != '') {
            $project = Project::where('project_id', '=', $project_id)->with('getProjectAssign.getDeveloper')->first();
            $AssignList = isset($project['getProjectAssign'][0]) ? $project['getProjectAssign'] : [];
            if (!empty($AssignList)) {
                foreach ($AssignList as $value) {
                    $selected_developer[] = $value['admin_id'];
                }
            }
        } else {
            return redirect('admin-panel/project/view-project')->withError('Project not found!');
        }
        $team_list = Team::select('admin_id', 'admin_name')->where(['status' => '1', 'type' => '2', 'is_terminate' => '0', 'is_resign' => '0'])->orderBy('admin_name')->get();
        $developer_list = [];
        if (!empty($team_list)) {
            foreach ($team_list as $value) {
                $developer_list[$value['admin_id']] = $value['admin_name'];
            }
        }
        $page_title = trans('language.project_assign');
        $submit_button = 'Save';
        $save_url = url('admin-panel/project/assign-project-save');
        $data = array(
            'page_title' => $page_title,
            'save_url' => $save_url,
            'submit_button' => $submit_button,
            'project_id' => $id,
            'project' => $project,
            'selected_developer' => $selected_developer,
            'project_status' => $project_status,
            'developer_list' => $developer_list,
            'login_info' => $loginInfo,
            'redirect_url' => url('admin-panel/project/view-project'),
        );
        return view('admin-panel.project.project-assign.add')->with($data);
    }

    /**
     *  Assign Project Save 
     *  @Bhuvanesh on 25 Jan 2018
     **/
    public function assignProjectSave(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $project = [];
        $project_id = get_decrypted_value($request->get('project_id'), true);
        if (!empty($project_id)) {
            $project = Project::find($project_id);
            if (!$project) {
                return redirect('admin-panel/project/view-project/')->withError('Project not found!');
            }
            $success_msg = 'Project assign updated successfully!';
        } else {
            return redirect('admin-panel/project/view-project/')->withError('Project not found!');
        }
        $validatior = Validator::make($request->all(), [
            'selected_developer' => 'required',
        ]);
        if ($validatior->fails()) {
            return redirect()->back()->withInput()->withErrors($validatior);
        } else {
            DB::beginTransaction();
            try {
                $developer_arr = Input::get('selected_developer');
                ProjectAssign::where('project_id', '=', $project->project_id)->update(['project_assign_status' => '0']);
                foreach ($developer_arr as $value) {
                    $project_assign = ProjectAssign::where(['project_id' => $project->project_id, 'admin_id' => $value])->first();
                    if (!empty($project_assign)) {
                        $project_assign->project_assign_status = '1';
                    } else {
                        $project_assign = new ProjectAssign();
                        $project_assign->project_id = $project->project_id;
                        $project_assign->admin_id = $value;
                    }
                    $project_assign->save();
                }
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/project/view-project')->withSuccess($success_msg);
    }

    /**
     *  Destroy Project data
     *  @Bhuvanesh on 24 Dec 2018
     **/
    public function destroy($id)
    {
        $project_id = get_decrypted_value($id, true);
        $project = Project::find($project_id);
        if ($project) {
            DB::beginTransaction();
            try {
                $project->delete();
                $success_msg = "Project deleted successfully!";
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors("It's already in use. We can't delete this.");
            }
            return redirect('admin-panel/project/view-project')->withSuccess($success_msg);
        } else {
            $error_message = "Project not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    /**
     *  Change Project status
     *  @Bhuvanesh on 30 Jan 2019
     **/
    public function changeStatus(Request $request)
    {
        $id = $request->get('project_id');
        $status = $request->get('project_status');
        $project = Project::find($id);
        if ($project) {
            $project->status = $status;
            $project->save();
            echo "Success";
        } else {
            echo "Fail";
        }
    }

    /**
     * Report for admin employee wise 
     * @Bhuvanesh on 01 Feb 2019
     **/
    public function employeeWiseReport(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $employee_list = [];
        $project_list = [];
        $work_type_list = [];
        $employee_arr = Team::select('admin_id', 'admin_name')->where(['type' => '2', 'status' => '1', 'is_terminate' => '0', 'is_resign' => '0'])->get()->toArray();
        if (!empty($employee_arr)) {
            foreach ($employee_arr as $key => $value) {
                $employee_list[$value['admin_id']] = $value['admin_name'];
            }
        }
        $employee_list = add_blank_option($employee_list, 'Select Employee');
        $project_list = add_blank_option($project_list, 'Select Project');
        $work_type_list = add_blank_option($work_type_list, 'Select Work Type');
        $data = array(
            'page_title' => trans('language.report_employee'),
            'employee_list' => $employee_list,
            'project_list' => $project_list,
            'work_type_list' => $work_type_list,
            'login_info' => $loginInfo,
        );
        return view('admin-panel.report.employee')->with($data);
    }

    /**
     * Data for admin employee wise 
     * @Bhuvanesh on 01 Feb 2019
     **/
    public function employeeWiseReportData(Request $request)
    {
        $report_data = [];
        if (isset($request) && $request->get('employee_name') != '') {
            $employee_id = $request->get('employee_name');
            $date_range = explode('-', $request->get('report_date'));
            $date_create = date_create($date_range[0]);
            $start_date = date_format($date_create, 'Y-m-d');
            $date_create = date_create($date_range[1]);
            $end_date = date_format($date_create, 'Y-m-d');
            if ($request->get('report_type') == 1) {
                $query = DB::table('work_times as wt')
                    ->where('admin_id', $employee_id)
                    ->whereBetween('work_time_date', [$start_date, $end_date])
                    ->join('work_time_details as wtd', 'wt.work_time_id', '=', 'wtd.work_time_id')
                    ->join('projects as p', 'wtd.project_id', '=', 'p.project_id')
                    ->join('work_types', 'wtd.work_type_id', '=', 'work_types.work_type_id');
                if ($request->get('project_id') != '') {
                    $query->where('wtd.project_id', $request->get('project_id'));
                }
                if ($request->get('work_type_id') != '') {
                    $query->where('wtd.work_type_id', $request->get('work_type_id'));
                }
                $report_data = $query->groupBy('wtd.work_type_id')
                    ->groupBy('wtd.project_id')
                    ->orderBy('wtd.project_id')
                    ->select(
                        DB::raw('sum(work_time_detail_hrs) as total_hour'),
                        'wtd.work_time_detail_id',
                        'wtd.project_id',
                        'wtd.work_type_id',
                        'work_types.work_type_name',
                        'p.project_name'
                    )
                    ->get()
                    ->toArray();
                if (!empty($report_data)) {
                    $d = json_encode($report_data, true);
                    $q = json_decode($d, true);
                    $final_array = [];
                    $inc = 0;
                    $final_array_footer = [];
                    foreach ($q as $key => $value) {
                        $k = array_search($value['project_id'], array_column($q, 'project_id'));
                        $final_array[$value['project_id']]['project_name'] = $value['project_name'];
                        $final_array[$value['project_id']]['details'][] = $value;
                        // $final_array[$inc++] = $value;
                        $final_array_footer[$value['work_type_id']]['work_type_name'] = $value['work_type_name'];
                        $final_array_footer[$value['work_type_id']]['time'][] = $value;
                    }
                    $data = array(
                        'summeryByWorkType' => $final_array_footer,
                        'final_array' => $final_array,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                    );
                    // p($final_array_footer);
                    // $final_array_foot = [];
                    $preview = View::make('admin-panel.report.report-preview')->with($data)->render();
                    if (!empty($preview)) {
                        return response()->json(['status' => 'success', 'data' => $preview]);
                    } else {
                        return response()->json(['status' => 'error', 'data' => $preview]);
                    }
                } else {
                    return response()->json(['status' => 'error', 'data' => '']);
                }
            } else {
                $query = DB::table('work_times as wt')
                    ->where('admin_id', $employee_id)
                    ->whereBetween('work_time_date', [$start_date, $end_date])
                    ->join('work_time_details as wtd', 'wt.work_time_id', '=', 'wtd.work_time_id')
                    ->join('projects as p', 'wtd.project_id', '=', 'p.project_id')
                    ->join('work_types', 'wtd.work_type_id', '=', 'work_types.work_type_id');
                if ($request->get('project_id') != '') {
                    $query->where('wtd.project_id', $request->get('project_id'));
                }
                if ($request->get('work_type_id') != '') {
                    $query->where('wtd.work_type_id', $request->get('work_type_id'));
                }
                $report_data = $query->orderBy('wt.work_time_date', 'desc')
                    ->get()
                    ->toArray();
                if (!empty($report_data)) {
                    $d = json_encode($report_data, true);
                    $q = json_decode($d, true);
                    $final_array = [];
                    $inc = 0;
                    foreach ($q as $key => $value) {
                        $final_array[$value['work_time_date']]['work_time_date'] = $value['work_time_date'];
                        $final_array[$value['work_time_date']]['details'][] = $value;
                    }
                    foreach ($final_array as $key => $value) {
                        foreach ($value['details'] as $detail_key => $detail_value) {
                            $k = array_search($detail_value['project_name'], array_column($value['details'], 'project_name'));
                            $super_array[$value['work_time_date']]['work_time_date'] = $value['work_time_date'];
                            $super_array[$value['work_time_date']]['count'] = count($value['details']);
                            $super_array[$value['work_time_date']]['details'][$detail_value['project_id']]['project_name'] = $detail_value['project_name'];
                            $super_array[$value['work_time_date']]['details'][$detail_value['project_id']]['details'][] = $detail_value;
                        }
                    }
                    // p($super_array);
                    // p($final_array);
                    $data = array(
                        'final_array' => $super_array,
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                    );
                    // p($final_array_footer);
                    $preview = View::make('admin-panel.report.day-wise-report-preview')->with($data)->render();
                    if (!empty($preview)) {
                        return response()->json(['status' => 'success', 'data' => $preview]);
                    } else {
                        return response()->json(['status' => 'error', 'data' => $preview]);
                    }
                } else {
                    return response()->json(['status' => 'error', 'data' => '']);
                }
            }
        } else {
            return response()->json(['status' => 'error', 'data' => '']);
        }
    }

    /**
     * Get Project list of employee 
     * @Bhuvanesh on 01 Feb 2019
     **/
    public function getEmployeeProjects(Request $request)
    {
        $assignProject = [];
        $work_type_arr = [];
        if (!empty($request) && $request->get('employee_id') != '') {
            $project_list = ProjectAssign::where('admin_id', $request->get('employee_id'))->with('getProject')->get()->toArray();
            foreach ($project_list as $project) {
                $assignProject[$project['project_id']] = $project['get_project']['project_name'];
            }
            // Work Type List
            $assignRoles = AssignUserRole::where(['admin_id' => $request->get('employee_id'), 'assign_user_role_status' => '1'])->with('getUserRole.getResponsibility.getWorkType')->get()->toArray();
            foreach ($assignRoles as $getRole) {
                if (!empty($getRole['get_user_role'])) {
                    if (!empty($getRole['get_user_role']['get_responsibility'])) {
                        foreach ($getRole['get_user_role']['get_responsibility'] as $respo) {
                            if (!empty($respo)) {
                                $work_type_arr[$respo['get_work_type']['work_type_id']] = $respo['get_work_type']['work_type_name'];
                            }
                        }
                    }
                }
            }
        }
        if (!empty($assignProject)) {
            return ['projects' => $assignProject, 'work_types' => $work_type_arr, 'status' => 'success'];
        } else {
            return ['projects' => $assignProject, 'work_types' => $work_type_arr, 'status' => 'error'];
        }
    }

    /**
     * Report for admin project wise 
     * @Bhuvanesh on 01 Feb 2019
     **/
    public function projectWiseReport(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $project_list = [];
        $project_arr = Project::select('project_id', 'project_name')->get()->toArray();
        if (!empty($project_arr)) {
            foreach ($project_arr as $key => $value) {
                $project_list[$value['project_id']] = $value['project_name'];
            }
        }
        $project_list = add_blank_option($project_list, 'Select Project');
        $data = array(
            'page_title' => trans('language.report_project'),
            'project_list' => $project_list,
            'login_info' => $loginInfo,
        );
        return view('admin-panel.report.project')->with($data);
    }

    /**
     * Data for admin project wise 
     * @Bhuvanesh on 01 Feb 2019
     **/
    public function projectWiseReportData(Request $request)
    {
        // p($request->all());
        $report_data = [];
        if (isset($request) && $request->get('project_name') != '') {
            $project_id = $request->get('project_name');
            $date_range = explode('-', $request->get('report_date'));
            $date_create = date_create($date_range[0]);
            $start_date = date_format($date_create, 'Y-m-d');
            $date_create = date_create($date_range[1]);
            $end_date = date_format($date_create, 'Y-m-d');
            $report_data = DB::table('work_times as wt')
                ->where('wtd.project_id', $project_id)
                ->whereBetween('work_time_date', [$start_date, $end_date])
                ->join('work_time_details as wtd', 'wt.work_time_id', '=', 'wtd.work_time_id')
                ->join('admins as a', 'wt.admin_id', '=', 'a.admin_id')
                ->join('work_types', 'wtd.work_type_id', '=', 'work_types.work_type_id')
                ->groupBy('wtd.work_type_id')
                ->groupBy('wt.admin_id')
                ->orderBy('wtd.project_id')
                ->select(
                    DB::raw('sum(work_time_detail_hrs) as total_hour'),
                    DB::raw('count(work_time_detail_hrs) as involvement_days'),
                    'wtd.work_time_detail_id',
                    'wtd.project_id',
                    'wtd.work_type_id',
                    'a.admin_id',
                    'work_types.work_type_id',
                    'work_types.work_type_name',
                    'a.admin_name'
                )
                ->get()
                ->toArray();
            $employee_arr = Team::where(['type' => '2', 'status' => '1', 'is_terminate' => '0', 'is_resign' => '0'])
                ->get()
                ->pluck('admin_id');
            $summary_emp_data = DB::table('work_times as wt')
                ->whereIn('wt.admin_id', $employee_arr)
                ->where('wtd.project_id', $project_id)
                ->whereBetween('work_time_date', [$start_date, $end_date])
                ->join('work_time_details as wtd', 'wt.work_time_id', '=', 'wtd.work_time_id')
                ->join('admins as a', 'wt.admin_id', '=', 'a.admin_id')
                ->groupBy('wt.admin_id')
                ->orderBy('wtd.project_id')
                ->select(
                    DB::raw('sum(work_time_detail_hrs) as total_hour'),
                    DB::raw('count(DISTINCT wt.work_time_date) as involvement_days'),
                    'wtd.work_time_detail_id',
                    'wtd.project_id',
                    'wtd.work_type_id',
                    'a.admin_id',
                    'a.admin_name'
                )
                ->get()
                ->toArray();
            $work_type_arr = WorkType::get()->pluck('work_type_id');
            $summary_work_data = DB::table('work_times as wt')
                ->whereIn('wtd.work_type_id', $work_type_arr)
                ->where('wtd.project_id', $project_id)
                ->whereBetween('work_time_date', [$start_date, $end_date])
                ->join('work_time_details as wtd', 'wt.work_time_id', '=', 'wtd.work_time_id')
                ->join('work_types', 'wtd.work_type_id', '=', 'work_types.work_type_id')
                ->groupBy('wtd.work_type_id')
                ->orderBy('wtd.project_id')
                ->select(
                    DB::raw('sum(work_time_detail_hrs) as total_hour'),
                    DB::raw('count(DISTINCT wt.work_time_date) as involvement_days'),
                    'wtd.work_time_detail_id',
                    'wtd.project_id',
                    'work_types.work_type_id',
                    'work_types.work_type_name'
                )
                ->get()
                ->toArray();
            if (!empty($report_data)) {
                $emp_summary_obj = json_encode($summary_emp_data, true);
                $emp_summery_arr = json_decode($emp_summary_obj, true);
                $work_summary_obj = json_encode($summary_work_data, true);
                $work_summary_arr = json_decode($work_summary_obj, true);

                $report_data_obj = json_encode($report_data, true);
                $report_data_arr = json_decode($report_data_obj, true);

                $final_array = [];
                $inc = 0;
                foreach ($report_data_arr as $key => $value) {
                    $k = array_search($value['work_type_id'], array_column($report_data_arr, 'work_type_id'));
                    $final_array[$value['work_type_id']]['work_type_name'] = $value['work_type_name'];
                    $final_array[$value['work_type_id']]['details'][] = $value;
                }
                $data = array(
                    'summeryByEmployee' => $emp_summery_arr,
                    'summeryByWorkType' => $work_summary_arr,
                    'final_array' => $final_array,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                );
                $preview = View::make('admin-panel.report.project-report-preview')->with($data)->render();
                if (!empty($preview)) {
                    return response()->json(['status' => 'success', 'data' => $preview]);
                } else {
                    return response()->json(['status' => 'error', 'data' => $preview]);
                }
            } else {
                return response()->json(['status' => 'error', 'data' => '']);
            }
        } else {
            return response()->json(['status' => 'error', 'data' => '']);
        }
    }

    /**
     * Report for admin project wise 
     * @Bhuvanesh on 01 Feb 2019
     **/
    public function projectSummeryReport(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title' => trans('language.report_project_summery'),
            'login_info' => $loginInfo,
        );
        return view('admin-panel.report.project-summery')->with($data);
    }

    /**
     * Data for admin project wise 
     * @Bhuvanesh on 01 Feb 2019
     **/
    public function projectSummeryReportData(Request $request)
    {
        // p($request->all());
        $report_data = [];
        if (isset($request) && $request->get('report_date') != '') {
            $date_range = explode('-', $request->get('report_date'));
            $date_create = date_create($date_range[0]);
            $start_date = date_format($date_create, 'Y-m-d');
            $date_create = date_create($date_range[1]);
            $end_date = date_format($date_create, 'Y-m-d');
            $report_data = DB::table('work_times as wt')
                ->whereBetween('work_time_date', [$start_date, $end_date])
                ->join('work_time_details as wtd', 'wt.work_time_id', '=', 'wtd.work_time_id')
                ->join('projects as p', 'wtd.project_id', '=', 'p.project_id')
                ->join('work_types', 'wtd.work_type_id', '=', 'work_types.work_type_id')
                ->groupBy('wtd.project_id')
                ->groupBy('wtd.work_type_id')
                ->orderBy('wtd.project_id')
                ->select(
                    DB::raw('sum(work_time_detail_hrs) as total_hour'),
                    DB::raw('count(work_time_detail_hrs) as involvement_days'),
                    'wtd.work_time_detail_id',
                    'wtd.project_id',
                    'p.project_name',
                    'wtd.work_type_id',
                    'work_types.work_type_id',
                    'work_types.work_type_name'
                )
                ->get()
                ->toArray();
            if (!empty($report_data)) {
                $report_data_obj = json_encode($report_data, true);
                $report_data_arr = json_decode($report_data_obj, true);

                $final_array = [];
                $inc = 0;
                foreach ($report_data_arr as $key => $value) {
                    $k = array_search($value['work_type_id'], array_column($report_data_arr, 'work_type_id'));
                    $final_array[$value['project_id']]['project_name'] = $value['project_name'];
                    $final_array[$value['project_id']]['details'][] = $value;
                }
                $data = array(
                    'final_array' => $final_array,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                );
                $preview = View::make('admin-panel.report.project-summery-report-preview')->with($data)->render();
                if (!empty($preview)) {
                    return response()->json(['status' => 'success', 'data' => $preview]);
                } else {
                    return response()->json(['status' => 'error', 'data' => $preview]);
                }
            } else {
                return response()->json(['status' => 'error', 'data' => '']);
            }
        } else {
            return response()->json(['status' => 'error', 'data' => '']);
        }
    }

    /**
     * Report for admin project wise 
     * @Bhuvanesh on 01 Feb 2019
     **/
    public function codeReviewReport(Request $request)
    {
        $loginInfo = get_loggedin_user_data();
        $code_reviewer_arr = get_coder_reviewer(null, true);
        $code_reviewer_arr = add_blank_option($code_reviewer_arr, "Select Code Reviewer");
        $project_arr = Project::select('project_id', 'project_name')->get()->toArray();
        if (!empty($project_arr)) {
            foreach ($project_arr as $key => $value) {
                $project_list[$value['project_id']] = $value['project_name'];
            }
        }
        $project_list = add_blank_option($project_list, 'Select Project');
        $data = array(
            'page_title' => trans('language.report_code_review_view'),
            'code_reviewer_arr' => $code_reviewer_arr,
            'project_arr' => $project_list,
            'login_info' => $loginInfo,
        );
        return view('admin-panel.report.code-review')->with($data);
    }

    /**
     * Data for admin project wise 
     * @Bhuvanesh on 01 Feb 2019
     **/
    public function codeReviewReportData(Request $request)
    {
        $project_id = $request->get('project_id');
        $reportdata = '';
        $report_data = [];
        if (isset($request) && $request->get('report_date') != '') {
            $code_reviewer_arr = get_coder_reviewer(null, true);
            $employee_arr = get_employees(null, true);
            $code_review_status_arr    = Config::get('custom.code_review_status');
            $review_check_list         = ReviewCheckList::where('status', 1)->get()->pluck('review_check', 'review_check_id');
            $date_range = explode('-', $request->get('report_date'));
            $date_create = date_create($date_range[0]);
            $start_date = date_format($date_create, 'Y-m-d');
            $date_create = date_create($date_range[1]);
            $end_date = date_format($date_create, 'Y-m-d');
            $query = DB::table('code_reviews as cr')
                ->whereBetween('cri.review_date', [$start_date, $end_date])
                ->join('projects as p', 'cr.project_id', '=', 'p.project_id')
                ->join('code_review_details as crd', 'cr.code_review_id', '=', 'crd.code_review_id')
                ->join('code_review_infos as cri', 'crd.code_review_info_id', '=', 'cri.code_review_info_id')
                ->groupBy('cr.project_id')
                ->groupBy('cri.review_date')
                ->groupBy('cr.coder_id')
                ->groupBy('crd.review_check_id')
                ->orderBy('p.project_id')
                ->select(
                    'p.project_id',
                    'p.project_name',
                    'cr.coder_id',
                    'cr.code_reviewer_id',
                    'cr.check_points',
                    'crd.review_check_id',
                    'crd.comments',
                    'crd.review_status',
                    'cri.review_date',
                    'cri.code_review_status',
                    'cri.isSubmit'
                )->where('cri.isSubmit', '1');
            if ($request->get('project_id') != '') {
                $report_data = $query->where('cr.project_id', $project_id);
            } else {
                $report_data =  $query->get()->toArray();
            }
            if (!empty($report_data)) {
                $report_data_arr = json_decode(json_encode($report_data, true), true);
                $final_array = [];

                $project_rowspan = 0;
                $cr_rowspan = 0;
                $date_rowspan = 0;

                foreach ($report_data_arr as $key => $value) {
                    $final_array[$value['project_id']]['project_name'] = $value['project_name'];
                    $final_array[$value['project_id']]['details'][$value['code_reviewer_id']]['code_reviewer_id'] = $value['code_reviewer_id'];
                    $final_array[$value['project_id']]['details'][$value['code_reviewer_id']]['code_reviewer_details'][$value['review_date']]['review_date'] = $value['review_date'];
                    $final_array[$value['project_id']]['details'][$value['code_reviewer_id']]['code_reviewer_details'][$value['review_date']]['review_date_details'][$value['coder_id']]['coder_id'] = $value['coder_id'];
                    $final_array[$value['project_id']]['details'][$value['code_reviewer_id']]['code_reviewer_details'][$value['review_date']]['review_date_details'][$value['coder_id']]['coder_details'][] = $value;
                }
                $span_array = [];
                $p_row = [];
                foreach ($final_array as $key => $value) {
                    $project_rowspan = 0;
                    foreach ($value['details'] as $cr_details_key => $cr_details) {
                        $cr_rowspan = 0;
                        foreach ($cr_details['code_reviewer_details'] as $review_date_details_key => $review_date_details) {
                            $date_rowspan = 0;
                            foreach ($review_date_details['review_date_details'] as $coder_details) {
                                $project_rowspan += count($coder_details['coder_details']);
                                $cr_rowspan += count($coder_details['coder_details']);
                                $date_rowspan += count($coder_details['coder_details']);
                            }
                            $d_row[$key][$cr_details_key][$review_date_details_key] = $date_rowspan;
                        }
                        $c_row[$key][$cr_details_key] = $cr_rowspan;
                    }
                    $p_row[$key] = $project_rowspan;
                }

                $data = array(
                    'final_array' => $final_array,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'd_row' => $d_row,
                    'c_row' => $c_row,
                    'p_row' => $p_row,
                    'code_reviewer_arr' => $code_reviewer_arr,
                    'employee_arr' => $employee_arr,
                    'review_check_list' => $review_check_list,
                    'code_review_status_arr' => $code_review_status_arr,

                );
                $preview = View::make('admin-panel.report.code-review-report-preview')->with($data)->render();
                // p($preview);
                if (!empty($preview)) {
                    return response()->json(['status' => 'success', 'data' => $preview]);
                } else {
                    return response()->json(['status' => 'error', 'data' => $preview]);
                }
            } else {
                return response()->json(['status' => 'error', 'data' => '']);
            }
        } else {
            return response()->json(['status' => 'error', 'data' => '']);
        }
    }

    /**
     *  View page for employee Project
     *  @Bhuvanesh on 09 Apr 2018
     **/
    public function projectList()
    {
        $loginInfo = get_loggedin_user_data();
        $data = array(
            'page_title' => trans('language.project_code_review'),
            'redirect_url' => url('admin-panel/project/view-project'),
            'login_info' => $loginInfo
        );
        return view('admin-panel.project.my-projects')->with($data);
    }
    /**
     *  Get Data for employee Project view page(Datatables)
     *  @Bhuvanesh on 09 Apr 2018
     **/

    public function projectData(Request $request)
    {
        $project = [];
        $employee_list            = get_employees(null, true);
        $code_review_status_arr   = Config::get('custom.code_review_status');
        $loginInfo                = get_loggedin_user_data();
        $review_check_list        = ReviewCheckList::where('status', 1)->get()->pluck('review_check', 'review_check_id');
        $project                  = Project::where(function ($query) use ($request) {
            $query->where('status', '1');
        })->whereHas('getProjectAssignActive', function ($query) use ($loginInfo) {
            $query->where('admin_id', $loginInfo['admin_id']);
        })->with(['getCodeReviewsActive' => function ($q) use ($loginInfo) {
            $q->where('coder_id', $loginInfo['admin_id']);
        }, 'getCodeReviewsActive.getCodeReviewDetails.getCodeReviewInfo'])->orderBy('project_id', 'desc')->get()->toArray();
        return Datatables::of($project)
            ->addColumn('review_log', function ($project) use ($employee_list, $code_review_status_arr, $review_check_list) {
                if (isset($project['get_code_reviews_active'][0])) {
                    $reviewer = $employee_list[$project['get_code_reviews_active'][0]['code_reviewer_id']];
                    $reviews = $project['get_code_reviews_active'][0]['get_code_review_details'];
                    $review_log = [];
                    foreach ($reviews as $key => $value) {
                        $review_log[$value['get_code_review_info']['review_date']][] = array(
                            'review_date' => $value['get_code_review_info']['review_date'],
                            'code_reviewer' =>  $reviewer,
                            'review_check' => $review_check_list[$value['review_check_id']],
                            'comments' => $value['comments'],
                            'review_status' => $code_review_status_arr[$value['review_status']],
                            'review_status_id' => $value['review_status'],
                        );
                    }
                    return $review_log;
                }
            })
            ->addColumn('status', function ($project) {
                if ($project['status'] == 1) {
                    return 'Active';
                } else {
                    return 'Inactive';
                }
            })
            ->addColumn('view_code_reviews', function ($project) {
                $encrypted_project_id = get_encrypted_value($project['project_id'], true);
                return '<button type="button" class="btn-sm btn-info btn-rounded view-code-review" data-toggle="modal" data-target="#view-code-review-log" rel="' . $encrypted_project_id . '">View Code Reviews</button>';
            })
            ->rawColumns(['review_log' => 'review_log', 'view_code_reviews' => 'view_code_reviews', 'status' => 'status'])->addIndexColumn()->make(true);
    }
}
