<?php

namespace App\Http\Controllers\adminPanel;

use DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Model\Project\Project;
use App\Model\Project\ProjectAssign;
use App\Model\Project\CodeReview;
use App\Model\Project\CodeReviewInfo;
use App\Model\Project\CodeReviewDetail;
use App\Http\Controllers\Controller;
use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;

class CronController extends Controller
{
    /**
     *  Cron Scheduling function for code reviewer
     *  @Bhuvanesh on 16 Apr 2019
     **/
    public function getCodeReviewerListForMail()
    {
        $i                      =  0;
        $current_date           =  date('Y-m-d');
        $employee_list          =  get_employees(null, true);
        $mail_for_codereviewer  =  [];
        // Code review status 2 means pending
        $project_details        =  DB::table('code_review_infos as cri')
            ->where('cri.code_review_status', '2')
            ->join('projects as p', 'cri.project_id', '=', 'p.project_id')
            ->join('code_reviews as cr', 'p.project_id', '=', 'cr.project_id')
            ->join('admins as ad', 'cri.code_reviewer_id', '=', 'ad.admin_id')
            ->select('cri.code_review_info_id', 'p.project_id', 'cri.code_reviewer_id', 'cri.review_upcoming_date', 'p.project_name', 'cr.frequently_code_review', 'ad.admin_name', 'ad.email')->get();
        $project_details = json_decode(json_encode($project_details, true), true);

        $final_array = [];
        foreach ($project_details as $project_detail) {
            if ($project_detail['review_upcoming_date'] == $current_date) {
                array_push($final_array, $project_detail);
            } else {
                $lastDate = strtotime($project_detail['review_upcoming_date']);
                if ($project_detail['frequently_code_review'] == 1) {
                    $lastDate = strtotime("-7 day", $lastDate);
                } else if ($project_detail['frequently_code_review'] == 2) {
                    $lastDate = strtotime('-15 day', $lastDate);
                } else if ($project_detail['frequently_code_review'] == 3) {
                    $lastDate = strtotime('-30 day', $lastDate);
                }
                $upcoming_date = date('Y-m-d', $lastDate);
                if ($upcoming_date == $current_date) {
                    array_push($final_array, $project_detail);
                }
            }
        }
        $mail_array = [];
        foreach ($final_array as $code_review) {
            $review_start_date = '';
            $review_start_date = strtotime($code_review['review_upcoming_date']);
            if ($code_review['frequently_code_review'] == 1) {
                $review_start_date = strtotime("-7 day", $review_start_date);
            } else if ($code_review['frequently_code_review'] == 2) {
                $review_start_date = strtotime('-15 day', $review_start_date);
            } else if ($code_review['frequently_code_review'] == 3) {
                $review_start_date = strtotime('-30 day', $review_start_date);
            }
            $code_review['review_start_date'] = date('Y-m-d', $review_start_date);
            $mail_array[$code_review['email']]['admin_name'] = $code_review['admin_name'];
            $mail_array[$code_review['email']]['email'] = $code_review['email'];
            $mail_array[$code_review['email']]['details'][] = $code_review;
        }
        // p($mail_array);
        if (!empty($mail_array)) {
            foreach ($mail_array as $mailData) {
                $data = array(
                    'email'      => $mailData['email'],
                    'to_name'    => $mailData['admin_name'],
                    'details'    => $mailData['details'],
                    'subject'    => 'Code Review',
                    'from'       => 'v2rteam@gmail.com',
                    'from_name'  => 'V2R Solution'
                );
                Mail::send('admin-panel.auth.mail.code_review_mail', $data, function ($message) use ($data) {
                    $message->to($data['email'])->from($data['from'], $data['from_name'])->subject($data['subject']);
                });
            }
        }
    }
}
