<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\adminPanel\CodeReviewController;


class CodeReviewerEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Email:codereviewer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an email to code reviewer for code review';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $controller = new CodeReviewController();
        $mail_for_codereviewer  =  $controller->getCodeReviewerListForMail();
        if (!empty($mail_for_codereviewer)) {
            foreach ($mail_for_codereviewer as $mailData) {
                $data = array(
                    'email'      => $mailData['code_reviewer_email'],
                    'status'     => $mailData['review_status'],
                    'to_name'    => $mailData['code_reviewer_name'],
                    'project'    => $mailData['project'],
                    'platform'   => $mailData['platform'],
                    'developer'  => $mailData['developer'],
                    'subject'    => 'Project Code Review',
                    'from'       => 'v2rteam@gmail.com',
                    'from_name'  => 'V2R Solution'
                );

                Mail::send('admin-panel.auth.mailForCodeReviewer', $data, function ($message) use ($data) {
                    $message->to($data['email'])->from($data['from'], $data['from_name'])->subject($data['subject']);
                });
            }
        }
    }
}
