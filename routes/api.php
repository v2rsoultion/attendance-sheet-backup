<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// View category, sub category and language
Route::get('view-category', 'Api\ApiController@viewCategory');

// View paper list
Route::get('view-paper-list', 'Api\ApiController@viewPaperList');

// Show all question list
Route::get('show-all-question', 'Api\ApiController@showAllQuestion');

// Show all available section
Route::get('view-available-section', 'Api\ApiController@viewSection');

// Show all question list according to section
Route::get('view-all-section-question', 'Api\ApiController@ViewAllSectionQuestion');

// User's Route
Route::post('user-registration', 'Api\ApiController@userRegistration');
Route::get('user-login', 'Api\ApiController@userLogin');
Route::get('user-social-login', 'Api\ApiController@userSocialLogin');
Route::post('set-app-version', 'Api\ApiController@setAppVersion');