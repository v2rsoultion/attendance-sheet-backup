<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
// Route::get('/site-up', function () {
//   Artisan::call('up');
//   return redirect('/');
//   // return wherever you want
// });

// Route::get('/site-down', function () {
//   Artisan::call('down');
//   return redirect('/');
//   // return wherever you want
// });

Route::get('/clear-cache', function () {
  Artisan::call('cache:clear');
  Artisan::call('route:clear');
  Artisan::call('view:clear');
  Artisan::call('config:cache');
  return "Cache clear successfully";
  // return redirect('/');
  // return wherever you want
});
Route::get('send-mail-code-reviewer', 'adminPanel\CronController@getCodeReviewerListForMail');
Route::get('/', 'AdminAuth\LoginController@showLoginForm')->name('login');
Route::get('/admin-panel/login', 'AdminAuth\LoginController@showLoginForm');

Route::get('/activate/{id?}', 'adminPanel\TeamController@verification');
Route::post('/create-password/{id?}', 'adminPanel\TeamController@createPassword');
Route::get('/cronScheduling', 'adminPanel\CodeReviewController@getCodeReviewerListForMail');



Route::group(['prefix' => 'admin-panel', 'middleware' => 'admin.guest'], function () {
  Route::get('/', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

  // Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  // Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

//For Admin 
// On 19 Dec 2018
// By @Bhuvanesh
Route::group(['prefix' => 'admin-panel', 'middleware' => 'admin'], function () {
  // For logout
  Route::get('/logout', 'AdminAuth\LoginController@logout')->name('logout');
  // For Dashboard
  Route::get('/dashboard', 'adminPanel\DashboardController@index');
  Route::get('/profile', 'adminPanel\DashboardController@profile');
  Route::get('/changepassword', 'adminPanel\DashboardController@changePassword');
  Route::post('/setpassword', 'adminPanel\DashboardController@setPassword');

  // For Pending Action
  Route::get('/pending-task', 'adminPanel\DashboardController@pendingTask');
  Route::get('/pending-task-data', 'adminPanel\DashboardController@pendingTaskData');

  Route::get('/markAsRead', 'adminPanel\DashboardController@markAsRead');

  Route::get('/attendance-time-in', 'adminPanel\DashboardController@registerTimeIn');
  Route::get('/attendance-time-out', 'adminPanel\DashboardController@registerTimeOut');
  Route::get('/attendance-time-out-submit', 'adminPanel\DashboardController@registerTimeOutSubmit');

  // For Configuration 
  // On 07 Jan 2019 
  // By @Bhuvanesh
  Route::post('/configuartion/save/{id}', 'adminPanel\CompanyController@save');
  Route::get('/configuartion/view-configuartion', 'adminPanel\CompanyController@index');
  Route::get('/configuartion/get-calendar-events', 'adminPanel\CompanyController@getCalendarEvents');
  Route::get('/configuartion/save-calendar', 'adminPanel\CompanyController@saveCalendar');
  Route::get('/configuartion/delete-event', 'adminPanel\CompanyController@destroyCalendar');

  // // For Department 
  // // On 11 Dec 2018 
  // // By @Bhuvanesh
  // Route::get('/department/add-department/{id?}', 'adminPanel\DepartmentController@add');
  // Route::post('/department/save/{id?}', 'adminPanel\DepartmentController@save');
  // Route::get('/department/view-department', 'adminPanel\DepartmentController@index');
  // Route::get('/department/data', 'adminPanel\DepartmentController@anyData');
  // Route::get('/department/department-delete/{id?}', 'adminPanel\DepartmentController@destroy');
  // Route::get('/department/department-status/{status?}/{id?}', 'adminPanel\DepartmentController@changeStatus');

  // For Team module 
  // On 12 Dec 2018 
  // By @Bhuvanesh
  Route::get('/team/add-team/{id?}', 'adminPanel\TeamController@add');
  Route::post('/team/save/{id?}', 'adminPanel\TeamController@save');
  Route::get('/team/view-team', 'adminPanel\TeamController@index');
  Route::get('/team/data', 'adminPanel\TeamController@anyData');
  Route::post('/team/team-delete/', 'adminPanel\TeamController@destroy');
  Route::get('/team/team-status/{status?}/{id?}', 'adminPanel\TeamController@changeStatus');
  Route::get('/team/view-resign-team', 'adminPanel\TeamController@resignView');
  Route::get('/team/resign-data', 'adminPanel\TeamController@anyDataResign');

  // For Release Plan 
  // On 8 Jan 2019 
  // By @Bhuvanesh
  Route::post('/release-plan/save/{id?}', 'adminPanel\ReleasePlanController@save');
  Route::get('/release-plan/view-release-plan', 'adminPanel\ReleasePlanController@index');
  Route::get('/release-plan/show-release-plan-data', 'adminPanel\ReleasePlanController@showReleasePlanData');
  Route::get('/release-plan/get-monthwise-data', 'adminPanel\ReleasePlanController@getReleasePlanMonthWiseData');
  // Route::get('/release-plan/add-release-plan/{year?}', 'adminPanel\ReleasePlanController@add');
  // Route::get('/release-plan/data', 'adminPanel\ReleasePlanController@anyData');
  // Route::get('/release-plan/release-plan-delete/{id?}', 'adminPanel\ReleasePlanController@destroy');
  // Route::get('/release-plan/release-plan-team-list', 'adminPanel\ReleasePlanController@teamList');
  // Route::get('/release-plan/year-validate', 'adminPanel\ReleasePlanController@yearValidate');
  // Route::get('/release-plan/show-release-plan/{year}', 'adminPanel\ReleasePlanController@showReleasePlan');



  // For Attendance 
  // On 10 Jan 2018 
  // By @Bhuvanesh
  Route::get('/attendance/add-attendance/{id?}', 'adminPanel\AttendanceController@add');
  Route::post('/attendance/preview', 'adminPanel\AttendanceController@savePreview');
  Route::get('/attendance/save', 'adminPanel\AttendanceController@save');
  Route::get('/attendance/cancel', 'adminPanel\AttendanceController@cancel');
  Route::get('/attendance/view-attendance', 'adminPanel\AttendanceController@index');
  Route::get('/attendance/data', 'adminPanel\AttendanceController@anyData');
  Route::get('/attendance/attendance-delete/{id?}', 'adminPanel\AttendanceController@destroy');
  Route::get('/attendance/download-attendance/{id?}', 'adminPanel\AttendanceController@downloadfile');
  Route::get('/attendance/show-attendance/{id?}', 'adminPanel\AttendanceController@showAttendance');
  Route::get('/attendance/attendance-data', 'adminPanel\AttendanceController@showAttendanceAnyData');
  Route::get('/attendance/update-attendance-data', 'adminPanel\AttendanceController@updateAttendanceData');

  // Route::get('/attendance-register/view-in-out/{condition?}', 'adminPanel\AttendanceController@viewInOut');
  // Route::get('/attendance-register/in-out-data', 'adminPanel\AttendanceController@inOutData');
  // Route::get('/attendance-register/change-request', 'adminPanel\AttendanceController@changeRequest');
  Route::get('/attendanceregister/view-in-out/{condition?}', 'adminPanel\AttendanceController@viewInOut');
  Route::get('/attendanceregister/in-out-data', 'adminPanel\AttendanceController@inOutData');
  Route::get('/attendance/changerequest', 'adminPanel\AttendanceController@changeRequest');
  Route::get('/attendance-register1/get-attendance-data', 'adminPanel\AttendanceController@getAttendanceRegister');
  Route::get('/attendance-register1/update-attendance', 'adminPanel\AttendanceController@updateAttendanceRegister');
  // For Attendance of team member
  // On 12 Dec 2018 
  // By @Bhuvanesh
  Route::get('/my-attendance/view-attendance', 'adminPanel\AttendanceController@myAttendanceIndex');
  Route::get('/my-attendance/data', 'adminPanel\AttendanceController@myAttendanceAnyData');

  // For User Role 
  // On 30 Jan 2019 
  // By @Bhuvanesh
  // Route::get('/userrole/add-userrole/{id?}', 'adminPanel\UserRoleController@add');
  Route::post('/userrole/save/{id?}', 'adminPanel\UserRoleController@save');
  Route::get('/userrole/view-userrole/{id?}', 'adminPanel\UserRoleController@index');
  Route::get('/userrole/data', 'adminPanel\UserRoleController@anyData');
  Route::get('/userrole/userrole-delete/{id?}', 'adminPanel\UserRoleController@destroy');
  Route::get('/userrole/userrole-status/{status?}/{id?}', 'adminPanel\UserRoleController@changeStatus');
  Route::get('/userrole/assign-responsibility/{id}', 'adminPanel\UserRoleController@assignResponsibility');
  Route::post('/userrole/assign-responsibility-save', 'adminPanel\UserRoleController@assignResponsibilitySave');

  // For Work Type 
  // On 26 Jan 2018 
  // By @Bhuvanesh
  // Route::get('/work-type/add-work-type/{id?}', 'adminPanel\WorkTypeController@add');
  Route::post('/work-type/save/{id?}', 'adminPanel\WorkTypeController@save');
  Route::get('/work-type/view-work-type/{id?}', 'adminPanel\WorkTypeController@index');
  Route::get('/work-type/data', 'adminPanel\WorkTypeController@anyData');
  Route::get('/work-type/work-type-delete/{id?}', 'adminPanel\WorkTypeController@destroy');
  Route::get('/work-type/work-type-status/{status?}/{id?}', 'adminPanel\WorkTypeController@changeStatus');

  // For Project module 
  // On 24 Dec 2018 
  // By @Bhuvanesh
  Route::get('/project/add-project/{id?}', 'adminPanel\ProjectController@add');
  Route::post('/project/save/{id?}', 'adminPanel\ProjectController@save');
  Route::get('/project/view-project', 'adminPanel\ProjectController@index');
  Route::get('/project/data', 'adminPanel\ProjectController@anyData');
  Route::get('/project/assign-project/{id}', 'adminPanel\ProjectController@assignProject');
  Route::post('/project/assign-project-save', 'adminPanel\ProjectController@assignProjectSave');
  Route::get('/project/project-delete/{id?}', 'adminPanel\ProjectController@destroy');
  Route::get('/project/project-status/{status?}/{id?}', 'adminPanel\ProjectController@changeStatus');

  // For Employee Project View (with View Code Reviews) 
  // On 09 Apr 2019 
  // By @Bhuvanesh
  Route::get('/project/my-project', 'adminPanel\ProjectController@projectList');
  Route::get('/project/projectdata', 'adminPanel\ProjectController@projectData');
  Route::get('/project/code-review-projectdata/{id}', 'adminPanel\ProjectController@projectCodeReviewData');

  // For Report Module
  // On 01 Feb 2018
  // By @Bhuvanesh
  Route::get('/report/view-employee', 'adminPanel\ProjectController@employeeWiseReport');
  Route::get('/report/data-view-employee', 'adminPanel\ProjectController@employeeWiseReportData');
  Route::get('/report/get-employee-project', 'adminPanel\ProjectController@getEmployeeProjects');
  Route::get('/report/view-project', 'adminPanel\ProjectController@projectWiseReport');
  Route::get('/report/data-view-project', 'adminPanel\ProjectController@projectWiseReportData');
  Route::get('/report/view-summery', 'adminPanel\ProjectController@projectSummeryReport');
  Route::get('/report/data-view-project-summery', 'adminPanel\ProjectController@projectSummeryReportData');
  Route::get('/report/code-review-report', 'adminPanel\ProjectController@codeReviewReport');
  Route::get('/report/data-code-review-report', 'adminPanel\ProjectController@codeReviewReportData');
  // For Project Work Time 
  // On 26 Jan 2018 
  // By @Bhuvanesh
  Route::get('/project-work-time/add-project-work-time/{id?}', 'adminPanel\WorkTimeController@add');
  Route::post('/project-work-time/save/{id?}', 'adminPanel\WorkTimeController@save');
  Route::get('/project-work-time/view-project-work-time', 'adminPanel\WorkTimeController@index');
  Route::get('/project-work-time/data', 'adminPanel\WorkTimeController@anyData');
  Route::get('/project-work-time/project-work-time-delete/{id?}', 'adminPanel\WorkTimeController@destroy');
  Route::get('/project-work-time/project-work-time-detail-delete', 'adminPanel\WorkTimeController@removeWorkTimeDetail');
  Route::get('/project-work-time/project-work-time-date-check', 'adminPanel\WorkTimeController@checkWorkTimeDate');

  // For In -out View of Employee
  // On 13 Feb 2019
  // By @Bhuvanesh
  Route::get('/in-out/view-in-out', 'adminPanel\TeamController@viewInOut');
  Route::get('/in-out/in-out-data', 'adminPanel\TeamController@inOutData');

  // For Leave Module
  // On 14 Feb 2019
  // By @Bhuvanesh
  Route::get('/leave/apply-leave', 'adminPanel\LeaveController@applyLeave');
  Route::get('/leave/view-leave/{condition?}', 'adminPanel\LeaveController@index');
  Route::get('/leave/leave-data', 'adminPanel\LeaveController@anyData');
  Route::get('/leave/leave-change-status/{id?}/{status?}/{comment?}', 'adminPanel\LeaveController@changeStatusLeave');
  Route::get('/my-leave/view-my-leave', 'adminPanel\LeaveController@viewMyLeave');
  Route::get('/my-leave/my-leave-data', 'adminPanel\LeaveController@anyDataMyLeave');
  Route::get('/my-leave/delete', 'adminPanel\LeaveController@destroy');
  Route::get('/my-leave/get-leave-data', 'adminPanel\LeaveController@getModifyLeave');

  // For Short Leave 
  // On 15 Feb 2019
  // By @Bhuvanesh
  Route::get('/early-leave/apply-early-leave', 'adminPanel\LeaveController@applyEarlyLeave');
  Route::get('/early-leave/view-early-leave/{condition?}', 'adminPanel\LeaveController@indexEarlyLeave');
  Route::get('/early-leave/early-leave-data', 'adminPanel\LeaveController@anyDataEarlyLeave');
  Route::get('/early-leave/early-leave-change-status/{id?}/{status?}', 'adminPanel\LeaveController@changeEarlyLeaveStatus');
  Route::get('/early-leave/early-leave-count-data', 'adminPanel\LeaveController@anyDataEarlyLeaveCount');

  // For Assign Code Reviewer 
  // On 02 Apr 2019
  // By @Bhuvanesh
  Route::get('/assign-codereviewer/{id}', 'adminPanel\CodeReviewController@assignCodeReviewer');
  Route::get('/assign-codereviewer-save/{id?}', 'adminPanel\CodeReviewController@saveCodeReviewer');

  // For Code Review project list for code reviewer
  // On 03 Apr 2019
  // By @Bhuvanesh
  Route::get('/codereview-projects/view-codereview', 'adminPanel\CodeReviewController@CodeReviewProjects');
  Route::get('/codereview-projects/codereview-data', 'adminPanel\CodeReviewController@CodeReviewProjectsData');

  Route::get('/codereview/add-codereview/{project_id}/{code_review_info_id?}/{isSubmitted?}', 'adminPanel\CodeReviewController@add');
  Route::post('/codereview/save/{project_id}/{code_review_info_id?}', 'adminPanel\CodeReviewController@save');
  Route::get('/codereview/view-codereview', 'adminPanel\CodeReviewController@index');
  Route::get('/codereview/data', 'adminPanel\CodeReviewController@anyData');

  // Route::get('/codereview/codereview-log/{id}', 'adminPanel\ProjectController@codeReviewLog');
  // Route::get('/codereview/data', 'adminPanel\ProjectController@codeReviewLogData');
  // Route::get('/codereview/dataForCodeFeeback', 'adminPanel\ProjectController@codeReviewFeeback');

  // For Team member projects
  // On 09 Apr 2019 
  // By @Bhuvanesh
  // Route::get('/my-project/project-list', 'adminPanel\ProjectController@myProjectList');
  // Route::get('/my-project/projectdata', 'adminPanel\ProjectController@myProjectListData');
  // Route::get('/my-project/myproject-review/{project_id}/{platform_id}', 'adminPanel\ProjectController@myProjectCodeReview');
  // Route::get('/my-project/myproject-review-data', 'adminPanel\ProjectController@myProjectCodeReviewData');

  // ********Used*****
  // // For Code Reviewer projects
  // // On 26 Dec 2018 
  // // By @Bhuvanesh
  // Route::get('/my-project/codereview-project-list', 'adminPanel\ProjectController@codeReviewProjectList');
  // Route::get('/my-project/codereview-projectdata', 'adminPanel\ProjectController@codeReviewProjectListData');

  // // For Code Review Add Update Delete
  // // On 26 Dec 2018 
  // // By @Bhuvanesh
  // Route::get('/my-project/codereview/add-codereview/{project_id}/{platform_id}/{id?}', 'adminPanel\ProjectController@codeReviewAdd');
  // Route::post('/my-project/codereview/save/{id?}', 'adminPanel\ProjectController@codeReviewSave');
  // Route::get('/my-project/codereview/view-codereview', 'adminPanel\ProjectController@codeReviewIndex');
  // Route::get('/my-project/codereview/data', 'adminPanel\ProjectController@codeReviewAnyData');
  // Route::get('/my-project/codereview/codereview-delete/{id?}', 'adminPanel\ProjectController@codeReviewDestroy');
  // Route::get('/my-project/codereview/codereview-status/{status?}/{id?}', 'adminPanel\ProjectController@codeReviewChangeStatus');

  // ********End Used*****
});
